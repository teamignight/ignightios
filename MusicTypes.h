//
//  MusicTypes.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MusicTypes : NSObject

typedef enum {
    Classic_Rock,
    Classical,
    Country,
    Electronic,
    Folk,
    Indie_Rock,
    Jazz,
    Latin,
    Oldies,
    Pop,
    RnB,
    Rap,
    Reggae,
    Rock,
    Top_40,
    
    MUSIC_COUNT
} Music;

+(NSString *)getMusicNameFromId: (Music )musicId;
+(Music)getMusicFromName: (NSString* )musicName;
+(NSArray *)getListOfValues;

@end
