//
//  AtmosphereTypes.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "AtmosphereTypes.h"

@implementation AtmosphereTypes

+(NSString *)getAtmosphereNameFromId: (Atmosphere )atmosphereId
{
    switch (atmosphereId) {
        case VIP_CLUB:
            return @"VIP Club";
        case TECHNO_CLUB:
            return @"Techno Club";
        case HIPSTER_LOUNGE:
            return @"Hipster Lounge";
        case COCKTAIL_LOUNGE:
            return @"Cocktail Lounge";
        case SPORTS_BAR:
            return @"Sports Bar";
        case DIVE_BAR:
            return @"Dive Bar";
        case GAY_BAR:
            return @"Gay Bar";
        case LIVE_MUSIC:
            return @"Live Music";
            
        default:
            return @"N/A";
    }
}

+(Atmosphere) getAtmosphereFromName: (NSString *) atmosphereName
{
    atmosphereName = [atmosphereName lowercaseString];
    
    if ([atmosphereName isEqualToString:@"vip club"])
        return VIP_CLUB;
    if ([atmosphereName isEqualToString:@"techno club"])
        return TECHNO_CLUB;
    if ([atmosphereName isEqualToString:@"hipster lounge"])
        return HIPSTER_LOUNGE;
    if ([atmosphereName isEqualToString:@"cocktail lounge"])
        return COCKTAIL_LOUNGE;
    if ([atmosphereName isEqualToString:@"sports bar"])
        return SPORTS_BAR;
    if ([atmosphereName isEqualToString:@"dive bar"])
        return DIVE_BAR;
    if ([atmosphereName isEqualToString:@"gay bar"])
        return GAY_BAR;
    if ([atmosphereName isEqualToString:@"live music"])
        return LIVE_MUSIC;
    
    return ATMOSPHERE_COUNT;
}

+(NSArray *)getListOfValues
{
    NSMutableArray *atmospheres = [[NSMutableArray alloc] init];
    for (int i = 0; i < ATMOSPHERE_COUNT; i++) {
        [atmospheres addObject:[AtmosphereTypes getAtmosphereNameFromId:i]];
    }
    return atmospheres;
}


@end
