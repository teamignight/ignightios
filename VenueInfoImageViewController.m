//
//  VenueInfoImageViewController.m
//  Ignight
//
//  Created by Abhinav Chordia on 1/25/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import "VenueInfoImageViewController.h"
#import "CityTrendingTableViewCell.h"
#import <AFNetworking/AFHTTPRequestOperation.h>

@interface VenueInfoImageViewController ()

@end

@implementation VenueInfoImageViewController
{
    UIImageView *imageView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, CITY_TRENDING_CELL_HEIGHT)];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView setClipsToBounds:YES];
    [self.view addSubview:imageView];
}

-(void)loadImage
{
    NSURLRequest *request = [NSURLRequest requestWithURL: [NSURL URLWithString: _imageUrl]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFImageResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        _image = responseObject;
        [imageView setImage:_image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogDebug(@"Failed To Donwload Image From: %@", _imageUrl);
    }];
    [operation start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
