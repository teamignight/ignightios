//
//  Cities.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cities : NSObject

typedef enum {
    CHICAGO,
    
    CITY_COUNT
} City;

+(NSString *)getCityNameFromId: (City )cityId;
+(City)getCityFromName: (NSString *)cityName;
+(NSArray *)getListOfValues;

@end
