//
//  RankToSelection.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/1/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "RankToSelection.h"

@implementation RankToSelection
{
    NSInteger minSelection;
    NSInteger maxSelections;
    
    NSInteger maxSelectedRank;
    
    NSMutableSet *ranksSelected;
    NSMutableSet *ranksAvailable;
    NSMutableDictionary* selectionToRank;
    NSMutableDictionary* rankToSelection;
}

-(id)initWithMaxSelections: (NSInteger)max minSelections: (NSInteger)min
{
    self = [super init];
    if (self) {
        minSelection = min;
        maxSelectedRank = 0;
        [self setMaxSelections:max];
        [self initMembers];
    }
    return self;
}

-(void)initMembers
{
    ranksSelected = [[NSMutableSet alloc] init];
    selectionToRank = [[NSMutableDictionary alloc] init];
    rankToSelection = [[NSMutableDictionary alloc] init];
}

-(void)setMaxSelections: (NSInteger)max
{
    maxSelections = max;
    [self fillAvailableRanks];
}

-(void)fillAvailableRanks
{
    ranksAvailable = [[NSMutableSet alloc] init];
    for (int i = 1; i <= maxSelections; i++) {
        [ranksAvailable addObject:[NSNumber numberWithInt:i]];
    }
}

-(BOOL)isRankAvailable
{
    return [ranksAvailable count] > 0;
}

-(BOOL)selectedMinSelections
{
    if ([ranksSelected count] != maxSelectedRank)
        return NO;

    return ((maxSelections - [ranksAvailable count]) >= minSelection);
}

-(NSInteger)count
{
    return [ranksSelected count];
}

-(NSNumber *)selectionAtRank: (NSInteger)rank
{
    return [rankToSelection objectForKey:[NSNumber numberWithInteger:rank]];
}

-(NSInteger)getNextSelectionRank
{
    NSInteger lowestRank = maxSelections + 1;
    for (NSNumber *rank in ranksAvailable) {
        lowestRank = MIN(lowestRank, [rank intValue]);
    }
    if (lowestRank > maxSelections)
        return -1;
    [ranksAvailable removeObject:[NSNumber numberWithInteger:lowestRank]];
    return lowestRank;
}

-(NSInteger)addToSelection:(id)selection
{
    if ([selectionToRank objectForKey:selection] != nil)
        return -1;
    NSInteger nextRank = [self getNextSelectionRank];
    maxSelectedRank = MAX(maxSelectedRank, nextRank);
    [selectionToRank setObject:[NSNumber numberWithInteger:nextRank] forKey:selection];
    [rankToSelection setObject:selection forKey:[NSNumber numberWithInteger:nextRank]];
    [ranksSelected addObject:[NSNumber numberWithInteger:nextRank]];
    return nextRank;
}

-(NSInteger)removeFromSelection: (id)selection
{
    NSNumber *rank = [selectionToRank objectForKey:selection];
    if (rank == nil)
        return -1;
    
    [selectionToRank removeObjectForKey:selection];
    [rankToSelection removeObjectForKey:rank];
    [ranksAvailable addObject:rank];
    [ranksSelected removeObject:rank];
    
    if (maxSelectedRank == [rank intValue])
        maxSelectedRank = [self highestRankSelected];
    return [rank intValue];
}

-(NSNumber *)rankForSelection: (id)selection
{
    NSNumber *rank = [selectionToRank objectForKey:selection];
    return rank;
}

-(NSArray *)selections {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (int i = 1; i <= [self count]; i++) {
        NSNumber *selectionId = [self selectionAtRank:i];
        if (selectionId != nil)
            [array addObject:selectionId];
    }
    return array;
}

-(NSInteger)highestRankSelected
{
    int maxRank = 0;
    for (NSNumber *rank in ranksSelected) {
        maxRank = MAX(maxRank, [rank intValue]);
    }
    return maxRank;
}

@end
