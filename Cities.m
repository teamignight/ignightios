//
//  Cities.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "Cities.h"

@implementation Cities

+(NSString *)getCityNameFromId: (City )cityId
{
    switch (cityId) {
        case CHICAGO:
            return @"Chicago";
        default:
            return nil;
            break;
    }
}

+(City)getCityFromName: (NSString *)cityName
{
    cityName = [cityName lowercaseString];
    
    if ([cityName isEqualToString:@"chicago"])
        return CHICAGO;
    
    return CITY_COUNT;
}

+(NSArray *)getListOfValues
{
    NSMutableArray *cities = [[NSMutableArray alloc] init];
    for (int i = 0; i < CITY_COUNT; i++) {
        [cities addObject:[Cities getCityNameFromId:i]];
    }
    return cities;
}

@end
