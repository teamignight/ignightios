//
//  MusicTypes.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "MusicTypes.h"

@implementation MusicTypes

+(NSString *)getMusicNameFromId: (Music )musicId
{
    switch (musicId) {
        case Top_40:
            return @"Top 40";
        case Pop:
            return @"Pop";
        case Rock:
            return @"Rock";
        case Rap:
            return @"Rap";
        case RnB:
            return @"R&B";
        case Electronic:
            return @"Electronic";
        case Country:
            return @"Country";
        case Indie_Rock:
            return @"Indie Rock";
        case Reggae:
            return @"Reggae";
        case Jazz:
            return @"Jazz";
        case Folk:
            return @"Folk";
        case Latin:
            return @"Latin";
        case Classic_Rock:
            return @"Classic Rock";
        case Oldies:
            return @"Oldies";
        case Classical:
            return @"Classical";
        default:
            return @"N/A";
    }
}

+(Music)getMusicFromName: (NSString* )musicName
{
    musicName = [musicName lowercaseString];
    
    if ([musicName isEqualToString:@"top 40"])
        return Top_40;
    if ([musicName isEqualToString:@"pop"])
        return Pop;
    if ([musicName isEqualToString:@"rock"])
        return Rock;
    if ([musicName isEqualToString:@"rap"])
        return Rap;
    if ([musicName isEqualToString:@"r&b"])
        return RnB;
    if ([musicName isEqualToString:@"electronic"])
        return Electronic;
    if ([musicName isEqualToString:@"country"])
        return Country;
    if ([musicName isEqualToString:@"indie rock"])
        return Indie_Rock;
    if ([musicName isEqualToString:@"reggae"])
        return Reggae;
    if ([musicName isEqualToString:@"jazz"])
        return Jazz;
    if ([musicName isEqualToString:@"folk"])
        return Folk;
    if ([musicName isEqualToString:@"latin"])
        return Latin;
    if ([musicName isEqualToString:@"classic rock"])
        return Classic_Rock;
    if ([musicName isEqualToString:@"oldies"])
        return Oldies;
    if ([musicName isEqualToString:@"classical"])
        return Classical;
    
    return MUSIC_COUNT;
}

+(NSArray *)getListOfValues
{
    NSMutableArray *music = [[NSMutableArray alloc] init];
    for (int i = 0; i < MUSIC_COUNT; i++) {
        [music addObject:[MusicTypes getMusicNameFromId:i]];
    }
    return music;
}

@end
