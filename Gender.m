//
//  Gender.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/30/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "Gender.h"

@implementation Gender

+(NSString *)getNameFromId: (Genders)genderId
{
    switch (genderId) {
        case MALE:
            return @"Male";
        case FEMALE:
            return @"Female";
        default:
            return nil;
    }
}

+(Genders)getGenderFromName: (NSString *)name
{
    name = [name lowercaseString];
    if ([name isEqualToString:@"male"])
        return MALE;
    if ([name isEqualToString:@"female"])
        return FEMALE;
    return GENDER_COUNT;
}

+(NSArray *)getListOfValues
{
    NSMutableArray *genders = [[NSMutableArray alloc] init];
    for (int i = 0; i < GENDER_COUNT; i++)
        [genders addObject:[Gender getNameFromId:i]];
    return genders;
}

@end
