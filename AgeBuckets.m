//
//  AgeBuckets.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/7/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "AgeBuckets.h"

@implementation AgeBuckets

+(NSString *)getAgeBucketNameFromId:(AgeBucket)ageBucket
{
    switch (ageBucket) {
        case EIGHTEEN_TO_20:
            return @"18-20";
        case TWENTYONE_TO_25:
            return @"21-25";
        case TWENTYSIX_TO_30:
            return @"26-30";
        case THIRTYONE_TO_35:
            return @"31-35";
        case THIRTYSIX_TO_40:
            return @"36-40";
        case FOURTYONEPLUS:
            return @"41+";
        default:
            return @"N/A";
    }
}

+(AgeBucket)getAgeBucketFromName:(NSString *)ageBucket
{
    if ([ageBucket isEqualToString:@"18-20"])
        return EIGHTEEN_TO_20;
    if ([ageBucket isEqualToString:@"21-25"])
        return TWENTYONE_TO_25;
    if ([ageBucket isEqualToString:@"26-30"])
        return TWENTYSIX_TO_30;
    if ([ageBucket isEqualToString:@"31-35"])
        return THIRTYONE_TO_35;
    if ([ageBucket isEqualToString:@"36-40"])
        return THIRTYSIX_TO_40;
    if ([ageBucket isEqualToString:@"41+"])
        return FOURTYONEPLUS;
    
    return AGE_BUCKETS_COUNT;
}
+(NSArray *)getListOfValues
{
    return @[@"18-20", @"21-25", @"26-30", @"31-35", @"36-40", @"41+"];
}

@end
