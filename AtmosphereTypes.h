//
//  AtmosphereTypes.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AtmosphereTypes : NSObject

typedef enum {
    COCKTAIL_LOUNGE,
    DIVE_BAR,
    GAY_BAR,
    HIPSTER_LOUNGE,
    LIVE_MUSIC,
    SPORTS_BAR,
    TECHNO_CLUB,
    VIP_CLUB,
    
    ATMOSPHERE_COUNT
} Atmosphere;

+(NSString *)getAtmosphereNameFromId: (Atmosphere )atmosphereId;
+(Atmosphere) getAtmosphereFromName: (NSString *) atmosphereName;
+(NSArray *)getListOfValues;

@end
