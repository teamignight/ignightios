//
//  SpendingTypes.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/21/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpendingTypes : NSObject

typedef enum {
    BROKE,
    GETTING_BY,
    DOING_WELL,
    LIVING_THE_DREAM,
    
    SPENDING_COUNT
} Spending;

+(NSString *)getSpendingNameFromId: (Spending )spendingId;
+(NSString *)getSpendingSymbolFromId: (Spending )spendingId;
+(Spending)getSpendingFromName: (NSString* )spendingName;
+(NSArray *)getArrayOfSpendingNames;

@end
