//
//  VenueDnaViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/28/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "VenueDnaViewController.h"

@interface VenueDnaViewController ()

@end

@implementation VenueDnaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
