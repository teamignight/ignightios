//
//  VenueInfoImageViewController.h
//  Ignight
//
//  Created by Abhinav Chordia on 1/25/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueInfoImageViewController : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (weak, nonatomic) NSString *imageUrl;
@property (weak, nonatomic) UIImage *image;

-(void)loadImage;

@end
