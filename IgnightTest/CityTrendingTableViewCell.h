//
//  CityTrendingTableViewCell.h
//  Ignight
//
//  Created by Abhinav Chordia on 1/20/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityVenueTrendingData.h"
#import "VenueData.h"
#import "VenueInfoImageViewController.h"

#define CITY_TRENDING_CELL_HEIGHT 160
#define USER_INPUT_VIEW_WIDTH 50
#define USER_INPUT_BUTTONS_WIDTH 40
#define VENUE_DISTANCE_LABEL_HEIGHT 10
#define VENUE_DISTANCE_LABEL_WIDTH 80

#define VENUE_TITLE_LABEL_HEIGHT 30

@protocol CityTrendingTableViewCellDelegate;

@interface CityTrendingTableViewCell : UITableViewCell <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (weak, nonatomic) id<CityTrendingTableViewCellDelegate> delegate;
@property (assign) BOOL groupMode;

-(void)resetWithData: (CityVenueTrendingData *)data;
-(void)resetWithVenueData:(VenueData *)venueData;
-(CityVenueTrendingData *)getData;
-(VenueData *)getVenueData;

@end

@protocol CityTrendingTableViewCellDelegate <NSObject>

-(void)cityTrendingCell: (CityTrendingTableViewCell *)cell updateUserInputMode: (UserInputMode)mode;

@end


