//
//  SideBarViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/25/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "GroupDetailsViewController.h"
#import "MailViewController.h"
#import "SideBarBuzzCell.h"
#import "TrendingDataCache.h"
#import "VenueViewController.h"
#import "Inbox.h"
#import "GroupHttpClient.h"
#import "VenueHttpClient.h"
#import "SideBarHeaderView.h"
#import "TutorialViewController.h"
#import "UserNotifications.h"

@interface SideBarViewController : UITableViewController <SWRevealViewControllerDelegate,
UserHttpClientDelegate, UserNotificationsDelegate>

@property (strong, nonatomic) IBOutlet UITableView *menu;


@end
