//
//  CityTrendingTableViewCell.m
//  Ignight
//
//  Created by Abhinav Chordia on 1/20/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import "CityTrendingTableViewCell.h"
#import "ChameleonFramework/Chameleon.h"

@implementation CityTrendingTableViewCell
{
    UIImageView *venueImageView;
    UIPageViewController *imagePageController;
    NSMutableArray *imageViewControllers;
    
    UITextView *venueName;
    UILabel *venueDistance;
    
    UILabel *userVenueValue;
    
    UIButton *upvoteButton;
    UIImageView *upvoteImageView;
    UIButton *downVoteButton;
    UIImageView *downvoteImageView;
    
    UIView *gradient;
    
    CityVenueTrendingData *data;
    VenueData *venueData;
    UserInfo *user;
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView setBackgroundColor: [UIColor whiteColor]];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        venueImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, CITY_TRENDING_CELL_HEIGHT)];
        [venueImageView setContentMode:UIViewContentModeScaleAspectFill];
        [venueImageView setClipsToBounds:YES];
        [self.contentView addSubview:venueImageView];
        
        imagePageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        imagePageController.delegate = self;
        imagePageController.dataSource = self;
        imagePageController.view.frame = self.contentView.frame;
        [self.contentView addSubview:imagePageController.view];
        
        upvoteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [upvoteButton setBackgroundColor:[UIColor clearColor]];
        [upvoteButton addTarget:self action:@selector(upvoteBtn:) forControlEvents:UIControlEventTouchUpInside];
        upvoteButton.frame = CGRectMake(screenWidth - USER_INPUT_BUTTONS_WIDTH - 5, (CITY_TRENDING_CELL_HEIGHT / 2) - USER_INPUT_BUTTONS_WIDTH - 5, USER_INPUT_BUTTONS_WIDTH, USER_INPUT_BUTTONS_WIDTH);
        [self.contentView addSubview:upvoteButton];
        
        upvoteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, upvoteButton.frame.size.width, upvoteButton.frame.size.height)];
        [upvoteImageView setImage:[UIImage imageNamed:@"upvote_off"]];
        [upvoteButton addSubview:upvoteImageView];
        
        downVoteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [downVoteButton setBackgroundColor:[UIColor clearColor]];
        [downVoteButton addTarget:self action:@selector(downvoteBtn:) forControlEvents:UIControlEventTouchUpInside];
        downVoteButton.frame = CGRectMake(screenWidth - USER_INPUT_BUTTONS_WIDTH - 5, upvoteButton.frame.origin.y + upvoteButton.frame.size.height + 10, USER_INPUT_BUTTONS_WIDTH, USER_INPUT_BUTTONS_WIDTH);
        [self.contentView addSubview:downVoteButton];
        
        downvoteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, downVoteButton.frame.size.width, downVoteButton.frame.size.height)];
        [downvoteImageView setImage:[UIImage imageNamed:@"downvote_off"]];
        [downVoteButton addSubview:downvoteImageView];
        
        //        gradient = [[UIView alloc] initWithFrame:CGRectMake(0, CITY_TRENDING_CELL_HEIGHT - VENUE_DISTANCE_LABEL_HEIGHT - VENUE_TITLE_LABEL_HEIGHT, [Utils screenWidth], VENUE_DISTANCE_LABEL_HEIGHT + VENUE_TITLE_LABEL_HEIGHT)];
        gradient = [[UIView alloc] initWithFrame:CGRectMake(0, 0, USER_INPUT_VIEW_WIDTH, CITY_TRENDING_CELL_HEIGHT)];
        [self.contentView addSubview:gradient];
        
        venueDistance = [[UILabel alloc] initWithFrame:CGRectMake(5, CITY_TRENDING_CELL_HEIGHT - VENUE_DISTANCE_LABEL_HEIGHT - 5, VENUE_DISTANCE_LABEL_WIDTH, VENUE_DISTANCE_LABEL_HEIGHT)];
        [venueDistance setTextColor:[UIColor whiteColor]];
        [venueDistance setFont:[UIFont boldSystemFontOfSize:12.0]];
        [self.contentView addSubview:venueDistance];
        
        venueName = [[UITextView alloc] initWithFrame:CGRectMake(0, CITY_TRENDING_CELL_HEIGHT - VENUE_DISTANCE_LABEL_HEIGHT - VENUE_TITLE_LABEL_HEIGHT, screenWidth - USER_INPUT_VIEW_WIDTH, VENUE_TITLE_LABEL_HEIGHT)];
        venueName.scrollEnabled = NO;
        venueName.editable = NO;
        venueName.userInteractionEnabled = NO;
        [venueName setTextColor:[UIColor whiteColor]];
        
        [venueName setBackgroundColor:[UIColor clearColor]];
        [venueName setFont:[UIFont boldSystemFontOfSize:25.0]];
        [self.contentView addSubview:venueName];
        
        userVenueValue = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 30, 30)];
        userVenueValue.adjustsFontSizeToFitWidth = YES;
        [[userVenueValue layer] setCornerRadius:15];
        [[userVenueValue layer] setMasksToBounds:YES];
        [userVenueValue setFont:[UIFont boldSystemFontOfSize:15.0]];
        [userVenueValue setTextColor:[UIColor flatGreenColor]];
        [userVenueValue setTextAlignment:NSTextAlignmentCenter];
        [userVenueValue setBackgroundColor:[UIColor flatWhiteColor]];
        userVenueValue.alpha = 0;
        [self.contentView addSubview:userVenueValue];
    }
    return self;
}

-(IBAction)upvoteBtn:(id)sender
{
    UserInputMode mode = [self getUserInput];
    
    if (mode == OFF) {
        [upvoteImageView setImage:[UIImage imageNamed:@"upvote_on"]];
        [self updateUserInput:UPVOTED];
    } else if(mode == DOWNVOTED) {
        [upvoteImageView setImage:[UIImage imageNamed:@"upvote_on"]];
        [downvoteImageView setImage:[UIImage imageNamed:@"downvote_off"]];
        [self updateUserInput:UPVOTED];
    } else {
        [upvoteImageView setImage:[UIImage imageNamed:@"upvote_off"]];
        [self updateUserInput:OFF];
    }
    
    [self.delegate cityTrendingCell:self updateUserInputMode:[self getUserInput]];
}

-(IBAction)downvoteBtn:(id)sender
{
    UserInputMode mode = [self getUserInput];
    if (mode == OFF) {
        [downvoteImageView setImage:[UIImage imageNamed:@"downvote_on"]];
        [self updateUserInput:DOWNVOTED];
    } else if(mode == UPVOTED) {
        [downvoteImageView setImage:[UIImage imageNamed:@"downvote_on"]];
        [upvoteImageView setImage:[UIImage imageNamed:@"upvote_off"]];
        [self updateUserInput:DOWNVOTED];
    } else {
        [downvoteImageView setImage:[UIImage imageNamed:@"downvote_off"]];
        [self updateUserInput:OFF];
    }
    
    [self.delegate cityTrendingCell:self updateUserInputMode:[self getUserInput]];
}

-(void)updateUserInput: (UserInputMode)mode
{
    if (data) {
        data.userInput = mode;
    } else {
        venueData.userInputMode = mode;
    }
}

-(UserInputMode)getUserInput
{
    if (data) {
        return data.userInput;
    } else {
        return venueData.userInputMode;
    }
}

-(void)resetWithVenueData:(VenueData *)venueInfoData
{
    if (venueInfoData == nil) {
        return;
    }
    
    //RESET
    venueData = venueInfoData;
    [upvoteImageView setImage:[UIImage imageNamed:@"upvote_off"]];
    [downvoteImageView setImage:[UIImage imageNamed:@"downvote_off"]];
    [venueImageView removeFromSuperview];
    
    venueDistance.text = nil;
    venueName.attributedText = nil;
    venueName.frame = CGRectMake(0, CITY_TRENDING_CELL_HEIGHT - VENUE_DISTANCE_LABEL_HEIGHT - VENUE_TITLE_LABEL_HEIGHT, screenWidth - USER_INPUT_VIEW_WIDTH, VENUE_TITLE_LABEL_HEIGHT);
    
    //Update
    user = [UserInfo getInstance];
    if (user.userLocation != nil) {
        CLLocation *venueLocation = [[CLLocation alloc] initWithLatitude:[venueData.latitude doubleValue] longitude:[venueData.longitude doubleValue]];
        venueDistance.text = [NSString stringWithFormat:@"%.2f miles", [self convertToMilesFromMeters:[user.userLocation getDistanceFrom:venueLocation]]];
    }
    //    if (data.venueImage != nil) {
    //        [venueImageView setImage:data.venueImage];
    //    }
    
    UIFont *font = [UIFont boldSystemFontOfSize:25.0];
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    [attributes setObject:font forKey:NSFontAttributeName];
    [attributes setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    venueName.attributedText = [[NSAttributedString alloc] initWithString:venueData.venueName attributes:attributes];
    CGFloat height = [self textViewHeightForAttributedText:venueName.attributedText andWidth:screenWidth - USER_INPUT_VIEW_WIDTH];
    venueName.frame = CGRectMake(0, CITY_TRENDING_CELL_HEIGHT - VENUE_DISTANCE_LABEL_HEIGHT - height, screenWidth - USER_INPUT_VIEW_WIDTH, height);
    
    if (venueData.userInputMode == UPVOTED) {
        upvoteImageView.image = [UIImage imageNamed:@"upvote_on"];
    } else if (venueData.userInputMode == DOWNVOTED) {
        downvoteImageView.image = [UIImage imageNamed:@"downvote_on"];
    }
    
    UserBarColor userBarColor = venueData.userBarColor;
    UIColor *gradientColor;
    if (userBarColor == GREEN) {
        gradientColor = [UIColor flatGreenColorDark];
    } else if (userBarColor == YELLOW) {
        gradientColor = [UIColor flatYellowColorDark];
    } else if (userBarColor == ORANGE) {
        gradientColor = [UIColor flatOrangeColorDark];
    } else if (userBarColor == RED) {
        gradientColor = [UIColor flatRedColorDark];
    } else {
        gradientColor = [IgnightUtil ignightGray];
    }
    
    //    UIColor *bgColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:(CGRect)gradient.frame andColors:[NSArray arrayWithObjects:[UIColor clearColor], gradientColor, nil]];
    UIColor *bgColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:(CGRect)gradient.frame andColors:[NSArray arrayWithObjects:gradientColor, [UIColor clearColor], nil]];
    [gradient setBackgroundColor:bgColor];
    
    if (imageViewControllers == nil) {
        imageViewControllers = [[NSMutableArray alloc] init];
    }
    
    if (imageViewControllers.count == 0) {
        for (int i = 0; i < [venueInfoData.venueImages count]; i++) {
            VenueInfoImageViewController *iC = [[VenueInfoImageViewController alloc] init];
            iC.index = i;
            iC.imageUrl = [venueInfoData.venueImages objectAtIndex:i];
            [imageViewControllers addObject:iC];
        }
    }
    if (imageViewControllers.count > 0) {
        NSArray *controllers = imagePageController.viewControllers;
        if (controllers == nil || controllers.count == 0) {
            VenueInfoImageViewController *controller = [imageViewControllers objectAtIndex:0];
            [controller loadImage];
            [imagePageController setViewControllers:[NSArray arrayWithObject:controller] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
        }
    } else {
        [imagePageController.view removeFromSuperview];
    }
    
    gradient.alpha = 1;
    userVenueValue.alpha = 0;
}

-(void)resetWithData: (CityVenueTrendingData *)venueTrendingData;
{
    //RESET
    data = venueTrendingData;
    [upvoteImageView setImage:[UIImage imageNamed:@"upvote_off"]];
    [downvoteImageView setImage:[UIImage imageNamed:@"downvote_off"]];
    [imagePageController.view removeFromSuperview];
    venueImageView.image = nil;
    venueDistance.text = nil;
    venueName.attributedText = nil;
    venueName.frame = CGRectMake(0, CITY_TRENDING_CELL_HEIGHT - VENUE_DISTANCE_LABEL_HEIGHT - VENUE_TITLE_LABEL_HEIGHT, screenWidth - USER_INPUT_VIEW_WIDTH, VENUE_TITLE_LABEL_HEIGHT);
    
    //Update
    user = [UserInfo getInstance];
    if (user.userLocation != nil) {
        CLLocation *venueLocation = [[CLLocation alloc] initWithLatitude:[self->data.latitude doubleValue] longitude:[self->data.longitude doubleValue]];
        venueDistance.text = [NSString stringWithFormat:@"%.2f miles", [self convertToMilesFromMeters:[user.userLocation getDistanceFrom:venueLocation]]];
    }
    if (data.venueImage != nil) {
        [venueImageView setImage:data.venueImage];
    }
    
    UIFont *font = [UIFont boldSystemFontOfSize:25.0];
    NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    [attributes setObject:font forKey:NSFontAttributeName];
    [attributes setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    venueName.attributedText = [[NSAttributedString alloc] initWithString:data.venueName attributes:attributes];
    CGFloat height = [self textViewHeightForAttributedText:venueName.attributedText andWidth:screenWidth - USER_INPUT_VIEW_WIDTH];
    venueName.frame = CGRectMake(0, CITY_TRENDING_CELL_HEIGHT - VENUE_DISTANCE_LABEL_HEIGHT - height, screenWidth - USER_INPUT_VIEW_WIDTH, height);
    
    if (self->data.userInput == UPVOTED) {
        upvoteImageView.image = [UIImage imageNamed:@"upvote_on"];
    } else if (self->data.userInput == DOWNVOTED) {
        downvoteImageView.image = [UIImage imageNamed:@"downvote_on"];
    }
    
    UserBarColor userBarColor = data.userBarColor;
    UIColor *gradientColor;
    if (userBarColor == GREEN) {
        gradientColor = [UIColor flatGreenColorDark];
    } else if (userBarColor == YELLOW) {
        gradientColor = [UIColor flatYellowColorDark];
    } else if (userBarColor == ORANGE) {
        gradientColor = [UIColor flatOrangeColorDark];
    } else if (userBarColor == RED) {
        gradientColor = [UIColor flatRedColorDark];
    } else {
        gradientColor = [IgnightUtil ignightGray];
    }
    
    if (_groupMode) {
        gradientColor = [IgnightUtil ignightGray];
        NSNumber *value = venueTrendingData.userVenueValue;
        if (value.integerValue > 0) {
            userVenueValue.textColor = [UIColor greenColor];
            userVenueValue.text = [NSString stringWithFormat:@"+%@", value];
        } else if (value.integerValue == 0) {
            userVenueValue.textColor = [UIColor grayColor];
            userVenueValue.text = [NSString stringWithFormat:@"%@", value];
        } else if (value.integerValue < 0) {
            userVenueValue.textColor = [UIColor redColor];
            userVenueValue.text = [NSString stringWithFormat:@"%@", value];
        }
        userVenueValue.alpha = 1;
        gradient.alpha = 0;
    } else {
        userVenueValue.alpha = 0;
        gradient.alpha = 1;
    }
    
    //    UIColor *bgColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:(CGRect)gradient.frame andColors:[NSArray arrayWithObjects:[UIColor clearColor], gradientColor, nil]];
    UIColor *bgColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:(CGRect)gradient.frame andColors:[NSArray arrayWithObjects:gradientColor, [UIColor clearColor], nil]];
    [gradient setBackgroundColor:bgColor];
}

#pragma mark - UIPageViewControllerDelegate
#pragma mark - UIPageViewControllerDataSource
//
//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController NS_AVAILABLE_IOS(6_0)
//{
//    NSInteger count = [venueData.venueImages count];
//    return count;
//}
//
//- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController NS_AVAILABLE_IOS(6_0)
//{
//    return 0;
//}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger index = [(VenueInfoImageViewController *)viewController index];
    if (index == 0) {
        return nil;
    }
    return [imageViewControllers objectAtIndex:(index - 1)];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger index = [(VenueInfoImageViewController *)viewController index];
    NSInteger maxIndex = [imageViewControllers count] - 1;
    if (index == maxIndex || maxIndex < 0) {
        return nil;
    }
    VenueInfoImageViewController *controller = [imageViewControllers objectAtIndex:(index + 1)];
    if (controller.image == nil) {
        [controller loadImage];
    }
    return controller;
}

#pragma mark - Other

- (CGFloat)textViewHeightForAttributedText:(NSAttributedString *)text andWidth:(CGFloat)width
{
    UITextView *textView = [[UITextView alloc] init];
    [textView setAttributedText:text];
    CGSize size = [textView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    return size.height;
}

-(CityVenueTrendingData *)getData
{
    return data;
}

-(VenueData *)getVenueData
{
    return venueData;
}

- (double)convertToMilesFromMeters: (double)distance
{
    return (distance / 1609.344);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
