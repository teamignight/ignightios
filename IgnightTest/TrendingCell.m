//
//  TrendingCell.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/10/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "TrendingCell.h"
#import "IgnightColors.h"

@implementation TrendingCell
{
    BOOL upVoted;
    BOOL downVoted;
    NSNumber *venueId;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect frame;
        
        CGRect dnaImageFrame = CGRectMake(7, 17.5, 25, 25);
        CGRect groupCounterFrame = CGRectMake(16, 33, 13, 13);
        _dnaImageView = [[UIImageView alloc] initWithFrame:dnaImageFrame];
        _groupInputValue = [[UILabel alloc] initWithFrame:groupCounterFrame];
        _groupInputValue.font = [UIFont systemFontOfSize:13];
        
        _venueName = [[UILabel alloc] initWithFrame:CGRectMake(42, 13, 180, 22)];
        [_venueName setFont:[UIFont systemFontOfSize:18]];
        _venueName.textColor = venueNameText;
        _venueName.backgroundColor = clearC;
        
        _venueDistance = [[UILabel alloc] initWithFrame:CGRectMake(42, 33, 161, 22)];
        [_venueDistance setFont:[UIFont systemFontOfSize:12]];
        _venueDistance.textColor = venueDistanceText;
        _venueDistance.backgroundColor = clearC;
        
        frame = CGRectMake(220, 0, 50, 60);
        _upVote = [[UIButton alloc] initWithFrame:frame];
        [_upVote addTarget:self action:@selector(upVote:) forControlEvents:UIControlEventTouchUpInside];
        _upVote.backgroundColor = clearC;
        
        _upvoteImgView = [[UIImageView alloc] initWithFrame:CGRectMake(227.5, 12.5, 35, 35)];
        [self.contentView addSubview:_upvoteImgView];
       
        frame = CGRectMake(270, 0, 50, 60);
        _downVote = [[UIButton alloc] initWithFrame:frame];
        [_downVote addTarget:self action:@selector(downVote:) forControlEvents:UIControlEventTouchUpInside];
        _downVote.backgroundColor = clearC;
        
        _downvoteImgView = [[UIImageView alloc] initWithFrame:CGRectMake(277.5, 12.5, 35, 35)];
        [self.contentView addSubview:_downvoteImgView];
        
        [self.contentView addSubview:_dnaImageView];
        [self.contentView insertSubview:_groupInputValue belowSubview:_dnaImageView];
        [self.contentView addSubview: _venueName];
        [self.contentView addSubview:_venueDistance];
        [self.contentView addSubview:_upVote];
        [self.contentView addSubview:_downVote];
        
        [self.contentView setBackgroundColor:trendingListBackground];
        
        upVoted = NO;
        downVoted = NO;
        venueId = nil;
    }
    return self;
}

-(IBAction)upVote:(id)sender
{
    upVoted = !upVoted;
    if (upVoted)
        downVoted = NO;
    [self updateUserInputState];
    [self updateUserActivity];
}

-(IBAction)downVote:(id)sender
{
    downVoted = !downVoted;
    if (downVoted)
        upVoted = NO;
    [self updateUserInputState];
    [self updateUserActivity];
}

- (void)updateUserActivity
{
    NSInteger userActivity = 0;
    if (upVoted)
        userActivity = 1;
    else if (downVoted)
        userActivity = -1;
    
    [self.delegate userActivityChangedTo:userActivity forVenueId:venueId atIndexPath:_indexPath];
}

-(void)updateUserInputState
{
    if (upVoted) {
        UIImage *upVoteOn = [UIImage imageNamed:@"upvote_on"];
        UIImage *downVoteOff = [UIImage imageNamed:@"downvote_off"];
        _upvoteImgView.image = upVoteOn;
        _downvoteImgView.image = downVoteOff;
    } else if (downVoted) {
        UIImage *upVoteOff = [UIImage imageNamed:@"upvote_off"];
        UIImage *downVoteOn = [UIImage imageNamed:@"downvote_on"];
        _upvoteImgView.image = upVoteOff;
        _downvoteImgView.image = downVoteOn;
    } else {
        UIImage *upVoteOff = [UIImage imageNamed:@"upvote_off"];
        UIImage *downVoteOff = [UIImage imageNamed:@"downvote_off"];
        _upvoteImgView.image = upVoteOff;
        _downvoteImgView.image = downVoteOff;
    }
}

-(void)setName: (NSString *)name withDistance: (NSString *)distance withGroupDnaValue: (NSNumber *)dnaValue withUserInput: (NSInteger)input withUserBarColor:(NSNumber *)barColor forVenueId:(NSNumber *)venue
{
    venueId = venue;
    self.venueName.text = name;
    if(distance) {
        self.venueDistance.alpha = 1;
        self.venueDistance.text = [NSString stringWithFormat:@"%@", distance];
    } else {
        self.venueDistance.alpha = 0;
    }
    
    if (input < 0) {
        upVoted = NO;
        downVoted = YES;
    } else if (input == 0) {
        upVoted = NO;
        downVoted = NO;
    } else {
        upVoted = YES;
        downVoted = NO;
    }
    [self updateUserInputState];
    
    NSInteger val = [dnaValue integerValue];
    
//    NSString *image = [NSString stringWithFormat:@"trend_%ld", 4 - [barColor integerValue]];
    
//    CGRect groupCounterFrame = CGRectMake(0, 33, 30, 30);
    CGRect groupCounterFrame = CGRectMake(4, 15, 34, 30);
    self.groupInputValue.frame = groupCounterFrame;
    
//    CGRect dnaImageFrame = CGRectMake(7, 11, 25, 25);
//    CGRect dnaImageFrame = CGRectMake(14, 33, 15, 15);
//    self.dnaImageView.frame = dnaImageFrame;
    self.dnaImageView.alpha = 0;
    
    self.groupInputValue.alpha = 1;
    self.groupInputValue.font = [UIFont boldSystemFontOfSize:13];
    self.groupInputValue.textAlignment = NSTextAlignmentCenter;

//    self.dnaImageView.image = [UIImage imageNamed:image];

    if (dnaValue == nil) {
        self.groupInputValue.text = @" -";
        self.groupInputValue.textColor = venueNameText;
    } else if (val > 0) {
        self.groupInputValue.text = [NSString stringWithFormat:@"%+ld", (long)val];
        self.groupInputValue.textColor = venueFunScorePositive;
    } else if (val < 0) {
        self.groupInputValue.text = [NSString stringWithFormat:@"%-ld", (long)val];
        self.groupInputValue.textColor = [UIColor redColor];
    } else {
        self.groupInputValue.text = @" 0";
        self.groupInputValue.textColor = venueNameText;
    }
}

-(void)setName: (NSString *)name withDistance: (NSString *)distance withDnaValue: (NSNumber *)dnaValue withUserInput: (NSInteger)input forVenueId: (NSNumber *)venue
{
    venueId = venue;
    self.venueName.text = name;
    if(distance) {
        self.venueDistance.alpha = 1;
        self.venueDistance.text = [NSString stringWithFormat:@"%@", distance];
    } else {
        self.venueDistance.alpha = 0;
    }
    if (input < 0) {
        upVoted = NO;
        downVoted = YES;
    } else if (input == 0) {
        upVoted = NO;
        downVoted = NO;
    } else {
        upVoted = YES;
        downVoted = NO;
    }
    [self updateUserInputState];
    
    self.dnaImageView.alpha = 1;
    CGRect dnaImageFrame = CGRectMake(7, 17.5, 25, 25);
    self.dnaImageView.frame = dnaImageFrame;
    
    self.groupInputValue.alpha = 0;
    
    NSInteger val = [dnaValue integerValue];
    if (val > 4)
        val = 4;
    NSString *imageName = [NSString stringWithFormat:@"trend_%ld", (long)(4 - val)];
    self.dnaImageView.image = [UIImage imageNamed:imageName];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
