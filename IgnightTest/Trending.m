//
//  Trending.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/3/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "Trending.h"
#import "TabBarSegue.h"
#import "SWRevealViewController.h"
#import "Group.h"
#import "Groups.h"
#import "Cities.h"
#import "TrendingDataCache.h"
#import "TrendingListView.h"
#import "TrendingMapView.h"
#import "MusicTypes.h"
#import "BuzzViewController.h"
#import "EditVenueViewController.h"
#import "FSImageViewerViewController.h"
#import "ImageViewViewController.h"
#import "GroupDetailsViewController.h"
#import "Inbox.h"

@interface Trending ()

@end

@implementation Trending
{
    int currentSegmentIndex;
    NSMutableDictionary *_viewControllersByIdentifier;
    NSMutableArray *searchVenues;

    Groups *groups;
    UserInfo *userInfo;
    UserHttpClient *userClient;
    GroupHttpClient *groupClient;
    
    TrendingDataCache *trendingDataCache;

    MusicFilter *musicFilter;

    GroupTabBarController *groupTabBarController;
    BuzzViewController *buzzViewController;
    GroupMembersViewController *groupMembersViewController;
    ImageViewViewController *imageViewController;

    UIRefreshControl *tableViewRefreshControl;
    
    IgnightTopBar *titleBar;
    
    BOOL isMapView;
    
    NSString *lastSearchString;
    NSInteger maxTValues;
    NSInteger maxSValues;
    BOOL loadMore;
    
    BOOL searchMode;
    BOOL showMusicFilter;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    DDLogDebug(@"Trending: viewDidLoad, searchMode: %@", searchMode ? @"YES" : @"NO");
    
    currentSegmentIndex = 0;
    _viewControllersByIdentifier = [NSMutableDictionary dictionary];
    
    groups = [Groups getInstance];
    userInfo = [UserInfo getInstance];
    
    userClient = [UserHttpClient getInstance];
    userClient.delegate = self;
    groupClient = [GroupHttpClient getInstance];
    trendingDataCache = [TrendingDataCache getInstance];
    [trendingDataCache clear];
    
    /* Setup Title Bar */
    [self.titleBarImgView setUserInteractionEnabled:YES];
    if (_groupMode) {
        titleBar = [[IgnightTopBar alloc] initWithTitle:_groupName withSubtitle:@"Trending" withSideBarButtonTarget: _goBack ? self : self.revealViewController];
        if (_goBack) {
            [titleBar setLeftButtonBackgroundImage:[UIImage imageNamed:@"back_gray"]];
        }
        [titleBar addRightContextMenuButton];
        titleBar.delegate = self;
    } else {
        NSString *title = [Cities getCityNameFromId:[userInfo.cityId intValue]];
        titleBar = [[IgnightTopBar alloc] initWithTitle:title withSubtitle:@"Trending" withSideBarButtonTarget:self.revealViewController];
    }
    [self.titleBarImgView addSubview:titleBar];
    
    /* Setup Status Bar */
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    CGColorRef borderColor = tabBorder.CGColor;
    CGFloat borderWidth = 0.3;

    _groupMenu.backgroundColor = tabBackground;
    _groupTrendingBtn.backgroundColor = clearC;
    [_groupTrendingBtn.layer setBorderColor:borderColor];
    [_groupTrendingBtn.layer setBorderWidth:borderWidth];
    _groupBuzzBtn.backgroundColor = clearC;
    [_groupBuzzBtn.layer setBorderColor:borderColor];
    [_groupBuzzBtn.layer setBorderWidth:borderWidth];
    _groupMembersBtn.backgroundColor = clearC;
    [_groupMembersBtn.layer setBorderColor:borderColor];
    [_groupMembersBtn.layer setBorderWidth:borderWidth];
    _groupInviteBtn.backgroundColor = clearC;
    [_groupInviteBtn.layer setBorderColor:borderColor];
    [_groupInviteBtn.layer setBorderWidth:borderWidth];
    
    _cityFilters.backgroundColor = tabBackground;
    _musicFilterView.backgroundColor = clearC;
    _musicFilterBtn.backgroundColor = clearC;
    _musicFilterLbl.backgroundColor = clearC;
    _musicFilterLbl.textColor = tabInactiveText;
    [_musicFilterBtn.layer setBorderColor:borderColor];
    [_musicFilterBtn.layer setBorderWidth:borderWidth];
    _upvoteFilterBtn.backgroundColor = clearC;
    _upvoteFilterLbl.backgroundColor = clearC;
    _upvoteFilterLbl.textColor = tabInactiveText;
    [_upvoteFilterBtn.layer setBorderColor:borderColor];
    [_upvoteFilterBtn.layer setBorderWidth:borderWidth];
    
    _listMapToggle.backgroundColor = footerBackground;
    [_listMapToggle setTitleColor:footerText forState:UIControlStateNormal];
    [[_listMapToggle layer] setBorderColor:borderColor];
    [[_listMapToggle layer] setBorderWidth:borderWidth];
    
    _clearMusicFilterBtn.backgroundColor = footerBackground;
    [_clearMusicFilterBtn setTitleColor:footerText forState:UIControlStateNormal];
    [[_clearMusicFilterBtn layer] setBorderColor:borderColor];
    [[_clearMusicFilterBtn layer] setBorderWidth:borderWidth];
    
    CGFloat mapBtnAlphas = 0.8;
    [[_mapNearMeBtn layer] setBorderColor:borderColor];
    [[_mapNearMeBtn layer] setBorderWidth:borderWidth];
    [_mapNearMeBtn setBackgroundColor:footerBackground];
    [_mapNearMeBtn setAlpha:mapBtnAlphas];
    _nearMeLabel.textColor = footerText;
    
    
    [_cityViewBtn setBackgroundColor:footerBackground];
    [_cityViewBtn setAlpha:mapBtnAlphas];
    [[_cityViewBtn layer] setBorderColor:borderColor];
    [[_cityViewBtn layer] setBorderWidth:borderWidth];
    _cityViewLabel.textColor = footerText;
    
    [_refreshBtn setBackgroundColor:footerBackground];
    [_refreshBtn setAlpha:mapBtnAlphas];
    [[_refreshBtn layer] setBorderColor:borderColor];
    [[_refreshBtn layer] setBorderWidth:borderWidth];
    _refreshLabel.textColor = footerText;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    } else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
    [self performSegueWithIdentifier:@"listTrending" sender:self];
    
    _center = CLLocationCoordinate2DMake(41.8820, -87.6278);
    _radius = 40 * METERS_PER_MILE;
    
    if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
        [self.mapFilterView setAlpha:0];
    }
    
    if (_groupMode) {
        _cityFilters.alpha = 0;
        _groupMenu.alpha = 1;
        _refreshImageView.image = [UIImage imageNamed:@"refresh_off"];
        _refreshImageView.alpha = 0.2;
        _refreshLabel.alpha = 0.3;
    } else {
        [self setupFilters];
        [self setRefreshToDefault];
    }
    
    if (_tabIndex == 0 && !_groupMode) {
        [self askForTrendingData];
    }
    if (_groupMode && _tabIndex == 1) {
        [self goToGroupBuzz:nil];
    }
}

- (void)revealToggle:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [trendingDataCache setShouldRefetchTo:YES];
    [trendingDataCache clear];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupPushNotificationObserver];
    DDLogDebug(@"Trending: viewWillAppear, searchMode: %@", searchMode ? @"YES" : @"NO");
    maxSValues = maxTrending;
    maxTValues = maxTrending;
    lastSearchString = @"";
    userClient.delegate = self;
    groupClient.delegate = self;

    /* Setup Side bar */
    self.revealViewController.delegate = self;
    if (self.revealViewController != nil) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    for (UIViewController *controller in self.childViewControllers) {
        if ([controller isKindOfClass:[MusicFilter class]]) {
            musicFilter = ((MusicFilter *)controller);
            musicFilter.delegate = self;
        }
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    self.navigationController.navigationBarHidden = YES;
    
    if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
        [self.mapFilterView setAlpha:0];
    }
    
    if (_groupMode) {
        _refreshImageView.image = [UIImage imageNamed:@"refresh_off"];
        _refreshImageView.alpha = 0.2;
        _refreshLabel.alpha = 0.3;
    } else {
        [self setupFilters];
        [self setRefreshToDefault];
    }
    
    if (!_groupMode && [trendingDataCache shouldRefetch]) {
        [trendingDataCache setShouldRefetchTo:NO];
        if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
            [self exitSearchModeAndAskForTrending:NO];
        }
        [self askForTrendingData];
    }
    self.navigationController.navigationBar.topItem.title = @"";
    
    //Because of invite contacts nav bar
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)viewDidLayoutSubviews
{
    [self setupFilters];
    NSInteger height = (self.clearMusicFilterBtn.frame.origin.y - (self.cityFilters.frame.origin.y + self.cityFilters.frame.size.height));
    CGRect mFrame;
    if (showMusicFilter) {
        mFrame = CGRectMake(0, 101, [[UIScreen mainScreen] bounds].size.width, height);
    } else {
        mFrame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width, height);
    }
    self.musicFilterView.frame = mFrame;
}

-(void)setRefreshToDefault
{
    _refreshImageView.image = [UIImage imageNamed:@"refresh_on"];
    _refreshImageView.alpha = 1;
    _refreshLabel.alpha = 1;
    _refreshLabel.textColor = footerText;
}

-(void)setRefreshActive
{
    _refreshLabel.textColor = trendingMusicToggleActiveColor;
    _refreshImageView.image = [UIImage imageNamed:@"refresh_active"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.groupMode) {
        [self showGroupMenu];
    } else {
        [self showCityFilters];
        [self setupFilters];
    }
    self.navigationController.navigationBar.topItem.title = @"";
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removePushNotificationObserver];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    self.navigationController.navigationBar.topItem.title = @"";
}

-(void)resetUserClientDelegate
{
    userClient.delegate = self;
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft) {
        userClient.delegate = self;
    }
}

- (void)revealController:(SWRevealViewController *)revealController animateToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft) {
        userClient.delegate = self;
    }
}

-(void)exitSearchModeAndAskForTrending: (BOOL)ask
{
    if (searchMode) {
        if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
            TrendingListView *listView = (TrendingListView *)self.destinationViewController;
            [listView dropKeyboard];
            [listView clearSearchBar];
            lastSearchString = nil;
            
            searchMode = NO;
            _musicFilterBtn.userInteractionEnabled = YES;
            _upvoteFilterBtn.userInteractionEnabled = YES;
            _cityFilters.alpha = 1;
            _listMapToggle.userInteractionEnabled = YES;
            _listMapToggle.alpha = 1;
            
            if (ask) {
                [self askForTrendingData];
            }
        }
    }
}

-(void)refreshTable: (UIRefreshControl *)refreshMe
{
    tableViewRefreshControl = refreshMe;
    lastSearchString = nil;
    [tableViewRefreshControl endRefreshing];
    [self exitSearchModeAndAskForTrending:NO];
    [self askForTrendingData];
}

-(void)showGroupMenu
{
    self.cityFilters.alpha = 0;
    self.groupMenu.alpha = 1;
}

-(void)showCityFilters
{
    self.cityFilters.alpha = 1;
    self.groupMenu.alpha = 0;
}

- (BOOL)checkIfVenueDataInCache
{
    return NO;//    return [venueDataCache isDataSet];
}

- (void)askForTrendingData
{
    DDLogDebug(@"Getting More Trending Data...");
    if (loadMore) {
        maxTValues += maxTrending;
    } else {
        maxTValues = maxTrending;
    }
    if (self.groupMode && _tabIndex == 0) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [groupClient getGroupTrendingData:_groupId from:0 to:maxTValues];
        loadMore = NO;
    } else if (!_groupMode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [userClient getUserTrendingListFromCoordinate:_center withRadius:_radius withCount:maxTValues];
        loadMore = NO;
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Segue

-(IBAction)switchTrendingView:(id)sender {
    if (isMapView) {
        [sender setTitle:@"Switch to Map View" forState:UIControlStateNormal];
        [self performSegueWithIdentifier:@"listTrending" sender:self];
    } else {
        [sender setTitle:@"Switch to List View" forState:UIControlStateNormal];
        if (!_groupMode) {
            [self setRefreshToDefault];
        }
        
        [self performSegueWithIdentifier:@"mapTrending" sender:self];
    }
    isMapView = !isMapView;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (![segue isKindOfClass:[TabBarSegue class]]) {
        if ([segue.identifier isEqualToString:@"groupView"]) {
            groupTabBarController = (GroupTabBarController *)segue.destinationViewController;
            buzzViewController = [[groupTabBarController viewControllers] objectAtIndex:1];
            groupMembersViewController = [[groupTabBarController viewControllers] objectAtIndex:2];
            imageViewController = [[groupTabBarController viewControllers] objectAtIndex:3];
            groupMembersViewController.groupId = _groupId;
        }
        [super prepareForSegue:segue sender:sender];
        return;
    }
    
    if ([segue.identifier isEqualToString:@"listTrending"]) {
        TrendingListView *trendingListView = (TrendingListView *)segue.destinationViewController;
        trendingListView.searchVenues = searchVenues;
        trendingListView.groupMode = _groupMode;
        trendingListView.groupName = _groupName;
        trendingListView.mainTrending = self;
    } else if ([segue.identifier isEqualToString:@"mapTrending"]) {
        TrendingMapView *trendingMapView = (TrendingMapView *)segue.destinationViewController;
        trendingMapView.groupMode = self.groupMode;
        trendingMapView.mainTrending = self;
        if (!_groupMode) {
            [self setRefreshToDefault];
        }
    }
    
    self.oldViewController = self.destinationViewController;
    
    if (![_viewControllersByIdentifier objectForKey:segue.identifier]) {
        [_viewControllersByIdentifier setObject:segue.destinationViewController forKey:segue.identifier];
    }
    
    self.destinationIdentifier = segue.identifier;
    self.destinationViewController = [_viewControllersByIdentifier objectForKey:self.destinationIdentifier];
    
    [self doFLipTransition];
}

-(void)doFLipTransition
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        _nearMeLabel.alpha = 0.4;
        _nearMeImageView.alpha = 0.4;
        [_mapNearMeBtn setUserInteractionEnabled:NO];
    }
    UIViewAnimationOptions animationType = (currentSegmentIndex == 0) ? UIViewAnimationOptionTransitionFlipFromLeft : UIViewAnimationOptionTransitionFlipFromRight;
    [UIView transitionFromView:self.oldViewController.view toView:self.destinationViewController.view duration:0.5 options:animationType completion:^(BOOL finished){
            self.mapFilterView.alpha = currentSegmentIndex;
            [self setTrendingInfoToTrue];
            currentSegmentIndex = !currentSegmentIndex;
    }];
}
     
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([self.destinationIdentifier isEqualToString:identifier]) {
        return NO;
    }
    return YES;
}

-(void)enteredSearchMode
{
    searchMode = YES;
    _musicFilterBtn.userInteractionEnabled = NO;
    _upvoteFilterBtn.userInteractionEnabled = NO;
    _cityFilters.alpha = 0.5;
    _listMapToggle.userInteractionEnabled = NO;
    _listMapToggle.alpha = 0.5;
    if ([self.destinationIdentifier isKindOfClass:[TrendingListView class]]) {
        [((TrendingListView *)self.destinationViewController) showKeyboard];
    }
}

-(void)cancelButtonPressed
{
    searchMode = NO;
    lastSearchString = nil;
    _musicFilterBtn.userInteractionEnabled = YES;
    _upvoteFilterBtn.userInteractionEnabled = YES;
    _cityFilters.alpha = 1;
    _listMapToggle.userInteractionEnabled = YES;
    _listMapToggle.alpha = 1;
    [self askForTrendingData];
    if ([self.destinationIdentifier isKindOfClass:[TrendingListView class]]) {
        [((TrendingListView *)self.destinationViewController) showLoadMore];
    }
}

-(void)searchVenuesWithString: (NSString *)searchString
{
    searchMode = YES;
    
    DDLogDebug(@"LastSearchString: %@, searchString: %@", lastSearchString, searchString);
    
    if (!loadMore && [lastSearchString isEqualToString:searchString]) {
        
    } else {
        if (loadMore && [lastSearchString isEqualToString:searchString]) {
            maxSValues += maxTrending;
        } else {
            maxSValues = maxTrending;
        }
        
        [self showLoadingSearch];
        if (_groupMode) {
            [groupClient searchVenuesForGroup:_groupId withSearch:searchString from:0 to:maxSValues];
        } else {
            [userClient searchTrendingListWith:searchString from:0 to:maxSValues];
        }
        lastSearchString = searchString;
    }
}

-(void)trendingVenuesFromCoordinate: (CLLocationCoordinate2D)coordinate withRadius: (CLLocationDistance)radius
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [userClient getUserTrendingListFromCoordinate:coordinate withRadius:radius withCount:maxTrending];
}

-(void)loadMore
{
    loadMore = YES;
    if (searchMode) {
        [self searchVenuesWithString:lastSearchString];
    } else {
        [self askForTrendingData];
    }
}

-(void)addVenue
{
//    [self exitSearchModeAndAskForTrending:YES];
    EditVenueViewController *addVenue = [self.storyboard instantiateViewControllerWithIdentifier:@"editVenue"];
    [self.navigationController pushViewController:addVenue animated:YES];
}

-(BOOL)inSearchMode
{
    return searchMode;
}

-(NSInteger)maxS
{
    return maxSValues;
}
-(NSInteger)maxT
{
    return maxTValues;
}

#pragma mark - Server Delegates

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    [self stopLoadingSearch];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)userHttpClient:(UserHttpClient *)client gotUserTrendingData:(NSDictionary *)response
{
    [self stopLoadingSearch];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [[response objectForKey:@"res"] boolValue];
    
    if (res) {
        NSArray *data = [response objectForKey:@"body"];
        if ([data count] < maxTValues) {
            if ([self.destinationIdentifier isKindOfClass:[TrendingListView class]]) {
                [(TrendingListView *)self.destinationViewController noMoreResults];
            }
        }
        [trendingDataCache setTrendingData:data forGroup:self.groupMode];
        [self setTrendingInfoToTrue];
    } else {
//        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client gotUserTrendingData:(NSDictionary *)response fromCoordinate:(CLLocationCoordinate2D)coordinate withRadius:(CLLocationDistance)radius
{
    [self stopLoadingSearch];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [[response objectForKey:@"res"] boolValue];
    if (res) {
        NSArray *data = [response objectForKey:@"body"];
        [trendingDataCache setTrendingData:data forGroup:self.groupMode];
        
        if ([self.destinationViewController  isKindOfClass:[TrendingMapView class]]) {
            [((TrendingMapView*)self.destinationViewController) reAnnotateMap];
        } else {
            if ([data count] < maxTValues) {
                [(TrendingListView *)self.destinationViewController noMoreResults];
            }
        }
        [self setTrendingInfoToTrue];
    }
}

-(void)userHttpClient:(UserHttpClient *)client gotSearchResults:(NSDictionary *)response
{
    [self stopLoadingSearch];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [[response objectForKey:@"res"] boolValue];
    
    if (res) {
        NSArray *data = [response objectForKey:@"body"];
        [trendingDataCache setTrendingData:data forGroup:self.groupMode];
        if ([data count] < maxSValues) {
            if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
                [(TrendingListView *)self.destinationViewController showAddVenue];
            }
        } else {
            if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
                [(TrendingListView *)self.destinationViewController showLoadMore];
            }
        }
        [self setTrendingInfoToTrue];
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client didFailWithError:(NSError *)error
{
    [self stopLoadingSearch];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    DDLogDebug(@"Group Trending: Could not reach server");
}

-(void)groupHttpClient:(GroupHttpClient *)client gotGroupVenueSearch:(NSDictionary*)response
{
    [self stopLoadingSearch];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [[response objectForKey:@"res"] boolValue];
    
    if (res) {
        NSArray *data = [response objectForKey:@"body"];
        [trendingDataCache setTrendingData:data forGroup:self.groupMode];
        if ([data count] < maxSValues) {
            if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
                [(TrendingListView *)self.destinationViewController showAddVenue];
            }
        } else {
            if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
                [(TrendingListView *)self.destinationViewController showLoadMore];
            }
        }
        [self setTrendingInfoToTrue];
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client gotGroupTrendingData:(NSDictionary *)response
{
    [self stopLoadingSearch];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [[response objectForKey:@"res"] boolValue];
    if (res) {
        NSArray *data = [response objectForKey:@"body"];
        if ([data count] < maxTValues) {
            if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
                [(TrendingListView *)self.destinationViewController noMoreResults];
            }
        }
        [trendingDataCache setTrendingData:data forGroup:self.groupMode];
        [self setTrendingInfoToTrue];
    }
}

- (void)setTrendingInfoToTrue
{
    if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
        [((TrendingListView*)self.destinationViewController).tableView reloadData];
    } else
        [((TrendingMapView*)self.destinationViewController) reAnnotateMap];
}

#pragma MusicFilterDelegate

-(void)musicFilter:(MusicFilter *)musicFilter didSelectMusicType:(NSInteger)musicId
{
    userInfo.activityFilterId = [NSNumber numberWithInteger:-1];
    userInfo.musicFilterId = [NSNumber numberWithInteger:musicId];
    [self setupFilters];
    [self askForTrendingData];
    [self hideMusicFilter];
}

-(void)setupFilters
{
    NSInteger mFilter = [userInfo.musicFilterId integerValue];
    NSInteger uFilter = [userInfo.activityFilterId integerValue];

    if (searchMode) {
        [self enteredSearchMode];
    } else if (mFilter >= 0) {
        [self musicActive];
        [self upvoteInactive];
    } else if (uFilter >= 0) {
        [self musicInactive];
        [self upvoteActive];
    } else {
        [self musicOff];
        [self upvoteOff];
    }
}

-(void)musicActive
{
    NSString *musicName = [MusicTypes getMusicNameFromId: (Music)[userInfo.musicFilterId integerValue]];
    CGSize musicSize = [musicName sizeWithAttributes:@{NSFontAttributeName : _musicFilterLbl.font}];

    float xPos = (160 - (musicSize.width + 16 + 9))/2.0;
    CGRect imgFrame = CGRectMake(xPos, 12, 16, 20);
    CGRect lblFrame = CGRectMake(xPos+25, 11, 85, 21);
    
    [self.musicFilterLbl setText:musicName];
    [self.musicFilterLbl setTextColor:trendingMusicToggleActiveColor];
    _musicFilterLbl.frame = lblFrame;

    [_musicFilterLbl reloadInputViews];

    [self.musicFilterImg setImage:[UIImage imageNamed:@"tab_icon_music_active_green"]];
    _musicFilterImg.contentMode = UIViewContentModeScaleAspectFill;
    _musicFilterImg.frame = imgFrame;
}

-(void)musicOff
{
    CGRect imgFrame = CGRectMake(50, 12, 16, 20);
    CGRect lblFrame = CGRectMake(75, 11, 85, 21);
    [self.musicFilterLbl setText:@"Music"];
    [self.musicFilterLbl setTextColor:trendingMusicToggleOffColor];
    _musicFilterLbl.frame = lblFrame;
    [self.musicFilterImg setImage:[UIImage imageNamed:@"tab_icon_music_off"]];
    _musicFilterImg.contentMode = UIViewContentModeScaleAspectFill;
    _musicFilterImg.frame = imgFrame;
}

-(void)musicInactive
{
    CGRect imgFrame = CGRectMake(50, 12, 16, 20);
    CGRect lblFrame = CGRectMake(75, 11, 85, 21);
    [self.musicFilterLbl setText:@"Music"];
    [self.musicFilterLbl setTextColor:trendingMusicToggleInactiveColor];
    _musicFilterLbl.frame = lblFrame;
    [self.musicFilterImg setImage:[UIImage imageNamed:@"tab_icon_music"]];
    _musicFilterImg.contentMode = UIViewContentModeScaleAspectFill;
    _musicFilterImg.frame = imgFrame;
}

-(void)upvoteActive
{
    [self.upvoteFilterLbl setText:@"Upvotes"];
    [self.upvoteFilterLbl setTextColor:trendingMusicToggleActiveColor];
    [self.upvoteFilterImg setImage:[UIImage imageNamed:@"tab_icon_upvote_active_green"]];
}

-(void)upvoteInactive
{
    [self.upvoteFilterLbl setText:@"Upvotes"];
    [self.upvoteFilterLbl setTextColor:trendingMusicToggleInactiveColor];
    [self.upvoteFilterImg setImage:[UIImage imageNamed:@"tab_icon_upvote"]];
}

-(void)upvoteOff
{
    [self.upvoteFilterLbl setText:@"Upvotes"];
    [self.upvoteFilterLbl setTextColor:trendingMusicToggleOffColor];
    [self.upvoteFilterImg setImage:[UIImage imageNamed:@"tab_icon_upvote_off"]];
}

#pragma mark - Actions

- (IBAction)selectMusicFilter:(id)sender {
    [musicFilter.collectionView reloadData];
    if (self.musicFilterView.alpha <= 0) {
        [self showMusicFilter];
    } else {
        [self hideMusicFilter];
    }
}

-(void)showMusicFilter
{
    showMusicFilter = YES;
    CGRect mFrame = CGRectMake(0, 101, [[UIScreen mainScreen] bounds].size.width, _musicFilterView.frame.size.height);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.destinationViewController.view.userInteractionEnabled = NO;
        self.musicFilterView.frame = mFrame;
        self.musicFilterView.alpha = 1;
        self.clearMusicFilterBtn.alpha = 1;
    }];
}

-(void)hideMusicFilter
{
    showMusicFilter = NO;
    NSInteger height = (self.clearMusicFilterBtn.frame.origin.y - (self.cityFilters.frame.origin.y + self.cityFilters.frame.size.height));
    CGRect mFrame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width, height);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.destinationViewController.view.userInteractionEnabled = YES;
        self.musicFilterView.frame = mFrame;
        self.musicFilterView.alpha = 0;
        self.clearMusicFilterBtn.alpha = 0;
    }];
}

- (IBAction)clearMusicFilter:(id)sender {
    if ([userInfo.musicFilterId intValue] >= 0) {
        userInfo.musicFilterId = [NSNumber numberWithInt:-1];
        searchMode = NO;
        [self askForTrendingData];
    }
    [self setupFilters];
    [self hideMusicFilter];
}

- (IBAction)selectUpvoteFilter:(id)sender {
    userInfo.musicFilterId = [NSNumber numberWithInt:-1];
    if (self.musicFilterView.alpha == 1){
        [self hideMusicFilter];
    }
    
    if ([userInfo.activityFilterId intValue] >= 0) {
        userInfo.activityFilterId = [NSNumber numberWithInteger:-1];
    } else {
        userInfo.activityFilterId = [NSNumber numberWithInteger:0];
    }
    [self setupFilters];
    [self askForTrendingData];
}

- (IBAction)nearMe:(id)sender {
    [self setRefreshToDefault];
    TrendingMapView *trendingMap = ((TrendingMapView*)_destinationViewController);
    [trendingMap zoomToCurrentLocation];
    [self setRefreshToDefault];
}

- (IBAction)cityView:(id)sender {
    [self setRefreshToDefault];
    TrendingMapView *trendingMap = ((TrendingMapView*)_destinationViewController);
    [trendingMap zoomToCityView];
    [self setRefreshToDefault];
}

- (IBAction)refreshMap:(id)sender {
    [self setRefreshToDefault];
    TrendingMapView *trendingMap = ((TrendingMapView*)_destinationViewController);
    [trendingMap refreshMap];
}

- (IBAction)cityViewSelected:(id)sender {
//    UIButton *btn = (UIButton *)sender;
//    _cityViewLabel.textColor = [UIColor whiteColor];
}

- (IBAction)cityViewUnselected:(id)sender {
//    UIButton *btn = (UIButton *)sender;
//    _cityViewLabel.textColor = [UIColor darkGrayColor];
}

- (IBAction)refreshSelected:(id)sender {
//    UIButton *btn = (UIButton *)sender;
//    _refreshLabel.textColor = [UIColor whiteColor];
}

- (IBAction)refreshUnselected:(id)sender {
//    UIButton *btn = (UIButton *)sender;
//    _refreshLabel.textColor = [UIColor darkGrayColor];
}

- (IBAction)nearMeSelected:(id)sender {
//    UIButton *btn = (UIButton *)sender;
//    _nearMeLabel.textColor = [UIColor whiteColor];
}

- (IBAction)nearMeUnselected:(id)sender {
//    UIButton *btn = (UIButton *)sender;
//    _nearMeLabel.textColor = [UIColor darkGrayColor];
}

- (IBAction)goToGroupMembers:(id)sender {
    if (_tabIndex == 2) {
        return;
    }
    
    self.groupView.alpha = 1;
    titleBar.subTitle.text = @"Members";
    
    UIImage *off = [UIImage imageNamed:@"group_trending"];
    _grpTrendingImg.image = off;

    off = [UIImage imageNamed:@"venue_buzz"];
    _grpBuzzImg.image = off;
    
    off = [UIImage imageNamed:@"venue_photos"];
    _grpInviteImg.image = off;
    
    UIImage *on = [UIImage imageNamed:@"group_members_active"];
    _groupMembersImg.image = on;
    
    self.mainTrendingView.alpha = 0;
    groupTabBarController.selectedIndex = 2;
    if (_tabIndex == 0 && [_destinationViewController isKindOfClass:[TrendingListView class]]) {
        [self exitSearchModeAndAskForTrending:NO];
    }
    
    [groupMembersViewController loadData:self.groupId];
    _tabIndex = 2;
}

- (IBAction)goToGroupBuzz:(id)sender {
    self.groupView.alpha = 1;
    titleBar.subTitle.text = @"Buzz";
    
    UIImage *off = [UIImage imageNamed:@"group_trending"];
    _grpTrendingImg.image = off;
    
    off = [UIImage imageNamed:@"venue_buzz_active"];
    _grpBuzzImg.image = off;
    
    off = [UIImage imageNamed:@"venue_photos"];
    _grpInviteImg.image = off;
    
    UIImage *on = [UIImage imageNamed:@"group_members"];
    _groupMembersImg.image = on;

    self.mainTrendingView.alpha = 0;
    buzzViewController.isGroup = YES;
    buzzViewController.buzzTypeId = _groupId;
    NSString *name = [[groups getGroupForId:_groupId] name];
    if ([name isEqualToString:@""] || name == nil) {
        name = _groupName;
    }
    buzzViewController.name = name;
    buzzViewController.delegate = self;
    groupTabBarController.selectedIndex = 1;
    
    if (_tabIndex == 0 && [_destinationViewController isKindOfClass:[TrendingListView class]]) {
        [self exitSearchModeAndAskForTrending:NO];
    }
    
    _tabIndex = 1;
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (IBAction)goToGroupTrending:(id)sender {
    if (_tabIndex == 0) {
        return;
    }
    
    self.groupView.alpha = 0;
    titleBar.subTitle.text = @"Trending";
    
    UIImage *off = [UIImage imageNamed:@"group_trending_active"];
    _grpTrendingImg.image = off;
    
    off = [UIImage imageNamed:@"venue_buzz"];
    _grpBuzzImg.image = off;
    
    off = [UIImage imageNamed:@"venue_photos"];
    _grpInviteImg.image = off;
    
    UIImage *on = [UIImage imageNamed:@"group_members"];
    _groupMembersImg.image = on;
    
    self.mainTrendingView.alpha = 1;
    groupTabBarController.selectedIndex = 0;
    groupClient.delegate = self;
    userClient.delegate = self;
    _tabIndex = 0;
    
    if ([trendingDataCache count] <= 0) {
        [self askForTrendingData];
    }
}

- (IBAction)goToGroupInvites:(id)sender {
    if (_tabIndex == 3) {
        return;
    }
    
    self.groupView.alpha = 1;
    titleBar.subTitle.text = @"Photos";
    
    UIImage *off = [UIImage imageNamed:@"group_trending"];
    _grpTrendingImg.image = off;
    
    off = [UIImage imageNamed:@"venue_buzz"];
    _grpBuzzImg.image = off;
    
    off = [UIImage imageNamed:@"venue_photos_active"];
    _grpInviteImg.image = off;
    
    UIImage *on = [UIImage imageNamed:@"group_members"];
    _groupMembersImg.image = on;
    
    self.mainTrendingView.alpha = 0;
    groupTabBarController.selectedIndex = 3;
    
    imageViewController.venueId = self.groupId;
    imageViewController.venueName = _groupName;
    imageViewController.venueMode = NO;
    [imageViewController loadImagesForGroup];
    imageViewController.delegate = self;
    
    if (_tabIndex == 0 && [_destinationViewController isKindOfClass:[TrendingListView class]]) {
        [self exitSearchModeAndAskForTrending:NO];
    }
    
    _tabIndex = 3;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    DDLogDebug(@"MEMORY WARNING IN TRENDING");
}

#pragma mark - SWRevelaViewControllerDelegate

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    //Set up so that when side bar is open, cant do anything on
    //the other screen other than pull side bar back
    if (position == FrontViewPositionRight) {
        for (UIView *subView in self.view.subviews) {
            [subView setUserInteractionEnabled:NO];
        }
        [self.titleBarImgView setUserInteractionEnabled:YES];
    } else {
        for (UIView *subView in self.view.subviews) {
            [subView setUserInteractionEnabled:YES];
        }
    }
}

#pragma mark - IgnightTopBarDelegate

-(void)ignightTopBar: (IgnightTopBar *)topBar rightSideButtonClicked: (UIButton *)button
{
    GroupDetailsViewController *groupDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"groupDetails"];
    groupDetailsViewController.groupId = _groupId;
    groupDetailsViewController.allowToJoin = NO;
    
    [self.navigationController pushViewController:groupDetailsViewController animated:YES];
}

-(void)clickedImage:(UIImage *)image
{
    FSBasicImage *photo = [[FSBasicImage alloc] initWithImage:image];
    
    FSBasicImageSource *source = [[FSBasicImageSource alloc] initWithImages:[NSArray arrayWithObject:photo]];
    FSImageViewerViewController *fsImgController = [[FSImageViewerViewController alloc] initWithImageSource:source imageIndex:(NSInteger)0];
    fsImgController.name = _groupName;
    
    self.navigationItem.title = @"";
    self.navigationController.navigationBar.topItem.title = @"";
    [self.navigationController pushViewController:fsImgController animated:YES];
}

-(void)clickedImageAtIndex:(NSInteger)row
{
    NSArray* imgList = [imageViewController getImageList];
    if (imgList == nil) {
        return;
    }
    
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (BuzzImageData *data in imgList) {
        FSBasicImage *photo;
        if (data.image != nil) {
            photo = [[FSBasicImage alloc] initWithImage:data.image];
        } else {
            photo = [[FSBasicImage alloc] initWithImageURL:data.imgUrl];
        }
        [images addObject:photo];
    }
    
    FSBasicImageSource *source = [[FSBasicImageSource alloc] initWithImages:images];
    FSImageViewerViewController *fsImgController = [[FSImageViewerViewController alloc] initWithImageSource:source imageIndex:(NSInteger)row];
    fsImgController.delegate = self;
    fsImgController.name = _groupName;
    
    self.navigationItem.title = @"";
    self.navigationController.navigationBar.topItem.title = @"";
    [self.navigationController pushViewController:fsImgController animated:YES];
}

#pragma mark - FSImageViewControllerDelegate

-(void)imageViewerViewController:(FSImageViewerViewController *)imageViewerViewController reportImageAtIndex:(NSInteger)index
{
    NSArray* imgList = [imageViewController getImageList];
    BuzzImageData *data = imgList[index];
    BuzzHttpClient *client = [BuzzHttpClient getInstance];
    [client flagBuzz:data.buzzId inGroup:_groupId];
}

#pragma mark - UITextViewDelegate


- (void)textViewDidEndEditing:(UITextView *)textView
{
    DDLogDebug(@"Text View Ended Editing To: %@", textView.text);
}


#pragma mark - Remote Notifications

-(void)setupPushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewMailPush:) name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewGroupBuzz:) name:@"NewGroupBuzzNotification" object:nil];
}

-(void)removePushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewGroupBuzzNotification" object:nil];
}

-(void)handleNewMailPush: (NSNotification *)notification
{
    GroupViewController *groupView = [self.storyboard instantiateViewControllerWithIdentifier:@"allGroupView"];
    groupView.tabIndex = 2;
    [self.navigationController pushViewController:groupView animated:YES];
}

-(void)handleNewGroupBuzz: (NSNotification *)notification
{
    NSDictionary *data = notification.userInfo;
    NSNumber *groupId = [NSNumber numberWithInteger:[data[@"groupId"] integerValue]];

    if (_groupMode && [groupId isEqualToNumber:_groupId]) {
        [self goToGroupBuzz:nil];
        [buzzViewController getBuzzData];
        return;
    }
    
    Trending *trending = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
    trending.goBack = NO;
    trending.groupMode = YES;
    trending.groupName = data[@"groupName"];
    trending.groupId = data[@"groupId"];
    trending.tabIndex = 1;
    
    [self.navigationController pushViewController:trending animated:YES];
}

-(void)stopLoadingSearch
{
    if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
        [((TrendingListView *)self.destinationViewController) stopLoadingInSearch];
    }
}

-(void)showLoadingSearch
{
    if ([self.destinationViewController isKindOfClass:[TrendingListView class]]) {
        [((TrendingListView *)self.destinationViewController) showLoadingInSearch];
    }
}

@end
