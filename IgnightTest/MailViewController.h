//
//  MailViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/16/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Group.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <AFHTTPRequestOperation.h>

@protocol MailViewDelegate;

@interface MailViewController : UIViewController <UITableViewDelegate,
                                                    UITableViewDataSource,
                                                    SWRevealViewControllerDelegate,
                                                    GroupHttpClientDelegate>

@property (nonatomic, assign) id<MailViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *topBar;
@property (strong, nonatomic) IBOutlet UITableView *mailView;

- (IBAction)acceptedInvite:(id)sender;
- (IBAction)rejectedInvite:(id)sender;

@end

@protocol MailViewDelegate <NSObject>

-(void)mailViewController:(MailViewController *)mailViewController acceptedInviteToGroup: (Group *)group;

@end