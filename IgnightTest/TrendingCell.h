//
//  TrendingCell.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/10/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TrendingCellDelegate <NSObject>

@required
- (void)userActivityChangedTo:(NSInteger)userActivity forVenueId: (NSNumber *)venueId atIndexPath:(NSIndexPath *)indexPath;
@end

@interface TrendingCell : UITableViewCell

@property (strong, nonatomic) id <TrendingCellDelegate> delegate;
@property (strong, nonatomic) UIImageView *dnaImageView;
@property (strong, nonatomic) UILabel *groupInputValue;
@property (strong, nonatomic) UILabel *venueName;
@property (strong, nonatomic) UILabel *venueDistance;
@property (strong, nonatomic) UIImageView *upvoteImgView;
@property (strong, nonatomic) UIImageView *downvoteImgView;
@property (strong, nonatomic) UIButton *upVote;
@property (strong, nonatomic) UIButton *downVote;
@property (strong, nonatomic) NSIndexPath *indexPath;

-(void)setName: (NSString *)name withDistance: (NSString *)distance withGroupDnaValue: (NSNumber *)dnaValue withUserInput: (NSInteger)input withUserBarColor: (NSNumber *)barColor forVenueId: (NSNumber *)venue;
-(void)setName: (NSString *)name withDistance: (NSString *)distance withDnaValue: (NSNumber *)dnaValue withUserInput: (NSInteger)input forVenueId: (NSNumber *)venue;

@end
