//
//  AtmosphereViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/21/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "AtmosphereViewController.h"
#import "UserInfo.h"
#import "AtmosphereTypes.h"

#define TAG_ADJ 100

@interface AtmosphereViewController ()

@end

#define MAX_RANK 3

@implementation AtmosphereViewController
{
    UserInfo *userInfo;
    int optionsInRow;
    
    NSMutableDictionary *selectionToRank;
    NSMutableDictionary *rankToSelection;
    int selectionCounter;
    int maxSelectedRank;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    optionsInRow = 3;
    IgnightPreferences *prefs = [[IgnightPreferences alloc] initWithPreferenceSet:[AtmosphereTypes getListOfValues] container:self.scrollView optionsInRow:optionsInRow forClass:self width:320];
    [prefs populateScrollView];

    selectionToRank = [[NSMutableDictionary alloc] init];
    rankToSelection = [[NSMutableDictionary alloc] init];
    selectionCounter = 0;
    maxSelectedRank = 0;
    
    userInfo = [UserInfo getInstance];
    TFLog(@"AtmosphereTypes: Loaded UserInfo with: [%@, %@, %@]",
          [AtmosphereTypes getAtmosphereNameFromId:[[userInfo.atmospheres objectAtIndex:0] intValue]],
          [AtmosphereTypes getAtmosphereNameFromId:[[userInfo.atmospheres objectAtIndex:1] intValue]],
          [AtmosphereTypes getAtmosphereNameFromId:[[userInfo.atmospheres objectAtIndex:2] intValue]]);
    
    [self resetSelections];
    self.navigationController.navigationBarHidden = YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)resetSelections
{
    for (int i = 0; i < MAX_RANK; i++) {
        NSInteger atmosphereId = [[userInfo.atmospheres objectAtIndex:i] intValue];
        if (atmosphereId < 0)
            return;
        
        NSString *name = [AtmosphereTypes getAtmosphereNameFromId:atmosphereId];
        [self setSelection:name asRank:i];
    }
}

- (IBAction)selectionOptionPressed:(UIButton *)sender {
    UIButton *button = (UIButton *)sender;
    
    NSString *buttonTitle = [[button titleLabel] text];
    
    if (button.selected == NO) {
        NSInteger nextRank = [self findNextAvailableRank];
        
        if (nextRank == -1)
            return;
        
        [self setSelection:buttonTitle asRank:nextRank];
    } else {
        NSInteger rank = [[selectionToRank objectForKey:buttonTitle] integerValue];
        [self removeSelection:buttonTitle asRank:rank];
    }
}


-(NSInteger)findNextAvailableRank {
    for (int i = 0; i < MAX_RANK; i++) {
        if (![[rankToSelection allKeys] containsObject:[NSString stringWithFormat:@"%d", i]])
            return i;
    }
    return -1;
}

-(void)setSelection: (NSString *)name asRank: (NSInteger)rank
{
    [selectionToRank setObject:[NSString stringWithFormat:@"%d", rank] forKey:name];
    [rankToSelection setObject:name forKey:[NSString stringWithFormat:@"%d", rank]];
    
    NSString *imagePath = [NSString stringWithFormat:@"selection-option-button-selected-%i", rank + 1];
    UIImage *newImage = [UIImage imageNamed:imagePath];
    
    NSInteger tag = [AtmosphereTypes getAtmosphereFromName:name];
    UIButton *button = (UIButton*)[self.scrollView viewWithTag:(tag + TAG_ADJ)];
    
    [button setBackgroundImage:newImage forState:UIControlStateSelected];
    [button  setSelected:YES];
    selectionCounter++;
    
    [self checkIfReadyToContinue];
}

-(void)removeSelection: (NSString *)name asRank: (NSInteger)rank
{
    [selectionToRank removeObjectForKey:name];
    [rankToSelection removeObjectForKey:[NSString stringWithFormat:@"%d", rank]];
    
    NSInteger tag = [AtmosphereTypes getAtmosphereFromName:name];
    UIButton *button = (UIButton*)[self.scrollView viewWithTag:(tag + TAG_ADJ)];
    
    [button setBackgroundImage:nil forState:UIControlStateSelected];
    [button setSelected:NO];
    selectionCounter--;
    
    [self checkIfReadyToContinue];
}

-(void)checkIfReadyToContinue
{
    self.continueButton.alpha = selectionCounter;
    if (selectionCounter == MAX_RANK) {
        self.scrollView.alpha = 0.35;
    } else
        self.scrollView.alpha = 1;
}


-(void)updateUserInfo
{
    [userInfo resetAtmosphere];
    for (int i = 0; i < [rankToSelection count]; i++) {
        NSString *name = [rankToSelection objectForKey:[NSString stringWithFormat:@"%d", i]];
        NSNumber *dnaValue = [NSNumber numberWithInt:[AtmosphereTypes getAtmosphereFromName:name]];
        [userInfo setAtmosphereDna:dnaValue atSelection:i];
    }
}

-(void)goToNext
{
    [self updateUserInfo];
    [self performSegueWithIdentifier:@"atmosphereToSpending" sender:self];
}

- (IBAction)continueButtonPressed:(id)sender {
    [self goToNext];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    UISwipeGestureRecognizer *gesture = (UISwipeGestureRecognizer*)gestureRecognizer;
    if (gesture.direction == UISwipeGestureRecognizerDirectionRight) {
        TFLog(@"DIRECTION: RIGHT");
        [self updateUserInfo];
        return YES;
    } else {
        TFLog(@"DIRECTION: LEFT");
        if (self.continueButton.alpha == 1) {
            [self goToNext];
            return YES;
        }
        return NO;
    }
}

@end
