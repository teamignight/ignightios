//
//  main.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/24/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightTestAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IgnightTestAppDelegate class]));
    }
}

