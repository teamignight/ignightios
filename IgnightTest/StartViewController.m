//
//  StartViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/17/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "StartViewController.h"
#import "LoginViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController
{
    UIImageView *backGroundImageView;
    UserInfo *userInfo;
    UserHttpClient *userClient;
    
    Groups *groups;
    Inbox *inbox;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    userInfo = [UserInfo getInstance];
    groups = [Groups getInstance];
    inbox = [Inbox getInstance];
    
    [[self navigationController] setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.navigationController.navigationBar.topItem.title = @"";
    
    backGroundImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    backGroundImageView.image = [UIImage imageNamed:@"Default"];
    [self.view addSubview:backGroundImageView];
    
    userClient = [UserHttpClient getInstance];
    userClient.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = navigationBarColor;
    
    DDLogDebug(@"StartViewController : viewWillAppear");
    
    StoredUserData *userData = [IgnightUtil getUserData];
    DDLogDebug(@"USER DATA: %@", userData.userId);
    if (userData != nil && userData.userId) {
        userInfo.userId = userData.userId;
        [userClient getUserInfo];
        [userClient getUserNotifications];
        return;
    }
    
    LoginViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
    [self.navigationController pushViewController:loginController animated:NO];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    DDLogDebug(@"Preparing for segue: %@", segue.identifier);
    DDLogDebug(@"Destination: %@", segue.destinationViewController);
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    DDLogDebug(@"StartViewController: Recieved Memmory Warning");
}

#pragma mark - UserHttpClientDelegate

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    LoginViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
    [self.navigationController pushViewController:loginController animated:NO];
    DDLogDebug(@"ERROR : %@", error);
    DDLogWarn(@"StartViewController: Failed to reach server, Going to Login Screen");
}

-(void)userHttpClient:(UserHttpClient *)client gotUserInfo:(NSDictionary *)response
{
    BOOL result = [[response objectForKey:@"res"] boolValue];
    if (result) {
        [self setupUserAfterLoggingIn:response];
    } else {
        LoginViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"login"];
        [self.navigationController pushViewController:loginController animated:NO];
    }
}

-(void)setupUserAfterLoggingIn: (NSDictionary*)response
{
    NSInteger result = [IgnightUtil updateUserInfoOnLogin:response];
    
    if (result > 0) {
        [self performSegueWithIdentifier:@"login" sender:self];
    } else {
        [self performSegueWithIdentifier:@"setupProfile" sender:self];
    }
}

@end
