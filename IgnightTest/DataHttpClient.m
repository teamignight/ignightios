//
//  DataHttpClient.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/13/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "DataHttpClient.h"

@implementation DataHttpClient
{
    UserInfo *userInfo;
}

+(DataHttpClient *)getInstance
{
    static DataHttpClient *_sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *url = server_url;
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:url]];
        [_sharedInstance.requestSerializer setValue:[IgnightProperties getHeaderToken] forHTTPHeaderField:@"X-IGNIGHT-TOKEN"];
        [_sharedInstance.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"Platform"];
        UserInfo* userInfo = [UserInfo getInstance];
        if (userInfo.userId != nil) {
            [_sharedInstance.requestSerializer setValue:userInfo.userId.stringValue forHTTPHeaderField:@"USER_ID"];
        }
        if (TARGET_IPHONE_SIMULATOR) {
        }else {
            _sharedInstance.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
            _sharedInstance.securityPolicy.allowInvalidCertificates = YES;
        }
    });
    return _sharedInstance;
}

-(instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        userInfo = [UserInfo getInstance];
    }
    
    return self;
}

@end
