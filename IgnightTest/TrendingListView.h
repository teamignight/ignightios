//
//  TrendingListView.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/8/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrendingCell.h"
#import <MapKit/MapKit.h>
#import "Trending.h"
#import "CityTrendingTableViewCell.h"

@interface TrendingListView : UITableViewController <UISearchBarDelegate,
                                                     UISearchDisplayDelegate,
                                                     TrendingCellDelegate,
                                                     VenueHttpClientDelegate,
                                                     CityTrendingTableViewCellDelegate,
                                                     CLLocationManagerDelegate>

@property (nonatomic, weak) Trending *mainTrending;
@property (nonatomic, strong) NSString *searchString;
@property (nonatomic, strong) NSMutableArray *searchVenues;
@property (nonatomic) BOOL groupMode;
@property (nonatomic, strong) NSString *groupName;

-(void)noMoreResults;
-(void)showLoadMore;
-(void)showAddVenue;
-(void)hideFooter;
-(void)clearSearchBar;
-(void)showKeyboard;
-(void)dropKeyboard;
-(void)showLoadingInSearch;
-(void)stopLoadingInSearch;

@end
