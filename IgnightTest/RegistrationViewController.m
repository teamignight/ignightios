//
//  RegistrationViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "RegistrationViewController.h"
#import "RESTHandler.h"
#import "UserInfo.h"
#import "IgnightTextField.h"

@interface RegistrationViewController ()

@end

@implementation RegistrationViewController
{
    RESTHandler *restHandler;
    NSMutableData *serverData;
    UserInfo *userInfo;
    BOOL inTextField;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initPrivateVariables
{
    restHandler = [RESTHandler getInstance];
    userInfo = [UserInfo getInstance];
    inTextField = NO;
    
    // setup text fields
    [IgnightTextField setupTextField:self.txtRegisterUserName];
    [IgnightTextField setupTextField:self.txtRegisterPassword];
    [IgnightTextField setupTextField:self.txtRegisterEmail];
}

- (void)setUpTapGestureRecognizer
{
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:singleTap];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initPrivateVariables];
    [self setUpTapGestureRecognizer];
}

- (void)dismissKeyboard
{
    [self.txtRegisterUserName resignFirstResponder];
    [self.txtRegisterPassword resignFirstResponder];
    [self.txtRegisterEmail resignFirstResponder];
    
    [self onTextFieldDidEndEditingSignupSetFrame];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

# pragma mark - Text Field Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField) {
        [textField resignFirstResponder];
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (inTextField == NO) {
        [self onTextFieldDidBeginEditingSignUpSetFrame];
        inTextField = YES;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

- (void)onTextFieldDidBeginEditingSignUpSetFrame
{
    [UIView animateWithDuration:0.35 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - 180, self.view.frame.size.width, self.view.frame.size.height);
    }];
    [self.registerView setFrame: CGRectMake(self.registerView.frame.origin.x, self.registerView.frame.origin.y - 20, self.registerView.frame.size.width, self.registerView.frame.size.height)];
    [self.registrationBtn setFrame:CGRectMake(self.registrationBtn.frame.origin.x, self.registrationBtn.frame.origin.y - 10, self.registrationBtn.frame.size.width, self.registrationBtn.frame.size.height)];
    [self.returnToLoginBtn setFrame:CGRectMake(self.returnToLoginBtn.frame.origin.x, self.returnToLoginBtn.frame.origin.y + 15, self.returnToLoginBtn.frame.size.width, self.returnToLoginBtn.frame.size.height)];
}

- (void)onTextFieldDidEndEditingSignupSetFrame
{
    if (inTextField) {
        [UIView animateWithDuration:0.35 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 180, self.view.frame.size.width, self.view.frame.size.height);
        }];
        [self.registerView setFrame: CGRectMake(self.registerView.frame.origin.x, self.registerView.frame.origin.y + 20, self.registerView.frame.size.width, self.registerView.frame.size.height)];
        [self.registrationBtn setFrame:CGRectMake(self.registrationBtn.frame.origin.x, self.registrationBtn.frame.origin.y + 10, self.registrationBtn.frame.size.width, self.registrationBtn.frame.size.height)];
        [self.returnToLoginBtn setFrame:CGRectMake(self.returnToLoginBtn.frame.origin.x, self.returnToLoginBtn.frame.origin.y - 15, self.returnToLoginBtn.frame.size.width, self.returnToLoginBtn.frame.size.height)];
        
        inTextField = NO;
    }
}

- (IBAction)btnRegisterAccount:(id)sender
{
    if ([self validateRegistrationInformation]) {
        NSDictionary *data = [[NSMutableDictionary alloc] init];
        [data setValue:@"add" forKey:@"type"];
        [data setValue:self.txtRegisterEmail.text forKey:@"email"];
        [data setValue:self.txtRegisterUserName.text forKey:@"userName"];
        [data setValue:self.txtRegisterPassword.text forKey:@"password"];
        NSDictionary *msg = [[NSMutableDictionary alloc] initWithObjectsAndKeys:data, @"msg", nil];
        [restHandler postToServlet:@"user" withBody:msg withDelegate:self];
    }
}

- (BOOL)validateRegistrationInformation
{
    if ([self.txtRegisterUserName.text isEqualToString:@""] ||
        [self.txtRegisterPassword.text isEqualToString:@""] ||
        [self.txtRegisterEmail.text isEqualToString:@""]) {
        return NO;
    }
    return YES;
}

#pragma mark - NSURLConnection

#pragma mark - NSURLConnectionDelegate protocols

- (void)connection: (NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    serverData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [serverData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *e;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:serverData options:kNilOptions error:&e];
    
    NSNumber *res = [json objectForKey:@"res"];
    BOOL result = [res boolValue];
    
    if (result) {
        NSDictionary *body = [json objectForKey:@"body"];
        NSNumber *userId = [body objectForKey:@"id"];
        userInfo.userId = userId;
        [self performSegueWithIdentifier:@"register" sender:self];
    } else {
        NSString *reason = [json objectForKey:@"reason"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:reason delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    TFLog(@"Connection Failed! Error - %@ %@", [error localizedDescription], [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

#pragma mark - AlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.txtRegisterUserName.text = @"";
    self.txtRegisterPassword.text = @"";
    self.txtRegisterEmail.text = @"";
    [self.txtRegisterUserName becomeFirstResponder];
}



@end
