//
//  UserProfileViewController.h
//  IgnightTest
//
//  Created by Rob Chipman on 8/20/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "IgnightTopBar.h"
#import "Cities.h"
#import "MusicTypes.h"
#import "AtmosphereTypes.h"
#import "SpendingTypes.h"
#import "AgeBuckets.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <QuartzCore/QuartzCore.h>
#import "ImageSelectionController.h"
#import "GroupViewController.h"

@interface UserProfileViewController : UIViewController <SWRevealViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, UIAlertViewDelegate, UserHttpClientDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImageSelectionControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *titleBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
