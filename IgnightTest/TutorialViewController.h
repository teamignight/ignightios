//
//  TutorialViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorialContentViewController.h"
#import "SWRevealViewController.h"
#import "Trending.h"
#import "GroupViewController.h"

@interface TutorialViewController : UIViewController<UIPageViewControllerDataSource, SWRevealViewControllerDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *images;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;

@property BOOL signupMode;

@end
