//
//  TutorialViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "TutorialViewController.h"
#import "SWRevealViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

- (void)viewDidLoad
{
    DDLogDebug(@"TutorialViewController : viewDidLoad");
    [super viewDidLoad];
    
    _images = @[@"tutorial_trending_list", @"tutorial_venue_profile", @"tutorial_groups_portal", @"tutorial_group_trending"];
    
    _pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    // Change the size of page view controller
    [[_pageViewController view] setFrame:CGRectMake(0, 0, [[self view] bounds].size.width, [[self view] bounds].size.height + 37)];
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [_pageViewController didMoveToParentViewController:self];
    
    TutorialContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = [[NSArray alloc] initWithObjects:startingViewController, nil];
    [_pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];

    _pageControl.numberOfPages = [_images count];
    [self.view bringSubviewToFront: _pageControl];
    
    [self.view bringSubviewToFront:_backBtn];
    [_backBtn setBackgroundColor:[UIColor clearColor]];
    [_backBtn addTarget:_signupMode ? self : self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    if (!_signupMode) {
        UIImage *sb = [UIImage imageNamed:@"side-menu-button"];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:sb];
        imgView.frame = CGRectMake(8, 30, 25, 20);
        [self.view addSubview:imgView];
        [_backBtn setTitle:@"" forState:UIControlStateNormal];
    } else {
        [[_backBtn layer] setBorderColor:[UIColor whiteColor].CGColor];
        [[_backBtn layer] setBorderWidth:0.4];
        [_backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupPushNotificationObserver];
    
    //Because of invite contacts nav bar
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removePushNotificationObserver];
}

- (void)revealToggle:(id)sender
{
    [self performSegueWithIdentifier:@"trending" sender:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    for (UIView *view in self.pageViewController.view.subviews ) {
//        if ([view isKindOfClass:[UIScrollView class]]) {
//            DDLogDebug(@"SCROLLLLL VIEW");
//            UIScrollView *scroll = (UIScrollView *)view;
//            scroll.bounces = NO;
//        }
//    }
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == 3) {
//        _backBtn.title
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (TutorialContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([_images count] == 0) || (index >= [_images count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    TutorialContentViewController *pageContentViewController = [[TutorialContentViewController alloc] init];
    pageContentViewController.imageFile = _images[index];
    pageContentViewController.index = index;
    return pageContentViewController;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UIPageViewControllerDataSource

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [_images count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((TutorialContentViewController*) viewController).index;
    
    [self updatePageControlWithIndex:index];
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((TutorialContentViewController*) viewController).index;
    if (index == NSNotFound) {
        return nil;
    }
    
    [self updatePageControlWithIndex:index];
    
    index++;
    if (index == [_images count]) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

-(void)updatePageControlWithIndex: (NSInteger )index
{
    _pageControl.currentPage = index;
    if (_signupMode) {
        if (index == 3) {
            [_backBtn setTitle:@"Done" forState:UIControlStateNormal];
            [_backBtn setTitle:@"Done" forState:UIControlStateHighlighted];
        } else {
            [_backBtn setTitle:@"Skip" forState:UIControlStateNormal];
            [_backBtn setTitle:@"Skip" forState:UIControlStateHighlighted];
        }
    }
}

#pragma mark - Remote Notifications

-(void)setupPushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewMailPush:) name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewGroupBuzz:) name:@"NewGroupBuzzNotification" object:nil];
}

-(void)removePushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewGroupBuzzNotification" object:nil];
}

-(void)handleNewMailPush: (NSNotification *)notification
{
    GroupViewController *groupView = [self.storyboard instantiateViewControllerWithIdentifier:@"allGroupView"];
    groupView.tabIndex = 2;
    [self.navigationController pushViewController:groupView animated:YES];
}

-(void)handleNewGroupBuzz: (NSNotification *)notification
{
    NSDictionary *data = notification.userInfo;
    Trending *trending = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
    trending.goBack = NO;
    trending.groupMode = YES;
    trending.groupName = data[@"groupName"];
    trending.groupId = data[@"groupId"];
    trending.tabIndex = 1;
    [self.navigationController pushViewController:trending animated:YES];
}


@end
