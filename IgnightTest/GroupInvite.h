//
//  GroupInvite.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PushMessage.h"

@interface GroupInvite : PushMessage

@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, copy) NSString *userName;

@end
