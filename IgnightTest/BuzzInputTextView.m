//
//  BuzzInputTextView.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 3/11/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "BuzzInputTextView.h"
#import "IgnightUtil.h"
#import <QuartzCore/QuartzCore.h>
#import "IgnightColors.h"
#import "HPGrowingTextView.h"

@implementation BuzzInputTextView
{
    UIButton *send;
    UIButton *camera;
    HPGrowingTextView *textView;
    UIImageView *imgView;
    
    CGFloat oldHeight;
}

#define InitialHeight 50
#define CameraWdth 33
#define TextViewYCoordinate 8
#define TextViewWidthBegin 260
#define TextViewHeightBegin 30
#define TextViewWidthWithSend 200
#define SendWidth 60

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        //Setup The Camera Button
        CGRect cameraFrame = CGRectMake(0, 0, 60, InitialHeight);
        camera = [UIButton buttonWithType:UIButtonTypeCustom];
        [camera setFrame:cameraFrame];
        [camera setBackgroundColor:[UIColor clearColor]];
        [camera addTarget:self action:@selector(camera:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImage *orignal = [UIImage imageNamed:@"buzz_camera"];
        
        imgView = [[UIImageView alloc] initWithFrame:CGRectMake(14, 13.5, 32, 23)];
        imgView.image = orignal;
        [self addSubview:imgView];
        
        [self addSubview:camera];
        
        //Setup the Send Button
        CGRect sendFrame = CGRectMake(320 - SendWidth, 0, SendWidth, InitialHeight);
        send = [UIButton buttonWithType:UIButtonTypeCustom];
        [send setFrame: sendFrame];
        [send setBackgroundColor:[UIColor whiteColor]];
        [send addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
        send.titleLabel.font = [UIFont systemFontOfSize:16];
        [send setTitleColor:buzzSendButton forState:UIControlStateNormal];
        [send setTitleColor:[IgnightUtil backgroundGray] forState:UIControlStateSelected | UIControlStateHighlighted];
        [send setTitle:NSLocalizedString(@"Send", Nil) forState:UIControlStateNormal];
        [send setEnabled:NO];
        [[send titleLabel] setAlpha:0.5];
        [self addSubview:send];
        
        //Setup TextInput View
        CGRect textViewFrame = CGRectMake(60, TextViewYCoordinate, TextViewWidthWithSend, TextViewHeightBegin);
        textView = [[HPGrowingTextView alloc] initWithFrame:textViewFrame];
        [textView setDelegate:self];
        textView.backgroundColor = [UIColor colorWithWhite:245/255.0f alpha:1];
        //    textView.scrollIndicatorInsets = UIEdgeInsetsMake(13, 0, 0, 6);
        textView.font = [UIFont systemFontOfSize:15];
        [[textView layer] setCornerRadius:4];
        [[textView layer] setMasksToBounds:YES];
        [[textView layer] setBorderColor:[[IgnightUtil backgroundGray] CGColor]];
        [[textView layer] setBorderWidth:1.25];
        [textView setMaxNumberOfLines:6];
        
        [self addSubview:textView];
        
        oldHeight = textView.minHeight;
        
        [[self layer] setBorderWidth:0.3];
        [[self layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    }
    return self;
}

-(void)resignKeyboard
{
    [textView resignFirstResponder];
}

-(void)drawRect:(CGRect)rect
{
    CGRect frame = camera.frame;
    camera.frame = CGRectMake(frame.origin.x, self.frame.size.height - InitialHeight, frame.size.width, frame.size.height);
    
    frame = imgView.frame;
    imgView.frame = CGRectMake(frame.origin.x, self.frame.size.height - 36.5, frame.size.width, frame.size.height);
    
    frame = send.frame;
    send.frame = CGRectMake(frame.origin.x, self.frame.size.height - InitialHeight, frame.size.width, frame.size.height);
}

#pragma mark UITextViewDelegate

//- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
//{
//    return [self.delegate textViewShouldBeginEditing:textView];
//}

//- (BOOL)textViewShouldEndEditing:(UITextView *)textView
//{
//    return [self.delegate textViewShouldEndEditing:textView];
//}
//
//- (void)textViewDidBeginEditing:(UITextView *)textView
//{
//    [self.delegate textViewDidBeginEditing:textView];
//}
//
- (void)textViewDidEndEditing:(UITextView *)textView
{
    DDLogDebug(@"TV did end editing");
//    [self.delegate textViewDidEndEditing:textView];
}
//
//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//{
//    return [self.delegate textView:textView shouldChangeTextInRange:range replacementText:text];
//}
//

- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView;
{
    if ([growingTextView.text length]) {
        [send setEnabled:YES];
        send.titleLabel.alpha = 1;
    } else {
        [send setEnabled:NO];
        send.titleLabel.alpha = 0.5f; // Sam S. says 0.4f
    }
}

#pragma mark - HPGrowingTextViewDelegate

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    [self.delegate textViewChangingHeight:oldHeight toHeight:height];
    oldHeight = height;
}

#pragma mark - Camera Action

-(IBAction)camera:(id)sender
{
    [self.delegate goToCamera];
}

-(IBAction)send:(id)sender
{
    [send setHighlighted:YES];
    [send setSelected:YES];
    [self.delegate sendInput: textView.text];
    
    textView.text = nil;
}

@end
