//
//  Group.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/1/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Group : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *groupDescription;
@property (nonatomic) BOOL publicGroup;
@property (nonatomic) NSInteger groupId;
@property (strong, nonatomic) NSNumber *adminId;
@property (nonatomic) BOOL activity;
@property (nonatomic) NSInteger members;
@property (nonatomic) BOOL isMember;
@property (nonatomic, strong) NSNumber *lastBuzzTs;


-(NSComparisonResult)compare: (Group *)otherObject;

@end
