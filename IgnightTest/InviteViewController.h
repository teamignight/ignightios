//
//  InviteViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFHTTPSessionManager.h>
#import "UserHttpClient.h"
#import "GroupHttpClient.h"
#import "SearchedUser.h"
#import <UIImageView+AFNetworking.h>
#import <QuartzCore/QuartzCore.h>

@interface InviteViewController : UIViewController <UISearchBarDelegate,
                                                    UISearchDisplayDelegate,
                                                    UITableViewDelegate,
                                                    UITableViewDataSource,
                                                    UserHttpClientDelegate,
                                                    GroupHttpClientDelegate>


@property (strong, nonatomic) NSNumber *groupId;

@end
