//
//  ForgotPasswordViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/26/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "MBProgressHUD.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController
{
    UserHttpClient *userClient;
    UserInfo *userInfo;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(respondToTapGesture:)];
    tapRecognizer.numberOfTapsRequired = 1;
    
    [self.view addGestureRecognizer:tapRecognizer];
    
    [[_usernameView layer] setCornerRadius:3];
    [[_usernameView layer] setMasksToBounds:YES];
    
    [[_emailMeBtn layer] setCornerRadius:3];
    [[_emailMeBtn layer] setMasksToBounds:YES];
    [_emailMeBtn setBackgroundColor:blueBtnBackground];
    [_emailMeBtn setTitleColor:whiteBtnText forState:UIControlStateNormal];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    [_forgotPasswordTxt setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self respondToTapGesture:nil];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    userClient = [UserHttpClient getInstance];
    userClient.delegate = self;
    
    userInfo = [UserInfo getInstance];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.tintColor = [IgnightUtil backgroundGray];
    self.navigationController.navigationBar.alpha = 0.90;
    self.navigationController.navigationBar.topItem.title = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardNotification:) name:UIKeyboardWillHideNotification object:nil];

    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [gestureRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
    
    [self.view addGestureRecognizer:gestureRecognizer];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)handleSwipe: (UISwipeGestureRecognizer *)recognizer
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [userClient cancelAllOperations];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [_forgotPasswordTxt resignFirstResponder];
}

- (void)keyboardNotification:(NSNotification *)notification {
    NSTimeInterval animationDuration;
    NSDictionary *uInfo = [notification userInfo];
    
    CGPoint kbPoint = [[uInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    [[uInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    CGRect frame = self.view.frame;
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = CGRectMake(frame.origin.x, kbPoint.y - frame.size.height, frame.size.width, frame.size.height);
    }];
}

-(IBAction)respondToTapGesture:(id)sender
{
    [self.forgotPasswordTxt resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)forgotPassword:(id)sender {
//    userInfo.userName = self.forgotPasswordTxt.text;
//    UpdatePasswordViewController * updatePassword = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"updatePassword"];
//    [self.navigationController pushViewController:updatePassword animated:YES];

    if ([self.forgotPasswordTxt.text isEqualToString:@""]) {
        IgAlert(nil, @"Username is required.", nil);
    } else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [_forgotPasswordTxt resignFirstResponder];
        [userClient sendTemporaryPassword:self.forgotPasswordTxt.text];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)userHttpClient:(UserHttpClient *)client gotForgotPasswordResponse:(NSDictionary *)response
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        userInfo.userName = self.forgotPasswordTxt.text;
        UpdatePasswordViewController * updatePassword = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"updatePassword"];
        [self.navigationController pushViewController:updatePassword animated:YES];
    } else {
        NSString *reason = response[@"reason"];
        IgAlert(nil, reason, nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    IgAlert(nil, @"Failed To Reach Server", nil);
}

#pragma mark - UITextViewDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField              // called when 'return' key pressed. return NO to ignore.
{
    [self forgotPassword:nil];
    return YES;
}

@end
