//
//  StoredUserData.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/21/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoredUserData : NSObject

@property (nonatomic, strong) NSNumber *userId;
@property BOOL iOSTokenSet;

-(id)initWithData: (NSDictionary *)data;
-(NSDictionary *)getData;

@end
