//
//  IgnightTopBar.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/8/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightColors.h"
#import "UserNotifications.h"

@protocol IgnightTopBarDelegate;

@interface IgnightTopBar : UIView <UserNotificationsDelegate>

@property (nonatomic, assign) id<IgnightTopBarDelegate> delegate;

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImage *backgroundImage;

@property (strong, nonatomic) UIButton *leftSideButton;
@property (strong, nonatomic) UIImageView *leftBtnBackgroundImage;

@property (strong, nonatomic) UIButton *rightSideButton;

@property (strong, nonatomic) UILabel *mainTitle;
@property (strong, nonatomic) UILabel *subTitle;

- (id)initWithTitle: (NSString *)title withSubtitle: (NSString *)subtitle withSideBarButtonTarget:(id)target;
- (id)initWithTitle: (NSString *)title withSideBarButtonTarget:(id)target;
-(id)initWithWebViewTarget: (id)target;
-(void)setTitleText: (NSString* )text;
- (void) setTitle: (NSString *)title;
- (void) setTitle: (NSString *)title withSubTitle: (NSString *)subTitle;
- (void) addRightContextMenuButton;
-(void)addAddGroupButton;

-(void)hideRightContextMenuButton;
-(void)setLeftButtonBackgroundImage:(UIImage *)leftButtonBackgroundImage;

-(void)setupWebView;
@end

@protocol IgnightTopBarDelegate <NSObject>

@optional

-(void)ignightTopBar: (IgnightTopBar *)topBar rightSideButtonClicked: (UIButton *)button;

@end