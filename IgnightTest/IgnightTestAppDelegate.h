//
//  IgnightTestAppDelegate.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/24/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFHTTPSessionManager.h>

#define IgAlert(TITLE,MSG, DELEGATE) [[[UIAlertView alloc] initWithTitle:(@"IgNight") \
                                                     message:(MSG) \
                                                    delegate:(DELEGATE) \
                                           cancelButtonTitle:@"OK" \
                                           otherButtonTitles:nil] show]

#define screenHeight [[UIScreen mainScreen] bounds].size.height
#define screenWidth [[UIScreen mainScreen] bounds].size.width

#define MAX_BUZZ_MSGS 20
//#define server_url @"http://localhost:8080/ignight/";
//#define server_url @"http://dev.chicago.ignight.com/ignight_server/";
#define server_url @"https://production.chicago.ignight.com/ignight_server/";

@interface IgnightTestAppDelegate : UIResponder <UIApplicationDelegate, UserHttpClientDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void)flipHack;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
