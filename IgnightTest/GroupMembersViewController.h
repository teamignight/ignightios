//
//  GroupMembersViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 2/3/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Groups.h"
#import "Group.h"
#import <QuartzCore/QuartzCore.h>
#import "JLImageDL.h"
#import "SearchUserTableViewCell.h"

@interface GroupMembersViewController : UITableViewController<
UserHttpClientDelegate,
GroupHttpClientDelegate,
SearchUserCellDelegate,
UISearchBarDelegate>

@property (nonatomic, copy) NSNumber *groupId;

-(void)loadData: (NSNumber *)gId;
-(void)sideBarWillMoveToPosition: (FrontViewPosition )position;

@end
