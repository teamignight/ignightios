//
//  VenueData.m
//  Ignight
//
//  Created by Abhinav Chordia on 1/25/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import "VenueData.h"
#import "TrendingGroup.h"

@implementation VenueData

static NSString *VENUE_DATA_MAP = @"venue";
static NSString *VENUE_ID = @"venueId";
static NSString *NAME = @"name";
static NSString *ADDRESS = @"address";
static NSString *PHONE_NUMBER = @"phoneNumber";
static NSString *URL = @"url";
static NSString *LATITUDE = @"latitude";
static NSString *LONGITUDE = @"longitude";
static NSString *USER_INPUT = @"userInput";
static NSString *USER_BAR_COLOR = @"userBarColor";

static NSString *VENUE_STATS_MAP = @"venueStats";
static NSString *MUSIC_TYPE = @"musicTypeMode";
static NSString *ATMOSPHERE_TYPE = @"atmosphereTypeMode";
static NSString *AGE_BUCKET = @"ageBucketMode";
static NSString *AVG_SPENDING_LIMIT = @"avgSpendingLimit";

static NSString *VENUE_IMAGES_ARRAY = @"placeImage";
static NSString *TRENDING_GROUPS_MAP = @"trendingGroups";

-(id)initWithData: (NSDictionary *)data
{
    self = [super init];
    if (self) {
        DDLogDebug(@"Data; %@", data);
        NSDictionary *venue = data[VENUE_DATA_MAP];
        _venueId = venue[VENUE_ID];
        _venueName = venue[NAME];
        _address = venue[ADDRESS];
        _phoneNumber = venue[PHONE_NUMBER];
        _url = venue[URL];
        _latitude = venue[LATITUDE];
        _longitude = venue[LONGITUDE];
        
        _userInputMode = (UserInputMode)[data[USER_INPUT] integerValue];
        _userBarColor = (UserBarColor)[data[USER_BAR_COLOR] integerValue];
        
        NSDictionary *venueStats = data[VENUE_STATS_MAP];
        _musicTypeMode = venueStats[MUSIC_TYPE];
        _atmosphereTypeMode = venueStats[ATMOSPHERE_TYPE];
        _ageBucketTypeMode = venueStats[AGE_BUCKET];
        _avgSpendingLimit = venueStats[AVG_SPENDING_LIMIT];
        
        _venueImages = data[VENUE_IMAGES_ARRAY];
        
        NSDictionary *trendingGroups = data[TRENDING_GROUPS_MAP];
        NSMutableArray *trending = [[NSMutableArray alloc] init];
        for (NSDictionary *trendingData in trendingGroups) {
            TrendingGroup *group = [[TrendingGroup alloc] initWithData:trendingData];
            [trending addObject:group];
        }
        _trendingGroups = [NSArray arrayWithArray:trending];
    }
    return self;
}

@end
