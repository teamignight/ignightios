//
//  StartViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/17/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StartViewController.h"

@interface StartViewController : UIViewController<UserHttpClientDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
