//
//  TrendingListView.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/8/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "TrendingListView.h"
#import "UserInfo.h"
#import "TrendingDataCache.h"
#import "TrendingCell.h"
#import "VenueViewController.h"
#import "MusicTypes.h"
#import "CityTrendingTableViewCell.h"
#import "CityVenueTrendingData.h"

@interface TrendingListView ()

@end

@implementation TrendingListView
{
    NSMutableDictionary *venueIdToImage;
    
    UserInfo *userInfo;
    VenueHttpClient *venueClient;
    
    TrendingDataCache *trendingDataCache;
    
    //Search Bar for Table View
    UISearchBar *trendingSearchbar;
    
    //UITableView Footer - Add a Venue
    UIButton *footer;
    
    //CoreLocation
    CLLocationManager *locationManager;
    
    //refresh
    UIRefreshControl *refreshMe;
    
    UIActivityIndicatorView *spinner;
    UITextField *searchBarTxtField;
    UIView *searchBarLeftView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    venueIdToImage = [[NSMutableDictionary alloc] init];
    
    userInfo = [UserInfo getInstance];
    venueClient = [VenueHttpClient getInstance];
    trendingDataCache = [TrendingDataCache getInstance];
    
    [self setupSearchBar];
    [self setupTableView];
    [self setupLocationServices];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)setupSearchBar
{
    trendingSearchbar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    trendingSearchbar.searchBarStyle = UISearchBarStyleProminent;
    trendingSearchbar.delegate = self;
    trendingSearchbar.placeholder = @"Search for Venues";
    trendingSearchbar.tintColor = [UIColor darkGrayColor];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    for (UIView *subview in [[trendingSearchbar.subviews objectAtIndex:0] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            searchBarTxtField = (UITextField *)subview;
            searchBarLeftView = searchBarTxtField.leftView;
        }
    }
}

-(void)setupTableView
{
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView setBackgroundColor:[IgnightUtil backgroundGray]];
    
    refreshMe = [[UIRefreshControl alloc] init];
    [refreshMe setTintColor:[IgnightUtil backgroundBlue]];
    [refreshMe addTarget:[self parentViewController] action:@selector(refreshTable:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshMe;
    [self.tableView setTableHeaderView:trendingSearchbar];
    
    footer = [[UIButton alloc] init];
    footer.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 40);
    footer.backgroundColor = [IgnightUtil backgroundGray];
    [footer setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    footer.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    footer.titleLabel.textAlignment = NSTextAlignmentCenter;
}

-(void)setupLocationServices
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            [locationManager requestWhenInUseAuthorization];
        }
    }
    

    [locationManager startUpdatingLocation];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    DDLogDebug(@"TrendingList: viewWillAppear : %@", _groupMode ? @"GROUP" : @"CITY");
    [locationManager startUpdatingLocation];
    venueClient.delegate = self;
    if (_mainTrending.inSearchMode) {
        [self showKeyboard];
    } else if (_groupMode) {
        [_mainTrending askForTrendingData];
    }
    [self adjustFooterView];
    self.navigationController.navigationBar.topItem.title = @"";
    
    [self.tableView reloadData];
}

-(void)adjustFooterView
{
    if ([_mainTrending inSearchMode]) {
        if ([trendingDataCache count] < [_mainTrending maxS]) {
            [self showAddVenue];
        } else {
            [self showLoadMore];
        }
    } else {
        if ([trendingDataCache count] < [_mainTrending maxT]) {
            [self noMoreResults];
        } else {
            [self showLoadMore];
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [locationManager stopUpdatingLocation];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.navigationController.navigationBar.topItem.title = @"";
}


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [trendingSearchbar resignFirstResponder];
}

-(void)showKeyboard
{
    [trendingSearchbar becomeFirstResponder];
}

-(void)dropKeyboard
{
    [trendingSearchbar resignFirstResponder];
}

-(void)clearSearchBar
{
    trendingSearchbar.text = @"";
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    [self adjustFooterView];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.tableView == tableView) {
        if (self.groupMode)
            return [trendingDataCache countForGroup];
        return [trendingDataCache count];
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (self.tableView == tableView) {
        cell = [self tableView:tableView tableViewCell:cell setupTrendingCellForVenueInRow:indexPath.row];
    }
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView tableViewCell:(UITableViewCell *)cell setupTrendingCellForVenueInRow: (NSInteger)row
{
    cell = [tableView dequeueReusableCellWithIdentifier:@"trendingCell"];
    if (cell == nil) {
        cell = [[TrendingCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"trendingCell"];
    }
    
    NSDictionary *venueData;
    if (_groupMode) {
        venueData = [trendingDataCache getGroupTrendingDataForVenueAtIndex:row];
    } else {
        venueData = [trendingDataCache getTrendingDataForVenueAtIndex:row];
    }
    
    NSNumber *userBarColor = venueData[@"userBarColor"];
    NSNumber *venueId = [venueData objectForKey:@"venueId"];
    NSNumber *userVenueValue = [venueData objectForKey:@"userVenueValue"];
    
    NSNumber *lat = [venueData objectForKey:@"latitude"];
    NSNumber *lon = [venueData objectForKey:@"longitude"];
    NSString *venueName = [venueData objectForKey:@"venueName"];
    
    CLLocation *venLoc = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
    CLLocationDistance dist = [userInfo.userLocation distanceFromLocation
                            :venLoc];

    NSInteger userInput = [trendingDataCache getUserInputForVenue:venueId];
    
    if (self.groupMode) {
        CityVenueTrendingData *cellData = [[CityVenueTrendingData alloc] initWithData:venueData];
        
        CityTrendingTableViewCell* cell = (CityTrendingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CityTrendingCell"];
        if (cell == nil) {
            cell = [[CityTrendingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CityTrendingCell"];
        }
        cell.groupMode = self.groupMode;
        cell.delegate = self;
        cellData.venueImage = [venueIdToImage objectForKey:cellData.venueId];
        cellData.userInput = (UserInputMode)userInput;
        
        if (cellData.venueImage == nil && cellData.venueImageUrl != nil) {
            NSURLRequest *request = [NSURLRequest requestWithURL: [NSURL URLWithString: cellData.venueImageUrl]];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            operation.responseSerializer = [AFImageResponseSerializer serializer];
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                cellData.venueImage = responseObject;
                [venueIdToImage setObject:responseObject forKey:cellData.venueId];
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DDLogDebug(@"Failed To Donwload Image From: %@", cellData.venueImageUrl);
            }];
            [operation start];
        }
        [cell resetWithData:cellData];
        return cell;
    } else {
        CityVenueTrendingData *cellData = [[CityVenueTrendingData alloc] initWithData:venueData];
        CityTrendingTableViewCell* cell = (CityTrendingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CityTrendingCell"];
        if (cell == nil) {
            cell = [[CityTrendingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CityTrendingCell"];
        }
        cell.delegate = self;
        cellData.venueImage = [venueIdToImage objectForKey:cellData.venueId];
        cellData.userInput = (UserInputMode)userInput;
        
        if (cellData.venueImage == nil && cellData.venueImageUrl != nil) {
            NSURLRequest *request = [NSURLRequest requestWithURL: [NSURL URLWithString: cellData.venueImageUrl]];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            operation.responseSerializer = [AFImageResponseSerializer serializer];
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                cellData.venueImage = responseObject;
                [venueIdToImage setObject:responseObject forKey:cellData.venueId];
                [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DDLogDebug(@"Failed To Donwload Image From: %@", cellData.venueImageUrl);
            }];
            [operation start];
        }
        [cell resetWithData:cellData];
        return cell;
    }
}

#pragma mark - CityTrendingTableViewCellDelegate

-(void)cityTrendingCell:(CityTrendingTableViewCell *)cell updateUserInputMode:(UserInputMode)mode
{
    [venueClient userActivityChangedTo:mode forVenueId: [cell getData].venueId];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView) {
        return CITY_TRENDING_CELL_HEIGHT;
    }
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tableView == tableView) {
        NSDictionary *venInfo;
        if (_groupMode) {
            venInfo = [trendingDataCache getGroupTrendingDataForVenueAtIndex:indexPath.row];
        } else {
            venInfo = [trendingDataCache getTrendingDataForVenueAtIndex:indexPath.row];
        }
        [self goToVenueDetaislForVenueId:[venInfo objectForKey:@"venueId"]];
    }
}

- (void)goToVenueDetaislForVenueId: (NSNumber *)venueId
{
    NSDictionary *trendingData = [trendingDataCache getVenueTrendingDataForVenue:venueId];
    VenueViewController *venueInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"VenueInfo"];
    venueInfo.goBack = YES;
    venueInfo.venueId = venueId;
    venueInfo.userBarColor = trendingData[@"userBarColor"];
    [self.navigationController pushViewController:venueInfo animated:YES];
}

#pragma mark - TrendingCellDelegate

-(void)userActivityChangedTo:(NSInteger)userActivity forVenueId:(NSNumber *)venueId atIndexPath:(NSIndexPath *)indexPath
{
    NSInteger curUserActivity = [trendingDataCache getUserInputForVenue:venueId];
    if (curUserActivity != userActivity) {
        [trendingDataCache setNewUserInput:userActivity ForVenue:venueId];
        [venueClient userActivityChangedTo:userActivity forVenueId:venueId];
    }
}

#pragma mark - VenueHttpClientDelegate

-(void)venueHttpClient:(VenueHttpClient *)client didFailWithError:(NSError *)error
{
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)venueHttpClient:(VenueHttpClient *)client userActivityChangedTo:(NSInteger)activity forVenueId:(NSNumber *)venueId withResponse:(NSDictionary *)response
{
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        if (self.groupMode) {
            if (! [trendingSearchbar isFirstResponder] && ![_mainTrending inSearchMode]) {
                [_mainTrending askForTrendingData];
            }
        }
        [trendingDataCache setNewUserInput:activity ForVenue:venueId];
        [self.tableView reloadData];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

#pragma mark - SearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar                      // return NO to not become first responder
{
    searchBar.showsCancelButton = YES;
    [_mainTrending enteredSearchMode];
    return YES;
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar                        // return NO to not resign first responder
{
    if ([searchBar.text isEqualToString:@""]) {
        [_mainTrending cancelButtonPressed];
    }
    searchBar.showsCancelButton = NO;
    return YES;
}

-(void)doSearch
{
    [_mainTrending searchVenuesWithString:_searchString];
}

-(void)showLoadingInSearch
{
    DDLogDebug(@"SHOW LOADING");
    searchBarTxtField.leftView = spinner;
    [spinner startAnimating];
}

-(void)stopLoadingInSearch
{
    DDLogDebug(@"HIDE LOADING");
    searchBarTxtField.leftView = searchBarLeftView;
    [spinner stopAnimating];
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0) // called before text changes
{
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar                     // called when keyboard search button pressed
{
    _searchString = searchBar.text;
    [_mainTrending searchVenuesWithString:_searchString];
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar                    // called when cancel button pressed
{
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [_mainTrending exitSearchModeAndAskForTrending:YES];
//    [_mainTrending cancelButtonPressed];
}

-(void)showLoadMore
{
    if (_groupMode) {
        return;
    }
        
    [footer setTitle:@"Load More" forState:UIControlStateNormal];
    [footer removeTarget:_mainTrending action:@selector(addVenue) forControlEvents:UIControlEventTouchUpInside];
    [footer removeTarget:_mainTrending action:@selector(loadMore) forControlEvents:UIControlEventTouchUpInside];
    [footer addTarget:_mainTrending action:@selector(loadMore) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView setTableFooterView:footer];
}

-(void)showAddVenue
{
    [footer setTitle:@"Add a Venue" forState:UIControlStateNormal];
    [footer removeTarget:_mainTrending action:@selector(loadMore) forControlEvents:UIControlEventTouchUpInside];
    [footer removeTarget:_mainTrending action:@selector(addVenue) forControlEvents:UIControlEventTouchUpInside];
    [footer addTarget:_mainTrending action:@selector(addVenue) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView setTableFooterView:footer];
}

-(void)noMoreResults
{
    NSString *text = nil;
    NSInteger trendingVenueCount = [trendingDataCache count];
    if (_groupMode) {
        if (trendingVenueCount <= 0) {
            text = [NSString stringWithFormat:@"%@%@",@"No voting activity in ", _groupName];
        }
    } else {
        if (userInfo.musicFilterId.integerValue >= 0) {
            if (trendingVenueCount <= 0) {
                NSString *name = [MusicTypes getMusicNameFromId:((Music)userInfo.musicFilterId.integerValue)];
                text = [NSString stringWithFormat:@"No %@ venues trending yet today.", name];
            }
        } else if (userInfo.activityFilterId.integerValue >= 0) {
            if (trendingVenueCount <= 0) {
                text = @"You haven't upvoted anything yet today.";
            }
        }
    }
    
    if (text == nil) {
        text = @"No More Results";
    }
    
    [footer setTitle:text forState:UIControlStateNormal];
    [footer removeTarget:_mainTrending action:@selector(loadMore) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView setTableFooterView:footer];
}

-(void)hideFooter
{
    [self.tableView setTableFooterView:nil];
}

#pragma mark - CoreLocation

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    if (userInfo.userLocation == nil) {
        userInfo.userLocation = newLocation;
        return;
    }
    
    CLLocationDistance meters = [newLocation distanceFromLocation:oldLocation];
    if (meters > 0.1) {
        userInfo.userLocation = newLocation;
        [self.tableView reloadData];
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DDLogDebug(@"Fialed to Get Location In TrendingListView");
}

#pragma mark - Util

- (double)convertToMilesFromMeters: (double)distance
{
    return (distance / 1609.344);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
