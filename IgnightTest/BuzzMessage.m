//
//  BuzzMessage.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 11/19/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "BuzzMessage.h"

static NSString *const BuzzTypeId = @"buzzType";
static NSString *const BuzzImageKey = @"buzzText";
static NSString *const BuzzId = @"buzzId";
static NSString *const BuzzTextKey = @"buzzText";
static NSString *const SenderName = @"userName";
static NSString *const DateKey = @"buzzDateStamp";
static NSString *const UserImage = @"chatPictureURL";

@implementation BuzzMessage

-(id)initWithMyMessage: (NSDictionary *)msg
{
    self = [super init];
    if (self) {
        [self setAllFields:msg];
        _myMessage = YES;
    }
    return self;
}

-(id)initWithOtherMessage: (NSDictionary *)msg
{
    self = [super init];
    if (self) {
        [self setAllFields:msg];
        _myMessage = NO;
    }
    return self;
}

-(void)setAllFields: (NSDictionary *)msg
{
    NSString *type = [msg objectForKey:BuzzTypeId];
    if ([type isEqualToString:@"IMAGE"]) {
        _buzzType = [NSNumber numberWithInteger:1];
    } else {
        _buzzType = [NSNumber numberWithInteger:0];
    }
    
    if ([_buzzType intValue] == 0) {
        _message = [msg objectForKey:BuzzTextKey];
    } else if ([_buzzType intValue] == 1) {
        NSString *url = [msg objectForKey:BuzzImageKey];
        url = [url stringByReplacingOccurrencesOfString:@"https" withString:@"http"];
        _imageUrl = [NSURL URLWithString:url];
        _image = [UIImage imageNamed:@"default_image"];
    }
    _buzzId = [msg objectForKey:BuzzId];
    _senderName = [msg objectForKey:SenderName];
    
    NSString *imageUrl = [msg objectForKey:UserImage];
    NSString *url = [imageUrl stringByReplacingOccurrencesOfString:@"https" withString:@"http"];
    _userImageUrl = [NSURL URLWithString:url];
    
    [self setTimeStamp:[msg objectForKey:DateKey]];
}

-(void)setTimeStamp: (NSNumber *)milliTime
{
    NSTimeInterval interval = [milliTime doubleValue];
    _date = [[NSDate alloc] initWithTimeIntervalSince1970:interval / 1000];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"msg: %@, size: (%f, %f) ", _message, _bubbleSize.width, _bubbleSize.height];
}

@end
