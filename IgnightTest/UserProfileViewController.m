//
//  UserProfileViewController.m
//  IgnightTest
//
//  Created by Rob Chipman on 8/20/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "UserProfileViewController.h"
#import "RankToSelection.h"
#import "ImageSelectionController.h"

#define PROFILE_HEADER_HEIGHT 130
#define PROFILE_SECTION_HEIGHT 40
#define PROFILE_LOCATION_HEIGHT 50

#define PROFILE_MUSIC_HEIGHT 128
#define PROFILE_MUSIC_EDIT_HEIGHT (PROFILE_MUSIC_HEIGHT * 3)

#define PROFILE_ATMOSPHERE_HEIGHT 128
#define PROFILE_ATMOSPHERE_EDIT_HEIGHT (PROFILE_ATMOSPHERE_HEIGHT * 3)

#define PROFILE_SPENDING_HEIGHT 128
#define PROFILE_SPENDING_EDIT_HEIGHT (400)

#define PROFILE_AGE_HEIGHT 50
#define PROFILE_AGE_EDIT_HEIGHT (PROFILE_AGE_HEIGHT * 3)

#define PROFILE_EMAIL_HEIGHT 50

@implementation UserProfileViewController
{
    UserInfo *userInfo;
    UserHttpClient *userClient;
    
    UITableView *locationTable;
    BOOL editLocation;
    
    RankToSelection *musicRank;
    UICollectionView *musicCollectionView;
    BOOL editMusic;
    
    RankToSelection *atmosphereRank;
    UICollectionView *atmosphereCollectionView;
    BOOL editAtmosphere;
    
    UICollectionView *spendingCollectionView;
    BOOL editSpending;
    
    UICollectionView *ageCollectionView;
    UITableView *ageTable;
    BOOL editAge;
    
    UITextField *email;
    BOOL editEmail;
    
    BOOL notifyGroupInvites;
    
    NSInteger currentCity;
    NSInteger currentSpending;
    NSInteger currentAge;
    NSString *currentEmail;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupPrivateVariables];
    [self setupTitleBar];
    [self setupTableview];
    [self registerForKeyboardNotifications];
    [self setUpSideBarButton];
    [self loadUserImage];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    userClient.delegate = self;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self setupPushNotificationObserver];
    //Because of invite contacts nav bar
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removePushNotificationObserver];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft) {    //Hide
        userClient.delegate = self;
    }
}

- (void)revealController:(SWRevealViewController *)revealController animateToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft) {
        userClient.delegate = self;
    }
}

-(void)loadUserImage
{
    DDLogDebug(@"Asked To Load User Profile Image: %@", userInfo.profilePictureUrl);
    if (userInfo.profilePictureUrl != nil && ![userInfo.profilePictureUrl isEqualToString:@""]) {
        NSURL *url = [NSURL URLWithString:userInfo.profilePictureUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFImageResponseSerializer serializer];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            userInfo.profilePictureImage = responseObject;
            [_tableView reloadData];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DDLogDebug(@"Failed To Retrieve Image : %@ with error: %@", userInfo.profilePictureUrl, error);
        }];
        [operation start];
    }
}

- (void)setUpSideBarButton
{
    self.revealViewController.delegate = self;
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

-(void)setupPrivateVariables
{
    userInfo = [UserInfo getInstance];
    userClient = [UserHttpClient getInstance];
    editLocation = NO;
    currentCity = userInfo.cityId.integerValue;
    editMusic = NO;
    musicRank = [[RankToSelection alloc] initWithMaxSelections:3 minSelections:1];
    editAtmosphere = NO;
    atmosphereRank = [[RankToSelection alloc] initWithMaxSelections:3 minSelections:1];
    currentSpending = userInfo.spendingLimit.integerValue;
    editSpending = NO;
    currentAge = userInfo.ageBucketId.integerValue;
    editAge = NO;
    currentEmail = userInfo.email;
    editEmail = NO;
}

-(void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSTimeInterval animationDuration;
    NSDictionary *uInfo = [notification userInfo];
    
    [[uInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    CGPoint kbPointBeg = [[uInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].origin;
    CGPoint kbPointEnd = [[uInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    CGFloat adjustment = kbPointEnd.y - kbPointBeg.y;
    CGRect frame = self.view.frame;
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = CGRectMake(frame.origin.x, frame.origin.y + adjustment, frame.size.width, frame.size.height);
    }];
}

-(void)setupTitleBar
{
    IgnightTopBar *topBar = [[IgnightTopBar alloc] initWithTitle:@"Settings" withSideBarButtonTarget:self.revealViewController];
    [self.titleBar addSubview:topBar];
    self.titleBar.userInteractionEnabled = YES;
}

-(void)setupTableview
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView) {
        if (indexPath.section == 0) {
            return PROFILE_HEADER_HEIGHT;
        } else {
            if (indexPath.row == 0)
                return PROFILE_SECTION_HEIGHT;
            
            if (indexPath.section == 1) {
                if (editLocation)
                    return MIN(3, CITY_COUNT) * PROFILE_LOCATION_HEIGHT;
                return PROFILE_LOCATION_HEIGHT;
            } else if (indexPath.section == 2) {
                if (editMusic)
                    return PROFILE_MUSIC_EDIT_HEIGHT;
                return PROFILE_MUSIC_HEIGHT;
            } else if (indexPath.section == 3) {
                if (editAtmosphere)
                    return PROFILE_ATMOSPHERE_EDIT_HEIGHT;
                return PROFILE_ATMOSPHERE_HEIGHT;
            } else if (indexPath.section == 4) {
                if (editSpending)
                    return PROFILE_SPENDING_EDIT_HEIGHT;
                return PROFILE_SPENDING_HEIGHT;
            } else if (indexPath.section == 5) {
                if (editAge)
                    return PROFILE_AGE_EDIT_HEIGHT;
                return PROFILE_AGE_HEIGHT;
            } else if (indexPath.section == 6) {
                return PROFILE_EMAIL_HEIGHT;
            } else if (indexPath.section == 7) {
                return PROFILE_EMAIL_HEIGHT;
            }
        }
    } else if (tableView == locationTable) {
        return PROFILE_LOCATION_HEIGHT;
    } else if (tableView == ageTable) {
        return PROFILE_AGE_HEIGHT;
    }
    return 0;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == locationTable) {
        currentCity = indexPath.row;
        [locationTable reloadData];
    } else if (tableView == ageTable) {
        currentAge = indexPath.row;
        [ageTable reloadData];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.tableView) {
        return 8;
    } else if (tableView == locationTable) {
        return 1;
    } else if (tableView == ageTable) {
        return 1;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView) {
        if (section == 0)   //Profile Header
            return 1;
        
        if (section == 1)   //Profile - Location
            return 2;
        
        if (section == 2)   //Profile - Music
            return 2;
        
        if (section == 3)   //Profile - Atmosphere
            return 2;
        
        if (section == 4) //Profile - Spending
            return 2;
        
        if (section == 5)  //Profile - Age
            return 2;
        
        if (section == 6)   //Profile - Email
            return 2;
        
        if (section == 7)  //Profile - Preferences
            return 2;
    } else if (tableView == locationTable) {
        return CITY_COUNT;
    } else if (tableView == ageTable) {
        return AGE_BUCKETS_COUNT;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (tableView == self.tableView) {
        if (indexPath.section == 0) {
            cell = [self getProfileHeaderFromTableView:tableView];
        } else {
            if (indexPath.row == 0) {
                cell = [self getHeaderFromTableView:tableView forSection:indexPath.section];
            } else if (indexPath.section == 1) {
                cell = [self getLocationCellFromTableView:tableView];
            } else if (indexPath.section == 2) {
                cell = [self getMusicCellFromTableView:tableView];
            } else if (indexPath.section == 3) {
                cell = [self getAtmosphereCellFromTableView:tableView];
            } else if (indexPath.section == 4) {
                cell = [self getSpendingCellFromTableView: tableView];
            } else if (indexPath.section == 5) {
                cell = [self getAgeCellFromTableView: tableView];
            } else if (indexPath.section == 6) {
                cell = [self getEmailCellFromTableView: tableView];
            } else if (indexPath.section == 7) {
                cell = [self getPreferencesCellFromTableView:tableView];
            }
        }
    } else if (tableView == locationTable) {
        cell = [self getCityTableCellFromTableView:tableView atIndex:indexPath.row];
    } else if (tableView == ageTable) {
        cell = [self getAgeTableCellFromTableView:tableView atIndex:indexPath.row];
    }
    
    return cell;
}

-(UITableViewCell *)getAgeTableCellFromTableView: (UITableView *)tableView atIndex: (NSInteger) index
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"age"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"age"];
        cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        UILabel *age = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 320, PROFILE_AGE_HEIGHT - (2 * 15))];
        age.tag = 100;
        [cell.contentView addSubview:age];
    }

    UILabel *age = (UILabel *)[cell viewWithTag:100];
    age.text = [AgeBuckets getAgeBucketNameFromId:(AgeBucket)index];

    if (currentAge == index)
        age.textColor = venueListText;
    else
        age.textColor = [UIColor lightGrayColor];

    return cell;
}

-(UITableViewCell *)getAgeCellFromTableView: (UITableView *)tableView
{
    UITableViewCell *cell;
    if (editAge) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"editAge"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"editAge"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            ageTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, PROFILE_AGE_EDIT_HEIGHT)];
            ageTable.delegate = self;
            ageTable.dataSource = self;
            [ageTable setSeparatorInset:UIEdgeInsetsZero];
            [cell.contentView addSubview:ageTable];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"age"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"age"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UILabel *age = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 320, PROFILE_AGE_HEIGHT - (2 * 15))];
            age.textColor = venueListText;
            age.tag = 100;
            [cell.contentView addSubview:age];
        }

        UILabel *age = (UILabel *)[cell viewWithTag:100];
        age.text = [AgeBuckets getAgeBucketNameFromId:(AgeBucket)[userInfo.ageBucketId integerValue]];
    }
    return cell;
}

-(UITableViewCell *)getPreferencesCellFromTableView: (UITableView *)tableView
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"editPreferences"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"editPreferences"];
        cell.backgroundColor = venueListBackgrond;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *preference = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 220, PROFILE_EMAIL_HEIGHT - (2 * 15))];
        preference.tag = 100;
        preference.textColor = venueListText;
        [cell.contentView addSubview:preference];
        
        UISwitch *switchPreference = [[UISwitch alloc] init];
        switchPreference.tag = 101;
        switchPreference.on = userInfo.notifyOfGroupInvites;
        [switchPreference addTarget:self action:@selector(notifyOfInvites:) forControlEvents:UIControlEventValueChanged];
        [cell setAccessoryView:switchPreference];
    }
    
    UILabel *preference = (UILabel *)[cell viewWithTag:100];
    preference.text = @"Notify me of group invites:";
    
    UISwitch *switchPreference = (UISwitch *)[cell viewWithTag:101];
    switchPreference.on = userInfo.notifyOfGroupInvites;
    [switchPreference reloadInputViews];
    return cell;
}

-(void)notifyOfInvites: (id)sender
{
    UISwitch *switchPreference = (UISwitch *)sender;
    notifyGroupInvites = switchPreference.on;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [userClient updateGroupInvitesPreference:notifyGroupInvites];
}

-(UITableViewCell *)getEmailCellFromTableView: (UITableView *) tableView
{
    UITableViewCell *cell;
    if (editEmail) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"editEmail"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"editMail"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            email = [[UITextField alloc] initWithFrame:CGRectMake(15, 5, 295, PROFILE_EMAIL_HEIGHT - (2 * 5))];
            email.backgroundColor = venueListBackgrond;
            email.delegate = self;
            email.returnKeyType = UIReturnKeyDone;
            email.textColor = venueListText;
            email.tag = 100;
            [cell.contentView addSubview:email];
            [email becomeFirstResponder];
        }
        
        UITextField *userEmail = (UITextField *)[cell viewWithTag:100];
        userEmail.text = userInfo.email;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"email"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"email"];
            cell.backgroundColor = venueListBackgrond;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UILabel *userEmail = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 320, PROFILE_EMAIL_HEIGHT - (2 * 15))];
            userEmail.tag = 100;
            userEmail.textColor = venueListText;
            [cell.contentView addSubview:userEmail];
        }
        
        UILabel *userEmail = (UILabel *)[cell viewWithTag:100];
        userEmail.text = userInfo.email;
    }
    return cell;
}

-(UITableViewCell *)getAtmosphereCellFromTableView: (UITableView *)tableView
{
    UITableViewCell *cell;
    if (editAtmosphere) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"editAtmosphere"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"editAtmosphere"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
            [layout setItemSize:CGSizeMake(106, 128)];
            [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
            [layout setMinimumInteritemSpacing:0];
            [layout setSectionInset:UIEdgeInsetsZero];
            
            UINib *cellNib = [UINib nibWithNibName:@"profileDnaOption" bundle:nil];
            atmosphereCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, PROFILE_ATMOSPHERE_EDIT_HEIGHT) collectionViewLayout:layout];
            atmosphereCollectionView.delegate = self;
            atmosphereCollectionView.dataSource = self;
            atmosphereCollectionView.backgroundColor = trendingToggleUnselectedColor;
            [atmosphereCollectionView setAllowsMultipleSelection:YES];
            [atmosphereCollectionView setContentSize:CGSizeMake(cell.contentView.frame.size.width,
                                                                PROFILE_ATMOSPHERE_HEIGHT * 4)];
            [atmosphereCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"profileDnaOption"];
            
            [cell.contentView addSubview:atmosphereCollectionView];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"atmosphere"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"atmosphere"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIImageView *first = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 91, 91)];
            first.tag = 100;
            [cell.contentView addSubview:first];
            UILabel *firstName = [[UILabel alloc] initWithFrame:CGRectMake(0, 107, 107, 21)];
            firstName.numberOfLines = 2;
            [firstName setFont:[UIFont systemFontOfSize:14]];
            firstName.textAlignment = NSTextAlignmentCenter;
            firstName.textColor = venueListText;
            firstName.tag = 101;
            [cell.contentView addSubview:firstName];
            
            CGRect frame = CGRectMake(115, 8, 91, 91);
            
            UIImageView *second = [[UIImageView alloc] initWithFrame:frame];
            second.tag = 102;
            [cell.contentView addSubview:second];
            UILabel *secondName = [[UILabel alloc] initWithFrame:CGRectMake(107, 107, 107, 21)];
            secondName.textAlignment = NSTextAlignmentCenter;
            secondName.tag = 103;
            secondName.textColor = venueListText;
            secondName.numberOfLines = 2;
            secondName.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:secondName];
            
            UIImageView *third = [[UIImageView alloc] initWithFrame:CGRectMake(222, 8, 91, 91)];
            third.tag = 104;
            [cell.contentView addSubview:third];
            UILabel *thirdName = [[UILabel alloc] initWithFrame:CGRectMake(214, 107, 107, 21)];
            thirdName.textAlignment = NSTextAlignmentCenter;
            thirdName.tag = 105;
            thirdName.textColor = venueListText;
            thirdName.numberOfLines = 2;
            thirdName.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:thirdName];
        }
        
        int count = 0;
        for (NSNumber *atmosphereId in userInfo.atmospheres) {
            UIImageView *iView;
            UILabel *label;
            
            if (count == 0) {
                iView = (UIImageView *)[cell viewWithTag:100];
                label = (UILabel *)[cell viewWithTag:101];
            } else if (count == 1) {
                iView = (UIImageView *)[cell viewWithTag:102];
                label = (UILabel *)[cell viewWithTag:103];
            } else if (count == 2) {
                iView = (UIImageView *)[cell viewWithTag:104];
                label = (UILabel *)[cell viewWithTag:105];
            }
            
            if ([atmosphereId intValue] < 0) {
                iView.image = nil;
                label.text = nil;
            } else {
                NSString *imageName = [[[AtmosphereTypes getAtmosphereNameFromId:(Atmosphere)[atmosphereId intValue]] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
                iView.image = [UIImage imageNamed:imageName];
                [[iView layer] setCornerRadius:iView.frame.size.width/2];
                [[iView layer] setMasksToBounds:YES];
                label.text = [AtmosphereTypes getAtmosphereNameFromId:(Atmosphere)[atmosphereId integerValue]];
            }
            
            count++;
        }
    }
    return cell;
}

-(UITableViewCell *)getMusicCellFromTableView: (UITableView *)tableView
{
    UITableViewCell *cell;
    if (editMusic) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"editMusic"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"editMusic"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
            [layout setItemSize:CGSizeMake(106, 128)];
            [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
            [layout setMinimumInteritemSpacing:0];
            [layout setSectionInset:UIEdgeInsetsZero];
            
            UINib *cellNib = [UINib nibWithNibName:@"profileDnaOption" bundle:nil];
            musicCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, PROFILE_MUSIC_EDIT_HEIGHT) collectionViewLayout:layout];
            musicCollectionView.delegate = self;
            musicCollectionView.dataSource = self;
            musicCollectionView.backgroundColor = trendingToggleUnselectedColor;
            [musicCollectionView setAllowsMultipleSelection:YES];
            [musicCollectionView setContentSize:CGSizeMake(cell.contentView.frame.size.width, PROFILE_MUSIC_HEIGHT * 5)];
            [musicCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"profileDnaOption"];
            [cell.contentView addSubview:musicCollectionView];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"music"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"music"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIImageView *first = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 91, 91)];
            first.tag = 100;
            [cell.contentView addSubview:first];
            UILabel *firstName = [[UILabel alloc] initWithFrame:CGRectMake(0, 107, 107, 21)];
            firstName.textAlignment = NSTextAlignmentCenter;
            firstName.tag = 101;
            firstName.textColor = venueListText;
            firstName.numberOfLines = 2;
            [firstName setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:firstName];
            
            CGRect frame = CGRectMake(115, 8, 91, 91);
            UIImageView *second = [[UIImageView alloc] initWithFrame:frame];
            second.tag = 102;
            [cell.contentView addSubview:second];
            UILabel *secondName = [[UILabel alloc] initWithFrame:CGRectMake(107, 107, 107, 21)];
            secondName.textAlignment = NSTextAlignmentCenter;
            secondName.tag = 103;
            secondName.textColor = venueListText;
            secondName.numberOfLines = 2;
            [secondName setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:secondName];
            
            UIImageView *third = [[UIImageView alloc] initWithFrame:CGRectMake(222, 8, 91, 91)];
            third.tag = 104;
            [cell.contentView addSubview:third];
            UILabel *thirdName = [[UILabel alloc] initWithFrame:CGRectMake(214, 107, 107, 21)];
            thirdName.textAlignment = NSTextAlignmentCenter;
            thirdName.tag = 105;
            thirdName.textColor = venueListText;
            thirdName.numberOfLines = 2;
            thirdName.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:thirdName];
        }
        
        int count = 0;
        for (NSNumber *musicId in userInfo.music) {
            UIImageView *iView;
            UILabel *label;
            
            if (count == 0) {
                iView = (UIImageView *)[cell viewWithTag:100];
                label = (UILabel *)[cell viewWithTag:101];
            } else if (count == 1) {
                iView = (UIImageView *)[cell viewWithTag:102];
                label = (UILabel *)[cell viewWithTag:103];
            } else if (count == 2) {
                iView = (UIImageView *)[cell viewWithTag:104];
                label = (UILabel *)[cell viewWithTag:105];
            }
            if ([musicId intValue] < 0){
                iView.image = nil;
                label.text = nil;
            } else {
                NSString *imageName = [[[MusicTypes getMusicNameFromId:(Music)[musicId intValue]] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
                iView.image = [UIImage imageNamed:imageName];
                [[iView layer] setCornerRadius:iView.frame.size.width/2];
                [[iView layer] setMasksToBounds:YES];
                label.text = [MusicTypes getMusicNameFromId:(Music)[musicId integerValue]];
            }
            count++;
        }
    }
    return cell;
}



-(UITableViewCell *)getSpendingCellFromTableView: (UITableView *)tableView
{
    UITableViewCell *cell;
    if (editSpending) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"editSpending"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"editSpending"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
            [layout setItemSize:CGSizeMake(160, 181)];
            [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
            [layout setMinimumInteritemSpacing:0];
            [layout setSectionInset:UIEdgeInsetsZero];
            
            UINib *cellNib = [UINib nibWithNibName:@"profileDnaOption2" bundle:nil];
            spendingCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, PROFILE_SPENDING_EDIT_HEIGHT) collectionViewLayout:layout];
            spendingCollectionView.delegate = self;
            spendingCollectionView.dataSource = self;
            spendingCollectionView.backgroundColor = trendingToggleUnselectedColor;
            [spendingCollectionView setAllowsMultipleSelection:YES];
            [spendingCollectionView setContentSize:CGSizeMake(cell.contentView.frame.size.width, PROFILE_MUSIC_HEIGHT * 3)];
            [spendingCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"profileDnaOption2"];
            [cell.contentView addSubview:spendingCollectionView];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"spending"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"spending"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIImageView *first = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 91, 91)];
            first.tag = 100;
            [cell.contentView addSubview:first];
            UILabel *firstName = [[UILabel alloc] initWithFrame:CGRectMake(0, 107, 107, 21)];
            firstName.textAlignment = NSTextAlignmentCenter;
            firstName.tag = 101;
            firstName.textColor = venueListText;
            firstName.numberOfLines = 2;
            firstName.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:firstName];
        }
        
        UIImageView *iView = (UIImageView *)[cell viewWithTag:100];
        NSString *imgName = [[[SpendingTypes getSpendingNameFromId:(Spending)[userInfo.spendingLimit integerValue]] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        iView.image = [UIImage imageNamed:imgName];
        [[iView layer] setCornerRadius:iView.frame.size.width/2];
        [[iView layer] setMasksToBounds:YES];
    
        UILabel *label = (UILabel *)[cell viewWithTag:101];
        label.text = [SpendingTypes getSpendingNameFromId:(Spending)[userInfo.spendingLimit integerValue]];
    }
    return cell;
}

-(UITableViewCell *)getCityTableCellFromTableView: (UITableView *)tableView atIndex: (NSInteger )index
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"location"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"location"];
        cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        UILabel *city = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 320, PROFILE_LOCATION_HEIGHT - (2 * 15))];
        city.tag = 100;
        [cell.contentView addSubview:city];
    }
    
    UILabel *city = (UILabel *)[cell viewWithTag:100];
    city.text = [Cities getCityNameFromId:(City)index];
    
    if (currentCity == index)
        city.textColor = [UIColor blackColor];
    else
        city.textColor = [UIColor lightGrayColor];
    
    return cell;
}

-(UITableViewCell *)getLocationCellFromTableView: (UITableView *)tableView
{
    UITableViewCell *cell;
    if (editLocation) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"editLocation"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"editLocation"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            locationTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, PROFILE_LOCATION_HEIGHT * 3)];
            locationTable.delegate = self;
            locationTable.dataSource = self;
            [cell.contentView addSubview:locationTable];
        }
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"location"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"location"];
            cell.backgroundColor = UIColorFromRGB(0xf2f2f2);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UILabel *city = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 320, PROFILE_LOCATION_HEIGHT - (2 * 15))];
            city.tag = 100;
            city.textColor = venueListText;
            [cell.contentView addSubview:city];
        }
        
        UILabel *city = (UILabel *)[cell viewWithTag:100];
        city.text = [Cities getCityNameFromId:(City)[userInfo.cityId integerValue]];
    }
    return cell;
}

-(UITableViewCell *)getHeaderFromTableView: (UITableView *)tableView forSection: (NSInteger)section
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sectionHeader"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"sectionHeader"];
        cell.backgroundColor = venueSectionHeaderBackground;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 305 - PROFILE_SECTION_HEIGHT, PROFILE_SECTION_HEIGHT)];
        name.tag = 100;
        name.backgroundColor = [UIColor clearColor];
        [name setFont:[UIFont systemFontOfSize:18]];
        [name setTextColor:[UIColor blackColor]];
        
        UIButton *edit = [[UIButton alloc] initWithFrame: CGRectMake(320 - PROFILE_SECTION_HEIGHT, 0, PROFILE_SECTION_HEIGHT, PROFILE_SECTION_HEIGHT)];
        edit.tag = 101;
        
        UIImage *editImage = [UIImage imageNamed:@"edit_icon"];
        UIImageView *editImgView = [[UIImageView alloc] initWithFrame:CGRectMake(320 - PROFILE_SECTION_HEIGHT + 10, 10, PROFILE_SECTION_HEIGHT - 20, PROFILE_SECTION_HEIGHT - 20)];
        editImgView.image = editImage;
        editImgView.tag = 102;

        [cell.contentView addSubview:name];
        [cell.contentView addSubview:edit];
        [cell.contentView addSubview:editImgView];
    }
    
    UILabel *name = (UILabel *)[cell viewWithTag:100];
    name.textColor = venueListText;
    UIButton *edit = (UIButton *)[cell viewWithTag:101];
    UIImageView *editImgView = (UIImageView *)[cell viewWithTag:102];
    editImgView.image = [UIImage imageNamed:@"edit_icon"];
    editImgView.alpha = 1;
    edit.alpha = 1;
    [edit setUserInteractionEnabled:YES];
    
    if (section == 1) {
        name.text = @"Location";
        [edit setUserInteractionEnabled:NO];
        edit.alpha = 0;
        [edit removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [edit addTarget:self action:@selector(editLocation:) forControlEvents:UIControlEventTouchUpInside];
        editImgView.alpha = 0;
    } else if (section == 2) {
        name.text = @"Music Preferences";
        [edit removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [edit addTarget:self action:@selector(editMusic:) forControlEvents:UIControlEventTouchUpInside];
        if (editMusic) {
            editImgView.image = [UIImage imageNamed:@"edit_icon_active"];
        }
    } else if (section == 3) {
        name.text = @"Atmosphere Preferences";
        [edit removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [edit addTarget:self action:@selector(editAtmosphere:) forControlEvents:UIControlEventTouchUpInside];
        if (editAtmosphere) {
            editImgView.image = [UIImage imageNamed:@"edit_icon_active"];
        }
    } else if (section == 4) {
        name.text = @"Spending Preference";
        [edit removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [edit addTarget:self action:@selector(editSpendingLimit:) forControlEvents:UIControlEventTouchUpInside];
        if (editSpending) {
            editImgView.image = [UIImage imageNamed:@"edit_icon_active"];
        }
    } else if (section == 5) {
        name.text = @"Age";
        [edit removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [edit addTarget:self action:@selector(editAge:) forControlEvents:UIControlEventTouchUpInside];
        if (editAge) {
            editImgView.image = [UIImage imageNamed:@"edit_icon_active"];
        }
    } else if (section == 6) {
        name.text = @"Email";
        [edit removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [edit addTarget:self action:@selector(editEmail:) forControlEvents:UIControlEventTouchUpInside];
        if (editEmail) {
            editImgView.image = [UIImage imageNamed:@"edit_icon_active"];
        }
    } else if (section == 7) {
        name.text = @"Preferences";
        [edit removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        edit.alpha = 0;
        editImgView.alpha = 0;
    }
    
    return cell;
}

-(UITableViewCell *)getProfileHeaderFromTableView: (UITableView *)tableView
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"profileHeader"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"profileHeader"];
        UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, PROFILE_HEADER_HEIGHT)];
        background.backgroundColor = [UIColor clearColor];
        background.image = [UIImage imageNamed:@"bg_user_profile"];
        background.tag = 100;
        
        UIView *profilePicView = [[UIView alloc] initWithFrame:CGRectMake(110, 5, 100, 100)];
        profilePicView.tag = 101;
        
        UIImageView *profilePic = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, profilePicView.frame.size.width, profilePicView.frame.size.height)];
        profilePic.contentMode = UIViewContentModeCenter;
        profilePic.clipsToBounds = YES;
        profilePic.tag = 102;
        profilePic.image = [UIImage imageNamed:@"group_member_default"];
        profilePic.backgroundColor = buzzUserProfileBackground;
        profilePic.contentMode = UIViewContentModeScaleAspectFill;
        [profilePic.layer setCornerRadius:50];
        [profilePic.layer setMasksToBounds:YES];
        [profilePicView addSubview:profilePic];
        
        UIButton *profilePicEdit = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, profilePicView.frame.size.width, profilePicView.frame.size.height)];
        profilePicEdit.backgroundColor = [UIColor clearColor];
        profilePicEdit.tag = 103;
        [profilePicEdit addTarget:self action:@selector(editProfilePic:) forControlEvents:UIControlEventTouchUpInside];
        [profilePicView addSubview:profilePicEdit];
        
        UILabel *userName = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 320, PROFILE_HEADER_HEIGHT - 100)];
        [userName setTextAlignment:NSTextAlignmentCenter];
        [userName setTextColor:[UIColor whiteColor]];
        userName.tag = 104;
        
        [cell.contentView addSubview:background];
        [cell.contentView addSubview:profilePicView];
        [cell.contentView addSubview:userName];
    }
    
    UILabel *userName = (UILabel *)[cell viewWithTag:104];
    userName.text = userInfo.userName;
    
    if (userInfo.profilePictureImage != nil) {
        UIImageView *profilePic = (UIImageView *)[cell viewWithTag:102];
        profilePic.image = [IgnightUtil getProfilePic:userInfo.profilePictureImage];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == musicCollectionView)
        return [musicRank isRankAvailable];
    else if (collectionView == atmosphereCollectionView)
        return [atmosphereRank isRankAvailable];
    else if (collectionView == spendingCollectionView)
        return YES;
    else if (collectionView == ageCollectionView)
        return YES;
    return NO;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [self setImageForCell:cell forRow:indexPath.row];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [self removeImageForCell:cell forRow:indexPath.row];
}

-(void)setImageForCell: (UICollectionViewCell *)cell forRow: (NSInteger)row
{
    UICollectionView *view = (UICollectionView *)[cell superview];
    NSInteger rank = 0;
    if (view == musicCollectionView) {
        rank = [musicRank addToSelection:[NSNumber numberWithInt:(Music)row]];
        if (rank > 0) {
            [self setImageForCell:cell forRow:row atRank:rank];
        } else {
            [self removeImageForCell:cell forRow:row];
        }
    } else if (view == atmosphereCollectionView) {
        rank = [atmosphereRank addToSelection:[NSNumber numberWithInt:(Atmosphere)row]];
        if (rank > 0) {
            [self setImageForCell:cell forRow:row atRank:rank];
        }
    } else  if (view == spendingCollectionView) {
        currentSpending = row;
        [self setSelectedImageForCell:cell forRow:row];
    } else if (view == ageCollectionView) {
        currentAge = row;
        [self setSelectedImageForCell:cell forRow:row];
    }
}

-(void)setImageForCell: (UICollectionViewCell *)cell forRow:(NSInteger)row atRank: (NSInteger)rank
{
    UILabel *label = (UILabel *)[cell viewWithTag:110];
    label.text = [NSString stringWithFormat:@"%ld", rank];
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:60]];
    label.alpha = 1;
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
    [[cover layer] setCornerRadius:cover.frame.size.width / 2];
    [[cover layer] setMasksToBounds:YES];
    cover.alpha = 0.4;
    
    UIImageView* imageView = (UIImageView *)[cell viewWithTag:101];
    imageView.alpha = 1;
}

-(void)setSelectedImageForCell: (UICollectionViewCell *)cell forRow: (NSInteger)row
{
    NSString *imagePath = [NSString stringWithFormat:@"white_check"];
    UIImage *rankImage = [UIImage imageNamed:imagePath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:102];
    imageView.image = rankImage;
    imageView.alpha = 1;
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
    [[cover layer] setCornerRadius:cover.frame.size.width / 2];
    [[cover layer] setMasksToBounds:YES];
    cover.alpha = 0.4;
    
    imageView = (UIImageView *)[cell viewWithTag:101];
    imageView.alpha = 1;

    UICollectionView *view = (UICollectionView *)[cell superview];
    [view reloadSections:[NSIndexSet indexSetWithIndex:0]];
}

-(void)clearReusedCellRankImageForCell:(UICollectionViewCell *)cell
{
    UIImageView *rankImage = (UIImageView *)[cell viewWithTag:102];
    rankImage.image = nil;
    rankImage.alpha = 0;
    
    UIImageView *cover =(UIImageView *)[cell viewWithTag:103];
    cover.alpha = 0;
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:101];
    imageView.alpha = 1;
}

-(void)removeImageForCell: (UICollectionViewCell*)cell forRow: (NSInteger)row
{
    UICollectionView *view = (UICollectionView *)[cell superview];
    if (view == musicCollectionView)
        [musicRank removeFromSelection:[NSNumber numberWithLong:row]];
    else if (view == atmosphereCollectionView)
        [atmosphereRank removeFromSelection:[NSNumber numberWithLong:row]];
    else if (view == ageCollectionView) {
        if (currentAge == row)
            currentAge = -1;
    } else if (view == spendingCollectionView) {
        if (currentSpending == row)
            currentSpending = -1;
    }

    UILabel *label = (UILabel *)[cell viewWithTag:110];
    label.alpha = 0;
    label.text = @"";
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
    cover.alpha = 0;
    
    UIImageView *image = (UIImageView *)[cell viewWithTag:102];
    image.image = nil;
    
    UIImageView* imageView = (UIImageView *)[cell viewWithTag:101];
    imageView.alpha = 1;
}

-(void)saveMusicOptions
{
    [userInfo resetMusic];
    for (int i = 1; i <= [musicRank count]; i++) {
        NSNumber *selectionId = [musicRank selectionAtRank:i];
        [userInfo setMusicDna:selectionId atSelection:i - 1];
    }
}

-(void)saveAtmosphereOptions
{
    [userInfo resetAtmosphere];
    for (int i = 1; i <= [atmosphereRank count]; i++) {
        NSNumber *selectionId = [atmosphereRank selectionAtRank:i];
        [userInfo setAtmosphereDna:selectionId atSelection:i -1];
    }
}

-(void)saveSpendingOptions
{
    userInfo.spendingLimit = [NSNumber numberWithInteger:currentSpending];
}

-(void)saveAgeOptions
{
    userInfo.ageBucketId = [NSNumber numberWithInteger:currentAge];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == musicCollectionView)
        return MUSIC_COUNT;
    else if (collectionView == atmosphereCollectionView)
        return ATMOSPHERE_COUNT;
    else if (collectionView == spendingCollectionView)
        return SPENDING_COUNT;
    else if (collectionView == ageCollectionView) {
        return AGE_BUCKETS_COUNT;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView == musicCollectionView) {
        for (NSNumber *music in userInfo.music) {
            if ([music integerValue] != -1)
                [musicRank addToSelection:music];
        }
    } else if (collectionView == atmosphereCollectionView) {
        for (NSNumber *atmosphere in userInfo.atmospheres) {
            if ([atmosphere integerValue] != -1)
                [atmosphereRank addToSelection:atmosphere];
        }
    }
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if (collectionView != spendingCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"profileDnaOption" forIndexPath:indexPath];
    } else {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"profileDnaOption2" forIndexPath:indexPath];
    }
    
    UILabel *name = (UILabel*)[cell viewWithTag:100];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:101];
    UILabel *rankLabel = (UILabel *)[cell viewWithTag:110];
    NSNumber *rank;
    
    if (collectionView == musicCollectionView) {
        name.text = [MusicTypes getMusicNameFromId:(Music)indexPath.row];
        NSString *imageName = [[[MusicTypes getMusicNameFromId:(Music)indexPath.row] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        imageView.image = [UIImage imageNamed:imageName];
        rank = [musicRank rankForSelection:[NSNumber numberWithLong:indexPath.row]];
    } else if (collectionView == atmosphereCollectionView) {
        name.text = [AtmosphereTypes getAtmosphereNameFromId:(Atmosphere)indexPath.row];
        NSString *imageName = [[[AtmosphereTypes getAtmosphereNameFromId:(Atmosphere)indexPath.row] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        imageView.image = [UIImage imageNamed:imageName];
        rank = [atmosphereRank rankForSelection:[NSNumber numberWithLong:indexPath.row]];
    } else if (collectionView == spendingCollectionView) {
        name.text = [SpendingTypes getSpendingNameFromId:(Spending)indexPath.row];
        NSString *imageName = [[[SpendingTypes getSpendingNameFromId:(Spending)indexPath.row] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        imageView.image = [UIImage imageNamed:imageName];
        if (indexPath.row == currentSpending)
            rank = [NSNumber numberWithInteger:1];
    } else if (collectionView == ageCollectionView) {
        name.text = [AgeBuckets getAgeBucketNameFromId:(AgeBucket)indexPath.row];
        NSString *imageName = [NSString stringWithFormat:@"dna_age_%ld", indexPath.row + 1];
        imageView.image = [UIImage imageNamed:imageName];
        if (indexPath.row == currentAge)
            rank = [NSNumber numberWithInteger:1];
    }
    
    [[imageView layer] setCornerRadius:imageView.frame.size.width/2];
    [[imageView layer] setMasksToBounds:YES];
    
    name.textAlignment = NSTextAlignmentCenter;
    name.textColor = venueListText;
    name.font = [UIFont systemFontOfSize:14];
    name.numberOfLines = 2;
    rankLabel.text = @"";
    
    if (rank != nil) {
        if (collectionView == musicCollectionView || collectionView == atmosphereCollectionView) {
            [self setImageForCell:cell forRow:indexPath.row atRank:[rank integerValue]];
        } else {
            [self setSelectedImageForCell:cell forRow:indexPath.row];
        }
        cell.selected = YES;
        cell.highlighted = YES;
        [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    } else {
        [self clearReusedCellRankImageForCell:cell];
    }
    
    return cell;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField              // called when 'return' key pressed. return NO to ignore.
{
    currentEmail = textField.text;
    [textField resignFirstResponder];
    currentEmail = email.text;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [userClient updateEmail:currentEmail];
    return YES;
}

#pragma mark - Edit Profile Pic

-(IBAction)editProfilePic:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take Photo", @"Choose Existing", nil];
    [actionSheet showInView: self.view];
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 2) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        
        if (buttonIndex == 0) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else if (buttonIndex == 1) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *newImage = [IgnightUtil scaleToFitScreen:image];
    
    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        ImageSelectionController *imageSelectionController = [self.storyboard instantiateViewControllerWithIdentifier:@"imageSelection"];
        imageSelectionController.delegate = self;
        imageSelectionController.imageSize = [IgnightUtil getSizeForImageOnScreen:image];
        imageSelectionController.image = newImage;
        self.navigationController.navigationBar.topItem.title = @"";
        [self.navigationController pushViewController:imageSelectionController animated:YES];
    } else {
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
        [self userSelectedImage:[IgnightUtil scaleUserProfileImageForUpload:image]];
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

#pragma mark - ImageSelectionControllerDelegate

-(void)userClickedCancel
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)userSelectedImage:(UIImage *)image
{
    if (image != nil) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [userClient updateProfilePicture:UIImageJPEGRepresentation(image, [IgnightProperties getImageScalingValue])];
    } else {
        DDLogError(@"Image was NULL");
    }
}

#pragma mark - Edit Location

-(IBAction)editLocation:(id)sender
{
    if (editLocation) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [userClient updateCity:[NSNumber numberWithLong:currentCity]];
    } else {
        editLocation = YES;
        [self.tableView reloadData];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        [self.tableView setScrollEnabled:NO];
    }
}

#pragma mark - Edit Music Choices

-(IBAction)editMusic:(id)sender
{
    if (editMusic) {
        if (![musicRank selectedMinSelections]) {
            IgAlert(nil, @"Please select a minimum of 1 preference.", nil);
            return;
        }
        if (![self isSameMusic]) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [userClient updateMusic:[musicRank selections]];
        } else {
            editMusic = NO;
            [self saveMusicOptions];
            [self setupTableViewOnSave];
        }
    } else {
        editMusic = YES;
        [self.tableView reloadData];
        [self.tableView setScrollEnabled:NO];
    }
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(BOOL)isSameMusic
{
    NSArray *music = userInfo.music;
    NSArray *selections = [musicRank selections];
    DDLogDebug(@"Music: %@, Selections: %@", music, selections);
    for (int i = 0; i < 3; i++) {
        NSNumber *m = music[i];
        if ([selections count] - 1 < i) {
            if (m.intValue == -1) {
                break;
            }
            return NO;
        }
        NSNumber *n = selections[i];
        if (![m isEqualToNumber:n]) {
            return NO;
        }
    }
    return YES;
}

-(BOOL)isSameAtmosphere
{
    NSArray *atmosphere = userInfo.atmospheres;
    NSArray *selections = [atmosphereRank selections];
    for (int i = 0; i < 3; i++) {
        NSNumber *m = atmosphere[i];
        if ([selections count] - 1 < i) {
            if (m.intValue == -1) {
                break;
            }
            return NO;
        }
        NSNumber *n = selections[i];
        if (![m isEqualToNumber:n]) {
            return NO;
        }
    }
    
    return YES;
}



#pragma mark - Edit Atmosphere Choices

-(void)setupTableViewOnEdit
{
    [self.tableView reloadData];
    [self.tableView setScrollEnabled:NO];
    [self.tableView setBounces:NO];
}

-(IBAction)editAtmosphere:(id)sender
{
    if (editAtmosphere) {
        if (![atmosphereRank selectedMinSelections]) {
            IgAlert(nil, @"Please select a minimum of 1 preference.", nil);
            return;
        }
        if (![self isSameAtmosphere]) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [userClient updateAtmosphere:[atmosphereRank selections]];
        } else {
            editAtmosphere = NO;
            [self saveAtmosphereOptions];
            [self setupTableViewOnSave];
        }
    } else {
        editAtmosphere = YES;
        [self setupTableViewOnEdit];
    }
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - Edit Spending limit

-(IBAction)editSpendingLimit:(id)sender
{
    if (editSpending) {
        if (currentSpending < 0) {
            IgAlert(nil, @"Please select a spending preference.", nil);
            return;
        }
        if (!(currentSpending == userInfo.spendingLimit.intValue)) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [userClient updateSpedningLimit :currentSpending];
        } else {
            editSpending = NO;
            [self saveSpendingOptions];
            [self setupTableViewOnSave];
        }
    } else {
        editSpending = YES;
        [self setupTableViewOnEdit];
    }
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:4] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - Edit Age

-(IBAction)editAge:(id)sender
{
    if (editAge) {
        if (!(currentAge == userInfo.ageBucketId.intValue)) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [userClient updateAge:currentAge];
        } else {
            editAge = NO;
            [self saveAgeOptions];
            [self setupTableViewOnSave];
        }
    } else {
        editAge = YES;
        [self setupTableViewOnEdit];
    }
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:5] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - Edit E-mail

-(IBAction)editEmail:(id)sender
{
    if (editEmail) {
        if (![IgnightUtil validEmail:email.text]) {
            IgAlert(nil, @"Please enter a valid email address.", nil);
            return;
        }
        currentEmail = email.text;
        if (!([currentEmail isEqualToString:userInfo.email])) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [userClient updateEmail:currentEmail];
        } else {
            editEmail = NO;
            [self setupTableViewOnSave];
        }
    } else {
        editEmail = YES;
        [self setupTableViewOnEdit];
    }
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:6] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - UserHttpClientDelegate

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)setupTableViewOnSave
{
    [self.tableView reloadData];
    [self.tableView setScrollEnabled:YES];
    [self.tableView setBounces:YES];
}

-(void)userHttpClient:(UserHttpClient *)client updatedCity:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        editLocation = NO;
        userInfo.cityId = [NSNumber numberWithLong:currentCity];
        [self setupTableViewOnSave];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client updatedEmail:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        editEmail = NO;
        userInfo.email = currentEmail;
        [self setupTableViewOnSave];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client updatedAge:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        editAge = NO;
        userInfo.ageBucketId = [NSNumber numberWithLong:currentAge];
        [self saveAgeOptions];
        [self setupTableViewOnSave];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client updatedMusic:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        editMusic = NO;
        [self saveMusicOptions];
        [self setupTableViewOnSave];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client updatedAtmosphere:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        editAtmosphere = NO;
        [self saveAtmosphereOptions];
        [self setupTableViewOnSave];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client updatedSpendingLimit:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        editSpending = NO;
        userInfo.spendingLimit = [NSNumber numberWithLong:currentSpending];
        [self saveSpendingOptions];
        [self setupTableViewOnSave];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client updatedGroupInvitePreference:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    DDLogDebug(@"Got response : %@", response);
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        userInfo.notifyOfGroupInvites = notifyGroupInvites;
    } else {
        userInfo.notifyOfGroupInvites = !notifyGroupInvites;
        IgAlert(nil, response[@"reason"], nil);
    }
    [self setupTableViewOnSave];
}

-(void)userHttpClient:(UserHttpClient *)client updatedProfilePricture:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSString *imageUrl = response[@"body"];
        userInfo.profilePictureUrl = imageUrl;
        [self loadUserImage];
    } else {
        userInfo.profilePictureImage = nil;
        IgAlert(nil, response[@"reason"], nil);
    }
}

#pragma mark - Remote Notifications

-(void)setupPushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewMailPush:) name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewGroupBuzz:) name:@"NewGroupBuzzNotification" object:nil];
}

-(void)removePushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewGroupBuzzNotification" object:nil];
}

-(void)handleNewMailPush: (NSNotification *)notification
{
    GroupViewController *groupView = [self.storyboard instantiateViewControllerWithIdentifier:@"allGroupView"];
    groupView.tabIndex = 2;
    [self.navigationController pushViewController:groupView animated:YES];
}


-(void)handleNewGroupBuzz: (NSNotification *)notification
{
    NSDictionary *data = notification.userInfo;
    Trending *trending = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
    trending.goBack = NO;
    trending.groupMode = YES;
    trending.groupName = data[@"groupName"];
    trending.groupId = data[@"groupId"];
    trending.tabIndex = 1;
    [self.navigationController pushViewController:trending animated:YES];
}


@end
