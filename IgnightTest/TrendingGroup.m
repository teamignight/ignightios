//
//  TrendingGroup.m
//  Ignight
//
//  Created by Abhinav Chordia on 1/25/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import "TrendingGroup.h"

@implementation TrendingGroup

static NSString* GROUP_NAME = @"groupName";
static NSString* PRIVATE_GROUP = @"privateGroup";
static NSString* IS_MEMBER_OF_GROUP = @"isMemberOfGroup";
static NSString* GROUP_ID = @"groupId";

-(id)initWithData: (NSDictionary *)data
{
    self = [super init];
    if (self) {
        _groupId = data[GROUP_ID];
        _groupName = data[GROUP_NAME];
        _isMemberOfGroup = [data[IS_MEMBER_OF_GROUP] boolValue];
        _privateGroup = [data[PRIVATE_GROUP] boolValue];
    }
    return self;
}

@end
