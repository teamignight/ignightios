//
//  BuzzTableViewCell.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/27/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuzzMessage.h"
#import "SpeechBubbleView.h"

@interface BuzzTableViewCell : UITableViewCell

-(void)setMessage: (BuzzMessage *)message;

@end
