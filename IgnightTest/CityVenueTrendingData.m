//
//  CityVenueTrendingData.m
//  Ignight
//
//  Created by Abhinav Chordia on 1/20/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import "CityVenueTrendingData.h"

@implementation CityVenueTrendingData

-(id)initWithData: (NSDictionary *)data
{
    self = [super init];
    if (self) {
        _venueId = data[@"venueId"];
        _venueName = data[@"venueName"];
        _userBarColor = (UserBarColor)[data[@"userBarColor"] integerValue];
        _userInput = (UserInputMode)[data[@"userInput"] integerValue];
        _longitude = data[@"longitude"];
        _latitude = data[@"latitude"];
        _userVenueValue = data[@"userVenueValue"];
        _venueImageUrl = data[@"placeImage"];
        _venueImage = nil;
    }
    return self;
}

@end
