//
//  VenueAnnotation.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/20/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface VenueAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSNumber *venueId;
@property (nonatomic, copy) NSNumber *userVenueValue;

@end
