//
//  GroupHttpClient.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/13/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

@protocol GroupHttpClientDelegate;

@interface GroupHttpClient : AFHTTPSessionManager

@property (nonatomic, weak) id<GroupHttpClientDelegate> delegate;

+(GroupHttpClient *)getInstance;
-(instancetype)initWithBaseURL:(NSURL *)url;
-(void)cancelAllOperations;

-(void)inviteUser: (NSNumber *)targetUserId toGroup: (NSNumber *)groupId;
-(void)leaveGroup: (NSNumber *)groupId;
-(void)getGroupTrendingData: (NSNumber *)groupId from:(NSInteger)from to:(NSInteger)to;

-(void)getGroupInfo: (NSNumber*)groupId;

-(void)createGroup: (NSString *)name withDescription: (NSString *)description withPrivate: (BOOL)privateGroup;
-(void)joinGroup: (NSNumber *)groupId;

-(void)getMembersForGroup: (NSNumber *)groupId;

-(void)getIncomingRequestsForUser;
-(void)respondToGroup: (NSNumber *)groupId withResponse: (BOOL)response;

-(void)searchForGroups:(NSString *)search withOffset: (NSInteger)offset WithLimit: (NSInteger)limit;

-(void)getPopularGroupsWithOffset: (NSInteger)offset WithLimit:(NSInteger)limit;

-(void)registerForGroupPush: (NSNumber *)groupId;
-(void)unregisterForGroupPush: (NSNumber *)groupId;

-(void)searchVenuesForGroup: (NSNumber *)groupId withSearch: (NSString *)searchString from: (NSInteger)from to:(NSInteger)to;
-(void)flagGroup:(NSNumber *)groupId;

@end

@protocol GroupHttpClientDelegate <NSObject>

@optional

-(void)groupHttpClient: (GroupHttpClient *)client gotGroupVenueSearch: (NSDictionary *)response;

-(void)groupHttpClient: (GroupHttpClient *)client gotPopularGroups: (NSDictionary *)response withOffset: (NSInteger)offset;

-(void)groupHttpClient: (GroupHttpClient *)client successfullyInvitedUser: (NSDictionary *)response;
-(void)groupHttpClient: (GroupHttpClient *)client joinedGroupWithResponse: (NSDictionary *)response;
-(void)groupHttpClient: (GroupHttpClient *)client leftGroup: (NSNumber *)groupId withResponse: (NSDictionary *)response;

-(void)groupHttpClient: (GroupHttpClient *)client gotGroupTrendingData: (NSDictionary *)response;
-(void)groupHttpClient: (GroupHttpClient *)client gotGroupCreationResponse: (NSDictionary *)response;
-(void)groupHttpClient: (GroupHttpClient *)client gotGroupInfo: (NSDictionary *)response;

-(void)groupHttpClient: (GroupHttpClient *)client gotGroupMembers: (NSDictionary *)response;

-(void)groupHttpClient: (GroupHttpClient *)client gotIncomingGroupRequests: (NSDictionary *)response;
-(void)groupHttpClient: (GroupHttpClient *)client groupInviteConfirmedForGroup: (NSNumber *)groupId withResponse: (NSDictionary *)response;
-(void)groupHttpClient: (GroupHttpClient *)client groupRejectedConfirmedForGroup: (NSNumber *)groupId withResponse: (NSDictionary *)response;

-(void)groupHttpClient: (GroupHttpClient *)client gotGroupSearchResults: (NSDictionary *)response withOffset:(NSInteger)offset;

-(void)groupHttpClient: (GroupHttpClient *)client didFailWithError: (NSError *)error;

-(void)groupHttpClient: (GroupHttpClient *)client registeredForPushWithResponse: (NSDictionary *)response;
-(void)groupHttpClient: (GroupHttpClient *)client unregisteredForPushWithResponse: (NSDictionary *)response;

@end
