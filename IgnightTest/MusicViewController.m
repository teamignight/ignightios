//
//  SelectionViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/20/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "MusicViewController.h"
#import "UserInfo.h"
#import "MusicTypes.h"
#import "IgnightPreferences.h"

@interface MusicViewController ()

@end

#define MAX_RANK 3
#define TAG_ADJ 100

@implementation MusicViewController
{
    UserInfo *userInfo;
    int optionsInRow;
    
    NSMutableDictionary *selectionToRank;
    NSMutableDictionary *rankToSelection;
    int selectionCounter;
    int maxSelectedRank;
} 

- (void)viewDidLoad
{
    [super viewDidLoad];
    optionsInRow = 3;
    
    IgnightPreferences *prefs = [[IgnightPreferences alloc] initWithPreferenceSet:[MusicTypes getListOfValues] container:self.scrollView optionsInRow:optionsInRow forClass:self width:320];
    [prefs populateScrollView];
    
    userInfo = [UserInfo getInstance];
    TFLog(@"MusicViewController: Loaded UserInfo with: [%@, %@, %@]",
                        [MusicTypes getMusicNameFromId:[[userInfo.music objectAtIndex:0] intValue]],
                        [MusicTypes getMusicNameFromId:[[userInfo.music objectAtIndex:1] intValue]],
                        [MusicTypes getMusicNameFromId:[[userInfo.music objectAtIndex:2] intValue]]);
    
    selectionToRank = [[NSMutableDictionary alloc] init];
    rankToSelection = [[NSMutableDictionary alloc] init];
    selectionCounter = 0;
    maxSelectedRank = 0;
    
    [self resetMusicSelections];
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    TFLog(@"Received Memmory Warning");
    [super didReceiveMemoryWarning];
}

-(void)resetMusicSelections
{
    for (int i = 0; i < MAX_RANK; i++) {
        NSInteger musicId = [[userInfo.music objectAtIndex:i] intValue];
        if (musicId < 0)
            return;
        
        NSString *name = [MusicTypes getMusicNameFromId:musicId];
        [self setSelection:name asRank:i];
    }
}

- (IBAction)selectionOptionPressed:(UIButton *)sender {
    UIButton *button = (UIButton *)sender;
    
    NSString *buttonTitle = [[button titleLabel] text];
    
    if (button.selected == NO) {
        NSInteger nextRank = [self findNextAvailableRank];
        
        if (nextRank == -1)
            return;
        
        [self setSelection:buttonTitle asRank:nextRank];
        
    } else {
        NSInteger rank = [[selectionToRank objectForKey:buttonTitle] integerValue];
        [self removeSelection:buttonTitle asRank:rank];
    }
}

-(NSInteger)findNextAvailableRank {
    for (int i = 0; i < MAX_RANK; i++) {
        if (![[rankToSelection allKeys] containsObject:[NSString stringWithFormat:@"%d", i]])
            return i;
    }
    return -1;
}

-(void)setSelection: (NSString *)name asRank: (NSInteger)rank
{
    [selectionToRank setObject:[NSString stringWithFormat:@"%d", rank] forKey:name];
    [rankToSelection setObject:name forKey:[NSString stringWithFormat:@"%d", rank]];
    
    NSString *imagePath = [NSString stringWithFormat:@"selection-option-button-selected-%i", rank + 1];
    UIImage *newImage = [UIImage imageNamed:imagePath];
    
    NSInteger tag = [MusicTypes getMusicFromName:name];
    UIButton *button = (UIButton *)[self.scrollView viewWithTag:(tag + TAG_ADJ)];
    
    [button setBackgroundImage:newImage forState:UIControlStateSelected];
    [button  setSelected:YES];
    selectionCounter++;
    
    [self checkIfReadyToContinue];
}

-(void)removeSelection: (NSString *)name asRank: (NSInteger)rank
{
    [selectionToRank removeObjectForKey:name];
    [rankToSelection removeObjectForKey:[NSString stringWithFormat:@"%d", rank]];
    
    NSInteger tag = [MusicTypes getMusicFromName:name];
    UIButton *button = (UIButton *)[self.scrollView viewWithTag:(tag + TAG_ADJ)];
    
    [button setBackgroundImage:nil forState:UIControlStateSelected];
    [button setSelected:NO];
    selectionCounter--;
    
    [self checkIfReadyToContinue];
}

-(void)checkIfReadyToContinue
{
    self.continueButton.alpha = selectionCounter;
    if (selectionCounter == MAX_RANK) {
        self.scrollView.alpha = 0.35;
    } else
        self.scrollView.alpha = 1;
}

-(void)updateUserInfo
{
    [userInfo resetMusic];
    for (int i = 0; i < [rankToSelection count]; i++) {
        NSString *name = [rankToSelection objectForKey:[NSString stringWithFormat:@"%d", i]];
        NSNumber *dnaValue = [NSNumber numberWithInt:[MusicTypes getMusicFromName:name]];
        [userInfo setMusicDna:dnaValue atSelection:i];
    }
}

- (void)goToNext
{
    [self updateUserInfo];
    [self performSegueWithIdentifier:@"musicToAtmosphere" sender:self];
}

- (IBAction)continueButtonPressed:(id)sender {
    [self goToNext];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    TFLog(@"gestureRecognizerShouldBegin");
    UISwipeGestureRecognizer *gesture = (UISwipeGestureRecognizer*)gestureRecognizer;
    if (gesture.direction == UISwipeGestureRecognizerDirectionRight) {
        TFLog(@"Direction: RIGHT");
        [self updateUserInfo];
        return YES;
    } else {
        TFLog(@"Direction: LEFT");
        if (self.continueButton.alpha == 1) {
            [self goToNext];
            return YES;
        }
        return NO;
    }
}

@end
