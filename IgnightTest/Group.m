//
//  Group.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/1/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "Group.h"

@implementation Group


-(NSComparisonResult)compare: (Group *)otherObject {
    return [self.name localizedCaseInsensitiveCompare:otherObject.name];
//    if (self.name.lowercaseString < otherObject.name.lowercaseString)
//        return NSOrderedAscending;
//    else if (self.name.lowercaseString > otherObject.name.lowercaseString)
//        return NSOrderedDescending;
//    return NSOrderedSame;
//    return [self.name compare:otherObject.name];
}

@end
