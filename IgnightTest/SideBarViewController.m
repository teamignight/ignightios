//
//  SideBarViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/25/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "SideBarViewController.h"
#import "SWRevealViewController.h"
#import "Cities.h"
#import "Groups.h"
#import "GroupDetailsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "InviteViewController.h"
#import "MailViewController.h"
#import "SideBarMenuCell.h"
#import "SideBarBuzzCell.h"
#import "SideBarHeaderView.h"
#import "Trending.h"
#import "SBGroupInfo.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "APAddressBook.h"
#import "APContact.h"
#import "APPhoneWithLabel.h"
#import "ConfirmUserPhoneNumberViewController.h"
#import "UserContactsTableViewController.h"

@interface SideBarViewController ()

@end

@implementation SideBarViewController
{
    UserInfo *userInfo;
    NSNumber *currentVenueId;
    
    UserHttpClient *userClient;
    UserNotifications *userNotifications;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect tFrame = self.tableView.frame;
    self.tableView.frame = CGRectMake(tFrame.origin.x, tFrame.origin.y + 20, tFrame.size.width, tFrame.size.height - 20);
    
    userInfo = [UserInfo getInstance];
    userClient = [UserHttpClient getInstance];
    
    self.view.backgroundColor = sideBarBackground;
    self.tableView.backgroundColor = sideBarListBackground;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.alwaysBounceVertical = NO;
    
    [self registerCellsWithTableView];
    self.navigationController.navigationBarHidden = YES;
    
    userNotifications = [UserNotifications getInstance];
}

-(void)registerCellsWithTableView
{
    [self.tableView registerClass:[SideBarMenuCell class] forCellReuseIdentifier:@"main"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    userClient.delegate = self;
    
    IgnightTestAppDelegate *appDelegate = (IgnightTestAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate flipHack];
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 0.01)];
    
    [self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    if (self.tableView.contentOffset.y == 0.0) {
        [self.tableView setContentOffset:CGPointMake(0, -20) animated:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    IgnightTestAppDelegate *appDelegate = (IgnightTestAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate flipHack];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - UserHttpClientDelegate

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    IgAlert(@"IgNight", @"Failed to log out. Please try again.", nil);
}

-(void)userHttpClient:(UserHttpClient *)client gotLogoutResponse:(NSDictionary *)response
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [[response objectForKey:@"res"] boolValue];
    if (res) {
        [userInfo clearAllInfo];
        [IgnightUtil clearUserCredentials];
        [self performSegueWithIdentifier:@"logout" sender:self];
    } else {
        IgAlert(@"IgNight", response[@"reason"], nil);
    }
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 0) {
//        return 10.0f;
//    }
    return 54.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSString *title;
    NSString *icon;
    NSString *activityAmount = nil;
    
    UIImage *image;
    if (indexPath.section == 0) {
        int i = 0;
        if (indexPath.row == i++) {
            title = userInfo.userName;
            if (userInfo.profilePictureImage != nil) {
                image = [IgnightUtil getBuzzUserPic:userInfo.profilePictureImage];
            } else {
                image = [UIImage imageNamed:@"group_member_default"];
                if (userInfo.profilePictureUrl != nil && ![userInfo.profilePictureUrl isEqualToString:@""]) {
                    NSURL *url = [NSURL URLWithString:userInfo.profilePictureUrl];
                    NSURLRequest *request = [NSURLRequest requestWithURL:url];
                    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                    operation.responseSerializer = [AFImageResponseSerializer serializer];
                    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                        userInfo.profilePictureImage = responseObject;
                        [tableView reloadData];
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        DDLogDebug(@"Failed To Retrieve Image : %@ with error: %@", userInfo.profilePictureUrl, error);
                    }];
                    [operation start];
                }
            }
        } else if (indexPath.row == i++) {
            title = [Cities getCityNameFromId:[userInfo.cityId intValue]];
            icon = @"sb-city";
            image = [UIImage imageNamed:icon];
        } else if (indexPath.row == i++) {
            title = @"Groups";
            icon = @"sidebar_groups";
            image = [UIImage imageNamed:icon];
        } else if (indexPath.row == i++) {
            title = @"Invite Contacts";
            icon = @"group_members";
            image = [UIImage imageNamed:icon];
        } else if (indexPath.row == i++) {
            title = @"Tutorial";
            icon = @"sidebar_tutorial";
            image = [UIImage imageNamed:icon];
        } else if (indexPath.row == i++) {
            title = @"Logout";
            icon = @"sb-log-out";
            image = [UIImage imageNamed:icon];
        }
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"main" forIndexPath:indexPath];
        
        SideBarMenuCell *sCell = (SideBarMenuCell *)cell;
        
        if (cell == nil) {
            sCell = [[SideBarMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"main"];
        }
        
        [sCell setTitle:title];
        [sCell setImageForIcon:image];
        if (indexPath.row == 0) {
            [sCell adjustUserImage];
            [sCell makeSeparatorThicker];
        } else if (indexPath.row == 3) {
            CGRect frame = CGRectMake(15, 12, 54 - 20, 54 - 24);
            sCell.icon.frame = frame;
        } else {
            if (indexPath.row == 2) {
                if (userNotifications.groupBuzz || userNotifications.inbox) {
                    [sCell makeSeparatorLightUp];
                } else {
                    [sCell makeSeparatorRegular];
                }
            } else {
                [sCell makeSeparatorRegular];
            }
        }
        [(SideBarMenuCell*)cell makeActivityIndicatorVisibleWithText:activityAmount];
    }

    if (cell == nil) {
        return [[UITableViewCell alloc] init];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        int i = 0;
        if (indexPath.row == i++) {
            DDLogDebug(@"SELECTED PROFILE");
            [self performSegueWithIdentifier:@"profile" sender:self];
        } else if (indexPath.row == i++) {
            [self performSegueWithIdentifier:@"universe" sender:self];
        } else if (indexPath.row == i++) {
            [self performSegueWithIdentifier:@"groupView" sender:self];
        } else if (indexPath.row == i++) {
            ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
            ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
            if (status == kABAuthorizationStatusNotDetermined) {
                self.navigationController.title = @"";
                [self performSegueWithIdentifier:@"confirmUser" sender:self];
            } else if (status == kABAuthorizationStatusAuthorized) {
                self.navigationController.title = @"";
                [self performSegueWithIdentifier:@"inviteContacts" sender:self];
            } else if (status == kABAuthorizationStatusDenied || status == kABAuthorizationStatusRestricted) {
                [[[UIAlertView alloc] initWithTitle:@"Contacts Settings" message:@"If you would like to use your contacts, please go to your settings and give IgNight access to your contacts." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            if (addressBookRef != nil) {
                CFRelease(addressBookRef);
            }
            [self performSegueWithIdentifier:@"inviteContacts" sender:self];
        } else if (indexPath.row == i++) {
            [self performSegueWithIdentifier:@"tutorial" sender:self];
        } else if (indexPath.row == i++) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"IgNight" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
            [alertView show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [userClient logout];
    }
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    NSString *identifier = [segue identifier];
    
    if ([identifier isEqualToString:@"groups"]) {
        SBGroupInfo *info = (SBGroupInfo *)sender;
        Trending *trendingView = segue.destinationViewController;
        trendingView.groupMode = YES;
        trendingView.groupId = info.groupId;
        trendingView.goBack = NO;
        trendingView.groupName = info.groupName;
    }
    else if ([identifier isEqualToString:@"logout"]) {
        [userInfo clearAllInfo];
        [[TrendingDataCache getInstance] clear];
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
    }
}
@end
