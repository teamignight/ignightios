//
//  GroupViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupCell.h"
#import "Groups.h"
#import "Group.h"
#import "IgnightTopBar.h"
#import "SWRevealViewController.h"
#import "CreateAGroupView.h"
#import "PopularGroups.h"
#import "NSDate+TimeAgo.h"
#import "GroupDetailsViewController.h"
#import "Trending.h"
#import "MailCell.h"
#import "UserNotifications.h"

@interface GroupViewController : UIViewController <
            UITableViewDataSource,
            UITableViewDelegate,
            UISearchBarDelegate,
            IgnightTopBarDelegate,
            GroupCellDelegate,
            SWRevealViewControllerDelegate,
            MailCellDelegate,
            GroupHttpClientDelegate,
            UserNotificationsDelegate,
            UserHttpClientDelegate>

@property NSInteger tabIndex;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopConstraint;

@property (strong, nonatomic) IBOutlet UIImageView *titleBar;

@property (strong, nonatomic) IBOutlet UIView *groupMenu;

@property (strong, nonatomic) IBOutlet UIImageView *groupBtnImg;
@property (strong, nonatomic) IBOutlet UIButton *groupBtn;
@property (strong, nonatomic) IBOutlet UIView *groupsView;
@property (strong, nonatomic) IBOutlet UILabel *groupsNotificationLine;


@property (strong, nonatomic) IBOutlet UIImageView *exploreBtnImg;
@property (strong, nonatomic) IBOutlet UIButton *exploreBtn;
@property (strong, nonatomic) IBOutlet UIView *exploreView;

@property (strong, nonatomic) IBOutlet UIImageView *inboxBtnImg;
@property (strong, nonatomic) IBOutlet UIButton *inboxBtn;
@property (strong, nonatomic) IBOutlet UIView *inboxView;
@property (strong, nonatomic) IBOutlet UILabel *inboxNotificationLine;


@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;


#pragma mark - Actions

- (IBAction)goToGroups:(id)sender;
- (IBAction)goToExplore:(id)sender;
- (IBAction)goToInbox:(id)sender;

@end
