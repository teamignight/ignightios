//
//  VenueMetaDataTableViewCell.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 2/12/15.
//  Copyright (c) 2015 Abhinav Chordia. All rights reserved.
//

#import "VenueMetaDataTableViewCell.h"

@implementation VenueMetaDataTableViewCell
{
    UserInfo *userInfo;
    
    UIButton *addressBtn;
    UIImageView *addressImgView;
    
    UIButton *phoneBtn;
    UIImageView *phoneImgView;
    
    UIButton *websiteBtn;
    UIImageView *websiteImgView;
    
    UIButton *errorBtn;
    UIImageView *errorImgView;
    
    UIView *firstSeparator;
    UIView *secondSeparator;
    UIView *thirdSeparator;
    
    NSMutableArray *buttons;
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        addressBtn = [self addButtonWithImageView:addressImgView withImage:[UIImage imageNamed:@"venue_address"]];
        [addressBtn addTarget:self action:@selector(selectAddress:) forControlEvents:UIControlEventTouchUpInside];
        phoneBtn = [self addButtonWithImageView:phoneImgView withImage:[UIImage imageNamed:@"venue_phone"]];
        [phoneBtn addTarget:self action:@selector(selectPhone:) forControlEvents:UIControlEventTouchUpInside];
        websiteBtn = [self addButtonWithImageView:websiteImgView withImage:[UIImage imageNamed:@"venue_url"]];
        [websiteBtn addTarget:self action:@selector(selectWebsite:) forControlEvents:UIControlEventTouchUpInside];
        errorBtn = [self addButtonWithImageView:errorImgView withImage:[UIImage imageNamed:@"venue_error"]];
        [errorBtn addTarget:self action:@selector(selectError:) forControlEvents:UIControlEventTouchUpInside];
        
        buttons = [[NSMutableArray alloc] init];
        
        [self.contentView setBackgroundColor:venueListBackgrond];
    }
    return self;
}

-(UIButton *)addButtonWithImageView: (UIImageView *)imageView withImage: (UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:button];
    
    imageView = [[UIImageView alloc] init];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [imageView setTag:100];
    imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [imageView setImage:image];
    imageView.frame = CGRectMake(0, 10, button.frame.size.width, VENUE_META_DATA_CELL_HEIGHT - 20);
    [button addSubview:imageView];
    
    return button;
}

-(void)updateWithAddress: (NSString *)addr withNumber: (NSString *)number withWebsite: (NSString *)website
{
    [self clearMetaData];
    
    _venueAddress = addr;
    _phoneNumber = number;
    _website = website;
    
    [self adjustView];
}

-(void)clearMetaData
{
    _venueAddress = nil;
    _phoneNumber = nil;
    _website = nil;
}

-(void)adjustView
{
    [self gatherButtonsToDisplay];
    NSInteger count = [buttons count];
    float buttonsWidth = screenWidth / (count * 1.0);
    
    for (int i = 0; i < count; i++) {
        UIButton *button = [buttons objectAtIndex:i];
        button.frame = CGRectMake(i * buttonsWidth, 0, buttonsWidth, VENUE_META_DATA_CELL_HEIGHT);
        UIImageView *imgView = (UIImageView *)[button viewWithTag:100];
        imgView.frame = CGRectMake(0, 10, buttonsWidth, VENUE_META_DATA_CELL_HEIGHT - 20);
        [self.contentView addSubview:button];
    }
}

-(void)removeAllButtons
{
    [addressBtn removeFromSuperview];
    [phoneBtn removeFromSuperview];
    [websiteBtn removeFromSuperview];
    [errorBtn removeFromSuperview];
}

-(void)gatherButtonsToDisplay
{
    [buttons removeAllObjects];
    if (_venueAddress != nil && ![_venueAddress isEqualToString:@""]) {
        [buttons addObject:addressBtn];
    }
    if (_phoneNumber != nil && ![_phoneNumber isEqualToString:@""]) {
        [buttons addObject:phoneBtn];
    }
    if (_website != nil && ![_website isEqualToString:@""]) {
        [buttons addObject:websiteBtn];
    }
    [buttons addObject:errorBtn];
}

-(IBAction)selectAddress:(id)sender
{
    userInfo = [UserInfo getInstance];
    NSString *address = [NSString stringWithFormat:@"%@, Chicago, IL", _venueAddress];
    NSString *url = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%@",
                     userInfo.userLocation.coordinate.latitude,
                     userInfo.userLocation.coordinate.longitude,
                     [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

-(IBAction)selectPhone:(id)sender
{
    NSString *phoneNo = _phoneNumber;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNo]];
    [[UIApplication sharedApplication] openURL:phoneUrl];
}

-(IBAction)selectWebsite:(id)sender
{
    [_delegate goToWebsite];
}

-(IBAction)selectError:(id)sender
{
    [_delegate goToErrorScreen];
}

@end
