//
//  DNAStepFour.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/2/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "DNAStepFour.h"
#import "RankToSelection.h"
#import "SpendingTypes.h"

@interface DNAStepFour ()

@end

@implementation DNAStepFour
{
    RankToSelection *rankToSelection;
    UserInfo *userInfo;
    UserHttpClient *client;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    rankToSelection = [[RankToSelection alloc] initWithMaxSelections:1 minSelections:1];
    userInfo = [UserInfo getInstance];
    client = [UserHttpClient getInstance];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"step_n_bg"]];
    self.collectionView.backgroundView.alpha = .15;
    [self.collectionView reloadData];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.allowsMultipleSelection = YES;
    
    self.continueBtn.backgroundColor = whiteFooterBackground;
    [self.continueBtn setTitleColor:grayHeaderFooterText forState:UIControlStateNormal];
    [[self.continueBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[self.continueBtn layer] setBorderWidth:0.3];
    
    self.headerLabel.backgroundColor = whiteHeaderBackground;
    [self.headerLabel setTextColor:grayHeaderFooterText];
    [[self.headerLabel layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[self.headerLabel layer] setBorderWidth:0.3];
    
    [self adjustView];
    [self addSwipeGesture];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    client.delegate = self;
}

-(void)addSwipeGesture
{
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [gesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:gesture];
}

-(void)handleSwipe: (UISwipeGestureRecognizer *)recognizer
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (userInfo.spendingLimit.integerValue == indexPath.row) {
        [userInfo setSpendingLimit:[NSNumber numberWithInteger:-1]];
    } else  {
        userInfo.spendingLimit = [NSNumber numberWithInteger:indexPath.row];
    }
    [collectionView reloadData];
}

-(void)setImageForCell: (UICollectionViewCell *)cell forRow: (NSInteger)row
{
    UIImage *rankImage = [UIImage imageNamed:@"white_check"];
    UIImageView *rankView = (UIImageView *)[cell viewWithTag:103];
    rankView.image = rankImage;
    rankView.alpha = 1;
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:102];
    cover.alpha = 0.3;
    
    [self adjustView];
}

-(void)removeImageForCell: (UICollectionViewCell*)cell forRow: (NSInteger)row
{
    UIImageView *rankView = (UIImageView *)[cell viewWithTag:103];
    rankView.image = nil;
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:102];
    cover.alpha = 0;
    
    [self adjustView];
}

-(void)adjustView
{
    if (userInfo.spendingLimit.integerValue >= 0) {
        self.continueBtn.alpha = 1;
        self.continueBtn.userInteractionEnabled = YES;
        [self.continueBtn setTitleColor:registrationContinueBtnColor forState:UIControlStateNormal];
    } else {
        self.continueBtn.alpha = 0.4;
        [self.continueBtn setTitleColor:grayHeaderFooterText forState:UIControlStateNormal];
        self.continueBtn.userInteractionEnabled = NO;
    }
}

#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return SPENDING_COUNT;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"spendingOption" forIndexPath:indexPath];
    
    UILabel *name = (UILabel*)[cell viewWithTag:100];
    name.text = [SpendingTypes getSpendingNameFromId:(Spending)indexPath.row];
    name.textAlignment = NSTextAlignmentCenter;
    name.textColor = venueListText;
    name.numberOfLines = 2;
    name.font = [UIFont systemFontOfSize:14];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:101];
    NSString *spendingImage = [[[SpendingTypes getSpendingNameFromId:(Spending)indexPath.row] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    imageView.image = [UIImage imageNamed:spendingImage];
    [[imageView layer] setCornerRadius:imageView.frame.size.width/2];
    [[imageView layer] setMasksToBounds:YES];
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:102];
    [[cover layer] setCornerRadius:cover.frame.size.width/2];
    [[cover layer] setMasksToBounds:YES];
    
    if (userInfo.spendingLimit.integerValue == indexPath.row) {
        [self setImageForCell:cell forRow:indexPath.row];
    } else {
        [self removeImageForCell:cell forRow:indexPath.row];
    }
    
    return cell;
}

#pragma mark - UserHttpClientDelegate

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)userHttpClient:(UserHttpClient *)client registeredUser:(NSDictionary *)response
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    DDLogDebug(@"Got Registration Response: %@", response);
    if (res) {
        [IgnightUtil updateUserInfoOnLogin:response];
        [IgnightUtil saveUserData];
        TutorialViewController *tutorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
        tutorialViewController.signupMode = YES;
        [self.navigationController pushViewController:tutorialViewController animated:YES];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

- (IBAction)registerUser:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [client registerUser];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
