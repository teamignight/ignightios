//
//  GroupDetailsViewController.h
//  IgnightTest
//
//  Created by Rob Chipman on 9/7/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Group.h"
#import "Trending.h"


@interface GroupDetailsViewController : UIViewController <GroupHttpClientDelegate,
                                                           SWRevealViewControllerDelegate>

@property (strong, nonatomic) NSNumber *groupId;
@property (nonatomic) BOOL allowToJoin;
@property (nonatomic) BOOL dontGoBack;
@property (nonatomic) BOOL acceptInvite;

@property (strong, nonatomic) IBOutlet UIImageView *topBar;

@property (strong, nonatomic) IBOutlet UIImageView *musicImgView;
@property (strong, nonatomic) IBOutlet UILabel *musicLbl;

@property (strong, nonatomic) IBOutlet UIImageView *atmosphereImgView;
@property (strong, nonatomic) IBOutlet UILabel *atmosphereLbl;

@property (strong, nonatomic) IBOutlet UIImageView *spendingImgView;
@property (strong, nonatomic) IBOutlet UILabel *spendingLbl;

@property (strong, nonatomic) IBOutlet UIImageView *ageImgView;
@property (strong, nonatomic) IBOutlet UILabel *ageLbl;

@property (weak, nonatomic) IBOutlet UILabel *lblGroupMemberCount;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupType;

@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UITextView *tvGroupDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnJoinLeave;

@property (strong, nonatomic) IBOutlet UILabel *subscriptionLbl;
@property (strong, nonatomic) IBOutlet UISwitch *subscribtionSwitch;


- (IBAction)joinGroup:(id)sender;
- (IBAction)toggleSubscription:(id)sender;


@end
