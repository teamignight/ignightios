//
//  ForgotPasswordViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/26/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightTestAppDelegate.h"
#import "UserHttpClient.h"
#import "UpdatePasswordViewController.h"

@interface ForgotPasswordViewController : UIViewController<UIAlertViewDelegate,
                                                           UserHttpClientDelegate>


@property (strong, nonatomic) IBOutlet UIView *usernameView;
@property (strong, nonatomic) IBOutlet UITextField *forgotPasswordTxt;
@property (strong, nonatomic) IBOutlet UIButton *emailMeBtn;

- (IBAction)goBack:(id)sender;
- (IBAction)forgotPassword:(id)sender;

@end
