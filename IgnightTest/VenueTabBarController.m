//
//  VenueTabBarController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/22/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "VenueTabBarController.h"

@interface VenueTabBarController ()

@end

@implementation VenueTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabBarController.hidesBottomBarWhenPushed = YES;
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
