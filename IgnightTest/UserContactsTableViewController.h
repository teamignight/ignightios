//
//  UserContactsTableViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 3/15/15.
//  Copyright (c) 2015 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "SWRevealViewController.h"

@interface UserContactsTableViewController : UITableViewController <MFMessageComposeViewControllerDelegate,
SWRevealViewControllerDelegate>

@property BOOL goBack;

@end
