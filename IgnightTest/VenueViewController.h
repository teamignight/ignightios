//
//  VenueViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/22/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import "IgnightTestAppDelegate.h"
#import "ImageViewViewController.h"
#import "BuzzViewController.h"
#import "GroupViewController.h"
#import "FSImageViewerViewController.h"
#import "Trending.h"

@interface VenueViewController : UIViewController
                <SWRevealViewControllerDelegate,
                 VenueHttpClientDelegate,
                 MKMapViewDelegate,
                 CLLocationManagerDelegate,
                 BuzzViewControllerDelegate,
                 FSImageViewerViewControllerDelegate,
                 ImageViewControllerDelegate>

@property (nonatomic) NSInteger tabIndex;
@property (strong, nonatomic) NSNumber *venueId;
@property (strong, nonatomic) NSString *vName;
@property BOOL goBack;

@property (strong, nonatomic) IBOutlet UIImageView *titleBar;
@property (strong, nonatomic) IBOutlet UIView *venueCell;
@property (strong, nonatomic) IBOutlet UIImageView *dnaImageView;
@property (strong, nonatomic) IBOutlet UILabel *venueName;
@property (strong, nonatomic) IBOutlet UILabel *venueDistance;
@property (strong, nonatomic) IBOutlet UIButton *upVoteBtn;
@property (strong, nonatomic) IBOutlet UIButton *downVoteBtn;
@property (strong, nonatomic) NSNumber *userBarColor;

@property (strong, nonatomic) IBOutlet UIView *containerView;

-(void)getVenueInfo;

#pragma mark - Actions

- (IBAction)upVoteSelected:(id)sender;
- (IBAction)downVoteSelected:(id)sender;

- (void)revealToggle:(id)sender;

@end
