//
//  IgnightUtil.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 1/6/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "IgnightUtil.h"

@implementation IgnightUtil

static NSString *baseServletUrl = @"http://54.88.35.14:8080/IgNight/";

+(NSString *)dynamicFormatPhoneNumber: (NSString *)number
{
    if (number == nil)
        return number;
    
    NSMutableString *newNumber = [NSMutableString stringWithString:[number stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    newNumber = [NSMutableString stringWithString:[newNumber stringByReplacingOccurrencesOfString:@" " withString:@""]];
    if (newNumber.length == 3) {
        [newNumber insertString:@"-" atIndex:3];
    } else if (newNumber.length == 6) {
        [newNumber insertString:@"-" atIndex:3];
        [newNumber insertString:@"-" atIndex:7];
    } else if (newNumber.length == 10) {
        [newNumber insertString:@"-" atIndex:1];
        [newNumber insertString:@"-" atIndex:5];
        [newNumber insertString:@"-" atIndex:9];
    } else
        return number;
    return newNumber;
}

+(NSString *)formatPhoneNumber: (NSString *)number
{
    if (number == nil)
        return number;
    
    NSMutableString *newNumber = [NSMutableString stringWithString:[number stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    if (newNumber.length == 7) {
        [newNumber insertString:@"-" atIndex:3];
    } else if ([newNumber length] == 10) {
        [newNumber insertString:@"-" atIndex:3];
        [newNumber insertString:@"-" atIndex:7];
    } else if ([newNumber length] == 11) {
        [newNumber insertString:@"-" atIndex:1];
        [newNumber insertString:@"-" atIndex:5];
        [newNumber insertString:@"-" atIndex:9];
    } else {
        return number;
    }
    return newNumber;
}

+(UIImage *)getSquareImageFromImage:(UIImage *)image withSize: (CGSize )size
{
    CGFloat aspectRatio = image.size.width / image.size.height;
    CGFloat screenW = size.width;
    CGFloat screenH = size.height;
    
    CGFloat scale = 1;
    
    CGSize newSize = CGSizeZero;
    if (aspectRatio < 1) {  //Length is Bigger
        if (screenH > image.size.height) {
            scale = 1;
        } else {
            scale = screenH / image.size.height;
        }
        newSize.height = image.size.height * scale;
        newSize.width = ceil(image.size.width * scale);
    } else {    //Widht is Bugger
        if (screenW > image.size.width) {
            scale = 1;
        } else {
            scale = screenW / image.size.width;
        }
        newSize.width =  image.size.width * scale;
        newSize.height = ceil(image.size.height * scale);
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, image.scale);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(UIImage*)getSquareImageFromImage: (UIImage *)image
{
    return [self getSquareImageFromImage:image withSize:CGSizeMake(160, 160)];
}

+(UIImage *)getProfilePic: (UIImage *)image
{
    CGSize newSize = CGSizeMake(100, 100);
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(UIImage *)getBuzzUserPic: (UIImage *)image
{
    CGSize newSize = CGSizeMake(35, 35);
    double ratio;
    double delta;
    CGPoint offset;

    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);

    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }

    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);


    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }

    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(UIImage *)scaleForUpload: (UIImage *)image
{
    CGFloat screenW = 480;
    CGFloat screenH = 852;
    
    CGFloat scale = 1;
    
    CGSize newSize = CGSizeZero;
    CGFloat heightScale;
    CGFloat widthScale;
    if (screenH > image.size.height) {
        heightScale = 1;
    } else {
        heightScale = screenH / image.size.height;
    }
    if (screenW > image.size.width) {
        widthScale = 1;
    } else {
        widthScale = screenW / image.size.width;
    }
    if (widthScale < heightScale) {
        scale = widthScale;
    } else {
        scale = heightScale;
    }
    newSize.height = image.size.height * scale;
    newSize.width = ceil(image.size.width * scale);
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, image.scale);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(UIImage *)scaleUserProfileImageForUpload: (UIImage *)image
{
    CGFloat screenW = 160;
    CGFloat screenH = 281;
    
    CGFloat scale = 1;
    
    CGSize newSize = CGSizeZero;
    CGFloat heightScale;
    CGFloat widthScale;
    if (screenH > image.size.height) {
        heightScale = 1;
    } else {
        heightScale = screenH / image.size.height;
    }
    if (screenW > image.size.width) {
        widthScale = 1;
    } else {
        widthScale = screenW / image.size.width;
    }
    if (widthScale < heightScale) {
        scale = widthScale;
    } else {
        scale = heightScale;
    }
    newSize.height = image.size.height * scale;
    newSize.width = ceil(image.size.width * scale);
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, image.scale);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(CGSize)getSizeForImageOnScreen: (UIImage *)image
{
    CGFloat screenW = [[UIScreen mainScreen] bounds].size.width;
    CGFloat screenH = [[UIScreen mainScreen] bounds].size.height;
    
    CGFloat scale = 1;
    
    CGSize newSize = CGSizeZero;
    CGFloat heightScale;
    CGFloat widthScale;
    if (screenH > image.size.height) {
        heightScale = 1;
    } else {
        heightScale = screenH / image.size.height;
    }
    if (screenW > image.size.width) {
        widthScale = 1;
    } else {
        widthScale = screenW / image.size.width;
    }
    if (widthScale < heightScale) {
        scale = widthScale;
    } else {
        scale = heightScale;
    }
    newSize.height = image.size.height * scale;
    newSize.width = ceil(image.size.width * scale);

    return newSize;
}

+(UIImage *)scaleToFitScreen: (UIImage *)image
{
    CGFloat screenW = [[UIScreen mainScreen] bounds].size.width;
    CGFloat screenH = [[UIScreen mainScreen] bounds].size.height;

    CGFloat scale = 1;
    
    CGSize newSize = CGSizeZero;
    CGFloat heightScale;
    CGFloat widthScale;
    if (screenH > image.size.height) {
        heightScale = 1;
    } else {
        heightScale = screenH / image.size.height;
    }
    if (screenW > image.size.width) {
        widthScale = 1;
    } else {
        widthScale = screenW / image.size.width;
    }
    if (widthScale < heightScale) {
        scale = widthScale;
    } else {
        scale = heightScale;
    }
    newSize.height = image.size.height * scale;
    newSize.width = ceil(image.size.width * scale);

    UIGraphicsBeginImageContextWithOptions(newSize, NO, image.scale);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+(BOOL) validEmail:(NSString*) emailString {
    if([emailString length]==0) {
        return NO;
    }
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    DDLogDebug(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}

+(void)setIsIOSTokenSet: (BOOL)set
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *data = [userDefaults objectForKey:@"IgnightUser"];
    StoredUserData *userData = [[StoredUserData alloc] initWithData:data];
    userData.iOSTokenSet = set;
    [userDefaults setObject:[userData getData] forKey:@"IgnightUser"];
}

+(void)saveUserData
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *data = [userDefaults objectForKey:@"IgnightUser"];
    StoredUserData *userData = [[StoredUserData alloc] initWithData:data];
    userData.userId = [UserInfo getInstance].userId;
    [userDefaults setObject:[userData getData] forKey:@"IgnightUser"];
}

+(StoredUserData *)getUserData
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *data = [userDefaults objectForKey:@"IgnightUser"];
    StoredUserData *userData = [[StoredUserData alloc] initWithData:data];
    return userData;
}

+(BOOL)isIosTokenSet
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *data = [userDefaults objectForKey:@"IgnightUser"];
    StoredUserData *userData = [[StoredUserData alloc] initWithData:data];
    return userData.iOSTokenSet;
}

+(void)clearUserCredentials
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"IgnightUser"];
}

+(NSInteger)updateUserInfoOnLogin: (NSDictionary *)response
{
    UserInfo *userInfo = [UserInfo getInstance];
    Groups *groups = [Groups getInstance];
    Inbox *inbox = [Inbox getInstance];
    
    NSDictionary *body = [response objectForKey:@"body"];
    DDLogInfo(@"GOT LOGIN: %@", body);
    
    BOOL pushSet = [[body objectForKey:@"pushSet"] boolValue];
    [IgnightUtil setIsIOSTokenSet:pushSet];
    userInfo.userId = [body objectForKey:@"id"];
    BOOL isUserInfoSet = [(NSNumber *)[body objectForKey:@"userInfoSet"] boolValue];
    if (isUserInfoSet) {
        userInfo.ageBucketId = [body objectForKey:@"age"];
        userInfo.cityId = [body objectForKey:@"cityId"];
        userInfo.userName = [body objectForKey:@"userName"];
        userInfo.genderId = [body objectForKey:@"genderId"];
        userInfo.email = [body objectForKey:@"email"];
        userInfo.profilePictureUrl = [body objectForKey:@"profilePictureURL"];
        userInfo.notifyOfGroupInvites = [[body objectForKey:@"notifyOfGroupInvites"] boolValue];
    } else {
//        [self performSegueWithIdentifier:@"setupProfile" sender:self];
        return 0;
    }
    BOOL isDNASet = [(NSNumber *)[body objectForKey:@"dnaSet"] boolValue];
    if (isDNASet) {
        userInfo.music = [NSMutableArray arrayWithArray:[body objectForKey:@"music"]];
        userInfo.atmospheres = [NSMutableArray arrayWithArray:[body objectForKey:@"atmosphere"]];
        userInfo.spendingLimit = [NSNumber numberWithInt:[[body objectForKey:@"spending"] intValue]];
        [userInfo SetIsUserInfoSetTo:YES];
    }
    
    NSArray *grps = [body objectForKey:@"groups"];
    DDLogDebug(@"Got Groups: %@", grps);
    for (NSDictionary *g in grps) {
        Group *group = [[Group alloc] init];
        group.name = [g objectForKey:@"name"];
        group.groupId = [[g objectForKey:@"groupId"] integerValue];
        group.groupDescription = [g objectForKey:@"description"];
        group.publicGroup = ![[NSNumber numberWithInt:[[g objectForKey:@"private"] intValue]] boolValue];
        group.activity = [[NSNumber numberWithInt:[[g objectForKey:@"newGroupBuzzAvailable"] intValue]] boolValue];
        if (!group.publicGroup) {
            group.adminId = [g objectForKey:@"adminId"];
        }
        [groups addGroup:group];
    }
    
    NSInteger invites = [[body objectForKey:@"incomingGroupRequests"] integerValue];
    return 1;
}

+(NSString *)printUIEdgeInsets: (UIEdgeInsets)inset
{
    return [NSString stringWithFormat:@"(%f, %f, %f, %f)", inset.top, inset.left, inset.bottom, inset.right];
}

+(NSString *)printCGRect: (CGRect)rect
{
    return [NSString stringWithFormat:@"(%f, %f), (%f, %f)", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height];
}

+(NSString *)printCGPoint: (CGPoint)point
{
    return [NSString stringWithFormat:@"(%f, %f)", point.x, point.y];
}

+(NSString *)printCGSize: (CGSize)size
{
    return [NSString stringWithFormat:@"(%f, %f)", size.width, size.height];
}

+(void)setBaseUrl: (NSString *)url
{
    baseServletUrl = url;
}

+(NSString *)getBaseUrl
{
    return baseServletUrl;
}

+(NSString *)getUrlForServlet: (NSString *)servlet
{
    return [NSString stringWithFormat:@"%@%@", baseServletUrl, servlet];
}

+(UIColor *)backgroundBlue
{
    return UIColorFromRGB(0x002945);
}

+(UIColor *)backgroundGray
{
    return UIColorFromRGB(0xe5e5e5);
}

+(UIColor*)ignightBlue
{
    return UIColorFromRGB(0x1d2636);
}

+(UIColor *)ignightGray
{
    return UIColorFromRGB(0xe5e5e5);
}

@end
