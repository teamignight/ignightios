//
//  TabBarSegue.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/8/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "TabBarSegue.h"
#import "Trending.h"

@implementation TabBarSegue

-(void)perform
{
    Trending *trending = (Trending *)self.sourceViewController;
    UIViewController *destinationViewController = (UIViewController *)trending.destinationViewController;
    
    if (trending.oldViewController) {
        [trending.oldViewController willMoveToParentViewController:nil];
        [trending.oldViewController.view removeFromSuperview];
        [trending.oldViewController removeFromParentViewController];
    }
    
    destinationViewController.view.frame = trending.listMapView.bounds;
    [trending addChildViewController:destinationViewController];
    [trending.listMapView addSubview:destinationViewController.view];
    [destinationViewController didMoveToParentViewController:trending];
}

@end
