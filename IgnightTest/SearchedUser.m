//
//  InvitedUser.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/13/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "SearchedUser.h"

static NSString *USER_ID_KEY = @"userId";
static NSString *USERNAME_KEY = @"userName";
static NSString *PROFILE_PICTURE_URL_KEY = @"profilePictureURL";
static NSString *CHAT_PICTURE_URL_KEY = @"chatPictureURL";
static NSString *GROUP_STATUS = @"groupStatus";

@implementation SearchedUser

-(id)initWithParams:(NSDictionary *)params
{
    self = [super init];
    if (self) {
        _userId = params[USER_ID_KEY];
        _userName = params[USERNAME_KEY];
        _profilePictureURL = params[PROFILE_PICTURE_URL_KEY];
        _chatPictureURL = params[CHAT_PICTURE_URL_KEY];
        _groupStatus = (GroupStatus)[params[GROUP_STATUS] integerValue];
    }
    return self;
}

-(NSString *)description
{
    NSDictionary *dic = @{@"userId" : _userId,
                          @"userName" : _userName,
                          @"groupStatus": [NSNumber numberWithInteger:_groupStatus]};
    return [dic description];
}

@end
