//
//  SBGroupInfo.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/1/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBGroupInfo : NSObject

@property (nonatomic, weak) NSString *groupName;
@property (nonatomic, weak) NSNumber *groupId;
@property BOOL publicGroup;

@end
