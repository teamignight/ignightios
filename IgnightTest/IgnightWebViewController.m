//
//  IgnightWebViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/15/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "IgnightWebViewController.h"

@interface IgnightWebViewController ()
{
    IgnightTopBar *titleBar;
}

@end

@implementation IgnightWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_topBar setUserInteractionEnabled:YES];
    titleBar = [[IgnightTopBar alloc] initWithTitle:_venueName withSideBarButtonTarget:self];
    [titleBar setLeftButtonBackgroundImage:[UIImage imageNamed:@"back_gray"]];
//    titleBar = [[IgnightTopBar alloc] initWithWebViewTarget:self];
    titleBar.delegate = self;
    [_topBar addSubview:titleBar];
    
    NSURL *url = [NSURL URLWithString:_website];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [MBProgressHUD showHUDAddedTo:_webView animated:YES];
    
    DDLogDebug(@"Loading: %@", url);
    
    [_webView loadRequest:request];
    
    _bottomBar.backgroundColor = barBackgroundColor;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    DDLogWarn(@"IgngihtWebView: Received Memmory Warning");
}

#pragma mark - IgnightTopBarDelegate


- (void)revealToggle:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)goBack:(id)sender {
    [_webView goBack];
}

- (IBAction)goForward:(UIButton *)sender {
    [_webView goForward];
}

-(void)refresh
{
    [_webView reload];
}

-(void)done
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:_webView animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:_webView animated:YES];
}

@end
