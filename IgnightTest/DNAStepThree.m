//
//  DNAStepThree.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/2/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "DNAStepThree.h"
#import "RankToSelection.h"
#import "AtmosphereTypes.h"
#import "UserInfo.h"

@interface DNAStepThree ()

@end

@implementation DNAStepThree
{
    RankToSelection *rankToSelection;
    UserInfo *userInfo;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    rankToSelection = [[RankToSelection alloc] initWithMaxSelections:3 minSelections:1];
    userInfo = [UserInfo getInstance];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"step_n_bg"]];
    self.collectionView.backgroundView.alpha = .15;
    [self.collectionView reloadData];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.allowsMultipleSelection = YES;
    
    self.continueBtn.backgroundColor = whiteFooterBackground;
    [self.continueBtn setTitleColor:grayHeaderFooterText forState:UIControlStateNormal];
    [[self.continueBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[self.continueBtn layer] setBorderWidth:0.3];
    
    self.headerLabel.backgroundColor = whiteHeaderBackground;
    [self.headerLabel setTextColor:grayHeaderFooterText];
    [[self.headerLabel layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[self.headerLabel layer] setBorderWidth:0.3];
    
    [self adjustView];
    [self addSwipeGesture];
}

-(void)addSwipeGesture
{
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [gesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:gesture];
}

-(void)handleSwipe: (UISwipeGestureRecognizer *)recognizer
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    DDLogDebug(@"MEMORY WARNING: DNASTEPTWO");
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [rankToSelection isRankAvailable];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [self setImageForCell:cell forRow:indexPath.row];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [self removeImageForCell:cell forRow:indexPath.row];
}

-(void)setImageForCell: (UICollectionViewCell *)cell forRow: (NSInteger)row
{
    NSInteger rank = [rankToSelection addToSelection:[NSNumber numberWithInteger:row]];
    [self setRank:[NSNumber numberWithInteger:rank] forCell:cell];
}

-(void)setRank: (NSNumber *)rank forCell: (UICollectionViewCell *)cell
{
    UILabel *label = (UILabel *)[cell viewWithTag:110];
    label.text = [NSString stringWithFormat:@"%@", rank];
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:60]];
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
    cover.alpha = 0.4;
    
    [self adjustView];
}

-(void)removeImageForCell: (UICollectionViewCell*)cell forRow: (NSInteger)row
{
    [rankToSelection removeFromSelection:[NSNumber numberWithInteger:row]];
    UILabel *label = (UILabel *)[cell viewWithTag:110];
    label.text = @"";
    if ([rankToSelection isRankAvailable])
        self.collectionView.alpha = 1;
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
    cover.alpha = 0;
    
    [self adjustView];
}

-(void)adjustView
{
    if ([rankToSelection selectedMinSelections]) {
        self.continueBtn.alpha = 1;
        self.continueBtn.userInteractionEnabled = YES;
        [self saveUserInformation];
        [self.continueBtn setTitleColor:registrationContinueBtnColor forState:UIControlStateNormal];
    } else {
        self.continueBtn.alpha = 0.4;
        self.continueBtn.userInteractionEnabled = NO;
        [self.continueBtn setTitleColor:grayHeaderFooterText forState:UIControlStateNormal];
    }
}

-(void)saveUserInformation
{
    [userInfo resetAtmosphere];
    for (int i = 0; i < [rankToSelection count]; i++) {
        NSNumber *selectionId = [rankToSelection selectionAtRank:(i + 1)];
        [userInfo setAtmosphereDna:selectionId atSelection:i];
    }
}

#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return ATMOSPHERE_COUNT;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"atmosphereOption" forIndexPath:indexPath];

    UILabel *name = (UILabel*)[cell viewWithTag:100];
    name.text = [AtmosphereTypes getAtmosphereNameFromId:(Atmosphere)indexPath.row];
    name.textAlignment = NSTextAlignmentCenter;
    name.numberOfLines = 2;
    name.textColor = venueListText;
    name.font = [UIFont systemFontOfSize:14];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:101];
    NSString *imageName = [AtmosphereTypes getAtmosphereNameFromId:(Atmosphere)indexPath.row];
    imageName = [[imageName lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    imageView.image = [UIImage imageNamed:imageName];
    [[imageView layer] setCornerRadius:imageView.frame.size.width/2];
    [[imageView layer] setMasksToBounds:YES];
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
    [[cover layer] setCornerRadius:cover.frame.size.width/2];
    [[cover layer] setMasksToBounds:YES];
    
    NSNumber *rank = [rankToSelection rankForSelection:[NSNumber numberWithLong:indexPath.row]];
    if (rank != nil) {
        [self setRank:rank forCell:cell];
    } else {
        imageView.alpha = 1;
        
        UILabel *label = (UILabel *)[cell viewWithTag:110];
        label.text = @"";
        
        UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
        cover.alpha = 0;
    }
    
    return cell;
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
