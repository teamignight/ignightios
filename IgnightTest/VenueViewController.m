//
//  VenueViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/22/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "VenueViewController.h"
#import "VenueTabBarController.h"
#import "SWRevealViewController.h"
#import "MusicTypes.h"
#import "AtmosphereTypes.h"
#import "SpendingTypes.h"
#import "AgeBuckets.h"
#import "VenueAnnotation.h"
#import "VenueInfoViewController.h"
#import "TrendingDataCache.h"
#import "BuzzViewController.h"
#import "ImageViewViewController.h"
#import "FSBasicImage.h"
#import "BuzzImageData.h"
#import "FSBasicImageSource.h"
#import "FSImageViewerViewController.h"

@interface VenueViewController ()

@end

@implementation VenueViewController
{
    VenueTabBarController *tabBar;
    VenueInfoViewController *venueInfoController;
    BuzzViewController *buzzViewController;
    ImageViewViewController *imageViewController;
    
    IgnightTopBar *topBar;
    VenueHttpClient *venueClient;
    UserInfo *userInfo;
    TrendingDataCache *trendingDataCache;
    
    CLLocationManager *locationManager;
    BOOL loaded;
    BOOL buzzActive;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    userInfo = [UserInfo getInstance];
    topBar = [[IgnightTopBar alloc] initWithTitle:_vName withSubtitle:@"Info" withSideBarButtonTarget:(self.goBack) ? self : self.revealViewController];
    if (self.goBack) {
        [topBar setLeftButtonBackgroundImage:[UIImage imageNamed:@"back_gray"]];
    }
    [self.titleBar addSubview:topBar];
    [self.titleBar setUserInteractionEnabled:YES];
    
    venueClient = [VenueHttpClient getInstance];
    
    trendingDataCache = [TrendingDataCache getInstance];
    
    [self setupLocationServices];
    
    CGColorRef borderColor = tabBorder.CGColor;
    CGFloat borderWidth = 0.3;

    [self setUpSideBarButton];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    [buzzViewController hideKeyboard];
}

- (void)setUpSideBarButton
{
    self.revealViewController.delegate = self;
    if (self.revealViewController != nil) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupPushNotificationObserver];
    [locationManager startUpdatingLocation];
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.title = @"";
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removePushNotificationObserver];
    [locationManager stopUpdatingLocation];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setupLocationServices
{
    //Setup Location Manager For MapView
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [locationManager startUpdatingLocation];
}

- (void)revealToggle:(id)sender
{
    [venueInfoController removeFromParentViewController];
    [buzzViewController removeFromParentViewController];
    [imageViewController removeFromParentViewController];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getVenueInfo
{
    if (_tabIndex == 0) {
        DDLogDebug(@"Getting venue info...");
        if (!loaded) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            loaded = YES;
        }
        venueClient.delegate = self;
        [venueClient getVenueInfo:_venueId];
    }
}

#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[VenueTabBarController class]]) {
        tabBar = (VenueTabBarController *)segue.destinationViewController;
        venueInfoController = [[tabBar viewControllers] objectAtIndex:0];
        venueInfoController.venueController = self;
    }
}

#pragma mark - VenueHttpClientDelegate

-(void)venueHttpClient:(VenueHttpClient *)client gotVenueInfo:(NSDictionary *)response
{
    BOOL res = [[response objectForKey:@"res"] boolValue];
    if (res) {
        NSDictionary *data = [response objectForKey:@"body"];
        NSDictionary *venue = [data objectForKey:@"venue"];
        _vName = [venue objectForKey:@"name"];
        topBar.mainTitle.text = _vName;
        NSString *venueAddress = [venue objectForKey:@"address"];
        NSString *phoneNumber = [[[venue objectForKey:@"phoneNumber"] componentsSeparatedByCharactersInSet:
                                  [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                 componentsJoinedByString:@""];
        NSString *websiteUrl = [venue objectForKey:@"url"];
        
        VenueData *venueData = [[VenueData alloc] initWithData:data];
        venueInfoController.venueData = venueData;

        [venueInfoController setVenueEventInfo: [data objectForKey:@"venueEventInfo"]];

        NSDictionary *venueStats = [data objectForKey:@"venueStats"];
        venueInfoController.musicTrendingId = [[venueStats objectForKey:@"musicTypeMode"] intValue];
        venueInfoController.atmosphereTrenidngId = [[venueStats objectForKey:@"atmosphereTypeMode"] intValue];
        venueInfoController.ageBucketId = [[venueStats objectForKey:@"ageBucketMode"] intValue];
        venueInfoController.spendingId = [[venueStats objectForKey:@"avgSpendingLimit"] intValue];
        venueInfoController.trendingGroups = [data objectForKey:@"trendingGroups"];
        venueInfoController.venueId = self.venueId;
        venueInfoController.phoneNumber = phoneNumber;
        venueInfoController.website = websiteUrl;
        venueInfoController.userBarColor = [data objectForKey:@"userBarColor"];
        
        NSInteger userInput = [[data objectForKey:@"userInput"] intValue];
        if (userInput < 0) {
            [self.downVoteBtn setImage:[UIImage imageNamed:@"downvote-on"] forState:UIControlStateNormal];
        } else if (userInput > 0) {
            [self.upVoteBtn setImage:[UIImage imageNamed:@"upvote-on"] forState:UIControlStateNormal];
        }
        
        self.venueName.text = _vName;
        NSNumber *lat = [venue objectForKey:@"latitude"];
        NSNumber *lon = [venue objectForKey:@"longitude"];
        CLLocation *venueLocation = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lon doubleValue]];
        CLLocationDistance dist = [userInfo.userLocation distanceFromLocation:venueLocation];
        NSString *distance = [NSString stringWithFormat:@"%.2f mi", [self convertToMilesFromMeters:dist]];
        self.venueDistance.text = distance;
        
        CLLocationCoordinate2D venueCoordinate = CLLocationCoordinate2DMake([lat floatValue], [lon floatValue]);
        venueInfoController.venueCoordinate = venueCoordinate;
        venueInfoController.venueName = _vName;
        venueInfoController.venueAddres = venueAddress;
        
        if (tabBar.selectedIndex == 0) {
            [venueInfoController setupInitialView];
        }
    } else {
        if ([[response allKeys] containsObject:@"reason"]) {
//            IgAlert(nil, response[@"reason"], nil);
        }
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)venueHttpClient:(VenueHttpClient *)client userActivityChangedTo:(NSInteger)activity forVenueId:(NSNumber *)venueId withResponse:(NSDictionary *)response
{
    BOOL res = [[response objectForKey:@"res"] boolValue];
    if ( res) {
        [trendingDataCache setNewUserInput:activity ForVenue:venueId];
        [venueInfoController.tableView reloadData];
    } else {
//        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)venueHttpClient:(VenueHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    IgAlert(nil, @"Failed to reach server", nil);
}

- (double)convertToMilesFromMeters: (double)distance
{
    return (distance / 1609.344);
}

#pragma mark - CoreLocation

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    if (userInfo.userLocation == nil) {
        userInfo.userLocation = newLocation;
        return;
    }
    
    CLLocationDistance meters = [newLocation distanceFromLocation:oldLocation];
    if (meters > 0.1) {
        userInfo.userLocation = newLocation;
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DDLogDebug(@"Failed to get location manager");
}

#pragma mark - Actions

- (IBAction)upVoteSelected:(id)sender {
    NSInteger userActivity = [trendingDataCache getUserInputForVenue:_venueId];
    if (userActivity != 1) {
        [venueClient userActivityChangedTo:1 forVenueId:_venueId];
    } else
        [venueClient userActivityChangedTo:0 forVenueId:_venueId];
}

- (IBAction)downVoteSelected:(id)sender {
    NSInteger userActivity = [trendingDataCache getUserInputForVenue:_venueId];
    if (userActivity != -1) {
        [venueClient userActivityChangedTo:-1 forVenueId:_venueId];
    } else {
        [venueClient userActivityChangedTo:0 forVenueId:_venueId];
    }
}

- (IBAction)goToInfoView:(id)sender {
    if (tabBar.selectedIndex == 0) {
        return;
    }
    
    tabBar.selectedIndex = 0;
    _tabIndex = 0;
    [self getVenueInfo];
    
    topBar.subTitle.text = @"Info";
    venueInfoController.venueController = self;
}


#pragma mark - ImageViewControllerDelegate / BuzzViewControllerDelegate

-(void)clickedImage:(UIImage *)image
{
    FSBasicImage *photo = [[FSBasicImage alloc] initWithImage:image];
    
    FSBasicImageSource *source = [[FSBasicImageSource alloc] initWithImages:[NSArray arrayWithObject:photo]];
    FSImageViewerViewController *fsImgController = [[FSImageViewerViewController alloc] initWithImageSource:source imageIndex:(NSInteger)0];
    fsImgController.name = _vName;
    
    self.navigationController.navigationBar.topItem.title = @"";
    [self.navigationController pushViewController:fsImgController animated:YES];
}

-(void)clickedImageAtIndex:(NSInteger)row
{
    NSArray* imgList = [imageViewController getImageList];
    if (imgList == nil) {
        return;
    }
    
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (BuzzImageData *data in imgList) {
        FSBasicImage *photo;
        if (data.image != nil) {
            photo = [[FSBasicImage alloc] initWithImage:data.image];
        } else {
            photo = [[FSBasicImage alloc] initWithImageURL:data.imgUrl];
        }
        [images addObject:photo];
    }
    
    FSBasicImageSource *source = [[FSBasicImageSource alloc] initWithImages:images];
    FSImageViewerViewController *fsImgController = [[FSImageViewerViewController alloc] initWithImageSource:source imageIndex:(NSInteger)row];
    fsImgController.delegate = self;
    fsImgController.name = _vName;
    
    self.navigationController.navigationBar.topItem.title = @"";
    [self.navigationController pushViewController:fsImgController animated:YES];
}

#pragma mark - FSImageViewControllerDelegate

-(void)imageViewerViewController:(FSImageViewerViewController *)imageViewerViewController reportImageAtIndex:(NSInteger)index
{
    NSArray* imgList = [imageViewController getImageList];
    BuzzImageData *data = imgList[index];
    BuzzHttpClient *client = [BuzzHttpClient getInstance];
    [client flagBuzz:data.buzzId inVenue:_venueId];
}

#pragma mark - Remote Notifications

-(void)setupPushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewMailPush:) name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewGroupBuzz:) name:@"NewGroupBuzzNotification" object:nil];
}

-(void)removePushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewGroupBuzzNotification" object:nil];
}

-(void)handleNewMailPush: (NSNotification *)notification
{
    GroupViewController *groupView = [self.storyboard instantiateViewControllerWithIdentifier:@"allGroupView"];
    groupView.tabIndex = 2;
    [self.navigationController pushViewController:groupView animated:YES];
}


-(void)handleNewGroupBuzz: (NSNotification *)notification
{
    NSDictionary *data = notification.userInfo;
    Trending *trending = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
    trending.goBack = NO;
    trending.groupMode = YES;
    trending.groupName = data[@"groupName"];
    trending.groupId = data[@"groupId"];
    trending.tabIndex = 1;
    [self.navigationController pushViewController:trending animated:YES];
}



@end
