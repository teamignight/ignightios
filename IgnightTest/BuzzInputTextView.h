//
//  BuzzInputTextView.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 3/11/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@protocol BuzzInputTextViewDelegate;

@interface BuzzInputTextView : UIView <UITextViewDelegate, HPGrowingTextViewDelegate>

@property (nonatomic, assign) id<BuzzInputTextViewDelegate> delegate;

-(void)resignKeyboard;

@end

@protocol BuzzInputTextViewDelegate <NSObject>

@optional

//- (BOOL)textViewShouldBeginEditing:(UITextView *)textView;
//- (BOOL)textViewShouldEndEditing:(UITextView *)textView;
//
//- (void)textViewDidBeginEditing:(UITextView *)textView;
//- (void)textViewDidEndEditing:(UITextView *)textView;
//
//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
//- (void)textViewDidChange:(UITextView *)textView;
//
//- (void)textViewDidChangeSelection:(UITextView *)textView;
//
//- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange NS_AVAILABLE_IOS(7_0);
//- (BOOL)textView:(UITextView *)textView shouldInteractWithTextAttachment:(NSTextAttachment *)textAttachment inRange:(NSRange)characterRange NS_AVAILABLE_IOS(7_0);

@required

-(void)sendInput: (NSString *)text;
-(void)textViewChangingHeight: (CGFloat)height toHeight: (CGFloat)newHeight;
-(void)goToCamera;

@end
