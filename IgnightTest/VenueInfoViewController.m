//
//  VenueInfoViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/22/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "SWRevealViewController.h"
#import "VenueInfoViewController.h"
#import "VenueAnnotation.h"
#import "TrendingCell.h"
#import "TrendingDataCache.h"
#import "Groups.h"
#import "GroupDetailsViewController.h"
#import "VenueHttpClient.h"
#import "CityTrendingTableViewCell.h"
#import "VenueMetaDataTableViewCell.h"

@interface VenueInfoViewController ()

@end

@implementation VenueInfoViewController
{
    VenueHttpClient *venueClient;
    UserInfo *userInfo;
    Groups *groups;
    TrendingDataCache *trendingDataCache;
    
    MKMapView *tMapView;
    
    NSString *venueEventInfo;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    venueClient = [VenueHttpClient getInstance];
    venueClient.delegate = self;

    tMapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 320, 168)];
    tMapView.delegate =self;
    
    userInfo = [UserInfo getInstance];
    groups = [Groups getInstance];
    trendingDataCache = [TrendingDataCache getInstance];

    _musicTrendingId = -1;
    _atmosphereTrenidngId = -1;
    _spendingId = -1;
    _ageBucketId = -1;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [_venueController getVenueInfo];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (tMapView == nil) {
        tMapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 320, 168)];
        tMapView.delegate =self;
    }
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView reloadData];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self purgeMapMemmory];
}

-(void)purgeMapMemmory
{
    tMapView.delegate = nil;
    tMapView.mapType = MKMapTypeStandard;
    [tMapView removeFromSuperview];
    tMapView = nil;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - setupInitialView

-(void)setupInitialView
{
    [self setupMapWithVenueAnnotation];
    [[self tableView] reloadData];
    venueClient.delegate = self;
}

-(void)setupMapWithVenueAnnotation
{
    [self setupRegionForMap];
    
    VenueAnnotation *ann = [[VenueAnnotation alloc] init];
    ann.title = self.venueName;
    ann.coordinate = self.venueCoordinate;
    ann.userVenueValue =  [NSNumber numberWithInt:(4 - [self.userBarColor intValue])];

    [tMapView addAnnotation:ann];
}

-(void)setupRegionForMap
{
    MKCoordinateSpan span = MKCoordinateSpanMake(0.005f, 0.005f);
    MKCoordinateRegion region = MKCoordinateRegionMake(self.venueCoordinate, span);
    region.center = CLLocationCoordinate2DMake(self.venueCoordinate.latitude, self.venueCoordinate.longitude);

    [tMapView setRegion:region animated:NO];
}

#pragma mark - MKMapViewDelegate

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[VenueAnnotation class]]) {
        VenueAnnotation *vAnnotation = annotation;
        
        MKAnnotationView *annView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"venuePin"];
        if (annView == nil) {
            annView = [[MKPinAnnotationView alloc] initWithAnnotation:vAnnotation reuseIdentifier:@"venuePins"];
        }
        
        NSInteger val = [vAnnotation.userVenueValue integerValue];
        if (val > 4)
            val = 4;
        
        annView.image = [UIImage imageNamed:[NSString stringWithFormat:@"map_pin_%ld.png", (long)val]];
        
        return annView;
    }
    return nil;
}

#pragma mark - UITableViewDelegate

-(BOOL)isAdmin
{
    return ([[userInfo.userName lowercaseString] isEqualToString:@"ignight"]);
}

-(BOOL)showVenueEventInfo
{
    return (venueEventInfo != nil && ![venueEventInfo isEqualToString:@""]);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)     //Map
        return CITY_TRENDING_CELL_HEIGHT;
    else if (indexPath.section == 1) {    //Trending Cell Bar
        if ([self isAdmin]) {
            return 200;
        } else if ([self showVenueEventInfo]) {
            UITextView *txView = [[UITextView alloc] init];
            txView.text = venueEventInfo;
            txView.textAlignment = NSTextAlignmentCenter;
            [txView sizeToFit];
            CGFloat height = txView.frame.size.height;
            return height;
        }
        return 0;
    } else if (indexPath.section == 2) {    //Info
        return VENUE_META_DATA_CELL_HEIGHT;
    } else if (indexPath.section == 3) {  //Trending Crowd
        if (indexPath.row == 0)
            return 50;
        return 110;
    } else if (indexPath.section == 4) { //Trending Groups
        if (indexPath.row == 0)
            return 50;
        return 40;
    }
    return 0;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.trendingGroups count] > 0)
        return 5;
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        if ([self isAdmin]) {
            return 1;
        }
        if ([self showVenueEventInfo]) {
            return 1;
        }
        return 0;
    } else if (section == 2) {
        return 1;
    } else if (section == 3) {
        if (self.musicTrendingId >= 0) {
            return 2;
        }
        return 1;
    } else if (section == 4) {
        NSInteger count = [_trendingGroups count];
        return (count > 0) ? (count + 1) : 0;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        CityTrendingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VenueImagesCell"];
        if (cell == nil) {
            cell = [[CityTrendingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VenueImagesCell"];
        }
        
        [cell resetWithVenueData:_venueData];
        cell.delegate = self;
        return cell;
//        cell = [tableView dequeueReusableCellWithIdentifier:@"mapCell"];
//        if (cell == nil) {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mapCell"];
//            [tMapView reloadInputViews];
//            [tMapView setTag:500];
//            [tMapView setUserInteractionEnabled:NO];
//            [cell.contentView addSubview:tMapView];
//        } else {
//            MKMapView *mapView = (MKMapView *)[cell viewWithTag:500];
//            if (mapView == nil) {
//                [tMapView reloadInputViews];
//                [tMapView setTag:500];
//                [tMapView setUserInteractionEnabled:NO];
//                [tMapView reloadInputViews];
//                [self setupMapWithVenueAnnotation];
//                [cell.contentView addSubview:tMapView];
//            } else
//                [mapView reloadInputViews];
//        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    } else if (indexPath.section == 1) {
        if ([self isAdmin]) {
            cell = [self setupAdminInputForTableView:tableView tableViewCell:cell];
//        cell = [self tableView:tableView tableViewCell:cell setupTrendingCellForVenueInRow:indexPath.row];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else if ([self showVenueEventInfo]) {
            cell = [self setupVenueEventInfoForTableView:tableView tableViewCell:cell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    } else if (indexPath.section == 2) {
        VenueMetaDataTableViewCell *venueInfoCell = [tableView dequeueReusableCellWithIdentifier:@"venue_info"];
        if (venueInfoCell == nil) {
            venueInfoCell = [[VenueMetaDataTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"venue_info"];
        }
        [venueInfoCell updateWithAddress:self.venueAddres withNumber:self.phoneNumber withWebsite:self.website];
        venueInfoCell.delegate = self;
        return venueInfoCell;
//        
//        cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell"];
//        if (cell == nil) {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"infoCell"];
//            UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 20, 20)];
//            iconView.tag = 100;
//            [cell.contentView addSubview:iconView];
//            
//            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, 260, 40)];
//            [lbl setFont:[UIFont systemFontOfSize:12.0]];
//            [lbl setTextColor:reportHeaderText];
//            lbl.tag = 101;
//            [cell.contentView addSubview:lbl];
//        }
//        UIImage *icon;
//        
//        UIImageView *iconView = (UIImageView *)[cell.contentView viewWithTag:100];
//        if (indexPath.row == 0) {
//            iconView.frame = CGRectMake(17.5, 7.5, 25, 25);
//        } else if (indexPath.row == 1) {
//            iconView.frame = CGRectMake(21, 7.5, 17.5, 25);
//        } else if (indexPath.row == 2) {
//            iconView.frame = CGRectMake(17.5, 7.5, 25, 25);
//        } else if (indexPath.row == 3) {
//            iconView.frame = CGRectMake(28, 7.5, 6, 25);
//        }
//        
//        UILabel *lbl = (UILabel *)[cell.contentView viewWithTag:101];
//        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        
//        if (indexPath.row == 0) {
//            icon = [UIImage imageNamed:@"venue_info_address"];
//            lbl.text = self.venueAddres;
//            if ([self.venueAddres isEqualToString:@""])
//                icon = nil;
//        } else if (indexPath.row == 1) {
//            icon = [UIImage imageNamed:@"venue_info_phone"];
//            lbl.text = [IgnightUtil formatPhoneNumber:self.phoneNumber];
//            if ([self.phoneNumber isEqualToString:@""])
//                icon = nil;
//        } else if (indexPath.row == 2) {
//            icon = [UIImage imageNamed:@"venue_info_website"];
//            lbl.text = self.website;
//            if ([self.website isEqualToString:@""])
//                icon = nil;
//        } else if (indexPath.row == 3) {
//            icon = [UIImage imageNamed:@"venue_info_report"];
//            lbl.text = @"Report an error";
//        }
//        
//        [iconView setImage:icon];
//        cell.contentView.backgroundColor = venueListBackgrond;
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            cell = [self tableView:tableView cellForHeaderAtIndexPath:indexPath];
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"trendingCrowdCell"];
            Music musicIndex = (Music)self.musicTrendingId;
            AgeBucket ageIndex = (AgeBucket)self.ageBucketId;
            Atmosphere atmosphereIndex = (Atmosphere)self.atmosphereTrenidngId;
            Spending spendingIndex = (Spending)self.spendingId;
            if (cell == nil) {
                NSInteger labelHeight = 29;
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"trendingCrowdCell"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                //Setup Music Cell
                {
                    UIImageView *music = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 70,70)];
                    music.tag = 200;
                    NSString *musicNameImg = [[[MusicTypes getMusicNameFromId:musicIndex] stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
                    [music setImage:[UIImage imageNamed:musicNameImg]];
                    [[music layer] setCornerRadius:music.frame.size.width/2];
                    [[music layer] setMasksToBounds:YES];
                    [cell.contentView addSubview:music];

                    UILabel *mName = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, 80, labelHeight)];
                    mName.text = [MusicTypes getMusicNameFromId:musicIndex];
                    mName.numberOfLines = 2;
                    [mName setFont:[UIFont systemFontOfSize:12]];
                    [mName setTextAlignment:NSTextAlignmentCenter];
                    [mName setTextColor:reportHeaderText];
                    [mName setTag:100];
                    [cell.contentView addSubview:mName];
                }
                
                //Setup Atmosphere Cell
                {
                    UIImageView *atmosphere = [[UIImageView alloc] initWithFrame:CGRectMake(85, 5, 70, 70)];
                    atmosphere.tag = 201;
                    NSString *atmosphereNameImg = [[[AtmosphereTypes getAtmosphereNameFromId:atmosphereIndex] stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
                    [atmosphere setImage:[UIImage imageNamed:atmosphereNameImg]];
                    [[atmosphere layer] setCornerRadius:atmosphere.frame.size.width/2];
                    [[atmosphere layer] setMasksToBounds:YES];
                    
                    [cell.contentView addSubview:atmosphere];
                    
                    UILabel *aName = [[UILabel alloc] initWithFrame:CGRectMake(80, 80, 80, labelHeight)];
                    aName.text = [AtmosphereTypes getAtmosphereNameFromId:atmosphereIndex];
                    aName.numberOfLines = 2;
                    [aName setFont:[UIFont systemFontOfSize:12]];
                    [aName setTextAlignment:NSTextAlignmentCenter];
                    [aName setTextColor:reportHeaderText];
                    [aName setTag:101];
                    [cell.contentView addSubview:aName];
                }
                
                //Setup Age Cell
                {
                    //TODO: Normalize the age image to be the same as other DNA Images.
                    //Had to change the frame for it to look the same
                    UIImageView* age = [[UIImageView alloc] initWithFrame:CGRectMake(162.5, 2.5, 75, 75)];
                    age.tag = 202;
                    [age setImage:[UIImage imageNamed:[NSString stringWithFormat:@"dna_age_%u", (ageIndex + 1)]]];
                    [cell.contentView addSubview:age];

                    UILabel *ageName = [[UILabel alloc] initWithFrame:CGRectMake(160, 80, 80, labelHeight)];
                    ageName.text = [AgeBuckets getAgeBucketNameFromId:ageIndex];
                    ageName.numberOfLines = 2;
                    [ageName setFont:[UIFont systemFontOfSize:12]];
                    [ageName setTextColor:reportHeaderText];
                    [ageName setTextAlignment:NSTextAlignmentCenter];
                    [ageName setTag:102];
                    [cell.contentView addSubview:ageName];
                }
                
                //Setup Spending Cell
                {
                    UIImageView *spending = [[UIImageView alloc] initWithFrame:CGRectMake(245, 5, 70, 70)];
                    spending.tag = 203;
                    NSString *spendingNameImg = [[[SpendingTypes getSpendingNameFromId:spendingIndex] stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
                    [spending setImage:[UIImage imageNamed:spendingNameImg]];
                    [[spending layer] setCornerRadius:spending.frame.size.width/2];
                    [[spending layer] setMasksToBounds:YES];
                    [cell.contentView addSubview:spending];
                    
                    UILabel *sName = [[UILabel alloc] initWithFrame:CGRectMake(240, 80, 80, labelHeight)];
                    sName.text = [SpendingTypes getSpendingNameFromId:spendingIndex];
                    sName.numberOfLines = 2;
                    [sName setFont:[UIFont systemFontOfSize:12]];
                    [sName setTextAlignment:NSTextAlignmentCenter];
                    [sName setTextColor:reportHeaderText];
                    [sName setTag:103];
                    [cell.contentView addSubview:sName];
                }
                
                [cell.contentView setBackgroundColor:venueListBackgrond];
            } else {
                //Setup Music
                {
                    UIImageView *music = (UIImageView *)[cell viewWithTag:200];
                    NSString *musicNameImg = [[[MusicTypes getMusicNameFromId:musicIndex] stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
                    [music setImage:[UIImage imageNamed:musicNameImg]];
                    UILabel *mName = (UILabel *)[cell.contentView viewWithTag:100];
                    mName.text = [MusicTypes getMusicNameFromId:musicIndex];
                }
                //Setup Atmosphere
                {
                    UIImageView *atmosphere = (UIImageView *)[cell viewWithTag:201];
                    NSString *atmosphereNameImg = [[[AtmosphereTypes getAtmosphereNameFromId:atmosphereIndex] stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
                    [atmosphere setImage:[UIImage imageNamed:atmosphereNameImg]];
                    UILabel *aName = (UILabel *)[cell.contentView viewWithTag:101];
                    aName.text = [AtmosphereTypes getAtmosphereNameFromId:atmosphereIndex];
                }
                //Setup Age
                {
                    UIImageView *age = (UIImageView *)[cell viewWithTag:202];
                    NSString *ageNameImg = [NSString stringWithFormat:@"dna_age_%u", (ageIndex + 1)];
                    [age setImage:[UIImage imageNamed:ageNameImg]];
                    UILabel *ageName = (UILabel *)[cell.contentView viewWithTag:102];
                    ageName.text = [AgeBuckets getAgeBucketNameFromId:ageIndex];
                }
                //Setup Spending
                {
                    UIImageView *spending = (UIImageView *)[cell viewWithTag:203];
                    NSString *spendingNameImg = [[[SpendingTypes getSpendingNameFromId:spendingIndex] stringByReplacingOccurrencesOfString:@" " withString:@"_"] lowercaseString];
                    [spending setImage:[UIImage imageNamed:spendingNameImg]];
                    UILabel *sName = (UILabel *)[cell.contentView viewWithTag:103];
                    sName.text = [SpendingTypes getSpendingNameFromId:spendingIndex];
                }
            }
        }
    } else if (indexPath.section == 4) {
        if (indexPath.row == 0) {
            cell = [self tableView:tableView cellForHeaderAtIndexPath:indexPath];
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"trendingGroupCell"];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"trendingGroupCell"];
                UIImageView *groupType = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 23, 20)];
                groupType.tag = 101;
                [cell.contentView addSubview:groupType];
                
                UILabel *trendingGroup = [[UILabel alloc] initWithFrame:CGRectMake(45, 0, 275, 40)];
                trendingGroup.font = [UIFont systemFontOfSize:14];
                trendingGroup.textColor = reportHeaderText;
                trendingGroup.tag = 100;
                [cell.contentView addSubview: trendingGroup];
                cell.contentView.backgroundColor = venueListBackgrond;
            }
            UILabel *trendingGroup = (UILabel *)[cell.contentView viewWithTag:100];
            NSDictionary *g = [_trendingGroups objectAtIndex:indexPath.row -1];
            trendingGroup.text = [g objectForKey:@"groupName"];
            
            NSDictionary *group = [_trendingGroups objectAtIndex:indexPath.row - 1];
            BOOL isPrivateGroup = [[group objectForKey:@"privateGroup"] boolValue];
            UIImageView *imgView = (UIImageView *)[cell viewWithTag:101];
            NSString *imgName;
            CGRect frame = CGRectZero;
            if (isPrivateGroup) {
                frame = CGRectMake(15, 10, 20, 20);
                imgName = @"venue_private_group_icon";
            } else {
                frame = CGRectMake(13, 10, 22, 20);
                imgName = @"venue_public_group_icon";
            }
            imgView.frame = frame;
            imgView.image =  [UIImage imageNamed:imgName];
        }
    }
    return cell;
}

- (UITableViewCell *)tableView: (UITableView *)tableView cellForHeaderAtIndexPath: (NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"headerCell"];
        UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 50)];
        header.tag = 100;
        header.font = [UIFont systemFontOfSize:18];
        [header setTextColor:reportHeaderText];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.contentView addSubview:header];
        [cell setBackgroundColor:venueSectionHeaderBackground];
    }
    
    UILabel *header = (UILabel *)[cell viewWithTag:100];
    if (indexPath.section == 3) {
        if (_musicTrendingId < 0) {
            header.text = @"No one is trending here yet.";
            header.textAlignment = NSTextAlignmentCenter;
        } else {
            header.text = @"Trending Crowd";
            header.textAlignment = NSTextAlignmentLeft;
        }
    } else if (indexPath.section == 4) {
        header.text = @"Trending Groups";
    }
    
    return cell;
}

-(UITableViewCell *)setupVenueEventInfoForTableView:(UITableView *)tableView tableViewCell: (UITableViewCell *)cell
{
    cell = [tableView dequeueReusableCellWithIdentifier:@"venueEventInfo"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"venueEventInfo"];
        UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, cell.contentView.frame.size.height)];
        textView.tag = 100;
        textView.textAlignment = NSTextAlignmentCenter;
        [textView setFont:[UIFont systemFontOfSize:12]];
        [textView setUserInteractionEnabled:NO];
        [textView setEditable:NO];
        [cell.contentView addSubview:textView];
    }
    
    UITextView *textView = (UITextView *)[cell.contentView viewWithTag:100];
    textView.frame = CGRectMake(0, 0, screenWidth, cell.contentView.frame.size.height);
    textView.text = venueEventInfo;
    
    return cell;
}

-(UITableViewCell *)setupAdminInputForTableView:(UITableView *)tableView tableViewCell: (UITableViewCell *)cell
{
    cell = [tableView dequeueReusableCellWithIdentifier:@"adminCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"adminCell"];
        UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 160)];
        textView.tag = 100;
        [textView setBackgroundColor:[UIColor blueColor]];
        [textView setFont:[UIFont systemFontOfSize:12]];
        [textView setTextColor:[UIColor whiteColor]];
        [cell.contentView addSubview:textView];
        
        UIButton *submit = [UIButton buttonWithType:UIButtonTypeCustom];
        submit.tag = 200;
        submit.frame = CGRectMake(0, 160, screenWidth, 40);
        [submit setTitle:@"Submit" forState:UIControlStateNormal];
        [submit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [submit setBackgroundColor:[UIColor blackColor]];
        [submit addTarget:self action:@selector(submitAdminInput:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview: submit];
    }
    
    UITextView *textView = (UITextView *)[cell.contentView viewWithTag:100];
    textView.text = venueEventInfo;
    
    return cell;
}

-(IBAction)submitAdminInput:(id)sender
{
    UITextView *text = (UITextView *)[[sender superview] viewWithTag:100];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [venueClient sendVenue:_venueId dailyEventInfo:text.text];
}

-(void)venueHttpClientSubmittedVenueEventInfo: (VenueHttpClient *)client
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView tableViewCell:(UITableViewCell *)cell setupTrendingCellForVenueInRow: (NSInteger)row
{
    cell = [tableView dequeueReusableCellWithIdentifier:@"trendingCell"];
    if (cell == nil) {
        cell = [[TrendingCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"trendingCell"];
    }
    
    CLLocation *venLoc = [[CLLocation alloc] initWithLatitude:self.venueCoordinate.latitude longitude:self.venueCoordinate.longitude];
    
    CLLocationDistance dist = [userInfo.userLocation distanceFromLocation:venLoc];
    
    NSInteger userInput = [trendingDataCache getUserInputForVenue:self.venueId];
    
    TrendingCell *tCell = (TrendingCell*)cell;
    [tCell setName:[self venueName]
      withDistance:(dist) ? [NSString stringWithFormat:@"%.2f miles", [self convertToMilesFromMeters:dist]] : nil
      withDnaValue:self.userBarColor
     withUserInput:userInput
        forVenueId:self.venueId];
    ((TrendingCell *)cell).delegate = self;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2) {
//        if (indexPath.row == 0) {
//            NSString *address = [NSString stringWithFormat:@"%@, %@, IL", self.venueAddres, [Cities getCityNameFromId:(City)[userInfo.cityId integerValue]]];
//            NSString *url = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%@",
//                             userInfo.userLocation.coordinate.latitude,
//                             userInfo.userLocation.coordinate.longitude,
//                             [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//        } else if (indexPath.row == 1) {
//            NSString *phoneNo = self.phoneNumber;
//            NSURL *phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", phoneNo]];
//            [[UIApplication sharedApplication] openURL:phoneUrl];
//        } else if (indexPath.row == 2) {
//            if (![self.website isEqualToString:@""]) {
//                IgnightWebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"web"];
//                webView.website = _website;
//                webView.venueName = _venueName;
//                [self.navigationController pushViewController:webView animated:YES];
//            }
//        } else if (indexPath.row == 3) {
//            [self performSegueWithIdentifier:@"editVenue" sender:self];
//        }
    } else if (indexPath.section == 4) {
        if (indexPath.row != 0) {
            NSDictionary *group = [_trendingGroups objectAtIndex:indexPath.row - 1];
            DDLogDebug(@"Selected Group: %@", group);
            NSNumber *groupId = [group objectForKey:@"groupId"];
            BOOL isMember = [[group objectForKey:@"isMemberOfGroup"] boolValue];
            BOOL isPrivateGroup = [[group objectForKey:@"privateGroup"] boolValue];
            if (isMember) {
//                [self.revealViewController.rearViewController performSegueWithIdentifier:@"groups" sender:groupId];
                Trending *trendingView = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
                trendingView.goBack = YES;
                trendingView.groupId = groupId;
                trendingView.groupMode = YES;
                trendingView.groupName = [group objectForKey:@"groupName"];
                [self.navigationController pushViewController:trendingView animated:YES];
            } else {
                GroupDetailsViewController *grpDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"groupDetails"];
                grpDetails.groupId = groupId;
                grpDetails.allowToJoin = !isPrivateGroup;
                [self.navigationController pushViewController:grpDetails animated:YES];
            }
        }
    }
}

-(void)goToWebsite
{
    if (![self.website isEqualToString:@""]) {
        IgnightWebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"web"];
        webView.website = _website;
        webView.venueName = _venueName;
        [self.navigationController pushViewController:webView animated:YES];
    }
}

-(void)goToErrorScreen
{
     [self performSegueWithIdentifier:@"editVenue" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *identifier = segue.identifier;
    if ([identifier isEqualToString:@"editVenue"]) {
        EditVenueViewController *controller = (EditVenueViewController *)(segue.destinationViewController);
        controller.venueName = _venueName;
        controller.venueAddress = _venueAddres;
        controller.venuePhone = _phoneNumber;
        controller.venueWebsite = _website;
        controller.venueId = _venueId;
    }
}

#pragma mark - Util

- (double)convertToMilesFromMeters: (double)distance
{
    return (distance / 1609.344);
}

#pragma mark - TrendingCellDelegate

-(void)cityTrendingCell: (CityTrendingTableViewCell *)cell updateUserInputMode: (UserInputMode)mode
{
    if (mode == UPVOTED) {
        [_venueController upVoteSelected:nil];
    } else if (mode == DOWNVOTED) {
        [_venueController downVoteSelected:nil];
    } else {
        NSInteger prevUserActivity = [trendingDataCache getUserInputForVenue:_venueId];
        if (prevUserActivity > 0) {
            [_venueController upVoteSelected:nil];
        } else {
            [_venueController downVoteSelected:nil];
        }
    }
}

- (void)userActivityChangedTo:(NSInteger)userActivity forVenueId: (NSNumber *)venueId atIndexPath:(NSIndexPath *)indexPath
{
    if (userActivity > 0) {
        [_venueController upVoteSelected:nil];
    } else if (userActivity < 0) {
        [_venueController downVoteSelected:nil];
    } else {
        NSInteger prevUserActivity = [trendingDataCache getUserInputForVenue:_venueId];
        if (prevUserActivity > 0) {
            [_venueController upVoteSelected:nil];
        } else {
            [_venueController downVoteSelected:nil];
        }
    }
}

-(void)setVenueEventInfo:(NSString *)venueEventInfo
{
    self->venueEventInfo = venueEventInfo;
}


@end
