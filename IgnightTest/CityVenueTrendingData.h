//
//  CityVenueTrendingData.h
//  Ignight
//
//  Created by Abhinav Chordia on 1/20/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import <Foundation/Foundation.h>

enum
{
    DOWNVOTED = -1,
    OFF = 0,
    UPVOTED = 1
};
typedef NSInteger UserInputMode;

enum
{
    GREEN = 0,
    YELLOW = 1,
    ORANGE = 2,
    RED = 3,
    GRAY = 4
};
typedef NSInteger UserBarColor;


@interface CityVenueTrendingData : NSObject

@property NSNumber *venueId;
@property NSString *venueName;
@property UserBarColor userBarColor;;
@property UserInputMode userInput;
@property NSNumber *longitude;
@property NSNumber *latitude;
@property NSString *venueImageUrl;
@property UIImage *venueImage;
@property NSNumber *userVenueValue;

-(id)initWithData: (NSDictionary *)data;

@end
