//
//  VenueData.h
//  Ignight
//
//  Created by Abhinav Chordia on 1/25/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityVenueTrendingData.h"

@interface VenueData : NSObject

@property NSNumber *venueId;
@property NSString *venueName;
@property NSString *address;
@property NSString *phoneNumber;
@property NSString *url;
@property NSNumber *latitude;
@property NSNumber *longitude;
@property NSArray *venueImages;

@property NSArray *trendingGroups;

@property BOOL buzzActive;
@property NSNumber *musicTypeMode;
@property NSNumber *atmosphereTypeMode;
@property NSNumber *ageBucketTypeMode;
@property NSNumber *avgSpendingLimit;
@property UserInputMode userInputMode;
@property UserBarColor userBarColor;

-(id)initWithData: (NSDictionary *)data;

@end
