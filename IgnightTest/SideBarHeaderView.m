//
//  SideBarHeaderView.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/24/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "SideBarHeaderView.h"
#import <QuartzCore/QuartzCore.h>

@implementation SideBarHeaderView
{
//    UIColor *sbBuzzHeaderGradientTop;
//    UIColor *sbBuzzHeaderGradientBottom;
    
    UIImageView *sideBarEdit;
    UIButton *dropDown;
}

-(void)initColors
{
//    sbBuzzHeaderGradientTop = UIColorFromRGB(0x454545);
//    sbBuzzHeaderGradientBottom = sbBuzzHeaderGradientTop;
}

-(id)initWithTitle: (NSString *)title forTarget:(id)target withTag: (NSInteger)tag
{
    CGRect frame = CGRectMake(0, 0, 320, 32);
    self = [super initWithFrame:frame];
    
    if (self) {
        [self initColors];
        
//        // set gradient
//        CAGradientLayer *layer = [CAGradientLayer layer];
//        
//        //define colors and location of gradient
//        layer.frame = frame;
//        layer.colors = @[(id)sbBuzzHeaderGradientTop.CGColor, (id)sbBuzzHeaderGradientBottom.CGColor];
//        
//        // add gradient to background layer of header view
//        [self.layer insertSublayer:layer atIndex:0];

        frame = CGRectMake(20, 0, frame.size.width - 80, frame.size.height);
        
        // build label
        self.label = [[UILabel alloc] initWithFrame:frame];
        self.label.font = [UIFont boldSystemFontOfSize:12.0];
        self.label.textColor = sideBarText;
        self.label.backgroundColor = clearC;
        self.label.text = title;
        [self addSubview:self.label];
        
        self.edit = [UIButton buttonWithType:UIButtonTypeCustom];
        self.edit.frame = CGRectMake(frame.size.width - 60, 0, 80, frame.size.height);
        [self.edit addTarget:target action:@selector(edit:) forControlEvents:UIControlEventTouchUpInside];
        [self.edit setTag:tag];
        [self addSubview:self.edit];
        
        dropDown = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect dropDownFrame = CGRectMake(0, 0, frame.size.width - 20, self.frame.size.height);
        dropDown.frame = dropDownFrame;
        dropDown.tag = tag;
        [dropDown addTarget:target action:@selector(collapse:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:dropDown];
        
        sideBarEdit = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width - 20, 6, 20, 20)];
        sideBarEdit.image = [UIImage imageNamed:@"side_bar_edit"];
        [self addSubview:sideBarEdit];
        self.backgroundColor = sideBarHeaderBackground;
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
