//
//  ImageSelectionController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/23/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageSelectionControllerDelegate;

@interface ImageSelectionController : UIViewController

@property (strong, nonatomic) id<ImageSelectionControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) UIImage *image;
@property (nonatomic) CGSize imageSize;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *useBtn;

- (IBAction)cancelClicked:(id)sender;
- (IBAction)useClicked:(id)sender;

@end

@protocol ImageSelectionControllerDelegate <NSObject>

-(void)userClickedCancel;
-(void)userSelectedImage: (UIImage *)image;

@end
