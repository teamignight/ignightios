
//
//  PopularGroups.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/16/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "PopularGroups.h"

static NSString *idKey = @"groupId";
static NSString *nameKey = @"groupName";
static NSString *memberCountKey = @"noOfMembers";

@implementation PopularGroups

-(id)initWithDictionary: (NSDictionary *)data
{
    self = [super init];
    if (self ) {
        _groupId = data[idKey];
        _name = data[nameKey];
        _memberCount = data[memberCountKey];
    }
    return self;
}

-(id)initWithSearchDictionary: (NSDictionary *)data
{
    self = [super init];
    if (self ) {
        _groupId = data[@"id"];
        _name = data[@"name"];
        _memberCount = data[@"members"];
    }
    return self;
}

@end
