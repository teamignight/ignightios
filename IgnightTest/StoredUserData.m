//
//  StoredUserData.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/21/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "StoredUserData.h"

static NSString *USER_ID = @"userId";
static NSString *IOS_TOKEN_SET = @"iOSTokenSet";

@implementation StoredUserData

-(id)initWithData: (NSDictionary *)data
{
    self = [super init];
    if (self) {
        _userId = data[USER_ID];
        _iOSTokenSet = [data[IOS_TOKEN_SET] boolValue];
    }
    return self;
}

-(NSDictionary *)getData
{
    if (_userId == nil) {
        return nil;
    }
    NSDictionary *data = @{
                           USER_ID : _userId,
                           IOS_TOKEN_SET : [NSNumber numberWithBool:_iOSTokenSet]};
    return data;
}

@end
