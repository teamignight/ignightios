//
//  PushMessageHandler.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "PushMessageHandler.h"

@implementation PushMessageHandler

+(PushMessageHandler *)getInstance
{
    static PushMessageHandler *_instance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _instance = [[PushMessageHandler alloc] init];
    });
    
    return _instance;
}

-(void)gotMessage: (PushMessage *)message
{
    if ([message isKindOfClass:[GroupInvite class]]) {
        [_delegate gotNewGroupInvite];
    } else if ([message isKindOfClass:[GroupBuzz class]]) {
        [_delegate gotNewGroupBuzz:[(GroupBuzz *)message groupId]];
    } else if ([message isKindOfClass:[VenueBuzz class]]) {
        [_delegate gotNewVenueBuzz:[(VenueBuzz *)message venueId]];
    }
}

@end
