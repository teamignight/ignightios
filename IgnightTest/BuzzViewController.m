//
//  BuzzViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/9/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "BuzzViewController.h"
#import "IgnightColors.h"
#import "MWKProgressIndicator.h"
#import "SWRevealViewController.h"
#import "Groups.h"
#import "Group.h"

#define UIKeyboardNotificationsObserve() \
NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter]; \
[notificationCenter addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil]

#define UIKeyboardNotificationsUnobserve() \
[[NSNotificationCenter defaultCenter] removeObserver:self];

#define ChatTextViewHeight 50

@interface BuzzViewController ()
{
    UITableView *buzzTableView;
    UILabel *emptyPlaceHolder;
    BuzzInputTextView *inputTextView;
    
    UserInfo *userInfo;
    
    UISwipeGestureRecognizer *gestureRecognizer;
    UITapGestureRecognizer *tapGesture;
    
    NSMutableDictionary *messages;
    NSMutableArray *messageArray;
    NSNumber *oldestBuzzId;
    NSNumber *newestBuzzId;
    
    BuzzHttpClient *buzzClient;
    
    UIButton *loadEarlier;
    
    Groups *groups;
    
    NSTimer *timer;
    
    BOOL noPushNotifications;
    
    BOOL goingToImages;
    
    BOOL loaded;
    
    NSString *keyboardNotificationName;
}

@end

@implementation BuzzViewController

- (void)viewDidLoad
{
    DDLogDebug(@"BuzzViewController: viewDidLoad");
    [super viewDidLoad];
    groups = [Groups getInstance];
    userInfo = [UserInfo getInstance];
    buzzClient = [BuzzHttpClient getInstance];
    
    oldestBuzzId = [NSNumber numberWithInteger:-1];
    newestBuzzId = [NSNumber numberWithInteger:-1];
    
    messages = [[NSMutableDictionary alloc] init];
    messageArray = [[NSMutableArray alloc] init];

    //Setup tableview
    buzzTableView = [[UITableView alloc] init];
    [buzzTableView setDelegate:self];
    [buzzTableView setDataSource:self];
    [buzzTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [buzzTableView setBackgroundColor:tabBackground];
    [self.view addSubview:buzzTableView];
    
    //Setup input text view
    inputTextView = [[BuzzInputTextView alloc] init];
    [inputTextView setBackgroundColor:[UIColor whiteColor]];
    [inputTextView setDelegate:self];
    [self.view addSubview:inputTextView];
    
    //Setup placeholder text
    [self.view setBackgroundColor:tabBackground];
    emptyPlaceHolder = [[UILabel alloc] init];
    emptyPlaceHolder.font = [UIFont systemFontOfSize:15];
    emptyPlaceHolder.numberOfLines = 3;
    emptyPlaceHolder.textColor = [UIColor grayColor];
    emptyPlaceHolder.backgroundColor = [UIColor clearColor];
    emptyPlaceHolder.textAlignment = NSTextAlignmentCenter;
    emptyPlaceHolder.alpha = 0;
    [self.view addSubview:emptyPlaceHolder];

    //Add Gesture Recognizers to hide keyboard
    gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [gestureRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [tapGesture setNumberOfTapsRequired:1];
    
    //Create load Earlier Bar for tableView
    loadEarlier = [[UIButton alloc] init];
    loadEarlier.frame = CGRectMake(0, 0, buzzTableView.frame.size.width, 35);
    loadEarlier.backgroundColor = [IgnightUtil backgroundGray];
    [loadEarlier setTitle:@"Load Earlier Messages" forState:UIControlStateNormal];
    [loadEarlier setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [loadEarlier setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    loadEarlier.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    loadEarlier.titleLabel.textAlignment = NSTextAlignmentCenter;
    [loadEarlier addTarget:self action:@selector(loadMoreBuzz) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationController.navigationBar.topItem.title = @"";
}

-(void)viewWillAppear:(BOOL)animated
{
    DDLogInfo(@"BuzzViewController: viewWillAppear: Group = %@", _isGroup ? @"YES" : @"NO");
    [super viewWillAppear: animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    buzzClient.delegate = self;
    self.navigationController.navigationBar.topItem.title = @"";
    
    if (!goingToImages) {
        //Adjust TableView
        CGRect oldFrame = [buzzTableView frame];
        if (oldFrame.size.height == 0) {
            CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - ChatTextViewHeight);
            buzzTableView.frame = frame;
        }
        
        //Adjust InputTextView
        oldFrame = [inputTextView frame];
        if (oldFrame.size.height == 0) {
            CGRect frame = CGRectMake(0, self.view.frame.size.height - ChatTextViewHeight, self.view.frame.size.width, ChatTextViewHeight);
            inputTextView.frame = frame;
            emptyPlaceHolder.frame = CGRectMake(frame.origin.x + 60, frame.origin.y - 60, 200, 60);
            
            //Set placeholder
            NSString *prePlaceHolder = @"Be the first to Buzz In \"";
            NSString *placeholder;
            if (_isGroup) {
                placeholder = [NSString stringWithFormat:@"%@%@\".", prePlaceHolder, _name];
            } else {
                placeholder = [NSString stringWithFormat:@"%@%@%@", prePlaceHolder, _name, @"\" today."];
            }
            NSMutableAttributedString *attributeText = [[NSMutableAttributedString alloc] initWithString:placeholder];
            NSRange boldRange;
            if (_name == nil) {
                emptyPlaceHolder.text = @"Be the first to Buzz today.";
            } else {
                boldRange = NSMakeRange(prePlaceHolder.length - 1, _name.length + 2);
                [attributeText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:15] range:boldRange];
                emptyPlaceHolder.attributedText = attributeText;
            }
        }
        
        //Setup Buzz Push Notifications
        if (_isGroup) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleGroupBuzzPush:) name:@"NewGroupBuzzInNotification" object:nil];
        } else {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleVenueBuzzPush:) name:@"NewVenueBuzzInNotification" object:nil];
        }
        
        //Adjust Status Bar
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        //Check to see if you need to setup polling by checking for push notifiactions enabled
        // and whether this is a venue that you've buzzed in already today
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            noPushNotifications = [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
        } else {
            noPushNotifications = ([UIApplication sharedApplication].enabledRemoteNotificationTypes == UIRemoteNotificationTypeNone);
        }
        if (noPushNotifications || (!_isGroup))
        {
            DDLogDebug(@"Setting up polling for every 30 seconds");
            timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(pollBuzzData) userInfo:nil repeats:YES];
        }
        
        if (!loaded) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
        [self getBuzzData];
    }
    loaded = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    DDLogInfo(@"BuzzViewController: viewDidAppear");
    if (_isGroup) { //Clear Group Activity
        [[groups getGroupForId:_buzzTypeId] setActivity:NO];
    }
    
    //Setup Keyboard Notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    DDLogDebug(@"BuzzViewController: viewWillDisappear");
    //Clear Push Notifications Reciever
    if (_isGroup) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewGroupBuzzInNotification" object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewVenueBuzzInNotification" object:nil];
    }
    
    if (_isGroup) {
        [buzzClient unsubscribeFromGroup:_buzzTypeId];
    } else {
        [buzzClient unsubscribeFromVenue:_buzzTypeId];
    }
    
    //Hide Keyboard and remove Obervers
    [inputTextView resignKeyboard];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    //Invalidate timer
    DDLogInfo(@"Invalidating Polling Timer");
    [timer invalidate];
    
    //Clear all progress views
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    self.navigationController.navigationBar.topItem.title = @"";
}

//-(void)viewDidLayoutSubviews
//{
////    [super viewDidLayoutSubviews];
//    
//    return;
//    DDLogDebug(@"VIEW DID LAYOUT");
//    //Important To be here otherwise on initial view,
//    //it does not move all the way to the bottom
//    if (!goingToImages) {
//        [self scrollToBottomAnimated:YES];
//    }
//}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    [self hideKeyboard];
}

-(void)hideKeyboard
{
    [inputTextView resignKeyboard];
}

/*
 On Group Buzz Push, clear activity and fetch more buzz
 */
-(void)handleGroupBuzzPush: (NSNotification *)notification
{
    NSDictionary *data = notification.userInfo;
    NSNumber *groupId = [NSNumber numberWithInt:[data[@"groupId"] intValue]];
    [[[Groups getInstance] getGroupForId:groupId] setActivity:NO];
    [self getBuzzData];
}

-(void)handleVenueBuzzPush: (NSNotification *)notification
{
    NSDictionary *data = notification.userInfo;
    NSNumber *venueId = [NSNumber numberWithInt:[data[@"venueId"] intValue]];
    if ([venueId isEqualToNumber:_buzzTypeId]) {
        [self getBuzzData];
    }
}

-(void)pollBuzzData
{
    if ([timer isValid]) {
        if (_isGroup) {
            [buzzClient getLatestBuzz:newestBuzzId forGroup:_buzzTypeId];
        } else if (_buzzTypeId != nil) {
            [buzzClient getLatestBuzz:newestBuzzId forVenue:_buzzTypeId];
        }
    } else {
        DDLogInfo(@"Invalid Timer Firing In Buzz");
    }
}

-(void)getBuzzData
{
    if (_isGroup) {
        [buzzClient getLatestBuzz:newestBuzzId forGroup:_buzzTypeId];
    } else if (_buzzTypeId != nil) {
        [buzzClient getLatestBuzz:newestBuzzId forVenue:_buzzTypeId];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UISwipGestureRecognizer

-(void)addSwipeGestureRecognizer
{
    [self.view addGestureRecognizer:gestureRecognizer];
    [[emptyPlaceHolder superview] addGestureRecognizer:tapGesture];
    [buzzTableView addGestureRecognizer:tapGesture];
}

-(void)removeSwipGestureRecognizer
{
    [self.view removeGestureRecognizer:gestureRecognizer];
    [[emptyPlaceHolder superview] addGestureRecognizer:tapGesture];
    [buzzTableView removeGestureRecognizer:tapGesture];
}

-(void)handleSwipe: (UISwipeGestureRecognizer *)recognizer {
    [inputTextView resignKeyboard];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section == 0) {
        CGFloat height = [self getHeightOfMessages];
        CGFloat diff = buzzTableView.frame.size.height - height;
        if (diff > 0) {
            return diff;
        }
        return 0;
    }
    
    if (indexPath.row == 0) {
        return 10;
    }
    
    CGFloat h = 0;
    NSInteger messageIndex = indexPath.row - 1;
    
	BuzzMessage* message = [messageArray objectAtIndex:messageIndex];
    if (message.message != nil) {
        message.bubbleSize = [SpeechBubbleView sizeForMessage:message];
        if (message.myMessage) {
            h = message.bubbleSize.height + 28;
        } else {
            h = message.bubbleSize.height + 55;
        }
    } else if (message.image != nil) {
        message.bubbleSize = [SpeechBubbleView sizeForImage:message.image];
        
        if (message.myMessage) {
            h = message.bubbleSize.height + 28;
        } else {
            h = message.bubbleSize.height + 55;
        }
    }

    return h;
}

-(CGFloat)getHeightOfMessages
{
    CGFloat height = 0;
    for (BuzzMessage *message in messageArray) {
        if (message.message != nil) {
            if (message.myMessage) {
                height += message.bubbleSize.height + 28;
            } else {
                height += message.bubbleSize.height + 55;
            }
        } else if (message.image != nil) {
            if (message.myMessage) {
                height += message.bubbleSize.height + 28;
            } else {
                height += message.bubbleSize.height + 55;
            }
        }
    }
    return height;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    NSInteger count = [messageArray count];
    if (count == 0) {
        tableView.alpha = 0;
        emptyPlaceHolder.alpha = 1;
    } else {
        tableView.alpha = 1;
        emptyPlaceHolder.alpha = 0;
    }
    return count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        UITableViewCell *spaceHeader = [tableView dequeueReusableCellWithIdentifier:@"blankHeader"];
        if (spaceHeader == nil) {
            spaceHeader = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"blankHeader"];
            spaceHeader.selectionStyle = UITableViewCellSelectionStyleNone;
            [[spaceHeader contentView] setBackgroundColor:buzzBackgroundColor];
        }
        return spaceHeader;
    }
    
    if (indexPath.row == 0)  {
        UITableViewCell *spaceHeader = [tableView dequeueReusableCellWithIdentifier:@"blankHeader"];
        if (spaceHeader == nil) {
            spaceHeader = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"blankHeader"];
            spaceHeader.selectionStyle = UITableViewCellSelectionStyleNone;
            [[spaceHeader contentView] setBackgroundColor:buzzBackgroundColor];
        }
        return spaceHeader;
    }
    
    static NSString* CellIdentifier = @"MessageCellIdentifier";
	MessageTableViewCell* cell = (MessageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[MessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.delegate = self;
    }
    
    NSInteger messageIndex = indexPath.row - 1;
	BuzzMessage* message = [messageArray objectAtIndex:messageIndex];
	[cell setMessage:message];

    return cell;
}

-(void)loadMoreBuzz
{
    [loadEarlier setUserInteractionEnabled:NO];
    if (_isGroup) {
        [buzzClient getGroupBuzz:_buzzTypeId withStartIndex:oldestBuzzId withCount:[NSNumber numberWithInteger:MAX_BUZZ_MSGS]];
    } else {
        [buzzClient getVenueBuzz:_buzzTypeId withStartIndex:oldestBuzzId withCount:[NSNumber numberWithInteger:MAX_BUZZ_MSGS]];
    }
}

#pragma mark - MessageTableViewCellDelegate

-(void)imageSelected:(UIImage *)image
{
    goingToImages = YES;
    self.navigationController.navigationBar.topItem.title = @"";
    [self.delegate clickedImage:image];
}

-(void)reportBuzz:(NSNumber *)buzzId
{
    if (_isGroup) {
        [buzzClient flagBuzz:buzzId inGroup:_buzzTypeId];
    } else {
        [buzzClient flagBuzz:buzzId inVenue:_buzzTypeId];
    }
}

-(void)imageSelectedWithBuzzId:(NSNumber *)buzzId withImageIndex:(NSNumber *)imageIndex
{
    self.navigationController.navigationBar.topItem.title = @"";
    [self.delegate clickedImageAtIndex:imageIndex.integerValue];
}

#pragma mark - BuzzHttpClientDelegate

-(void)buzzHttpClient:(BuzzHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    DDLogDebug(@"Buzz Could Not Reach Server : %@", [error debugDescription]);
    [loadEarlier setUserInteractionEnabled:YES];
//    IgAlert(nil, @"Failed to reach server.", nil);
}

-(void)buzzHttpClient:(BuzzHttpClient *)client gotLatestBuzz:(NSDictionary *)response
{
    [self gotLatestBuzz:response];
}

-(void)buzzHttpClient:(BuzzHttpClient *)client gotBuzz:(NSDictionary *)response
{
    [self gotBuzz:response];
}

-(void)buzzHttpClient:(BuzzHttpClient *)client postedTextBuzz:(NSDictionary *)response
{
    [self postedText:YES buzz:response];
}

-(void)buzzHttpClient:(BuzzHttpClient *)client postedImageBuzz:(NSDictionary *)response
{
    [self postedText:NO buzz:response];
}

-(void)postedText: (BOOL)textBuzz buzz: (NSDictionary *)response
{
    //If In VenueMode, check whether already buzzed in venue
    //If No, then add venue to buzzedVenues and if push notifications
    //enabled, then invalidate timer
    if (!_isGroup && !noPushNotifications) {
            [timer invalidate];
    }
    
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSDictionary *buzz = response[@"body"];
        NSNumber *buzzId = buzz[@"buzzId"];
        
        //Create BuzzMessage
        BuzzMessage *message = [[BuzzMessage alloc] init];
        message.myMessage = YES;
        message.message = buzz[@"buzz"];
        message.date = [NSDate new];
        
        //Add message to buzz
        [messages setObject:message forKey:buzzId];
        [messageArray addObject:message];
        newestBuzzId = buzzId;
        
        //If Image, then fetch image
        if (!textBuzz) {
            message.image = [UIImage imageNamed:@"default_image"];
            message.imageUrl = [NSURL URLWithString: buzz[@"buzzUrl"]];
            
            //Fetch Image
            {
                NSURLRequest *request = [NSURLRequest requestWithURL:message.imageUrl];
                AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                operation.responseSerializer = [AFImageResponseSerializer serializer];
                [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                    message.image = responseObject;
                    [buzzTableView reloadData];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    DDLogDebug(@"Failed To Retrieve Image From : %@ with error: %@", message.imageUrl, error);
                }];
                [operation start];
            }
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
        [buzzTableView reloadData];
        [self scrollToBottomAnimated:YES];
    }
}

-(void)gotBuzz: (NSDictionary *)buzzData
{
    BOOL res = [buzzData[@"res"] boolValue];
    if (res) {
        buzzData = buzzData[@"body"];
        [self processBuzz:buzzData];
    }
}

-(void)gotLatestBuzz: (NSDictionary *)buzzData
{
    BOOL res = [buzzData[@"res"] boolValue];
    
    if (res) {
        buzzData = buzzData[@"body"];
        [self processBuzz:buzzData];
    }
}

-(void)processBuzz: (NSDictionary *)buzzData
{
    if (![buzzData isKindOfClass:[NSDictionary class]]) {
        DDLogError(@"Got Array When Expecting Dictionary: processBuzz");
        return;
    }
    NSArray *buzz = [buzzData objectForKey:@"buzz"];
    NSInteger counter = 0;
    
    NSNumber *beg;
    NSNumber *end;
    
    BOOL first = YES;
    DDLogInfo(@"BUZZ: %@", buzzData);
    
    for (NSDictionary *b in buzz) {
        NSNumber *userId = [b objectForKey:@"userId"];
        NSNumber *buzzId = [b objectForKey:@"buzzId"];
        
        if (first) {
            beg = buzzId;
            first = NO;
        }
        end = buzzId;
        
        //Create BuzzMessage
        BuzzMessage *message;
        if ([userId isEqualToNumber:userInfo.userId]) {
            message = [[BuzzMessage alloc] initWithMyMessage:b];
        } else {
            message = [[BuzzMessage alloc] initWithOtherMessage:b];
            
            //TODO: Use AFImageCache
            //Get User Image
            message.userImage = [UIImage imageNamed:@"group_member_default"];
            NSURLRequest *request = [NSURLRequest requestWithURL:message.userImageUrl];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            operation.responseSerializer = [AFImageResponseSerializer serializer];
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                message.userImage = responseObject;
                [buzzTableView reloadData];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DDLogDebug(@"Failed To Retrieve Image From : %@ with error: %@", message.userImageUrl, error);
            }];
            [operation start];
        }
        
        //TODO: Use AFImageCache
        //Get Posted Image
        if ([message.buzzType intValue] == 1) {
            NSURLRequest *request = [NSURLRequest requestWithURL:message.imageUrl];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            operation.responseSerializer = [AFImageResponseSerializer serializer];
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                message.image = responseObject;
                [buzzTableView reloadData];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DDLogDebug(@"Failed To Retrieve Image From : %@ with error: %@", message.imageUrl, error);
            }];
            [operation start];
        }
        
        if (![[messages allKeys] containsObject:buzzId]) {
            [messages setObject:message forKey:buzzId];
            //Check whether is new or old data to decide where to insert the objects in array
            NSInteger currentBuzzId = buzzId.integerValue;
            if (currentBuzzId > newestBuzzId.integerValue) {
                DDLogDebug(@"Adding Message: %@ to end", buzzId);
                [messageArray addObject:message];
            }
            if (currentBuzzId < oldestBuzzId.integerValue) {
                DDLogDebug(@"Adding Message To Back: %@", buzzId);
                [messageArray insertObject:message atIndex:counter];
                counter++;
            }
        }
    }
    
    BOOL bottom = NO;
    if (beg != nil && end != nil) {
        if (beg.integerValue > end.integerValue) {
            if (oldestBuzzId.integerValue < 0 || oldestBuzzId.integerValue > end.integerValue) {
                DDLogDebug(@"Setting oldestBuzzId: %@, reload", end);
                oldestBuzzId = end;
            }
        }
        if (beg.integerValue < end.integerValue) {
            if (oldestBuzzId.integerValue < 0 || oldestBuzzId.integerValue > beg.integerValue) {
                DDLogDebug(@"Setting oldestBuzzId: %@", beg);
                oldestBuzzId = beg;
            }
            
            if (newestBuzzId.integerValue < 0 || newestBuzzId.integerValue < end.integerValue) {
                DDLogDebug(@"Setting newestBuzzId: %@, reload, scroll", end);
                newestBuzzId = end;
                bottom = YES;
            }
        }
        if (beg.integerValue == end.integerValue) {
            if (newestBuzzId.integerValue < 0 || newestBuzzId.integerValue < beg.integerValue) {
                DDLogDebug(@"Setting newestBuzzId: %@, reload, scroll", beg);
                newestBuzzId = beg;
                bottom = YES;
            } else if (oldestBuzzId.integerValue > 0 || oldestBuzzId.integerValue > end.integerValue) {
                DDLogDebug(@"Setting oldestBuzzId: %@, reload", end);
                oldestBuzzId = end;
            }
        }
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (oldestBuzzId.integerValue > 0) {
        [buzzTableView setTableHeaderView:loadEarlier];
    } else {
        [buzzTableView setTableHeaderView:nil];
    }
    
    if (_isGroup) {
        [groups getGroupForId:_buzzTypeId].activity = NO;
    }
    
    [loadEarlier setUserInteractionEnabled:YES];
    [buzzTableView reloadData];
    
    if (bottom) {
        [self scrollToBottomAnimated:YES];
    }
}

#pragma mark - BuzzInputTextViewDelegate

-(void)textViewChangingHeight:(CGFloat)height toHeight:(CGFloat)newHeight
{
    [UIView animateWithDuration:0.1 animations:^{
        CGRect oldFrame = inputTextView.frame;
        inputTextView.frame = CGRectMake(oldFrame.origin.x, oldFrame.origin.y - (newHeight - height), oldFrame.size.width, oldFrame.size.height + (newHeight - height));
        
        oldFrame = buzzTableView.frame;
        buzzTableView.frame = CGRectMake(oldFrame.origin.x, oldFrame.origin.y, oldFrame.size.width, oldFrame.size.height - (newHeight - height));
        
        oldFrame = emptyPlaceHolder.frame;
        emptyPlaceHolder.frame = CGRectMake(oldFrame.origin.x,
                                     oldFrame.origin.y - (newHeight - height),
                                     oldFrame.size.width,
                                     oldFrame.size.height);
        [self scrollToBottomAnimated:NO];
        
        [buzzTableView setNeedsDisplay];
        [inputTextView setNeedsDisplay];
    }];
}

-(void)sendInput: (NSString*)text
{
    if (_isGroup) {
        [buzzClient sendTextBuzz:text forGroup:_buzzTypeId];
    } else {
        [buzzClient sendTextBuzz:text forVenue:_buzzTypeId];
    }
    [self scrollToBottomAnimated:NO];
}

-(void)goToCamera
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take Photo", @"Choose Existing", nil];
    [actionSheet showInView: self.view];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 2) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        
        if (buttonIndex == 0) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else if (buttonIndex == 1) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
           [self presentViewController:imagePicker animated:YES completion:nil]; 
        }];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo NS_DEPRECATED_IOS(2_0, 3_0)
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = (UIImage *)info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *newImage = [IgnightUtil scaleForUpload:image];
    
    if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        ImageSelectionController *imageSelectionController = [self.storyboard instantiateViewControllerWithIdentifier:@"imageSelection"];
        imageSelectionController.delegate = self;
        imageSelectionController.imageSize = [IgnightUtil getSizeForImageOnScreen:image];
        imageSelectionController.image = newImage;
        self.navigationController.navigationBar.topItem.title = @"";
        [self.navigationController pushViewController:imageSelectionController animated:YES];
    } else {
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
        [self userSelectedImage:newImage];
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

#pragma mark - ImageSelectionControllerDelegate

-(void)userClickedCancel
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)userSelectedImage:(UIImage *)image
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData *imageData = UIImageJPEGRepresentation(image, [IgnightProperties getImageScalingValue]);
    if (_isGroup) {
        [buzzClient sendImageBuzz:imageData forGroup:_buzzTypeId];
    } else {
        [buzzClient sendImageBuzz:imageData forVenue:_buzzTypeId];
    }
}

#pragma mark - Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification {
    NSTimeInterval animationDuration;
    NSDictionary *uInfo = [notification userInfo];
    
    CGPoint kbPointBeg = [[uInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].origin;
    CGPoint kbPointEnd = [[uInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    CGFloat adjustment = kbPointEnd.y - kbPointBeg.y;
    
    [[uInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    [UIView animateWithDuration:animationDuration animations:^{
        buzzTableView.frame = CGRectMake(buzzTableView.frame.origin.x,
                                         buzzTableView.frame.origin.y,
                                         buzzTableView.frame.size.width,
                                         buzzTableView.frame.size.height + adjustment);
        inputTextView.frame = CGRectMake(inputTextView.frame.origin.x,
                                         inputTextView.frame.origin.y + adjustment,
                                         inputTextView.frame.size.width,
                                         inputTextView.frame.size.height);
        emptyPlaceHolder.frame = CGRectMake(emptyPlaceHolder.frame.origin.x,
                                            emptyPlaceHolder.frame.origin.y  + adjustment,
                                            emptyPlaceHolder.frame.size.width,
                                            emptyPlaceHolder.frame.size.height);
    }];
    
    if ([[notification name] isEqualToString:UIKeyboardWillShowNotification]) {
        [self addSwipeGestureRecognizer];
    } else if ([[notification name] isEqualToString:UIKeyboardWillHideNotification]) {
        [self removeSwipGestureRecognizer];
    }
    
    [buzzTableView reloadData];
    [self scrollToBottomAnimated:NO];
    
    [buzzTableView setBackgroundColor:[UIColor clearColor]];
}

#pragma mark - BuzzView Util

- (void)scrollToBottomAnimated:(BOOL)animated {
    DDLogDebug(@"Scroll To Bottom");
    if ([messageArray count] > 0) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:([messageArray count]) inSection:1];
        [buzzTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:animated];
        DDLogDebug(@"Setting to IndexPath: %@", indexPath);
    }
}

@end
