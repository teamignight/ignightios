//
//  UpdatePasswordViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/26/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserHttpClient.h"
#import "IgnightTestAppDelegate.h"
#import "Group.h"
#import "Groups.h"

@interface UpdatePasswordViewController : UIViewController<UserHttpClientDelegate, UIGestureRecognizerDelegate>


@property (strong, nonatomic) IBOutlet UIView *inputView;
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *temporaryPassword;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIButton *login;

@property (strong, nonatomic) IBOutlet UIView *usernamePswdSeparator;
@property (strong, nonatomic) IBOutlet UIView *pswdNewPswdSeparator;


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)goBack:(id)sender;
- (IBAction)loginNow:(id)sender;

@end
