//
//  CreateAGroupView.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/28/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "Trending.h"

@interface CreateAGroupView : UIViewController <UITextFieldDelegate,
                                                GroupHttpClientDelegate,
                                                UITextViewDelegate,
                                                SWRevealViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *titleBar;
@property (strong, nonatomic) IBOutlet UITextField *grpName;
@property (strong, nonatomic) IBOutlet UITextView *grpDescription;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *createGroupBtn;
@property (strong, nonatomic) IBOutlet UIView *groupInfoView;
@property (strong, nonatomic) IBOutlet UILabel *publicAccessLbl;


- (IBAction)createAGroup:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)groupAccess:(id)sender;

@end
