//
//  VenueBuzz.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "PushMessage.h"

@interface VenueBuzz : PushMessage

@property (nonatomic, copy) NSNumber* venueId;

@end
