//
//  SpeechBubbleView.m
//  PushChatStarter
//
//  Created by Kauserali on 28/03/13.
//  Copyright (c) 2013 Ray Wenderlich. All rights reserved.
//

#import "SpeechBubbleView.h"

static UIFont* font = nil;

const CGFloat TextLeftMargin = 10;   // insets for the text
const CGFloat TextRightMargin = 10;
const CGFloat TextTopMargin = 5;
const CGFloat TextBottomMargin = 5;

const CGFloat ImageLeftMargin = 5;
const CGFloat ImageRightMargin = 5;
const CGFloat ImageTopMargin = 5;
const CGFloat ImageBottomMargin = 5;

const CGFloat MinBubbleWidth = 30;   // minimum width of the bubble
const CGFloat MinBubbleHeight = 30;  // minimum height of the bubble

const CGFloat WrapWidth = 200;       // maximum width of text in the bubble

@interface SpeechBubbleView() {
    NSString *_text;
    UIImage *_image;
	BubbleType _bubbleType;
    UITextView *textView;
}
@end

@implementation SpeechBubbleView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        textView = [[UITextView alloc] init];
        textView.frame = CGRectZero;
        textView.backgroundColor = [UIColor clearColor];
        textView.userInteractionEnabled = NO;
    }
    return self;
}

+ (CGSize)sizeForMessage:(BuzzMessage *)message
{
    NSString *text = message.message;
    
    CGSize textSize = [text boundingRectWithSize:CGSizeMake(WrapWidth, 9999)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}
                                         context:nil].size;

	CGSize bubbleSize;
	bubbleSize.width = textSize.width + TextLeftMargin + TextRightMargin;
    bubbleSize.height = textSize.height + TextTopMargin + TextBottomMargin;
    
    if (message.myMessage) {
        
    } else {
        CGFloat min = [message.senderName boundingRectWithSize:CGSizeMake(WrapWidth, 9999)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}
                                                       context:nil].size.width;
        min += (TextLeftMargin + TextRightMargin);
        min = ceilf(min);
        
        if (bubbleSize.width < MinBubbleWidth) {
            bubbleSize.width = MinBubbleWidth;
        }
        bubbleSize.width = MAX(bubbleSize.width, min);
    }

	if (bubbleSize.height < MinBubbleHeight)
		bubbleSize.height = MinBubbleHeight;
    
    bubbleSize.width = ceilf(bubbleSize.width);
    bubbleSize.height = ceilf(bubbleSize.height);
    
	return bubbleSize;
}

+ (CGSize)sizeForImage: (UIImage *)image
{
    CGSize bubbleSize = CGSizeZero;
    bubbleSize.width = ceilf(160 + ImageLeftMargin + ImageRightMargin);
    bubbleSize.height = ceilf(160 + ImageTopMargin + ImageBottomMargin);
    
	return bubbleSize;
}

- (void)drawRect:(CGRect)rect
{
	[self.backgroundColor setFill];
	UIRectFill(rect);

	CGRect bubbleRect = CGRectInset(self.bounds, 0, 0);

	CGRect textRect;
	textRect.origin.y = bubbleRect.origin.y + TextTopMargin;
	textRect.size.width = bubbleRect.size.width - TextLeftMargin - TextRightMargin;
	textRect.size.height = bubbleRect.size.height - TextTopMargin - TextBottomMargin;

	if (_bubbleType == BubbleTypeLefthand)
	{
		textRect.origin.x = bubbleRect.origin.x + TextLeftMargin;
        [otherBuzzBubbleContentText set];
	}
	else
	{
		textRect.origin.x = bubbleRect.origin.x + TextRightMargin;
        [myBuzzBubbleContentText set];
	}
    
//    NSLog(@"BUBBLE FRAME: %@", self);
//    NSLog(@"Text Frame: (%f, %f), (%f, %f)", textRect.origin.x, textRect.origin.y, textRect.size.width, textRect.size.height);
//    
    [textView removeFromSuperview];
    
    if (_text != nil) {
        NSLog(@"ADDING TEXT: %@", _text);
        textView.text = _text;
        textView.font = [UIFont systemFontOfSize:15];
        textView.textColor = (_bubbleType == BubbleTypeLefthand) ? buzzTextColor : [UIColor whiteColor];
        textView.frame = CGRectMake(5, -2.5, textRect.size.width + 10, textRect.size.height + 10);
        textView.backgroundColor = [UIColor blueColor];
        [self addSubview:textView];
//        [_text drawInRect:textRect withAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],
//                                                    NSForegroundColorAttributeName:
//                                                        (_bubbleType == BubbleTypeLefthand) ? buzzTextColor :
//                                                                        [UIColor whiteColor]}];
    }
    else if (_image != nil) {
        NSLog(@"ADDING IMAGE: %@", _text);
        textView.frame = CGRectZero;
        textView.text = nil;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:_image];
        [self addSubview:imageView];
    }
}

- (void)setText:(NSString*)newText bubbleType:(BubbleType)newBubbleType
{
	_text = [newText copy];
	_bubbleType = newBubbleType;
	[self setNeedsDisplay];
}

- (void)setImage:(UIImage *)image bubbleType:(BubbleType)newBubbleType
{
    _text = nil;
    _image = [image copy];
    _bubbleType = newBubbleType;
    [self setNeedsDisplay];
}

- (CGFloat)getLeftPadding
{
    return TextLeftMargin;
}
- (CGFloat)getRightPadding
{
    return TextRightMargin;
}
- (CGFloat)getTopPadding
{
    return TextTopMargin;
}
- (CGFloat)getBottomPadding
{
    return TextBottomMargin;
}

- (CGFloat)getImageLeftPadding
{
    return ImageLeftMargin;
}
- (CGFloat)getImageRightPadding
{
    return ImageRightMargin;
}
- (CGFloat)getImageTopPadding
{
    return ImageTopMargin;
}
- (CGFloat)getImageBottomPadding
{
    return ImageBottomMargin;
}

@end
