//
//  IgnightTestAppDelegate.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/24/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "IgnightTestAppDelegate.h"
#import "IgnightColors.h"
#import <AFNetworkActivityIndicatorManager.h>
#import <HockeySDK/HockeySDK.h>
#import "Groups.h"
#import "Inbox.h"
#import "GroupViewController.h"

#import "DDLog.h"
#import "DDASLLogger.h"
#import "DDTTYLogger.h"
#import "DDFileLogger.h"
#import "Flurry.h"
#import "iRate.h"


@interface IgnightTestAppDelegate() <BITCrashManagerDelegate>{}
    @property  (nonatomic) DDFileLogger *fileLogger;
@end

@implementation IgnightTestAppDelegate
{
    NSString *_deviceToken;
    UserHttpClient *userClient;
    UIView *sBHack;
}

//@synthesize managedObjectContext = _managedObjectContext;
//@synthesize managedObjectModel = _managedObjectModel;
//@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"CN9BHCS5KCNVMJ2DHZ53"];
    
    // add Xcode console logger if not running in the App Store
    if (![[BITHockeyManager sharedHockeyManager] isAppStoreEnvironment]) {
        [DDLog addLogger:[DDTTYLogger sharedInstance]];
        [DDLog addLogger:[DDASLLogger sharedInstance]];
    }
    
    // Configure HockeyApp
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"b0675eecdf62bad3d697dad855ff8ece"];
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    [[BITHockeyManager sharedHockeyManager] testIdentifier];
    [BITHockeyManager setVersion:1.21];
    
    sBHack = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    sBHack.backgroundColor = [UIColor blackColor];
    [self.window.rootViewController.view addSubview:sBHack];
    sBHack.alpha = 0;
    
    //Remove Badge Once User Clicks on App
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //Get User Notifications
    if (userClient == nil) {
        userClient = [UserHttpClient getInstance];
    }
    [userClient getUserNotifications];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UIImagePickerController class], nil] setTintColor:[IgnightUtil backgroundGray]];
    
    if (launchOptions != nil) {
        NSDictionary *dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (dictionary != nil) {
            DDLogInfo(@"AD: %@", dictionary);
        }
    }
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = NO;

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"dna-step-bg"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"back_grayy"]];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage imageNamed:@"back_gray"]];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:18], NSFontAttributeName, [IgnightUtil backgroundGray], NSForegroundColorAttributeName, nil]];
    
    return YES;
}

-(void)flipHack
{
    sBHack.backgroundColor = [UIColor blackColor];
    sBHack.alpha = !sBHack.alpha;
}

-(BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}

-(BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    NSDictionary *data = userInfo;
    DDLogDebug(@"Got Push: %@", data);
    NSString *type = data[@"notificationType"];
    if ([type isEqualToString:@"INVITE_USER_TO_GROUP_REQUEST"]) {
        [[UserNotifications getInstance] updateInbox:YES];
        if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewMessageInInboxNotification" object:self userInfo:data];
        }
    } else if ([type isEqualToString:@"NEW_GROUP_BUZZ_AVAILABLE_NOTIFICATION"]) {
        [[UserNotifications getInstance] updateGroupBuzz:YES];
        if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewGroupBuzzNotification" object:self userInfo:data];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewGroupBuzzInNotification" object:self userInfo:data];
        }
    }
//    else if ([type isEqualToString:@"NEW_VENUE_BUZZ_AVAILABLE_NOTIFICATION"]) {
//        if (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground) {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewVenueBuzzNotification" object:self userInfo:data];
//        } else {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewVenueBuzzInNotification" object:self userInfo:data];
//        }
//    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    DDLogDebug(@"Posting Device Token: %@", deviceToken);
    _deviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    _deviceToken = [_deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (userClient == nil) {
        userClient = [UserHttpClient getInstance];
    }
    
    if (![IgnightUtil isIosTokenSet]) {
        [userClient addDeviceToken:_deviceToken];
    }
}

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    [IgnightUtil setIsIOSTokenSet:NO];
}

-(void)userHttpClient:(UserHttpClient *)client postedDeviceToken:(NSDictionary *)response
{
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        [IgnightUtil setIsIOSTokenSet:YES];
    } else {
        [IgnightUtil setIsIOSTokenSet:NO];
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	DDLogDebug(@"Failed to get token, error: %@", error);
}

#pragma mark - Core Data stack

- (void)saveContext
{
//    NSError *error = nil;
//    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
//    if (managedObjectContext != nil) {
//        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
//            // Replace this implementation with code to handle the error appropriately.
//            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//            //            abort();
//        }
//    }
}
//
//// Returns the managed object context for the application.
//// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
//- (NSManagedObjectContext *)managedObjectContext
//{
//    if (_managedObjectContext != nil) {
//        return _managedObjectContext;
//    }
//    
//    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
//    if (coordinator != nil) {
//        _managedObjectContext = [[NSManagedObjectContext alloc] init];
//        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
//    }
//    return _managedObjectContext;
//}
//
//// Returns the managed object model for the application.
//// If the model doesn't already exist, it is created from the application's model.
//- (NSManagedObjectModel *)managedObjectModel
//{
//    if (_managedObjectModel != nil) {
//        return _managedObjectModel;
//    }
//    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"IgnightTest" withExtension:@"momd"];
//    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
//    return _managedObjectModel;
//}
//
//// Returns the persistent store coordinator for the application.
//// If the coordinator doesn't already exist, it is created and the application's store added to it.
//- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
//{
//    if (_persistentStoreCoordinator != nil) {
//        return _persistentStoreCoordinator;
//    }
//    
//    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"IgnightTest.sqlite"];
//    
//    NSError *error = nil;
//    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
//    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
//        /*
//         Replace this implementation with code to handle the error appropriately.
//         
//         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//         
//         Typical reasons for an error here include:
//         * The persistent store is not accessible;
//         * The schema for the persistent store is incompatible with current managed object model.
//         Check the error message to determine what the actual problem was.
//         
//         
//         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
//         
//         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
//         * Simply deleting the existing store:
//         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
//         
//         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
//         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
//         
//         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
//         
//         */
//        abort();
//    }
//    
//    return _persistentStoreCoordinator;
//}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark  - Delegate Functions

- (void)applicationWillResignActive:(UIApplication *)application
{
    [userClient unsubscribeFromBuzz];
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [userClient unsubscribeFromBuzz];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    DDLogInfo(@"applicationWillEnterForeground");
    //Remove Badge Once User Clicks on App
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //Get User Notifications
    if (userClient == nil) {
        userClient = [UserHttpClient getInstance];
    }
    [userClient getUserNotifications];
    
//    [IgnightUtil getUserNotificationValues];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
//    [UIApplication sharedApplication] cancelLocalNotification:<#(UILocalNotification *)#>
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
