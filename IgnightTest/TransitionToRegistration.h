//
//  TransitionToRegistration.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransitionToRegistration : UIStoryboardSegue


@property (strong, nonatomic) IBOutlet UIButton *registerAccount;

@end
