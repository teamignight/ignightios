//
//  VenueHttpClient.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/13/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "VenueHttpClient.h"

@implementation VenueHttpClient
{
    UserInfo *userInfo;
}

+(VenueHttpClient *)getInstance
{
    static VenueHttpClient *_sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *url = server_url;
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:url]];
        [_sharedInstance.requestSerializer setValue:[IgnightProperties getHeaderToken] forHTTPHeaderField:@"X-IGNIGHT-TOKEN"];
        [_sharedInstance.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"Platform"];
        UserInfo* userInfo = [UserInfo getInstance];
        if (userInfo.userId != nil) {
            [_sharedInstance.requestSerializer setValue:userInfo.userId.stringValue forHTTPHeaderField:@"USER_ID"];
        }
        if (TARGET_IPHONE_SIMULATOR) {
        } else {
            _sharedInstance.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
            _sharedInstance.securityPolicy.allowInvalidCertificates = YES;
        }
    });
    return _sharedInstance;
}

-(instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        userInfo = [UserInfo getInstance];
    }
    
    return self;
}

-(void)cancelAllOperations
{
    [[self operationQueue] cancelAllOperations];
    for (NSURLSessionTask *t in [self tasks]) {
        [t cancel];
    }
}

-(void)getVenueImages:(NSNumber *)venueId
{
    NSDictionary *parameters = @{@"buzzImageStartId" : [NSNumber numberWithInt:-1],
                                 @"noOfBuzzImages" : [NSNumber numberWithInt:50]};
    
    NSString *path = [NSString stringWithFormat:@"images/venue/%@", venueId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(venueHttpClient:gotVenueImages:)]) {
            [self.delegate venueHttpClient:self gotVenueImages:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate venueHttpClient:self didFailWithError:error];
    }];
}

-(void)sendTextBuzz:(NSString *)buzz forVenue:(NSNumber *)venueId
{
    NSDictionary *data = @{@"userId" : userInfo.userId,
                           @"buzzText" : buzz};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"buzz/add/venue/text/%@", venueId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(venueHttpClient:postedTextBuzz:)]) {
            [self.delegate venueHttpClient:self postedTextBuzz:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate venueHttpClient:self didFailWithError:error];
    }];
}

-(void)sendImageBuzz:(NSData *)buzz forVenue:(NSNumber *)venueId
{
    NSDictionary *data = @{@"userId" : userInfo.userId};
    NSDictionary *msg = @{@"msg" : data};
    NSData *body = [NSJSONSerialization dataWithJSONObject:msg options:kNilOptions error:nil];
    NSDictionary *parameters = @{@"json_body" : body};
    
    NSString *path = [NSString stringWithFormat:@"buzz/add/venue/image/%@", venueId];
    [self POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFormData:buzz name:@"userImage"];
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(venueHttpClient:postedImageBuzz:)]) {
            [self.delegate venueHttpClient:self postedImageBuzz:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate venueHttpClient:self didFailWithError:error];
    }];
}

-(void)getLatestBuzz: (NSNumber *)lastBuzzId forVenue:(NSNumber *)venueId
{
    NSDictionary *parameters = @{@"lastBuzzId": lastBuzzId,
                                 @"count" : [NSNumber numberWithInteger:MAX_BUZZ_MSGS],
                                 @"userId" : userInfo.userId};
    
    DDLogDebug(@"Calling Venue getLatestBuzz with: %@", parameters);
    
    NSString *path = [NSString stringWithFormat:@"buzz/venue/%@", venueId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(venueHttpClient:gotLatestBuzz:)]) {
            [self.delegate venueHttpClient:self gotLatestBuzz:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate venueHttpClient:self didFailWithError:error];
    }];
}

-(void)getVenueInfo: (NSNumber *)venueId
{
    NSDictionary *parameters = @{@"userId" : userInfo.userId};
    
    NSString *path = [NSString stringWithFormat:@"venue/info/%@", venueId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(venueHttpClient:gotVenueInfo:)]) {
            [self.delegate venueHttpClient:self gotVenueInfo:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate venueHttpClient:self didFailWithError:error];
    }];
}

-(void)reportVenueDetailsError: (NSDictionary *)venueData forVenue: (NSNumber*)venueId
{
    NSDictionary *data = @{@"userId" : userInfo.userId,
                           @"venueName" : (venueData[@"venueName"] == nil) ? @"" : venueData[@"venueName"],
                           @"cityId" : userInfo.cityId,
                           @"address" : venueData[@"venueAddress"] == nil ? @"" : venueData[@"venueAddress"],
                           @"number" : venueData[@"venuePhone"] == nil ? @"" : venueData[@"venuePhone"],
                           @"url" : venueData[@"venueUrl"] == nil ? @"" : venueData[@"venueUrl"],
                           @"comment" : venueData[@"venueComments"] == nil ? @"" : venueData[@"venueComments"]};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"venue/report/error/%@", venueId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(venueHttpClientPostedVenueErrors:)]) {
            [self.delegate venueHttpClientPostedVenueErrors:self];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate venueHttpClient:self didFailWithError:error];
    }];
}

-(void)reportVenueAddition: (NSDictionary *)venueData
{
    NSDictionary *data = @{@"venueName" : (venueData[@"venueName"] == nil) ? @"" : venueData[@"venueName"],
                           @"cityId" : userInfo.cityId,
                           @"address" : venueData[@"venueAddress"] == nil ? @"" : venueData[@"venueAddress"],
                           @"number" : venueData[@"venuePhone"] == nil ? @"" : venueData[@"venuePhone"],
                           @"url" : venueData[@"venueUrl"] == nil ? @"" : venueData[@"venueUrl"],
                           @"comment" : venueData[@"venueComments"] == nil ? @"" : venueData[@"venueComments"]};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"venue/report/add/%@", userInfo.userId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(venueHttpClientPostedVenueErrors:)]) {
            [self.delegate venueHttpClientPostedVenueAdditions:self];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate venueHttpClient:self didFailWithError:error];
    }];
}

-(void)userActivityChangedTo: (NSInteger)userActivity forVenueId: (NSNumber *)venueId
{
    NSDictionary *data = @{@"userActivity" : [NSNumber numberWithInteger:userActivity],
                           @"userId" : userInfo.userId};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"venue/activity/%@", venueId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(venueHttpClient:userActivityChangedTo:forVenueId:withResponse:)]) {
            [self.delegate venueHttpClient:self userActivityChangedTo:userActivity forVenueId:venueId withResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate venueHttpClient:self didFailWithError:error];
    }];
}

-(void)sendVenue:(NSNumber *)venueId dailyEventInfo: (NSString *)eventInfo
{
    NSDictionary *data = @{@"venueEventInfo" : eventInfo};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"venue/event/%@", venueId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(venueHttpClientSubmittedVenueEventInfo:)]) {
            [_delegate venueHttpClientSubmittedVenueEventInfo:self];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate venueHttpClient:self didFailWithError:error];
    }];

}

@end
