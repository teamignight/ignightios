//
//  TutorialContentViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "TutorialContentViewController.h"

@interface TutorialContentViewController ()
{
    UIImageView *imageView;
}

@end

@implementation TutorialContentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:imageView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    imageView.image = [UIImage imageNamed:_imageFile];
    DDLogDebug(@"TutorialContentViewController : %@", _imageFile);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
