//
//  GroupTabBarController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/8/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupTabBarController : UITabBarController

@end
