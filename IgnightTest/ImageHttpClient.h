//
//  ImageHttpClient.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/6/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@protocol ImageHttpClientDelegate;

@interface ImageHttpClient : AFHTTPSessionManager

@property (nonatomic, weak) id<ImageHttpClientDelegate> delegate;

+(ImageHttpClient *)getInstance;

-(void)getVenueImages: (NSNumber *)venueId;
-(void)getGroupImages: (NSNumber *)groupId;

@end

@protocol ImageHttpClientDelegate <NSObject>

-(void)imageHttpClient: (ImageHttpClient *)client didFailWithError: (NSError *)error;
-(void)imageHttpClient: (ImageHttpClient *)client gotVenueImages: (NSDictionary *)response;
-(void)imageHttpClient: (ImageHttpClient *)client gotGroupImages: (NSDictionary *)response;

@optional

@end
