//
//  BuzzImageData.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/29/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BuzzImageData : NSObject

@property (nonatomic, copy) NSURL *imgUrl;
@property (nonatomic, copy) UIImage *image;
@property (nonatomic, copy) NSNumber *buzzId;

@end
