//
//  ImageSelectionController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/23/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "ImageSelectionController.h"

@interface ImageSelectionController ()

@end

@implementation ImageSelectionController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor blackColor]];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_cancelBtn setImage:[UIImage imageNamed:@"icon_no"] forState:UIControlStateNormal];
    [_useBtn setImage:[UIImage imageNamed:@"icon_ok"] forState:UIControlStateNormal];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGFloat height = [[UIScreen mainScreen] bounds].size.height;
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    
    CGRect frame = CGRectMake((width - _imageSize.width)/2, (height - _imageSize.height) / 2, _imageSize.width, _imageSize.height);
    _imageView.frame = frame;
    _imageView.image = self.image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelClicked:(id)sender {
    [self.delegate userClickedCancel];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)useClicked:(id)sender {
    [self.delegate userSelectedImage:self.image];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
