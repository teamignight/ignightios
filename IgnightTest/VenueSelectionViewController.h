//
//  VenueSelectionViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 5/30/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfo.h"

@interface VenueSelectionViewController : UIViewController <UITableViewDataSource,
                                                            UITableViewDelegate,
                                                            NSURLConnectionDataDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *venueTableView;

@property (strong, nonatomic) IBOutlet UIView *selectionOneContainer;
@property (strong, nonatomic) IBOutlet UIView *selectionTwoContainer;
@property (strong, nonatomic) IBOutlet UIView *selectionThreeContainer;

@property (strong, nonatomic) IBOutlet UIImageView *selectionOneImageView;
@property (strong, nonatomic) IBOutlet UIImageView *selectionTwoImageView;
@property (strong, nonatomic) IBOutlet UIImageView *selectionThreeImageView;

- (IBAction)selectionButton:(id)sender;
- (IBAction)continueButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *selectionOneButton;
@property (strong, nonatomic) IBOutlet UIButton *selectionTwoButton;
@property (strong, nonatomic) IBOutlet UIButton *selectionThreeButton;

@property (strong, nonatomic) IBOutlet UILabel *selectionOneLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectionTwoLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectionThreeLabel;

@property (strong, nonatomic) IBOutlet UIButton *continueButton;




@end
