//
//  BuzzHttpClient.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/6/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

@protocol BuzzHttpClientDelegate;

@interface BuzzHttpClient : AFHTTPSessionManager

@property (nonatomic, weak) id<BuzzHttpClientDelegate> delegate;

+(BuzzHttpClient *)getInstance;

-(void)sendTextBuzz: (NSString *)buzz forGroup: (NSNumber *)groupId;
-(void)sendImageBuzz: (NSData *)buzz forGroup: (NSNumber *)groupId;
-(void)getLatestBuzz: (NSNumber *)lastBuzzId forGroup:(NSNumber *)groupId;;
-(void)getGroupBuzz: (NSNumber *)groupId withStartIndex: (NSNumber *)startIndex withCount: (NSNumber *)count;

-(void)sendTextBuzz: (NSString *)buzz forVenue: (NSNumber *)venueId;
-(void)sendImageBuzz: (NSData *)buzz forVenue: (NSNumber *)venueId;
-(void)getLatestBuzz: (NSNumber *)lastBuzzId forVenue:(NSNumber *)venueId;
-(void)getVenueBuzz: (NSNumber *)venueId withStartIndex: (NSNumber *)startIndex withCount: (NSNumber *)count;

-(void)unsubscribeFromVenue: (NSNumber *)venueId;
-(void)unsubscribeFromGroup: (NSNumber *)groupId;

-(void)flagBuzz:(NSNumber *)buzzId inGroup: (NSNumber *)groupId;
-(void)flagBuzz:(NSNumber *)buzzId inVenue: (NSNumber *)venueId;

@end

@protocol BuzzHttpClientDelegate <NSObject>

@optional

-(void)buzzHttpClient: (BuzzHttpClient *)client didFailWithError: (NSError *)error;

-(void)buzzHttpClient: (BuzzHttpClient *)client postedTextBuzz: (NSDictionary *)response;
-(void)buzzHttpClient: (BuzzHttpClient *)client postedImageBuzz: (NSDictionary *)response;
-(void)buzzHttpClient: (BuzzHttpClient *)client gotLatestBuzz: (NSDictionary *)response;
-(void)buzzHttpClient: (BuzzHttpClient *)client gotBuzz: (NSDictionary *)response;

@end
