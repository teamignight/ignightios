//
//  ConfirmUserPhoneNumberViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 3/1/15.
//  Copyright (c) 2015 Abhinav Chordia. All rights reserved.
//

#import "ConfirmUserPhoneNumberViewController.h"
#import "IgnightUtil.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "APAddressBook.h"
#import "APContact.h"
#import "APPhoneWithLabel.h"
#import "IgnightContact.h"

@interface ConfirmUserPhoneNumberViewController ()

@end

@implementation ConfirmUserPhoneNumberViewController
{
    UILabel *label;
    UITextView *userNumber;
    UIButton *submit;
    
    UserHttpClient *userClient;
    
    NSString *userPhone;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    
    userClient = [UserHttpClient getInstance];
    userClient.delegate = self;
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    CGFloat yPos = 50;
    label = [[UILabel alloc] initWithFrame:CGRectMake(50, yPos, screenWidth - 100, 30)];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont systemFontOfSize:10];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 2;
    label.text = @"Please enter your 10 digit phone number so we can verify your identity.";
    [self.view addSubview:label];
    
    yPos += label.frame.size.height + 5;
    userNumber = [[UITextView alloc] initWithFrame:CGRectMake(50, yPos, screenWidth - 100, 35)];
    userNumber.textColor = [UIColor blackColor];
    userNumber.font = [UIFont systemFontOfSize:15];
    userNumber.textAlignment = NSTextAlignmentCenter;
    [[userNumber layer] setBorderColor:[UIColor blackColor].CGColor];
    [[userNumber layer] setBorderWidth:0.5];
    [userNumber setKeyboardType:UIKeyboardTypeNumberPad];
    [userNumber becomeFirstResponder];
    [self.view addSubview:userNumber];
    
    yPos += userNumber.frame.size.height + 5;
    submit = [UIButton buttonWithType:UIButtonTypeCustom];
    submit.frame = CGRectMake(50, yPos, screenWidth - 100, 35);
    [submit setTitle:@"Submit" forState:UIControlStateNormal];
    [submit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submit setBackgroundColor:[UIColor darkGrayColor]];
    [submit addTarget:self action:@selector(getVerificationCode:) forControlEvents:UIControlEventTouchUpInside];
    submit.titleLabel.font = [UIFont systemFontOfSize:13];
    
    [self.view addSubview:submit];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.title = @"Invite Your Friends";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(IBAction)getVerificationCode:(id)sender
{
    DDLogDebug(@"UserNumber: %@", userNumber.text);
    NSString *number = userNumber.text;
    NSUInteger length = number.length;
    if (length == 10) {
        number = [@"1" stringByAppendingString: number];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [userClient requestVerificationCode:number];
    } else if (length == 11) {
        if ([userNumber.text characterAtIndex:0] == '1') {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [userClient requestVerificationCode:userNumber.text];
        } else {
            [[[UIAlertView alloc] initWithTitle:nil message:@"Your phone number must begin with a 1." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    } else {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Please make sure you entered your full 10 digit phone number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

-(IBAction)verifyCode:(id)sender
{
    if ([userNumber.text length] > 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [userClient verifyPhoneNumber:userPhone withVerification:userNumber.text];
    }
}

-(IBAction)exitNumberView:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UserHttpClient

-(void)userHttpClient:(UserHttpClient *)client gotVerificationResposne:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    NSDictionary *data = (NSDictionary *)response;
    if ([data[@"res"] boolValue]) {
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                DDLogDebug(@"GRANTED ACCCESS");
                APAddressBook *addressBook = [[APAddressBook alloc] init];
                addressBook.fieldsMask = APContactFieldFirstName | APContactFieldLastName | APContactFieldPhonesWithLabels | APContactFieldEmails;
                [addressBook loadContacts:^(NSArray *contacts, NSError *error) {
                    NSMutableArray *userContacts = [[NSMutableArray alloc] init];
                    for (int i =0 ; i < [contacts count]; i++) {
                        IgnightContact *ignightContact = [[IgnightContact alloc] init];
                        APContact *cont = (APContact*)[contacts objectAtIndex:i];
                        ignightContact.firstName = cont.firstName;
                        ignightContact.lastName = cont.lastName;
                        DDLogDebug(@"FirstName : %@, LastName: %@", cont.firstName, cont.lastName);
                        for (APPhoneWithLabel *number  in cont.phonesWithLabels) {
                            NSString *phone = [number.phone stringByReplacingOccurrencesOfString:@" " withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@"+" withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
                            phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
                            [ignightContact.contacts addObject:phone];
                            DDLogDebug(@"Label: %@, Number: %@", number.label, number.phone);
                        }
                        [userContacts addObject:ignightContact];
                        DDLogDebug(@"Emails: %@", cont.emails);
                    }
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [userClient uploadContacts: userContacts];
                }];
            } else {
                if (addressBookRef != nil) {
                    CFRelease(addressBookRef);
                }
                [self exitNumberView:nil];
            }
        });
    } else {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Failed to veirfy, try again please." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

-(void)userHttpClient:(UserHttpClient *)client uploadedAddressbook:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    NSDictionary *data = (NSDictionary *)response;
    if ([[data objectForKey:@"res"] boolValue]) {
        [self exitNumberView:nil];
    }
}

-(void)userHttpClient:(UserHttpClient *)client sentVerificationCode:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    NSDictionary *data = (NSDictionary *)response;
    if ([[data objectForKey:@"res"] boolValue]) {
        userPhone = userNumber.text;
        userNumber.text = @"";
        [submit removeTarget:self action:@selector(getVerificationCode:) forControlEvents:UIControlEventTouchUpInside];
        [submit addTarget:self action:@selector(verifyCode:) forControlEvents:UIControlEventTouchUpInside];
        label.text = @"Please enter the verification code.";
    } else {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Failed to send verification code. Please make sure you entered your full 10 digit phone number. " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}


@end
