//
//  IgnightWebViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/15/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightTopBar.h"

@interface IgnightWebViewController : UIViewController <UIWebViewDelegate,
IgnightTopBarDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *topBar;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIView *bottomBar;


@property (strong, nonatomic) NSString *venueName;
@property (strong, nonatomic) NSString *website;

- (IBAction)goBack:(id)sender;
- (IBAction)goForward:(UIButton *)sender;

-(void)refresh;
-(void)done;

@end
