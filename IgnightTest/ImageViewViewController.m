//
//  ImageViewViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/29/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "ImageViewViewController.h"
#import "IgnightTestAppDelegate.h"
#import "BuzzImageData.h"

static NSString *BUZZ_ID = @"buzzId";
static NSString *BUZZ_IMAGE = @"buzzText";

@interface ImageViewViewController ()

@end

@implementation ImageViewViewController
{
    ImageHttpClient *imageClient;
    NSMutableArray *buzzImages;
    NSMutableDictionary *buzzIdToImage;
    
    UILabel *emptyPlaceHolder;
    BOOL askedForImages;
    
    BOOL loaded;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    imageClient = [ImageHttpClient getInstance];
    buzzImages = [[NSMutableArray alloc] init];
    buzzIdToImage = [[NSMutableDictionary alloc] init];
    [self.collectionView setBackgroundColor:trendingToggleUnselectedColor];
    
    emptyPlaceHolder = [[UILabel alloc] init];
    emptyPlaceHolder.font = [UIFont systemFontOfSize:15];
    emptyPlaceHolder.numberOfLines = 5;
    emptyPlaceHolder.textColor = [UIColor grayColor];
    emptyPlaceHolder.backgroundColor = [UIColor clearColor];
    emptyPlaceHolder.textAlignment = NSTextAlignmentCenter;
    emptyPlaceHolder.alpha = 0;
    emptyPlaceHolder.frame = CGRectMake(60, self.view.frame.size.height / 2 - 100, 200, 100);
    [self.view addSubview:emptyPlaceHolder];
}

-(void)viewWillAppear:(BOOL)animated
{
    DDLogDebug(@"Buzz will appear: venueMode: %@" , [NSNumber numberWithBool:_venueMode]);
    imageClient.delegate = self;

    DDLogDebug(@"Just fetched images");
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self.view setBackgroundColor:buzzBackgroundColor];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSString *prePlaceholder = @"Head to Buzz to post a picture. Don't get crazy with the selfies!";
    emptyPlaceHolder.text = prePlaceholder;
}

-(void)loadImagesForGroup
{
    if (!loaded) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        loaded = YES;
    }
    [imageClient getGroupImages:_venueId];
}

-(void)loadImagesForVenue
{
    if (!loaded) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        loaded = YES;
    }
    [imageClient getVenueImages:_venueId];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger count = [buzzImages count];
    if (count > 0 || !askedForImages) {
        self.collectionView.alpha = 1;
        emptyPlaceHolder.alpha = 0;
        return count;
    } else {
        emptyPlaceHolder.alpha = 1;
        self.collectionView.alpha = 0;
    }
    return 0;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    emptyPlaceHolder.alpha = 0;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:101];
    UIButton *imgBtn = (UIButton *)[cell viewWithTag:102];
    [imgBtn addTarget:self action:@selector(clickedImage:) forControlEvents:UIControlEventTouchUpInside];
    
    imgView.image = nil;
    BuzzImageData *imageData = [buzzImages objectAtIndex:indexPath.row];
    
    [MBProgressHUD hideHUDForView:cell.contentView animated:YES];
    if (imageData.image == nil) {
//        [MBProgressHUD showHUDAddedTo:cell.contentView animated:YES];
        NSURLRequest *request = [NSURLRequest requestWithURL:imageData.imgUrl];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFImageResponseSerializer serializer];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            imageData.image = responseObject;
            [collectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DDLogDebug(@"Failed To Retrieve Image From : %@ with error: %@", imageData.imgUrl, error);
        }];
        [operation start];
    } else {
//        [MBProgressHUD hideHUDForView:cell.contentView animated:YES];
        imgView.image = [IgnightUtil getSquareImageFromImage:imageData.image];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.clipsToBounds = YES;
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:101];
    imgView.image = nil;
}

#pragma mark - ImageHttpClientDelegate

-(void)imageHttpClient:(ImageHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    DDLogDebug(@"ImageView: %@", [error debugDescription]);
//    IgAlert(nil, @"Failed to reach server", nil);
    askedForImages = YES;
}

-(void)imageHttpClient:(ImageHttpClient *)client gotGroupImages:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSArray *images = response[@"body"][@"buzz"];
        NSMutableArray *tempBuzzImages = [[NSMutableArray alloc] initWithCapacity:[images count]];
        for (NSDictionary *data in images) {
            NSNumber *buzzId = data[BUZZ_ID];
            
            if (buzzIdToImage[buzzId] == nil) {
                BuzzImageData *imgData = [[BuzzImageData alloc] init];
                imgData.imgUrl = [NSURL URLWithString:data[BUZZ_IMAGE]];
                imgData.buzzId = buzzId;
                [tempBuzzImages insertObject:imgData atIndex:0];
                buzzIdToImage[buzzId] = imgData;
            }
        }
        
        for (BuzzImageData *imgData in tempBuzzImages) {
            [buzzImages insertObject:imgData atIndex:0];
        }
        emptyPlaceHolder.alpha = 0;
        [self.collectionView reloadData];
    } else {
//        IgAlert(nil, @"Failed to load images", nil);
    }
    askedForImages = YES;
}

-(void)imageHttpClient:(ImageHttpClient *)client gotVenueImages:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSArray *images = response[@"body"][@"buzz"];
        for (NSDictionary *data in images) {
            NSNumber *buzzId = data[BUZZ_ID];
            
            if (buzzIdToImage[buzzId] == nil) {
                BuzzImageData *imgData = [[BuzzImageData alloc] init];
                imgData.imgUrl = [NSURL URLWithString:data[BUZZ_IMAGE]];
                [buzzImages addObject:imgData];
                buzzIdToImage[buzzId] = imgData;
            }
        }
        emptyPlaceHolder.alpha = 0;
        [self.collectionView reloadData];
    } else {
//        IgAlert(nil, @"Failed to load images", nil);
    }
    askedForImages = YES;
}

#pragma mark - ImgBtn

-(IBAction)clickedImage:(id)sender
{
    UICollectionViewCell *cell = (UICollectionViewCell *)[[sender superview] superview];
    NSIndexPath *path = [self.collectionView indexPathForCell:cell];
    
    [self.delegate clickedImageAtIndex:path.row];
}

-(NSArray *)getImageList
{
    return buzzImages;
}
@end
