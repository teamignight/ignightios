//
//  UserHttpClient.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//


#import <MapKit/MapKit.h>


@protocol UserHttpClientDelegate;

@interface UserHttpClient : AFHTTPSessionManager

@property (nonatomic, weak) id<UserHttpClientDelegate> delegate;

+(UserHttpClient *)getInstance;
-(instancetype)initWithBaseURL:(NSURL *)url;

-(void)login;
-(void)logout;
-(void)getUserInfo;
-(void)searchForUsers: (NSString *)search inGroup: (NSNumber *)groupId withOffset: (NSInteger)offset withLimit: (NSInteger)limit;
-(void)sendTemporaryPassword: (NSString *)username;
-(void)resetPassword: (NSString *)password toNewPassword: (NSString *)newPassword forUser: (NSString *)userName;
-(void)getUserTrendingListFrom: (NSInteger)from to: (NSInteger)to;
-(void)searchTrendingListWith: (NSString *)searchString from: (NSInteger)from to:(NSInteger)to;
-(void)getUserTrendingListFromCoordinate: (CLLocationCoordinate2D)coordinate withRadius: (CLLocationDistance)radius withCount: (NSInteger) count;
-(void)registerUser;
-(void)createUserWithEmail: (NSString *)email withUserName: (NSString *)userName withPassword: (NSString *)password;
-(void)requestVerificationCode: (NSString *)number;
-(void)verifyPhoneNumber: (NSString *)number withVerification: (NSString *)verification;
-(void)uploadContacts: (NSArray *)contacts;

//Called From User Profile
-(void)updateProfilePicture: (NSData *)image;
-(void)updateCity: (NSNumber *)cityId;
-(void)updateMusic: (NSArray *)music;
-(void)updateAtmosphere: (NSArray *)atmospheres;
-(void)updateSpedningLimit: (NSInteger)spending;
-(void)updateAge: (NSInteger)age;
-(void)updateEmail: (NSString *)email;
-(void)updateGroupInvitesPreference: (BOOL)preference;

//Get SideBar Info
-(void)getUserGroups;

//Add device token
-(void)addDeviceToken:(NSString*)deviceToken;

//Unsubscribe from buzz
-(void)unsubscribeFromBuzz;

//User Notifications
-(void)getUserNotifications;
-(void)clearUserGroupBuzzNotifications;
-(void)clearUserGroupInviteNotifications;

-(void)cancelAllOperations;

@end

@protocol UserHttpClientDelegate <NSObject>

@optional

-(void)userHttpClient: (UserHttpClient *)client gotUserInfo: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client didLoginWithResponse: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client returnedUsersForSearch: (NSDictionary *)response withOffset: (NSInteger)offset;
-(void)userHttpClient: (UserHttpClient *)client gotForgotPasswordResponse: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client gotResetPasswordResponse: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client gotUserTrendingData: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client gotUserTrendingData: (NSDictionary *)response fromCoordinate: (CLLocationCoordinate2D)coordinate withRadius: (CLLocationDistance) radius;
-(void)userHttpClient: (UserHttpClient *)client gotSearchResults: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client registeredUser: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client createdUserWithResponse: (NSDictionary *)response;

-(void)userHttpClient: (UserHttpClient *)client updatedProfilePricture: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client updatedCity: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client updatedMusic: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client updatedAtmosphere: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client updatedSpendingLimit: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client updatedAge: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client updatedEmail: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client updatedGroupInvitePreference: (NSDictionary *)response;

-(void)userHttpClient: (UserHttpClient *)client postedDeviceToken: (NSDictionary *)response;

-(void)userHttpClient: (UserHttpClient *)client gotUserGroups: (NSDictionary *)response;

-(void)userHttpClient: (UserHttpClient *)client gotLogoutResponse: (NSDictionary *)response;

-(void)userHttpClient: (UserHttpClient *)client sentVerificationCode: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client gotVerificationResposne: (NSDictionary *)response;
-(void)userHttpClient: (UserHttpClient *)client uploadedAddressbook: (NSDictionary *)response;

-(void)userHttpClient: (UserHttpClient *)client didFailWithError: (NSError *)error;

@end
