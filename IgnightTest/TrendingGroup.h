//
//  TrendingGroup.h
//  Ignight
//
//  Created by Abhinav Chordia on 1/25/15.
//  Copyright (c) 2015 Ignight. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrendingGroup : NSObject

@property NSNumber *groupId;
@property NSString *groupName;
@property NSString *groupImageUrl;
@property UIImage *groupImage;
@property BOOL privateGroup;
@property BOOL isMemberOfGroup;

-(id)initWithData: (NSDictionary *)data;

@end
