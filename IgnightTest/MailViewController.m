//
//  MailViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/16/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "MailViewController.h"
#import "Groups.h"
#import "Group.h"
#import "IgnightTopBar.h"
#import "Inbox.h"
#import "SWRevealViewController.h"
#import "NSDate+TimeAgo.h"
#import "GroupDetailsViewController.h"

@interface MailViewController ()

@end

@implementation MailViewController
{
    GroupHttpClient *groupClient;
    UserInfo *userInfo;
    Groups *groups;
    Inbox *inbox;
    
    UIRefreshControl *refreshMe;
    
    BOOL waitingForAcceptInfo;
    
    IgnightTopBar *titleBar;
    
    UIView *bgView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initVariables];
    [self setUpSideBarButton];
    [self setupTitleBar];
    [self setupMailView];
    
    bgView = [[UIView alloc] initWithFrame:_mailView.backgroundView.frame];
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-mail"]];
    img.frame = CGRectMake(60, 110, 200, 200);
    [bgView addSubview:img];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(45, 250, 230, 100)];
    label.text = @"You have no new group invites right now.";
    label.textColor = [UIColor darkGrayColor];
    label.numberOfLines = 5;
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 100;
    [bgView addSubview:label];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    groupClient.delegate = self;
    [self setupPushNotificationObserver];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removePushNotificationObserver];
}

- (void)initVariables
{
    userInfo = [UserInfo getInstance];
    groups = [Groups getInstance];
    inbox = [Inbox getInstance];
    groupClient = [GroupHttpClient getInstance];
    
    waitingForAcceptInfo = NO;
}

- (void)setUpSideBarButton
{
    self.revealViewController.delegate = self;
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

-(void)setupTitleBar
{
    titleBar = [[IgnightTopBar alloc] initWithTitle:@"Inbox" withSideBarButtonTarget:self.revealViewController];
    [self.topBar addSubview:titleBar];
    [self.topBar setUserInteractionEnabled:YES];
}

-(void)setupMailView
{
    self.mailView.delegate = self;
    self.mailView.dataSource = self;
    
    refreshMe = [[UIRefreshControl alloc] init];
    [refreshMe setTintColor:[UIColor blackColor]];
    [refreshMe addTarget:self action:@selector(refreshMail:) forControlEvents:UIControlEventValueChanged];
    [self.mailView addSubview:refreshMe];
    self.mailView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.mailView.bounds.size.width, 0.01)];
    
    [self getNewMail];
}

-(IBAction)refreshMail: (UIRefreshControl *)refresh
{
    [self getNewMail];
    [refresh endRefreshing];
}

-(void)getNewMail
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [groupClient getIncomingRequestsForUser];
}

-(void)setupPushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewMailPush:) name:@"NewMessageInInboxNotification" object:nil];
}

-(void)handleNewMailPush: (NSNotification *)notification
{
    [self getNewMail];
}

-(void)removePushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewMessageInInboxNotification" object:nil];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *invites = [inbox getInvitesForGroups];
    NSDictionary *invite = [invites objectAtIndex:indexPath.row];
    NSNumber *groupId = invite[@"groupId"];
    
    GroupDetailsViewController *grpDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"groupDetails"];
    grpDetails.groupId = groupId;
    grpDetails.allowToJoin = YES;
    grpDetails.acceptInvite = YES;
    [self.navigationController pushViewController:grpDetails animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [[inbox getInvitesForGroups] count];
    if (count == 0) {
        [tableView setBackgroundView:bgView];
    } else {
        [tableView setBackgroundView:nil];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mail"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"mail"];
    }
    
    NSArray *invites = [inbox getInvitesForGroups];
    NSDictionary *invite = [invites objectAtIndex:indexPath.row];
    
    UIImageView *userImage = (UIImageView *)[cell viewWithTag:200];
    
    NSURL *url = [NSURL URLWithString:[invite objectForKey:@"userImage"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFImageResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        userImage.image = responseObject;
        [[userImage layer] setCornerRadius:25];
        [[userImage layer] setMasksToBounds:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogDebug(@"Failed To Retrieve Image From : %@ with error: %@", url, error);
    }];
    [operation start];
    
    UILabel *groupName = (UILabel *)[[cell contentView] viewWithTag:201];
    UILabel *requestFrom = (UILabel *)[cell viewWithTag:202];
    UILabel *howLongAgo = (UILabel *)[cell viewWithTag:203];
    
    groupName.text = [invite objectForKey:@"groupName"];
    requestFrom.text = [NSString stringWithFormat:@"Request from %@", [invite objectForKey:@"userName"]];
    
    NSNumber *milliTime = [invite objectForKey:@"timeStamp"];
    NSTimeInterval interval = [milliTime floatValue];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:interval / 1000];
    
    howLongAgo.text = [date timeAgo];
    
    cell.contentView.tag = [[invite objectForKey:@"groupId"] integerValue];
    
    return cell;
}

-(IBAction)acceptedInvite:(id)sender
{
    if (waitingForAcceptInfo == NO) {
        UIButton *button = (UIButton *)sender;
        NSNumber *groupId = [NSNumber numberWithInteger:[button superview].tag];
        
        [groupClient respondToGroup:groupId withResponse:YES];
        waitingForAcceptInfo = YES;
    }
}

-(IBAction)rejectedInvite:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSNumber *groupId = [NSNumber numberWithInteger:[button superview].tag];
    
    [groupClient respondToGroup:groupId withResponse:NO];
}

#pragma mark TableViewDelegates -


#pragma mark - GroupHttpClientDelegat

-(void)groupHttpClient:(GroupHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)groupHttpClient:(GroupHttpClient *)client gotIncomingGroupRequests:(NSDictionary *)response
{
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSArray *body = [response objectForKey:@"body"];
        [inbox gotNewGroupRequests:body];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
    [self.mailView reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)groupHttpClient:(GroupHttpClient *)client groupInviteConfirmedForGroup:(NSNumber *)groupId withResponse:(NSDictionary *)response
{
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSDictionary *msg = response[@"body"];
        Group *acceptedGroup = [[Group alloc] init];
        acceptedGroup.name = [msg objectForKey:@"name"];
        acceptedGroup.publicGroup = ![[msg objectForKey:@"private"] boolValue];
        acceptedGroup.groupId = groupId.integerValue;
        [groups addGroup:acceptedGroup];
        [self.delegate mailViewController:self acceptedInviteToGroup:acceptedGroup];
        
        [inbox deleteInviteFromGroup:[msg objectForKey:@"groupId"]];
        [self.mailView reloadData];
    } else {
//        IgAlert(nil, response[@"reason"], nil);
    }
    waitingForAcceptInfo = NO;
}

-(void)groupHttpClient:(GroupHttpClient *)client groupRejectedConfirmedForGroup:(NSNumber *)groupId withResponse:(NSDictionary *)response
{
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSDictionary *msg = response[@"body"];
        [inbox deleteInviteFromGroup:[msg objectForKey:@"groupId"]];
        [self.mailView reloadData];
    } else {
//        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)gotMail:(NSArray *)mail
{
    if (mail != nil) {
        [inbox gotNewGroupRequests:mail];
        [self.mailView reloadData];
    }
}

#pragma mark NSURLConnection methods -


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
