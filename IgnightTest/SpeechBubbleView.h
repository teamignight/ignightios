//
//  SpeechBubbleView.h
//  PushChatStarter
//
//  Created by Kauserali on 28/03/13.
//  Copyright (c) 2013 Ray Wenderlich. All rights reserved.
//

// The bubble type indicates whether it's a left-hand or right-hand bubble
typedef enum
{
	BubbleTypeLefthand = 0,
	BubbleTypeRighthand,
}
BubbleType;

#import "BuzzMessage.h"
#import "IgnightColors.h"

// A UIView that shows a speech bubble
@interface SpeechBubbleView : UIView 

// Calculates how big the speech bubble needs to be to fit the specified text
+ (CGSize)sizeForMessage:(BuzzMessage*)message;
+ (CGSize)sizeForImage: (UIImage *)image;

// Configures the speech bubble
- (void)setText:(NSString*)text bubbleType:(BubbleType)bubbleType;
- (void)setImage:(UIImage *)image bubbleType:(BubbleType)newBubbleType;

- (CGFloat)getLeftPadding;
- (CGFloat)getRightPadding;
- (CGFloat)getTopPadding;
- (CGFloat)getBottomPadding;

- (CGFloat)getImageLeftPadding;
- (CGFloat)getImageRightPadding;
- (CGFloat)getImageTopPadding;
- (CGFloat)getImageBottomPadding;

@end
