//
//  DNAStepOne.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/29/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "DNAStepOne.h"

#define CITY_TAG 100
#define AGE_TAG 200
#define GENDER_TAG 300

#define BUTTON_HEIGHT 50

@interface DNAStepOne ()

@end

@implementation DNAStepOne
{
    UserInfo *userInfo;
    int lastYPos;
    
    //State
    NSInteger citySelected;
    NSInteger ageBuckedSelected;
    NSInteger genderSelected;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    userInfo = [UserInfo getInstance];
    
    citySelected = -1;
    ageBuckedSelected = -1;
    genderSelected = -1;
    
    [self setupScrollView];
    self.navigationController.navigationBarHidden = YES;
    
    self.continueBtn.alpha = 0.4;
    self.continueBtn.userInteractionEnabled = NO;
    self.continueBtn.backgroundColor = whiteFooterBackground;
    [self.continueBtn setTitleColor:grayHeaderFooterText forState:UIControlStateNormal];
    [[self.continueBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[self.continueBtn layer] setBorderWidth:0.3];
    
    self.headerLabel.backgroundColor = whiteHeaderBackground;
    [self.headerLabel setTextColor:grayHeaderFooterText];
    [[self.headerLabel layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[self.headerLabel layer] setBorderWidth:0.3];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)setupScrollView
{
    [self.view addSubview:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(320.0, 411.0);

    [self setupScrollViewBackground];
    [self addHeader:@"Choose Your City"];
    [self addCityButtons];
    [self addHeader:@"Choose Your Age Group"];
    [self addAgeBucketButtons];
    [self addHeader:@"Choose Your Gender"];
    [self addGenderButtons];
}

-(void)setupScrollViewBackground
{
    UIImage *bg = [UIImage imageNamed:@"step_1_bg"];
    UIImageView *bgView = [[UIImageView alloc] initWithImage:bg];
    bgView.alpha = 0.15;
    [self.scrollView addSubview:bgView];
}

-(void)addHeader: (NSString *)header
{
    if (lastYPos != 0)
        lastYPos += 20;
    
    CGRect cityHeaderFrame = CGRectMake(0, lastYPos, self.scrollView.frame.size.width, 30);
    UILabel *cityHeader = [[UILabel alloc] initWithFrame:cityHeaderFrame];
    [cityHeader setBackgroundColor:inactiveBtnBackground];
    [cityHeader setText:header];
    [cityHeader setTextColor:inactiveBtnText];
    [cityHeader setTextAlignment:NSTextAlignmentCenter];
    [self.scrollView addSubview:cityHeader];
    
    lastYPos += cityHeader.frame.size.height;
    
    UILabel *topBorder = [[UILabel alloc] initWithFrame:CGRectMake(0, lastYPos, self.scrollView.frame.size.width, 0.5)];
    topBorder.backgroundColor = inactiveBtnBorder;
    [self.scrollView addSubview:topBorder];
}

-(void)addCityButtons
{
    int xPos = 20;
    lastYPos += 15;
    
    for (int i = 0; i < CITY_COUNT; i++) {
        CGRect buttonFrame = CGRectMake(xPos, lastYPos, 280, BUTTON_HEIGHT);
        UIButton *city = [UIButton buttonWithType:UIButtonTypeCustom];
        city.frame = buttonFrame;
        [city setTitle:[Cities getCityNameFromId:i] forState:UIControlStateNormal];
        [city setTitle:[Cities getCityNameFromId:i] forState:UIControlStateSelected];
        [[city titleLabel] setTextColor:inactiveBtnText];
        [[city layer] setBorderColor:inactiveBtnBorder.CGColor];
        [[city layer] setBorderWidth:0.5];
        [[city layer] setCornerRadius:5];
        [city addTarget:self action:@selector(cityButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [city setTag:i + CITY_TAG];
        [self.scrollView addSubview:city];
        
        [self setState:citySelected forButton:city];
        [city setUserInteractionEnabled:NO];
        citySelected = i + CITY_TAG;
        
        if (i == CITY_COUNT - 1)
            break;
        else if (i % 2 == 1) {
            xPos = 20;
            lastYPos += buttonFrame.size.height + 5;
        } else
            xPos = 168;
    }
    
    lastYPos += BUTTON_HEIGHT;
    
    UITextView *textView = [[UITextView alloc] init];
    textView.text = @"More cities coming soon!";
    textView.textAlignment = UITextAlignmentCenter;
    CGRect frame = CGRectMake(0, lastYPos - 22, [[UIScreen mainScreen] bounds].size.width, 30);
    textView.frame = frame;
    textView.textColor = UIColorFromRGB(0xffffff);
    textView.alpha = 0.75;
    textView.font = [UIFont systemFontOfSize:10];
    textView.backgroundColor = [UIColor clearColor];
    [textView setEditable:NO];
    
    [_scrollView addSubview:textView];
}

-(void)addAgeBucketButtons
{
    int xPos = 20;
    lastYPos += 15;
    
    for (int i = 0; i < AGE_BUCKETS_COUNT; i++) {
        CGRect ageBucketFrame = CGRectMake(xPos, lastYPos, 89, BUTTON_HEIGHT);
        UIButton *age = [UIButton buttonWithType:UIButtonTypeCustom];
        age.frame = ageBucketFrame;
        [age setTitle:[AgeBuckets getAgeBucketNameFromId:i] forState:UIControlStateNormal];
        [age setTitle:[AgeBuckets getAgeBucketNameFromId:i] forState:UIControlStateSelected];
        [[age titleLabel] setTextColor:inactiveBtnText];
        [[age layer] setBorderColor:inactiveBtnBorder.CGColor];
        [[age layer] setBorderWidth:0.5];
        [[age layer] setCornerRadius:5];
        [age addTarget:self action:@selector(ageButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [age setTag:i + AGE_TAG];
        
        [self clearSelectedButton:age];
        
        [self.scrollView addSubview:age];
        
        if (i == AGE_BUCKETS_COUNT - 1)
            break;
        else if (i % 3 == 2) {
            xPos = 20;
            lastYPos += BUTTON_HEIGHT + 10;
        } else
            xPos += 96;
    }
    lastYPos += BUTTON_HEIGHT;
}

-(void)addGenderButtons
{
    int xPos = 20;
    lastYPos += 15;
    
    for (int i = 0; i < GENDER_COUNT; i++) {
        CGRect genderFrame = CGRectMake(xPos, lastYPos, 132, BUTTON_HEIGHT);
        UIButton *buttonFrame = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonFrame.frame = genderFrame;
        [buttonFrame setTitle:[Gender getNameFromId:i] forState:UIControlStateNormal];
        [buttonFrame setTitle:[Gender getNameFromId:i] forState:UIControlStateSelected];
        [[buttonFrame titleLabel] setTextColor:inactiveBtnText];
        [[buttonFrame layer] setBorderColor:inactiveBtnBorder.CGColor];
        [[buttonFrame layer] setBorderWidth:0.5];
        [[buttonFrame layer] setCornerRadius:5];
        [buttonFrame addTarget:self action:@selector(genderButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [buttonFrame setTag:i + GENDER_TAG];
        [self.scrollView addSubview:buttonFrame];
        
        [self clearSelectedButton:buttonFrame];
        
        if (i == GENDER_COUNT - 1)
            break;
        else if (i % 2 == 1) {
            xPos = 20;
            lastYPos += genderFrame.size.height + 5;
        } else
            xPos = 168;
    }
}


-(IBAction)cityButtonClicked:(id)sender
{
    citySelected = [self setState:citySelected forButton:sender];
    [self continueToNext];
}

-(IBAction)ageButtonClicked:(id)sender
{
    ageBuckedSelected = [self setState:ageBuckedSelected forButton:sender];
    [self continueToNext];
}

-(IBAction)genderButtonClicked:(id)sender
{
    genderSelected = [self setState:genderSelected forButton:sender];
    [self continueToNext];
}

-(NSInteger)setState: (NSInteger)state forButton: (UIButton*)sender
{
    if (state == [(UIButton *)sender tag]) {
        return [self clearSelectedButton:sender];
    } else {
        if (state >= 0) {
            UIButton *selectedButton = (UIButton*)[[sender superview] viewWithTag:state];
            [self clearSelectedButton:selectedButton];
        }

        [sender setTitleColor:activeBtnText forState:UIControlStateNormal];
        [sender setBackgroundColor:activeBtnBackground];
        return [sender tag];
    }
}

-(NSInteger)clearSelectedButton: (UIButton*)sender
{
    [sender setTitleColor:inactiveBtnText forState:UIControlStateNormal];
    [sender setBackgroundColor:inactiveBtnBackground];
    return -1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    DDLogDebug(@"Memory Warning in DNAStepOne");
}

-(void)continueToNext
{
    if (citySelected < 0 || ageBuckedSelected < 0 || genderSelected < 0) {
        self.continueBtn.alpha = 0.4;
        self.continueBtn.userInteractionEnabled = NO;
        [self.continueBtn setTitleColor:grayHeaderFooterText forState:UIControlStateNormal];
        return;
    }

    userInfo.cityId = [NSNumber numberWithInteger:citySelected - CITY_TAG];
    userInfo.ageBucketId = [NSNumber numberWithInteger:ageBuckedSelected - AGE_TAG];
    userInfo.genderId = [NSNumber numberWithInteger:genderSelected - GENDER_TAG];

    self.continueBtn.alpha = 1;
    self.continueBtn.userInteractionEnabled = YES;
    [self.continueBtn setTitleColor:registrationContinueBtnColor forState:UIControlStateNormal];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
