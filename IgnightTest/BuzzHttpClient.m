//
//  BuzzHttpClient.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/6/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "BuzzHttpClient.h"

@implementation BuzzHttpClient
{
    UserInfo *userInfo;
}

+(BuzzHttpClient *)getInstance
{
    static BuzzHttpClient *_sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *url = server_url;
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:url]];
        [_sharedInstance.requestSerializer setValue:[IgnightProperties getHeaderToken] forHTTPHeaderField:@"X-IGNIGHT-TOKEN"];
        [_sharedInstance.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"Platform"];
        UserInfo* userInfo = [UserInfo getInstance];
        if (userInfo.userId != nil) {
            [_sharedInstance.requestSerializer setValue:userInfo.userId.stringValue forHTTPHeaderField:@"USER_ID"];
        }
        if (TARGET_IPHONE_SIMULATOR) {
        } else {
            _sharedInstance.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
            _sharedInstance.securityPolicy.allowInvalidCertificates = YES;
        }
    });
    return _sharedInstance;
}

-(instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        userInfo = [UserInfo getInstance];
    }
    
    return self;
}

-(void)sendTextBuzz:(NSString *)buzz forGroup:(NSNumber *)groupId
{
    NSDictionary *data = @{@"userId" : userInfo.userId,
                           @"buzzText" : buzz};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"buzz/add/group/text/%@", groupId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate buzzHttpClient:self postedTextBuzz:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate buzzHttpClient:self didFailWithError:error];
    }];
}

-(void)sendImageBuzz:(NSData *)buzz forGroup:(NSNumber *)groupId
{
    NSDictionary *data = @{@"userId" : userInfo.userId};
    NSDictionary *msg = @{@"msg" : data};
    NSData *body = [NSJSONSerialization dataWithJSONObject:msg options:kNilOptions error:nil];
    NSDictionary *parameters = @{@"json_body" : body};
    
    DDLogDebug(@"Sending Group Buzz: %@", data);
    
    NSString *path = [NSString stringWithFormat:@"buzz/add/group/image/%@", groupId];
    [self POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:buzz name:@"userImage" fileName:@"userImage" mimeType:@"image/jpeg"];
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate buzzHttpClient:self postedImageBuzz:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate buzzHttpClient:self didFailWithError:error];
    }];
}

-(void)sendImageBuzz:(NSData *)buzz forVenue:(NSNumber *)venueId
{
    NSDictionary *data = @{@"userId" : userInfo.userId};
    NSDictionary *msg = @{@"msg" : data};
    NSData *body = [NSJSONSerialization dataWithJSONObject:msg options:kNilOptions error:nil];
    NSDictionary *parameters = @{@"json_body" : body};
    
    DDLogDebug(@"Sending Venue Buzz: %@", data);
    
    NSString *path = [NSString stringWithFormat:@"buzz/add/venue/image/%@", venueId];
    [self POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:buzz name:@"userImage" fileName:@"userImage" mimeType:@"image/jpeg"];
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate buzzHttpClient:self postedImageBuzz:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate buzzHttpClient:self didFailWithError:error];
    }];
}

-(void)getLatestBuzz: (NSNumber *)lastBuzzId forGroup:(NSNumber *)groupId
{
    NSDictionary *parameters = @{@"lastBuzzId": lastBuzzId,
                                 @"count" : [NSNumber numberWithInteger:MAX_BUZZ_MSGS],
                                 @"latest" : [NSNumber numberWithBool:YES],
                                 @"userId" : userInfo.userId};
    
    DDLogDebug(@"Getting Latest Buzz: LastId: %@", lastBuzzId);
    
    NSString *path = [NSString stringWithFormat:@"buzz/group/%@", groupId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate buzzHttpClient:self gotLatestBuzz:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate buzzHttpClient:self didFailWithError:error];
    }];
}

-(void)getGroupBuzz: (NSNumber *)groupId withStartIndex: (NSNumber *)startIndex withCount: (NSNumber *)count
{
    NSDictionary *parameters = @{@"userId" : userInfo.userId,
                                 @"lastBuzzId" : startIndex,
                                 @"latest" : [NSNumber numberWithBool:NO],
                                 @"count" : count};
    
    DDLogDebug(@"Getting Buzz: LastId: %@, Count: %@", startIndex, count);
    
    NSString *path = [NSString stringWithFormat:@"buzz/group/%@", groupId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate buzzHttpClient:self gotBuzz:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate buzzHttpClient:self didFailWithError:error];
    }];
}

-(void)sendTextBuzz:(NSString *)buzz forVenue:(NSNumber *)venueId
{
    NSDictionary *data = @{@"userId" : userInfo.userId,
                           @"buzzText" : buzz};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"buzz/add/venue/text/%@", venueId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate buzzHttpClient:self postedTextBuzz:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate buzzHttpClient:self didFailWithError:error];
    }];
}

-(void)getLatestBuzz: (NSNumber *)lastBuzzId forVenue:(NSNumber *)venueId
{
    NSDictionary *parameters = @{@"userId" : userInfo.userId,
                                 @"lastBuzzId": lastBuzzId,
                                 @"latest" : [NSNumber numberWithBool:YES],
                                 @"count" : [NSNumber numberWithInteger:MAX_BUZZ_MSGS]};
    
    NSString *path = [NSString stringWithFormat:@"buzz/venue/%@", venueId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate buzzHttpClient:self gotLatestBuzz:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate buzzHttpClient:self didFailWithError:error];
    }];
}

-(void)getVenueBuzz: (NSNumber *)venueId withStartIndex: (NSNumber *)startIndex withCount: (NSNumber *)count
{
    NSDictionary *parameters = @{@"userId" : userInfo.userId,
                                 @"lastBuzzId" : startIndex,
                                 @"latest" : [NSNumber numberWithBool:NO],
                                 @"count" : count};

    NSString *path = [NSString stringWithFormat:@"buzz/venue/%@", venueId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate buzzHttpClient:self gotBuzz:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate buzzHttpClient:self didFailWithError:error];
    }];
}


-(void)unsubscribeFromVenue: (NSNumber *)venueId
{
    NSDictionary *data = @{@"venueId" : venueId};
    NSDictionary *msg = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"unsubscribe/venue/%@", userInfo.userId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

-(void)unsubscribeFromGroup: (NSNumber *)groupId
{
    NSDictionary *data = @{@"groupId" : groupId};
    NSDictionary *msg = @{@"msg" : data};
        NSString *path = [NSString stringWithFormat:@"unsubscribe/group/%@", userInfo.userId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

-(void)flagBuzz:(NSNumber *)buzzId inGroup:(NSNumber *)groupId
{
    NSDictionary *data = @{@"userId" : userInfo.userId,
                           @"buzzId" : buzzId};
    NSDictionary *msg = @{@"msg" : data};
    NSString *path = [NSString stringWithFormat:@"buzz/flag/group/%@", groupId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

-(void)flagBuzz:(NSNumber *)buzzId inVenue:(NSNumber *)venueId
{
    NSDictionary *data = @{@"userId" : userInfo.userId,
                           @"buzzId" : buzzId};
    NSDictionary *msg = @{@"msg" : data};
    NSString *path = [NSString stringWithFormat:@"buzz/flag/venue/%@", venueId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}






@end
