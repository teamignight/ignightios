//
//  MessageTableViewCell.m
//  PushChatStarter
//
//  Created by Kauserali on 28/03/13.
//  Copyright (c) 2013 Ray Wenderlich. All rights reserved.
//

#import "MessageTableViewCell.h"
#import "BuzzMessage.h"
#import "SpeechBubbleView.h"
#import "IgnightUtil.h"
#import "NSDate+TimeAgo.h"
#import <QuartzCore/QuartzCore.h>
#import "IgnightColors.h"

static UIColor* color = nil;

#define reportWidth 60

@interface MessageTableViewCell() {
    BuzzMessage *cellMessage;
    
    UIImageView *_userArrow;
    UIImageView *_otherUserArrow;
    
    UIImageView *_userImage;
    
    UIButton *_imageButton;
    UIImageView *_imageView;
    UIView *_imgViewBg;
    
    UITextView *_textView;
    SpeechBubbleView *_bubbleView;
	UILabel *_label;
    
    UILabel *_separator;
    
    UILabel *_timeStamp;
    UIButton *_report;
}
@end

@implementation MessageTableViewCell

+ (void)initialize
{
	if (self == [MessageTableViewCell class])
	{
        color = buzzBackgroundColor;
	}
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier
{
	if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]))
	{
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // Create the user image view
        _userImage = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_userImage setImage:[UIImage imageNamed:@"group_member_default"]];
        _userImage.contentMode = UIViewContentModeScaleAspectFill;
        _userImage.backgroundColor = buzzUserProfileBackground;
        _userImage.clipsToBounds = YES;
        [self.contentView addSubview:_userImage];
        
        _imgViewBg = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_imgViewBg];
        
        //Create the image view
        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _imageView.contentMode = UIViewContentModeCenter;
        _imageView.clipsToBounds = YES;
        [self.contentView addSubview:_imageView];
        
        //Create the imageView button
        _imageButton = [[UIButton alloc] initWithFrame:CGRectZero];
        _imageButton.backgroundColor = [UIColor clearColor];
        [_imageButton addTarget:self action:@selector(imageSelected:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_imageButton];
        
		// Create the speech bubble view
		_bubbleView = [[SpeechBubbleView alloc] initWithFrame:CGRectZero];
		_bubbleView.backgroundColor = [UIColor clearColor];
		_bubbleView.opaque = YES;
		_bubbleView.clearsContextBeforeDrawing = NO;
		_bubbleView.contentMode = UIViewContentModeRedraw;
		_bubbleView.autoresizingMask = 0;
		[self.contentView addSubview:_bubbleView];
        
        _textView = [[UITextView alloc] init];
        _textView.frame = CGRectZero;
        _textView.backgroundColor = [UIColor clearColor];
        _textView.editable = NO;
        _textView.scrollEnabled = NO;
        [self.contentView addSubview:_textView];

		// Create the label
		_label = [[UILabel alloc] initWithFrame:CGRectZero];
		_label.backgroundColor = [UIColor clearColor];
        _label.textAlignment = NSTextAlignmentLeft;
		_label.opaque = YES;
		_label.clearsContextBeforeDrawing = NO;
		_label.contentMode = UIViewContentModeRedraw;
		_label.autoresizingMask = 0;
		_label.font = [UIFont systemFontOfSize:13];
		_label.textColor = otherBuzzBubbleUsernameText;
		[self.contentView addSubview:_label];
        
        _timeStamp = [[UILabel alloc] initWithFrame:CGRectZero];
        _timeStamp.textColor = buzzTimeStampColor;
        _timeStamp.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_timeStamp];
        
        _report = [[UIButton alloc] initWithFrame:CGRectZero];
        [_report setTitleColor:otherBuzzBubbleUsernameText forState:UIControlStateNormal];
        [_report setTitleColor:otherBuzzBubbleUsernameText forState:UIControlStateHighlighted];
        [_report setTitleColor:otherBuzzBubbleUsernameText forState:UIControlStateSelected];
        [_report setTitle:@"Report" forState:UIControlStateNormal];
        [_report setTitle:@"Report" forState:UIControlStateSelected];
        [_report setTitle:@"Report" forState:UIControlStateHighlighted];
        [_report addTarget:self action:@selector(reportBuzz:) forControlEvents:UIControlEventTouchUpInside];
        [_report setAlpha:0.5];
        [_report setFont:[UIFont systemFontOfSize:10]];
        [self.contentView addSubview:_report];
        
        _separator = [[UILabel alloc] initWithFrame:CGRectZero];
        [_separator setBackgroundColor:trendingToggleUnselectedColor];
        [self.contentView addSubview:_separator];
        
        _userArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"buzz_arrow_mine"]];
        _userArrow.frame = CGRectZero;
        [self.contentView addSubview:_userArrow];
        
        _otherUserArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"buzz_arrow"]];
        _otherUserArrow.frame = CGRectZero;
        [self.contentView addSubview:_otherUserArrow];
	}
	return self;
}

- (void)layoutSubviews
{
	// This is a little trick to set the background color of a table view cell.
	[super layoutSubviews];
	self.backgroundColor = color;
}

-(void)imageSelected:(id)sender
{
//    [self.delegate imageSelectedWithBuzzId:cellMessage.buzzId withImageIndex:cellMessage.buzzImageIndex];
    [self.delegate imageSelected:cellMessage.image];
}

-(void)resetSubviews
{
    _userImage.frame = CGRectZero;
    _userImage.image = nil;
    _imageView.frame = CGRectZero;
    _imageView.image = nil;
    _imgViewBg.frame = CGRectZero;
    _imgViewBg.backgroundColor = [UIColor clearColor];
    _imageButton.frame = CGRectZero;

    _bubbleView.frame = CGRectZero;
    _textView.frame = CGRectZero;
    _textView.text = nil;
    _textView.scrollEnabled = NO;
    _textView.selectable = NO;
    _textView.editable = YES;
    _textView.editable = NO;
    _label.frame = CGRectZero;
    _label.text = @"";
    _separator.frame = CGRectZero;
    _timeStamp.frame = CGRectZero;
    
    _userArrow.frame = CGRectZero;
    _otherUserArrow.frame = CGRectZero;
    _report.frame = CGRectZero;
}

-(IBAction)reportBuzz:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"IgNight" message:@"Are you sure you want to report this as inappropriate?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [_delegate reportBuzz:cellMessage.buzzId];
    }
}

-(void)setTextMessage: (BuzzMessage *)message
{
    NSString* senderName;
    CGPoint point = CGPointZero;
    BubbleType bubbleType;
    
    NSInteger padding;
    if ([message myMessage]) {
        padding = 10;
        
        bubbleType = BubbleTypeRighthand;
        point.x = self.bounds.size.width - message.bubbleSize.width - padding;
        
        _userArrow.frame = CGRectMake(self.bounds.size.width - padding, point.y + 8, 6, 9);
        _timeStamp.textAlignment = NSTextAlignmentRight;
        [_imgViewBg setBackgroundColor:myBuzzBubbleBackground];
    } else {
        padding = 5;
        bubbleType = BubbleTypeLefthand;
        senderName = message.senderName;
        _timeStamp.textAlignment = NSTextAlignmentLeft;
        _label.text = [NSString stringWithFormat:@"  %@", senderName];
        
        point.x = padding;
        
        //Set the arrow location
        _otherUserArrow.frame = CGRectMake(point.x - 4, point.y + 8, 6, 9);
        
        //Set The User Image View
        CGRect frame;
        frame.origin = CGPointMake(point.x, 0);
        frame.size = CGSizeMake(35, 35);
        _userImage.frame = frame;
//        _userImage.image = [IgnightUtil getBuzzUserPic:message.userImage];
        _userImage.image = message.userImage;
        [_userImage.layer setCornerRadius:17.5];
        [_userImage.layer setMasksToBounds:YES];
        
        //Adjust point to be 40pixels from 0
        point.x += _userImage.frame.size.width;
        
        //Set the label
        CGFloat nameWidth = ceilf([senderName boundingRectWithSize:CGSizeMake(200, 9999)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]}
                                                       context:nil].size.width);
        nameWidth += ([_bubbleView getLeftPadding] + [_bubbleView getRightPadding]);

        [_label setFrame:CGRectMake(point.x + [_bubbleView getLeftPadding], point.y, nameWidth, 25)];
        [[_label layer] setCornerRadius:4];
        [[_label layer] setMasksToBounds:YES];
        _label.textColor = otherBuzzBubbleUsernameText;
        point.y += _label.frame.size.height;
        
        CGRect reportFrame = CGRectMake(screenWidth - reportWidth, _label.frame.origin.y, reportWidth, 15);
        _report.frame = reportFrame;
        
        //Set background view
        [_imgViewBg setBackgroundColor:otherBuzzBubbleBackground];
        
        //Adjust x coordinate for bubbleview
        point.x += 10;
        
        //Set the arrow
        _otherUserArrow.frame = CGRectMake(point.x - 6, 8, 6, 9);
    }
    
    //Set the rect for bubbleview
    CGRect rect;
    rect.origin = point;
    rect.size = message.bubbleSize;
    
    //Set the bubbleView
    _textView.attributedText = [[NSAttributedString alloc] initWithString:message.message attributes:nil];
    _textView.font = [UIFont systemFontOfSize:15];
    _textView.textColor = (bubbleType == BubbleTypeLefthand) ? buzzTextColor : [UIColor whiteColor];
    _textView.dataDetectorTypes = UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber | UIDataDetectorTypeAddress;
    _textView.frame = CGRectMake(rect.origin.x + 5, rect.origin.y - 2, rect.size.width - 10, rect.size.height + 10);//, <#double#> textRect.size.width + 10, textRect.size.height + 10);
    
    //Set the background
    CGRect imgFrame;
    imgFrame.origin.x = rect.origin.x;
    imgFrame.origin.y = 0;
    imgFrame.size.width = message.bubbleSize.width;
    imgFrame.size.height = message.bubbleSize.height + point.y;
    [_imgViewBg setFrame:imgFrame];
    [[_imgViewBg layer] setCornerRadius:4];
    [[_imgViewBg layer] setMasksToBounds:YES];
    
    /*
     Setup Timestamp Label
     */
    CGRect tsFrame;
    tsFrame.origin.x = _imgViewBg.frame.origin.x;
    tsFrame.origin.y = _imgViewBg.frame.origin.y + _imgViewBg.frame.size.height + 2;
    tsFrame.size = [[message.date timeAgo] boundingRectWithSize:CGSizeMake(200, 15)
                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10]}
                                                        context:nil].size;
    
    if (message.myMessage) {
        tsFrame.origin.x = _imgViewBg.frame.origin.x + _imgViewBg.frame.size.width - tsFrame.size.width - 2;
    } else {
        //Set the Separator
        [_separator setFrame:CGRectMake(imgFrame.origin.x, _label.frame.origin.y + _label.frame.size.height, imgFrame.size.width, 0.5)];
        point.y += _separator.frame.size.height;
        
        tsFrame.origin.x = _imgViewBg.frame.origin.x + 2;
    }
    
    _timeStamp.frame = tsFrame;
    _timeStamp.text = [message.date timeAgo];
}

-(void)setImageMessage: (BuzzMessage *)message
{
    CGPoint point = CGPointZero;
    NSInteger padding;
    
	if ([message myMessage])
	{
        padding = 10;
		point.x = self.bounds.size.width - message.bubbleSize.width - padding;
        [_imgViewBg setBackgroundColor:myBuzzBubbleBackground];
        _timeStamp.textAlignment = NSTextAlignmentRight;
        
        _userArrow.frame = CGRectMake(self.bounds.size.width - padding, point.y + 8, 6, 9);
	}
	else
	{
        padding = 5;
        _timeStamp.textAlignment = NSTextAlignmentLeft;
        _label.text = [NSString stringWithFormat:@"  %@", message.senderName];
        
        point.x = padding;
        
        //Set The User Image View
        CGRect frame;
        frame.origin = CGPointMake(point.x, 0);
        frame.size = CGSizeMake(35, 35);
        _userImage.frame = frame;
//        _userImage.image = [IgnightUtil getBuzzUserPic:message.userImage];
        _userImage.image = message.userImage;
        [_userImage.layer setCornerRadius:17.5];
        [_userImage.layer setMasksToBounds:YES];
        
        //Set x position for bubble
        point.x += _userImage.frame.size.width;
        
        [_label setFrame:CGRectMake(point.x + [_bubbleView getRightPadding], point.y, 160, 25)];
        point.y += _label.frame.size.height;
        _label.textColor = otherBuzzBubbleUsernameText;
        
        CGRect reportFrame = CGRectMake(screenWidth - reportWidth, _label.frame.origin.y, reportWidth, 15);
        _report.frame = reportFrame;
        
        [_imgViewBg setBackgroundColor:otherBuzzBubbleBackground];
        
        point.x += 10;
        
        //Set the arrow
        _otherUserArrow.frame = CGRectMake(point.x - 6, 8, 6, 9);
	}
    
    CGRect rect;
    rect.origin.x = point.x + [_bubbleView getImageLeftPadding];
    rect.origin.y = point.y + [_bubbleView getImageTopPadding];
    rect.size.width = message.bubbleSize.width - [_bubbleView getImageLeftPadding] - [_bubbleView getImageRightPadding];
    rect.size.height = message.bubbleSize.height - [_bubbleView getImageTopPadding] - [_bubbleView getBottomPadding];
    [_imageView setFrame:rect];
    [_imageView setContentMode:UIViewContentModeScaleAspectFill];

    [_imageView setImage:message.image];
    
    [_imageButton setFrame:rect];
    
    CGRect bgFrame;
    bgFrame.origin.x = point.x;
    bgFrame.origin.y = 0;
    bgFrame.size.width = message.bubbleSize.width;
    bgFrame.size.height = message.bubbleSize.height + point.y;
    
    [_imgViewBg setFrame:bgFrame];
    [[_imgViewBg layer] setCornerRadius:4];
    [[_imgViewBg layer] setMasksToBounds:YES];
    
    //Setup the timestamp
    CGRect tsFrame;
    tsFrame.origin.x = _imgViewBg.frame.origin.x;
    tsFrame.origin.y = _imgViewBg.frame.origin.y + _imgViewBg.frame.size.height + 2;
    tsFrame.size = [[message.date timeAgo] boundingRectWithSize:CGSizeMake(200, 15) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10]} context:nil].size;
    
    if (message.myMessage) {
        tsFrame.origin.x = _imgViewBg.frame.origin.x + _imgViewBg.frame.size.width - tsFrame.size.width - 2;
    } else {
        [_separator setFrame:CGRectMake(bgFrame.origin.x, _label.frame.origin.y + _label.frame.size.height, bgFrame.size.width, 0.5)];
        tsFrame.origin.x = _imgViewBg.frame.origin.x + 2;
    }
    
    _timeStamp.frame = tsFrame;
    _timeStamp.text = [message.date timeAgo];
}

- (void)setMessage:(BuzzMessage*)message
{
    cellMessage = message;
    
    [self resetSubviews];
    
    if (message.message != nil) {
        [self setTextMessage:message];
    } else if (message.image != nil) {
        [self setImageMessage:message];
    }
}

@end
