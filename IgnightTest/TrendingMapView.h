//
//  TrendingMapView.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/8/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Trending.h"

#define METERS_PER_MILE 1609.344

@interface TrendingMapView : UIViewController
                            <CLLocationManagerDelegate,
                             MKMapViewDelegate>

-(void)reAnnotateMap;
-(void)reAnnotateMapWithCoordinate: (CLLocationCoordinate2D)coordinate withRadius: (CLLocationDistance)radius;
-(void)zoomToCurrentLocation;
-(void)zoomToCityView;
-(void)refreshMap;

@property (nonatomic, weak) Trending *mainTrending;
@property (nonatomic) BOOL groupMode;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
