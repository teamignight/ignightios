//
//  GroupHttpClient.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/13/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "GroupHttpClient.h"

@implementation GroupHttpClient
{
    UserInfo *userInfo;
}

+(GroupHttpClient *)getInstance
{
    static GroupHttpClient *_sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *url = server_url;
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:url]];
        [_sharedInstance.requestSerializer setValue:[IgnightProperties getHeaderToken] forHTTPHeaderField:@"X-IGNIGHT-TOKEN"];
        [_sharedInstance.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"Platform"];
        UserInfo* userInfo = [UserInfo getInstance];
        if (userInfo.userId != nil) {
            [_sharedInstance.requestSerializer setValue:userInfo.userId.stringValue forHTTPHeaderField:@"USER_ID"];
        }
        if (TARGET_IPHONE_SIMULATOR) {
        } else {
            _sharedInstance.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
            _sharedInstance.securityPolicy.allowInvalidCertificates = YES;
        }
    });
    return _sharedInstance;
}

-(instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        userInfo = [UserInfo getInstance];
    }
    
    return self;
}

-(void)cancelAllOperations
{
    [[self operationQueue] cancelAllOperations];
}

-(void)inviteUser: (NSNumber *)targetUserId toGroup:(NSNumber *)groupId
{
    NSDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:userInfo.userId forKey:@"userId"];
    [data setValue:targetUserId forKey:@"targetUserId"];
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:data, @"msg",  nil];
    
    NSString *path = [NSString stringWithFormat:@"group/invite/%@", groupId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient:successfullyInvitedUser:)]) {
            [self.delegate groupHttpClient:self successfullyInvitedUser:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)joinGroup:(NSNumber *)groupId
{
    NSDictionary *data = @{@"userId" : userInfo.userId};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"group/join/%@", groupId];
    [self POST: path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient:joinedGroupWithResponse:)]) {
            [self.delegate groupHttpClient:self joinedGroupWithResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)leaveGroup:(NSNumber *)groupId
{
    NSDictionary *data = @{@"userId": userInfo.userId};
    NSDictionary *parameters = @{@"msg" : data};

    NSString *path = [NSString stringWithFormat:@"group/leave/%@", groupId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient:leftGroup:withResponse:)]) {
            [self.delegate groupHttpClient:self leftGroup:groupId withResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)getGroupTrendingData: (NSNumber *)groupId from:(NSInteger)from to:(NSInteger)to
{
    NSDictionary *parameters = @{@"userId" : userInfo.userId};
    NSString *path = [NSString stringWithFormat:@"group/trending/%@", groupId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient:gotGroupTrendingData:)]) {
            [self.delegate groupHttpClient:self gotGroupTrendingData:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)createGroup: (NSString *)name withDescription: (NSString *)description withPrivate: (BOOL)privateGroup
{
    NSDictionary *data = @{@"cityId" : userInfo.cityId,
                           @"isPrivateGroup" : [NSNumber numberWithBool:privateGroup],
                           @"groupName" : name,
                           @"groupDescription" : description};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"group/add/%@", userInfo.userId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient:gotGroupCreationResponse:)]) {
            [self.delegate groupHttpClient:self gotGroupCreationResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)getGroupInfo:(NSNumber *)groupId
{
    NSDictionary *parameters = @{@"userId" : userInfo.userId};
    NSString *path = [NSString stringWithFormat:@"group/info/%@", groupId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient:gotGroupInfo:)]) {
            [self.delegate groupHttpClient:self gotGroupInfo:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)getMembersForGroup: (NSNumber *)groupId
{
    NSDictionary *parameters = @{@"userId" : userInfo.userId};
    
    NSString *path = [NSString stringWithFormat:@"group/members/%@", groupId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient: gotGroupMembers:)]) {
            [self.delegate groupHttpClient:self gotGroupMembers:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)getIncomingRequestsForUser
{
    NSString *path = [NSString stringWithFormat:@"inbox/%@", userInfo.userId];
    [self GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient: gotIncomingGroupRequests:)]) {
            [self.delegate groupHttpClient:self gotIncomingGroupRequests:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)searchForGroups:(NSString *)search withOffset: (NSInteger)offset WithLimit: (NSInteger)limit;
{
    NSDictionary *parameters = @{@"search" : search,
                                 @"offset" : [NSNumber numberWithInteger:offset],
                                 @"limit" : [NSNumber numberWithInteger:limit]};
    NSString *path = [NSString stringWithFormat:@"group/search/%@", userInfo.userId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient: gotGroupSearchResults:withOffset:)]) {
            [_delegate groupHttpClient:self gotGroupSearchResults:responseObject withOffset:offset];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [_delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)getPopularGroupsWithOffset: (NSInteger)offset WithLimit:(NSInteger)limit;
{
    NSDictionary *parameters = @{@"offset" : [NSNumber numberWithInteger:offset],
                                 @"limit" : [NSNumber numberWithInteger:limit]};
    
    NSString *path = [NSString stringWithFormat:@"group/popular/%@", userInfo.userId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient:gotPopularGroups:withOffset:)]) {
            [_delegate groupHttpClient:self gotPopularGroups:responseObject withOffset:offset];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [_delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)respondToGroup: (NSNumber *)groupId withResponse: (BOOL)response
{
    NSDictionary *data = @{@"userId" : userInfo.userId,
                           @"accept" : [NSNumber numberWithBool:response]};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"invite/respond/%@", groupId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if (response) {
            if ([_delegate respondsToSelector:@selector(groupHttpClient: groupInviteConfirmedForGroup:withResponse:)]) {
                [self.delegate groupHttpClient:self groupInviteConfirmedForGroup:groupId withResponse:responseObject];
            }
        } else {
            if ([_delegate respondsToSelector:@selector(groupHttpClient: groupRejectedConfirmedForGroup:withResponse:)]) {
                [self.delegate groupHttpClient:self groupRejectedConfirmedForGroup:groupId withResponse:responseObject];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)searchVenuesForGroup: (NSNumber *)groupId withSearch: (NSString *)searchString from: (NSInteger)from to:(NSInteger)to
{
    NSLog(@"Searching For Venues With: %@", searchString);
    NSDictionary *parameters = @{@"search" : searchString,
                                 @"userId" : userInfo.userId,
                                 @"count" : [NSNumber numberWithInteger:to]};
    
    NSString *path = [NSString stringWithFormat:@"group/venue/search/%@", groupId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient: gotGroupVenueSearch:)]) {
            [self.delegate groupHttpClient:self gotGroupVenueSearch:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)registerForGroupPush: (NSNumber *)groupId
{
    NSDictionary *data = @{@"userId" : userInfo.userId};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"group/register/push/%@", groupId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient:registeredForPushWithResponse:)]) {
            [self.delegate groupHttpClient:self registeredForPushWithResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)unregisterForGroupPush: (NSNumber *)groupId
{
    NSDictionary *data = @{@"userId" : userInfo.userId};
    NSDictionary *parameters = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"group/unregister/push/%@", groupId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(groupHttpClient:registeredForPushWithResponse:)]) {
            [self.delegate groupHttpClient:self registeredForPushWithResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate groupHttpClient:self didFailWithError:error];
    }];
}

-(void)flagGroup:(NSNumber *)groupId
{
    NSDictionary *data = @{@"userId" : userInfo.userId};
    NSDictionary *msg = @{@"msg" : data};
    NSString *path = [NSString stringWithFormat:@"flag/group/%@", groupId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

@end
