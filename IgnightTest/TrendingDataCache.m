//
//  TrendingDataCache.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/17/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "TrendingDataCache.h"

@implementation TrendingDataCache
{
    NSArray *trendingData;
    NSMutableSet *downVotedVenues;
    NSMutableSet *upVotedVenues;
    
    NSArray *sortedForGroupTrendingData;
    NSMutableDictionary *venueIdToInfo;
    NSSortDescriptor *sortDescriptor;
    BOOL groupMode;
    
    BOOL shouldRefetch;
}

+ (TrendingDataCache *)getInstance
{
    static TrendingDataCache* instance = nil;
    @synchronized(self) {
        if (!instance) {
            instance = [[TrendingDataCache alloc] init];
        }
    }
    return instance;
}

-(TrendingDataCache *) init
{
    self = [super init];
    if (self) {
        [self initMembers];
    }
    return self;
}

-(void)setShouldRefetchTo:(BOOL)fetch
{
    DDLogDebug(@"SETTING SHOULD REFETCH TO %@", fetch ? @"yes" : @"no");
    shouldRefetch = fetch;
}

-(BOOL)shouldRefetch
{
    DDLogDebug(@"Returning SHOULD REFETCH : %@", shouldRefetch ? @"yes" : @"no");
    return shouldRefetch;
}

-(void)clear
{
    [self initMembers];
    sortedForGroupTrendingData = nil;
    venueIdToInfo = nil;
}

- (void)initMembers
{
    trendingData = [[NSArray alloc] init];
    downVotedVenues = [[NSMutableSet alloc] init];
    upVotedVenues = [[NSMutableSet alloc] init];
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userVenueValue" ascending:NO];
    groupMode = NO;
}

-(void)setTrendingData: (NSArray *)data forGroup:(BOOL)group
{
    if (![data isKindOfClass:[NSArray class]]) {
        DDLogError(@"Expecting Array, Got: %@", data);
        return;
    }
    trendingData = [NSArray arrayWithArray:data];
    venueIdToInfo = [[NSMutableDictionary alloc] init];
    
    for (NSDictionary *venueTrendingData in data) {
        NSNumber *userInput = [venueTrendingData objectForKey:@"userInput"];
        NSNumber *venueId = [venueTrendingData objectForKey:@"venueId"];
        if ([userInput intValue] == 0) {
            [downVotedVenues removeObject:venueId];
            [upVotedVenues removeObject:venueId];
        } else if ([userInput intValue] == -1) {
            [downVotedVenues addObject:venueId];
            [upVotedVenues removeObject:venueId];
        } else {
            [upVotedVenues addObject:venueId];
            [downVotedVenues removeObject:venueId];
        }
        [venueIdToInfo setObject:[venueTrendingData mutableCopy] forKey:venueId];
    }
    groupMode = group;

    if (group) {
        sortedForGroupTrendingData = [trendingData sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    }
}

-(NSDictionary *)getVenueTrendingDataForVenue:(NSNumber *)venueId
{
    return [venueIdToInfo objectForKey:venueId];
}

-(NSInteger)getUserInputForVenue: (NSNumber *)venueId
{
    if ([upVotedVenues containsObject:venueId])
        return 1;
    else if ([downVotedVenues containsObject:venueId])
        return -1;
    return 0;
}

-(NSInteger)setNewUserInput:(NSInteger)input ForVenue:(NSNumber *)venueId
{
    NSInteger prevValue = [self getUserInputForVenue:venueId];
    if (input == 0) {
        [downVotedVenues removeObject:venueId];
        [upVotedVenues removeObject:venueId];
    } else if (input == 1) {
        [downVotedVenues removeObject:venueId];
        [upVotedVenues addObject:venueId];
    } else if (input == -1){
        [upVotedVenues removeObject:venueId];
        [downVotedVenues addObject:venueId];
    }
    
    if (groupMode) {
        NSMutableDictionary *venueInfo = [venueIdToInfo objectForKey:[NSString stringWithFormat:@"%@", venueId]];
        NSInteger delta = input - prevValue;
        NSNumber *userVenueValue = [NSNumber numberWithInt:[[venueInfo objectForKey:@"userVenueValue"] intValue]];
        userVenueValue = [NSNumber numberWithInteger:([userVenueValue intValue] + delta)];
        
        NSInteger newVal = [userVenueValue integerValue];
        
        DDLogDebug (@"PrevValue: %ld, NewValue: %ld", prevValue, newVal);
        
        NSInteger row = -1;
        if (prevValue < newVal) {
            for (row = 0; row < [sortedForGroupTrendingData count]; row++) {
                NSDictionary *venInfo = [sortedForGroupTrendingData objectAtIndex:row];
                NSInteger val = [[venInfo objectForKey:@"userVenueValue"] integerValue];
                if (newVal > val)
                    break;
            }
        } else if (newVal < prevValue) {
            for (row = [sortedForGroupTrendingData count] - 1; row >= 0; row--) {
                NSDictionary *venInfo = [sortedForGroupTrendingData objectAtIndex:row];
                NSInteger val = [[venInfo objectForKey:@"userVenueValue"] integerValue];
                if (newVal < val)
                    break;
            }
        }
        
        [venueInfo setObject:userVenueValue forKey:@"userVenueValue"];
        sortedForGroupTrendingData = [[venueIdToInfo allValues] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
        return row;
    }
    return -1;
}

-(NSInteger)countForGroup
{
    return [sortedForGroupTrendingData count];
}

-(NSInteger)count
{
    return [trendingData count];
}

-(NSDictionary *)getGroupTrendingDataForVenueAtIndex:(NSInteger)index
{
    return [sortedForGroupTrendingData objectAtIndex:index];
}

-(NSDictionary *)getTrendingDataForVenueAtIndex:(NSInteger)index
{
    return [trendingData objectAtIndex:index];
}

@end
