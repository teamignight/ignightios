//
//  Groups.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/1/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Group.h"

@interface Groups : NSObject

+ (Groups *)getInstance;

-(void)addGroup: (Group *)group;
-(void)removeGroup: (Group *)group;
-(void)clearAllGroups;
- (Group*)getGroupForId: (NSNumber *)groupId;
-(BOOL)isUserMemberOfGroup: (NSNumber *)groupId;
-(NSInteger)count;

-(NSMutableArray *)getAllGroups;
-(NSMutableArray *)getAllGroupsAlphabatezied;

@end
