//
//  SpendingViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/21/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightPreferences.h"

@interface SpendingViewController : UIViewController <IgnightPreferencesProtocol, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UIView *selectionHolder;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIView *selectionViewOne;
@property (strong, nonatomic) IBOutlet UIImageView *selectionImageOne;
@property (strong, nonatomic) IBOutlet UIButton *selectionButtonOne;
@property (strong, nonatomic) IBOutlet UILabel *selectionLabelOne;

@property (strong, nonatomic) IBOutlet UIButton *continueButton;

- (IBAction)continueButtonPressed:(id)sender;

@end
