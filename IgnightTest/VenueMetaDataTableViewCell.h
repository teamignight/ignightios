//
//  VenueMetaDataTableViewCell.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 2/12/15.
//  Copyright (c) 2015 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

#define VENUE_META_DATA_CELL_HEIGHT 60

@protocol VenueMetaDataTableViewCellDelegate;

@interface VenueMetaDataTableViewCell : UITableViewCell

@property (nonatomic, weak) id<VenueMetaDataTableViewCellDelegate> delegate;

@property (nonatomic, weak) NSString *venueAddress;
@property (nonatomic, weak) NSString *phoneNumber;
@property (nonatomic, weak) NSString *website;

-(void)updateWithAddress: (NSString *)addr withNumber: (NSString *)number withWebsite: (NSString *)website;

@end

@protocol VenueMetaDataTableViewCellDelegate <NSObject>

-(void)goToWebsite;
-(void)goToErrorScreen;

@end
