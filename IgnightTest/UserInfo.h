//
//  UserInfo.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/10/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface UserInfo : NSObject

@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *password;

@property (strong, nonatomic) NSNumber *userId;
@property (strong, nonatomic) NSNumber *cityId;
@property (strong, nonatomic) NSNumber *genderId;
@property (strong, nonatomic) NSNumber *ageBucketId;

@property (strong, nonatomic) NSMutableArray *music;
@property (strong, nonatomic) NSMutableArray *atmospheres;
@property (strong, nonatomic) NSNumber *spendingLimit;

@property (strong, nonatomic) NSNumber *musicFilterId;
@property (strong, nonatomic) NSNumber *activityFilterId;

@property (strong, nonatomic) CLLocation *userLocation;
@property (strong, nonatomic) NSString *profilePictureUrl;
@property (strong, nonatomic) UIImage *profilePictureImage;

@property (nonatomic) BOOL notifyOfGroupInvites;


+ (UserInfo *)getInstance;

-(void)resetMusic;
-(void)resetAtmosphere;

-(void)reset;

- (void)setMusicChoices: (NSMutableArray *)musicChoices;
- (void)setAtmosphereChoices: (NSMutableArray *)atmosphereChoices;

-(void)setMusicDna: (NSNumber *)musicId atSelection: (NSInteger)selection;

-(void)setAtmosphereDna: (NSNumber *)atmosphereId atSelection: (NSInteger)selection;
-(void)setSpendingLimitDna: (NSNumber *)spendingLimit;
-(BOOL)isUserInfoSet;
-(void)SetIsUserInfoSetTo: (BOOL)set;
-(void)clearAllInfo;

-(NSString *)description;

@end
