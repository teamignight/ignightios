//
//  BuzzMessage.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 11/19/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFHTTPRequestOperation.h>

@interface BuzzMessage : NSObject


-(id)initWithMyMessage: (NSDictionary *)msg;
-(id)initWithOtherMessage: (NSDictionary *)msg;

@property (nonatomic, copy) NSNumber *buzzId;
@property (nonatomic, copy) NSNumber *buzzType;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *senderName;
@property (nonatomic, copy) NSDate *date;

@property (nonatomic, assign) CGSize bubbleSize;

@property (nonatomic, copy) UIImage *image;
@property (nonatomic, copy) NSURL *imageUrl;

@property (nonatomic, copy) UIImage *userImage;
@property (nonatomic, copy) NSURL *userImageUrl;

@property BOOL myMessage;

@end
