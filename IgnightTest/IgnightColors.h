//
//  IgnightColors.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/23/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#define blueBtnBackground UIColorFromRGB(0x004679)
#define whiteBtnText UIColorFromRGB(0xffffff)

#define barBackgroundColor UIColorFromRGB(0x002946)

/* ------ DNA Sign Up Colors -------- */
#define whiteHeaderBackground UIColorFromRGB(0xffffff)
#define grayHeaderFooterText UIColorFromRGB(0x606060)
#define whiteFooterBackground UIColorFromRGB(0xf2f2f2)
#define dnaTitleText UIColorFromRGB(0x797979)
#define activeBtnBackground UIColorFromRGB(0x002946)
#define activeBtnText UIColorFromRGB(0xffffff)
#define inactiveBtnBackground [UIColor clearColor]
#define inactiveBtnText UIColorFromRGB(0x606060)
#define inactiveBtnBorder UIColorFromRGB(0xa2a2a2)
/* ------ DNA Sign Up Colors -------- */

/*-------- Trending List Colors --------*/
#define tabBackground UIColorFromRGB(0xf2f2f2)
#define tabActiveText UIColorFromRGB(0x)
#define tabInactiveText UIColorFromRGB(0x606060)
#define tabNonFocusText UIColorFromRGB(0x)
#define footerBackground UIColorFromRGB(0xf2f2f2)
#define footerText UIColorFromRGB(0x606060)
#define trendingListBackground UIColorFromRGB(0xffffff)
#define venueNameText UIColorFromRGB(0x000000)
#define venueDistanceText UIColorFromRGB(0x929292)
#define venueFunScorePositive UIColorFromRGB(0x39C630)
#define venueFunScoreNegative UIColorFromRGB(0x)
/*-------- Trending List Colors --------*/

/*--------- Side Bar Colors ------------*/
#define sideBarBackground UIColorFromRGB(0x565656)
#define sideBarText UIColorFromRGB(0xffffff)
#define sideBarHeaderBackground UIColorFromRGB(0x454545)
#define sideBarListBackground UIColorFromRGB(0x6b6b6b)
#define sideBarListSeparator UIColorFromRGB(0x7a7a7a)
#define sideBarActiveNotificationCircle UIColorFromRGB(0x78c542)
/*--------- Side Bar Colors ------------*/

/*---------- Venue Profile Colors --------*/
#define tabBackground UIColorFromRGB(0xf2f2f2)
#define tabBorder UIColorFromRGB(0xd7d7d7)
#define venueSectionHeaderBackground UIColorFromRGB(0xffffff)
#define venueSectionHeaderText UIColorFromRGB(0x000000)
#define venueSectionHeaderMiles UIColorFromRGB(0x929292)
#define venueListBackgrond UIColorFromRGB(0xf2f2f2)
#define venueListBorder UIColorFromRGB(0xd8d8d8)
#define venueListText UIColorFromRGB(0x002946)
/*---------- Venue Profile Colors --------*/

/*---------- Venue Buzz Colors --------*/
#define tabBackground UIColorFromRGB(0xf2f2f2)
#define otherBuzzBubbleBackground UIColorFromRGB(0xffffff)
#define myBuzzBubbleBackground UIColorFromRGB(0x002946)
#define otherBuzzBubbleUsernameText UIColorFromRGB(0x000000)
#define otherBuzzBubbleContentText UIColorFromRGB(0x373737)
#define myBuzzBubbleContentText UIColorFromRGB(0xffffff)
#define buzzListBottomBar UIColorFromRGB(0x)
#define buzzSendButton UIColorFromRGB(0x002946)
/*---------- Venue Buzz Colors --------*/

/*--------- Report a Venue Colors ---------- */
#define reportMainBackground UIColorFromRGB(0xf2f2f2)
#define reportHeaderText UIColorFromRGB(0x373737)
#define reportCancelButtonBackground UIColorFromRGB(0x949494)
#define reportCacnelButtonText UIColorFromRGB(0xffffff)
#define reportSubmitButtonBackground UIColorFromRGB(0x002946)
#define reportSubmitButtonText UIColorFromRGB(0xffffff)
#define reportContentSectionBackground UIColorFromRGB(0xffffff)
#define reportContentSectionText UIColorFromRGB(0x002946)
#define reportContentSectionHeaderText UIColorFromRGB(0x000000)
/*--------- Report a Venue Colors ---------- */


#define clearC [UIColor clearColor]
#define navigationBarColor UIColorFromRGB(0x031a35)

#define trendingToggleUnselectedColor UIColorFromRGB(0xececec)
#define trendingToggleTextColor UIColorFromRGB(0x4c4c4c)  // Deprecated
#define trendingSwitchMapListColor UIColorFromRGB(0x606060)

#define trendingMusicToggleOffColor UIColorFromRGB(0x6a6a6a)
#define trendingMusicToggleActiveColor UIColorFromRGB(0x78C542)
#define trendingMusicToggleInactiveColor UIColorFromRGB(0xa7a7a7)

#define trendingMusicFilterLabelsColor UIColorFromRGB(0x797979)

#define venueInfoHeaderCellColor UIColorFromRGB(0xf4f4f4)

#define registrationScreenHeaderColor UIColorFromRGB(0xfffefe)
#define registrationContinueBtnColor UIColorFromRGB(0x2db520)
#define registrationHeaderTextColor UIColorFromRGB(0x434343)
#define myBuzzBubbleColor UIColorFromRGB(0x01172c)
#define buzzUserProfileBackground UIColorFromRGB(0xd6d6d6)
#define buzzUserNameTextColor UIColorFromRGB(0x294e5f)
#define buzzTimeStampColor UIColorFromRGB(0x9a9a9a)
#define buzzTextColor UIColorFromRGB(0x9a9a9a)
#define buzzSendButtonColor UIColorFromRGB(0x01172d)
#define buzzBackgroundColor UIColorFromRGB(0xf2f2f2)

@interface IgnightColors : NSObject

@end
