//
//  UpdatePasswordViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/26/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "UpdatePasswordViewController.h"

@interface UpdatePasswordViewController ()

@end

@implementation UpdatePasswordViewController
{
    UserHttpClient *userClient;
    UserInfo *userInfo;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(respondToTapGesture:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
    
    [[_inputView layer] setCornerRadius:3];
    [[_inputView layer] setMasksToBounds:YES];
    
    [[_login layer] setCornerRadius:3];
    [[_login layer] setMasksToBounds:YES];
    [_login setBackgroundColor:blueBtnBackground];
    [_login setTitleColor:whiteBtnText forState:UIControlStateNormal];
    
    [_usernamePswdSeparator setBackgroundColor:tabBorder];
    [_usernamePswdSeparator setAlpha:0.7];
    [_pswdNewPswdSeparator setBackgroundColor:tabBorder];
    [_pswdNewPswdSeparator setAlpha:0.7];
    
    _temporaryPassword.secureTextEntry = YES;
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    [_username setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_temporaryPassword setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_password setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Forgot Password" message:@"Your temporary password is on its way. You should receive an e-mail shortly." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self respondToTapGesture:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    userClient = [UserHttpClient getInstance];
    userClient.delegate = self;
    
    userInfo = [UserInfo getInstance];
    
    self.username.text = userInfo.userName;
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.tintColor = [IgnightUtil backgroundGray];
    self.navigationController.navigationBar.alpha = 0.9;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboard:) name:UIKeyboardWillHideNotification object:nil];
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [gestureRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    [self respondToTapGesture:nil];
}

-(void)handleSwipe: (UISwipeGestureRecognizer *)recognizer
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self respondToTapGesture:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [userClient cancelAllOperations];
}

- (void)keyboard:(NSNotification *)notification {
    NSTimeInterval animationDuration;
    NSDictionary *uInfo = [notification userInfo];
    
    DDLogDebug(@"NOTIF: %@", notification);
    
    [[uInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    CGPoint kbPointBeg = [[uInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].origin;
    CGPoint kbPointEnd = [[uInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    CGFloat adjustment = kbPointEnd.y - kbPointBeg.y;
    CGRect frame = self.view.frame;
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = CGRectMake(frame.origin.x, frame.origin.y + adjustment, frame.size.width, frame.size.height);
    }];
}

-(IBAction)respondToTapGesture:(id)sender
{
    [self.username resignFirstResponder];
    [self.password resignFirstResponder];
    [self.temporaryPassword resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    DDLogDebug(@"Got Memmory Warning Message in UpdatePasswordViewController");
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)loginNow:(id)sender {
    if ([self.username.text isEqualToString:@""]) {
        [_username becomeFirstResponder];
        IgAlert(nil, @"Please enter the username.", nil);
        return;
    }
    
    if ([self.temporaryPassword.text isEqualToString:@""]) {
        [_temporaryPassword becomeFirstResponder];
        IgAlert(nil, @"Please enter the temporary password.", nil);
        return;
    }
    
    if ([self.password.text isEqualToString:@""]) {
        [_password becomeFirstResponder];
        IgAlert(nil, @"Please enter a new password.", nil);
        return;
    }
    
    if (_password.text.length < 6) {
        IgAlert(nil, @"Password must be atleast 6 characters.", nil);
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self respondToTapGesture:nil];
    [userClient resetPassword:self.temporaryPassword.text toNewPassword:self.password.text forUser:self.username.text];
}

-(void)login: (NSDictionary *)response
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSInteger result = [IgnightUtil updateUserInfoOnLogin:response];
    [IgnightUtil saveUserData];
    if (result > 0) {
        [self performSegueWithIdentifier:@"login" sender:self];
    } else {
        [self performSegueWithIdentifier:@"setupProfile" sender:self];
    }
}

-(void)userHttpClient:(UserHttpClient *)client gotResetPasswordResponse:(NSDictionary *)response
{
    DDLogDebug(@"Got Reset Pass: %@", response);
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        [self login:response];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    IgAlert(nil, @"Could not reach server.", nil);
}

#pragma mark - UITextViewDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField              // called when 'return' key pressed. return NO to ignore.
{
    if (textField == _username) {
        [_temporaryPassword becomeFirstResponder];
    } else if (textField == _temporaryPassword) {
        [_password becomeFirstResponder];
    } else if (textField == _password) {
        [self loginNow:nil];
    }
    return YES;
}


@end
