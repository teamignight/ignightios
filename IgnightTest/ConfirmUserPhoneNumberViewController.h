//
//  ConfirmUserPhoneNumberViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 3/1/15.
//  Copyright (c) 2015 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface ConfirmUserPhoneNumberViewController : UIViewController <UserHttpClientDelegate, SWRevealViewControllerDelegate>

@end
