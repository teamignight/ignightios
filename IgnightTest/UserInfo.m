//
//  UserInfo.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/10/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo
{
    BOOL isUserInfoSet;
}

+ (UserInfo *)getInstance
{
    static UserInfo* instance = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

-(UserInfo *) init
{
    self = [super init];
    if (self) {
        isUserInfoSet = NO;
        [self reset];
    }
    return self;
}

-(BOOL)isUserInfoSet
{
    return isUserInfoSet;
}

-(void)resetMusic
{
    self.music = [[NSMutableArray alloc] initWithArray:@[[NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1]]];
}

-(void)resetAtmosphere
{
    self.atmospheres = [[NSMutableArray alloc] initWithArray:@[[NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1]]];
}

-(void)reset
{
    self.email = @"";
    self.userName = @"";
    self.password = @"";
    
    self.userId = [NSNumber numberWithInt:-1];
    self.cityId = [NSNumber numberWithInt:-1];
    self.genderId = [NSNumber numberWithInt:-1];
    self.ageBucketId = [NSNumber numberWithInt:-1];
    
    [self resetMusic];
    [self resetAtmosphere];
    self.spendingLimit = [NSNumber numberWithInt:-1];
    
    self.musicFilterId = [NSNumber numberWithInt:-1];
    self.activityFilterId = [NSNumber numberWithInt:-1];
    
    _profilePictureImage = nil;
    _profilePictureUrl = nil;
    isUserInfoSet = NO;
}

-(void)SetIsUserInfoSetTo: (BOOL)set
{
    if (set == NO) {
        [self reset];
    } else {
        isUserInfoSet = YES;
    }
}

-(void)clearAllInfo
{
    [self reset];
}

- (void)setMusicChoices: (NSMutableArray *)musicChoices
{
    self.music = musicChoices;
}

- (void)setAtmosphereChoices: (NSMutableArray *)atmosphereChoices
{
    self.atmospheres = atmosphereChoices;
}

-(void)setMusicDna: (NSNumber *)musicId atSelection: (NSInteger)selection
{
    if (self.music == nil || [self.music count] == 0)
        self.music = [[NSMutableArray alloc] initWithCapacity:3];
    [self.music setObject:musicId atIndexedSubscript:selection];
}
-(void)setAtmosphereDna: (NSNumber *)atmosphereId atSelection: (NSInteger)selection
{
    if (self.atmospheres == nil || [self.atmospheres count] == 0)
        self.atmospheres = [[NSMutableArray alloc] initWithCapacity:3];
    [self.atmospheres setObject:atmosphereId atIndexedSubscript:selection];
}
-(void)setSpendingLimitDna: (NSNumber *)limit
{
    self.spendingLimit = limit;
}

-(void)setNotifyGroupInvites: (BOOL)invite
{
    _notifyOfGroupInvites = invite;
}

-(NSString *)description
{
    NSString *desc = [NSString stringWithFormat:@"userName: %@, userId: %@, cityId: %@, genderId: %@, age: %@", self.userName, self.userId, self.cityId, self.genderId, self.ageBucketId];
    return desc;
}

@end
