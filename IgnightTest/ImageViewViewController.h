//
//  ImageViewViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/29/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageViewControllerDelegate;

@interface ImageViewViewController : UICollectionViewController<
ImageHttpClientDelegate>

@property (nonatomic, assign) id<ImageViewControllerDelegate> delegate;
@property (nonatomic, strong) NSNumber *venueId;
@property (nonatomic, strong) NSString *venueName;
@property (nonatomic) BOOL venueMode;

-(NSArray *)getImageList;
-(void)loadImagesForGroup;
-(void)loadImagesForVenue;

@end

@protocol ImageViewControllerDelegate <NSObject>

-(void)clickedImageAtIndex: (NSInteger)row;

@end