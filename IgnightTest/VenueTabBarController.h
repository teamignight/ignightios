//
//  VenueTabBarController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/22/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueTabBarController : UITabBarController

@end
