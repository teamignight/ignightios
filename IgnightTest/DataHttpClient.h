//
//  DataHttpClient.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/13/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@protocol DataHttpClientDelegate;

@interface DataHttpClient : AFHTTPSessionManager

@property (nonatomic, weak) id<DataHttpClientDelegate> delegate;

+(DataHttpClient *)getInstance;
-(instancetype)initWithBaseURL:(NSURL *)url;

@end


@protocol DataHttpClientDelegate <NSObject>

@optional

-(void)dataHttpClient: (DataHttpClient *)client didFailWithError: (NSError *)error;

@end