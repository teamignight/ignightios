//
//  CreateAGroupView.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/28/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "CreateAGroupView.h"
#import "SWRevealViewController.h"
#import "UserInfo.h"
#import "Groups.h"
#import "Group.h"
#import "IgnightTopBar.h"
#import "IgnightUtil.h"
#import <GPUImage.h>
#import <AFNetworking/AFHTTPRequestOperation.h>

#define PLACEHOLDER_TXT @"Description (250 characters max)"

@interface CreateAGroupView ()

@end

@implementation CreateAGroupView
{
    GroupHttpClient *groupClient;
    UserInfo *userInfo;
    
    //Create A Group Info
    BOOL isPrivate;
    
    IgnightTopBar *topBar;
    
    Group *groupToAdd;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    groupToAdd = [[Group alloc] init];
    
    topBar = [[IgnightTopBar alloc] initWithTitle:@"Create A Group" withSideBarButtonTarget:self];
    [_titleBar addSubview:topBar];
    [_titleBar setUserInteractionEnabled:YES];
    [topBar setLeftButtonBackgroundImage:[UIImage imageNamed:@"back_gray"]];

    self.revealViewController.delegate = self;
    [self.view setBackgroundColor:UIColorFromRGB(0xf2f2f2)];
    
//    [self setUpSideBarButton];
    [self initVariables];
    [self setupGestureRecognizer];
    
    groupClient = [GroupHttpClient getInstance];
    
    [[_groupInfoView layer] setCornerRadius:3];
    [[_groupInfoView layer] setMasksToBounds:YES];
    
    [[_createGroupBtn layer] setCornerRadius:3];
    [[_createGroupBtn layer] setMasksToBounds:YES];
    
    [[_cancelBtn layer] setCornerRadius:3];
    [[_cancelBtn layer] setMasksToBounds:YES];
    
    _cancelBtn.backgroundColor = reportCancelButtonBackground;
    [_cancelBtn setTitleColor:reportSubmitButtonText forState:UIControlStateNormal];
    _createGroupBtn.backgroundColor = reportSubmitButtonBackground;
    [_createGroupBtn setTitleColor:reportSubmitButtonText forState:UIControlStateNormal];
    _grpDescription.textColor = [UIColor lightGrayColor];
}

- (void)revealToggle:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//- (void)setUpSideBarButton
//{
//    self.revealViewController.delegate = self;
//    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//}

-(void)initVariables
{
    userInfo = [UserInfo getInstance];
    
    _grpName.delegate = self;
    _grpDescription.delegate = self;
}

- (void)setupGestureRecognizer
{
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:singleTap];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupPushNotificationObserver];
    groupClient.delegate = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removePushNotificationObserver];
}

- (void)dismissKeyboard
{
    [self.grpName resignFirstResponder];
    [self.grpDescription resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)createAGroup:(id)sender {
    if ([_grpName.text isEqualToString:@""]) {
        IgAlert(nil, @"Group must have a name.", nil);
        return;
    }
    if ([_grpDescription.text length] < 10 ||
        [_grpDescription.text isEqualToString:PLACEHOLDER_TXT]) {
        IgAlert(nil, @"Group description must be at least 10 characters.", nil);
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [groupClient createGroup:_grpName.text withDescription:_grpDescription.text withPrivate:isPrivate];
}

- (IBAction)cancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)groupAccess:(id)sender {
    UISwitch *switchView = sender;
    if (switchView.on) {
        isPrivate = NO;
    } else {
        isPrivate = YES;
    }
}

#pragma mark Actions -

#pragma mark - TextField Delegates

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.grpName == textField) {
        groupToAdd.name = textField.text;
    }
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_grpDescription performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.0];
    return YES;
}

#pragma mark UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView;
{
    if ([textView.text isEqualToString:PLACEHOLDER_TXT]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        textView.textAlignment = NSTextAlignmentNatural;
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSInteger textLength = textView.text.length;
    NSInteger replaceMentTextLength = text.length;
    NSInteger finalLength = textLength + replaceMentTextLength - range.length;
    
    if (finalLength <= 250)
        return YES;
    return NO;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Description (250 characters max)";
        textView.textColor = [UIColor lightGrayColor];
        textView.textAlignment = NSTextAlignmentCenter;
    }
}


#pragma mark - GroupHttpClientDelegate

-(void)groupHttpClient:(GroupHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)groupHttpClient:(GroupHttpClient *)client gotGroupCreationResponse:(NSDictionary *)response
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSDictionary *body = [response objectForKey:@"body"];
        groupToAdd.groupId = [[body objectForKey:@"id"] integerValue];
        groupToAdd.publicGroup = !isPrivate;
        groupToAdd.name = _grpName.text;
        groupToAdd.groupDescription = _grpName.text;
        groupToAdd.adminId = userInfo.userId;
        
        Trending *trendingView = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
        trendingView.groupId = [NSNumber numberWithInteger:groupToAdd.groupId];
        trendingView.groupMode = YES;
        trendingView.goBack = NO;
        trendingView.groupName = groupToAdd.name;
        [self.navigationController pushViewController:trendingView animated:YES];
    } else {
        IgAlert(nil, response[@"reason"], nil);
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionRight) {
        for (UIView *subView in self.view.subviews) {
            [subView setUserInteractionEnabled:NO];
        }
        [topBar setUserInteractionEnabled:YES];
    } else {
        for (UIView *subView in self.view.subviews) {
            [subView setUserInteractionEnabled:YES];
        }
    }
}

#pragma mark - Remote Notifications

-(void)setupPushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewMailPush:) name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewGroupBuzz:) name:@"NewGroupBuzzNotification" object:nil];
}

-(void)removePushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewGroupBuzzNotification" object:nil];
}

-(void)handleNewMailPush: (NSNotification *)notification
{
    GroupViewController *groupView = [self.storyboard instantiateViewControllerWithIdentifier:@"allGroupView"];
    groupView.tabIndex = 2;
    [self.navigationController pushViewController:groupView animated:YES];
}

-(void)handleNewGroupBuzz: (NSNotification *)notification
{
    NSDictionary *data = notification.userInfo;
    Trending *trending = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
    trending.goBack = NO;
    trending.groupMode = YES;
    trending.groupName = data[@"groupName"];
    trending.groupId = data[@"groupId"];
    trending.tabIndex = 1;
    [self.navigationController pushViewController:trending animated:YES];
}

@end
