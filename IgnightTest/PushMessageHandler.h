//
//  PushMessageHandler.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PushMessage.h"
#import "GroupInvite.h"
#import "GroupBuzz.h"
#import "VenueBuzz.h"

@protocol PushMessangerHandlerDelegate;

@interface PushMessageHandler : NSObject

@property (nonatomic, assign) id<PushMessangerHandlerDelegate> delegate;

+(PushMessageHandler *)getInstance;
-(void)gotMessage: (PushMessage *)message;

@end

@protocol PushMessangerHandlerDelegate <NSObject>

@optional

-(void)gotNewGroupInvite;
-(void)gotNewGroupBuzz: (NSNumber *)groupId;
-(void)gotNewVenueBuzz: (NSNumber *)venueId;

@end