//
//  UserHttpClient.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "UserHttpClient.h"

#import "IgnightContact.h"

@implementation UserHttpClient
{
    UserInfo *userInfo;
}

+(UserHttpClient *)getInstance
{
    static UserHttpClient *_sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *url = server_url;
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:url]];
        [_sharedInstance.requestSerializer setValue:[IgnightProperties getHeaderToken] forHTTPHeaderField:@"X-IGNIGHT-TOKEN"];
        [_sharedInstance.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"Platform"];
        UserInfo* userInfo = [UserInfo getInstance];
        if (userInfo.userId != nil) {
            [_sharedInstance.requestSerializer setValue:userInfo.userId.stringValue forHTTPHeaderField:@"USER_ID"];
        }
        if (TARGET_IPHONE_SIMULATOR) {
        } else {
            _sharedInstance.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
            _sharedInstance.securityPolicy.allowInvalidCertificates = YES;
        }
    });
    return _sharedInstance;
}

-(instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        userInfo = [UserInfo getInstance];
    }
    
    return self;
}

-(void)login
{
    NSDictionary *msg = @{@"userName": userInfo.userName,
                          @"password": userInfo.password};
    NSDictionary *parameters = @{@"msg" : msg};
    
    [self POST:@"login" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient: didLoginWithResponse:)]) {
            [self.delegate userHttpClient:self didLoginWithResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)logout
{
    NSDictionary *msg = [[NSDictionary alloc] init];
    NSDictionary *parameters = @{@"msg" : msg};
    NSString *path = [NSString stringWithFormat:@"logout/%@", userInfo.userId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient: gotLogoutResponse:)]) {
            [self.delegate userHttpClient:self gotLogoutResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)getUserInfo
{
    NSString *path = [NSString stringWithFormat:@"user/%@", userInfo.userId];
    [self GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:gotUserInfo:)]) {
            [self.delegate userHttpClient:self gotUserInfo:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)searchForUsers: (NSString *)search inGroup: (NSNumber *)groupId withOffset:(NSInteger)offset withLimit:(NSInteger)limit
{
    NSDictionary *parameters = @{@"groupId" : groupId,
                                  @"search" : search,
                                 @"offset" : [NSNumber numberWithInteger:offset],
                                 @"limit" : [NSNumber numberWithInteger:limit],
                                  @"cityId" : userInfo.cityId};

    NSString *path = [NSString stringWithFormat:@"groups/users/search/%@", userInfo.userId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:returnedUsersForSearch: withOffset:)]) {
            [self.delegate userHttpClient:self returnedUsersForSearch:responseObject withOffset:offset];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)sendTemporaryPassword:(NSString *)username
{
    NSString *path = [NSString stringWithFormat:@"forgot_password/%@", username];
    [self GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:gotForgotPasswordResponse:)]) {
            [self.delegate userHttpClient:self gotForgotPasswordResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)resetPassword:(NSString *)password toNewPassword:(NSString *)newPassword forUser:(NSString *)userName
{
    NSDictionary *msg = @{@"userName" : userName,
                          @"password" : newPassword,
                          @"tempPassword" : password};
    NSDictionary *parameters = @{@"msg" : msg};
    
    [self POST:@"reset_password" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:gotResetPasswordResponse:)]) {
            [self.delegate userHttpClient:self gotResetPasswordResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)getUserTrendingListFrom: (NSInteger)from to: (NSInteger)to
{
    NSDictionary *parameters = @{@"type" : @"trendingList",
                                 @"musicPreference" : (userInfo.musicFilterId && userInfo.musicFilterId.integerValue >= 0) ? userInfo.musicFilterId : [NSNumber numberWithInteger:-1],
                                 @"trendingFilter" : (userInfo.activityFilterId && userInfo.activityFilterId.integerValue >= 0) ? userInfo.activityFilterId : [NSNumber numberWithInteger:-1],
                                 @"userId" : userInfo.userId,
                                 @"trendingVenueStartIndex" : [NSNumber numberWithInteger:from],
                                 @"trendingNoOfVenues" : [NSNumber numberWithInteger:(to - from)]};
    [self GET:@"user" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient: gotUserTrendingData: )]) {
            [self.delegate userHttpClient:self gotUserTrendingData:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)searchTrendingListWith: (NSString *)searchString from: (NSInteger)from to:(NSInteger)to
{
    NSLog(@"Searching For Venues With: %@", searchString);
    NSDictionary *parameters = @{@"search" : searchString,
                                 @"count" : [NSNumber numberWithInteger:to]};

    NSString *path = [NSString stringWithFormat:@"trending/%@", userInfo.userId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:gotSearchResults:)]) {
            [self.delegate userHttpClient:self gotSearchResults:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)getUserTrendingListFromCoordinate: (CLLocationCoordinate2D)coordinate withRadius: (CLLocationDistance)radius withCount: (NSInteger) count;
{
    NSDictionary *template = @{@"count" : [NSNumber numberWithInteger:count],
                                 @"latitude" : [NSNumber numberWithFloat:coordinate.latitude],
                                 @"longitude" : [NSNumber numberWithFloat:coordinate.longitude],
                                 @"search_radius" : [NSNumber numberWithFloat:radius]};
    NSMutableDictionary *parameters  = [[NSMutableDictionary alloc] initWithDictionary:template];
    if (userInfo.musicFilterId && userInfo.musicFilterId.integerValue >= 0) {
        [parameters setObject:userInfo.musicFilterId forKey:@"music_filter"];
    }
    
    if (userInfo.activityFilterId && userInfo.activityFilterId.integerValue >= 0) {
        [parameters setObject:@"true" forKey:@"upvote_filter"];
    }
    
    NSString *path = [NSString stringWithFormat:@"trending/%@", userInfo.userId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient: gotUserTrendingData:fromCoordinate:withRadius:)]) {
            [self.delegate userHttpClient:self gotUserTrendingData:responseObject fromCoordinate:coordinate withRadius:radius];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)getUserGroups
{
    NSString *path = [NSString stringWithFormat:@"groups/user/%@", userInfo.userId];
    [self GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:gotUserGroups:)]) {
            [self.delegate userHttpClient:self gotUserGroups: responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)registerUser
{
    NSDictionary *msg = @{@"cityId" : userInfo.cityId,
                          @"genderId" : userInfo.genderId,
                          @"age" : userInfo.ageBucketId,
                          @"music" : userInfo.music,
                          @"atmosphere" : userInfo.atmospheres,
                          @"spendingLimit" : userInfo.spendingLimit};
    NSDictionary *parameters = @{@"msg": msg};
    
    NSString *path = [NSString stringWithFormat:@"user/%@", userInfo.userId];
    [self PUT:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:registeredUser:)]) {
            [self.delegate userHttpClient:self registeredUser:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)createUserWithEmail: (NSString *)email withUserName: (NSString *)userName withPassword: (NSString *)password
{
    NSDictionary *msg = @{@"email" : email,
                          @"userName" : userName,
                          @"password" : password};
    NSDictionary *parameters = @{@"msg" : msg};
    
    [self POST:@"user" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:createdUserWithResponse:)]) {
            [self.delegate userHttpClient:self createdUserWithResponse:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)updateProfilePicture: (NSData *)image
{
    if (image == nil) {
        DDLogError(@"Failed to Update Profile Picture: Image is null");
        return;
    }
    
    NSDictionary *data = @{@"userId" : userInfo.userId};
    NSDictionary *msg = @{@"msg" : data};
    NSData *body = [NSJSONSerialization dataWithJSONObject:msg options:kNilOptions error:nil];
    NSDictionary *parameters = @{@"json_body" : body};

    NSString *path = [NSString stringWithFormat:@"profile_image/%@", userInfo.userId];
    [self POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:image name:@"userImage" fileName:@"userImage" mimeType:@"image/jpeg"];
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:updatedProfilePricture:)]) {
            [self.delegate userHttpClient:self updatedProfilePricture:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)updateCity: (NSNumber *)cityId
{
    NSDictionary *msg = @{@"type" : @"updateCity",
                          @"newCityId" : cityId};
    NSString *path = [NSString stringWithFormat:@"user/%@", userInfo.userId];
    NSDictionary *parameters = @{@"msg" : msg};
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:updatedCity:)]) {
            [self.delegate userHttpClient:self updatedCity:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)updateMusic: (NSArray *)music
{
    NSDictionary *msg = @{@"music" : music};
    NSDictionary *parameters = @{@"msg" : msg};
    
    NSString *path = [NSString stringWithFormat:@"user/%@", userInfo.userId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:updatedMusic:)]) {
            [self.delegate userHttpClient:self updatedMusic: responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)updateAtmosphere: (NSArray *)atmospheres
{
    NSDictionary *msg = @{@"atmosphere" : atmospheres};
    NSDictionary *parameters = @{@"msg" : msg};
    
    NSString *path = [NSString stringWithFormat:@"user/%@", userInfo.userId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:updatedAtmosphere:)]) {
            [self.delegate userHttpClient:self updatedAtmosphere: responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)updateSpedningLimit: (NSInteger)spending
{
    NSDictionary *msg = @{@"spendingLimit" : [NSNumber numberWithInteger:spending]};
    NSDictionary *parameters = @{@"msg" : msg};
    
    NSString *path = [NSString stringWithFormat:@"user/%@", userInfo.userId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:updatedSpendingLimit:)]) {
            [self.delegate userHttpClient:self updatedSpendingLimit:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)updateAge: (NSInteger)age
{
    NSDictionary *msg = @{@"age" : [NSNumber numberWithInteger:age]};
    NSDictionary *parameters = @{@"msg" : msg};
    
    NSString *path = [NSString stringWithFormat:@"user/%@", userInfo.userId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:updatedAge:)]) {
            [self.delegate userHttpClient:self updatedAge: responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)updateEmail: (NSString *)email
{
    NSDictionary *msg = @{@"email" : email};
    NSDictionary *parameters = @{@"msg" : msg};
    
    NSString *path = [NSString stringWithFormat:@"user/%@", userInfo.userId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:updatedEmail:)]) {
            [self.delegate userHttpClient:self updatedEmail: responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)updateGroupInvitesPreference: (BOOL)preference
{
    NSDictionary *msg = @{@"notifyOfGroupInvites" : [NSNumber numberWithBool:preference]};
    NSDictionary *parameters = @{@"msg" : msg};
    
    NSString *path = [NSString stringWithFormat:@"user/%@", userInfo.userId];
    [self POST:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([_delegate respondsToSelector:@selector(userHttpClient:updatedGroupInvitePreference:)]) {
            [self.delegate userHttpClient:self updatedGroupInvitePreference:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)addDeviceToken:(NSString*)deviceToken
{
    if (userInfo == nil) {
        userInfo = [UserInfo getInstance];
    }
    NSDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:[NSNumber numberWithInt:0] forKey:@"osId"];
    [data setValue:deviceToken forKey:@"token"];
    
    NSDictionary *msg = @{@"msg" : data};
    
    NSString *path = [NSString stringWithFormat:@"push_token/%@", userInfo.userId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        DDLogDebug(@"Posted Device Token");
        [IgnightUtil setIsIOSTokenSet:YES];
        if ([_delegate respondsToSelector:@selector(userHttpClient: postedDeviceToken: )]) {
            [_delegate userHttpClient:self postedDeviceToken:responseObject];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"Failed to Post Device Token");
        [IgnightUtil setIsIOSTokenSet:NO];
        [self.delegate userHttpClient:self didFailWithError:error];
    }];
}

-(void)unsubscribeFromBuzz
{
    if (userInfo.userId.integerValue < 0) {
        return;
    }
    NSDictionary *data = [[NSMutableDictionary alloc] init];
    NSDictionary *msg = @{@"msg" : data};
    NSString *path = [NSString stringWithFormat:@"unsubscribe/buzz/%@", userInfo.userId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
    }];
}

-(void)getUserNotifications
{
    if (userInfo.userId.integerValue < 0) {
        return;
    }
    NSString *path = [NSString stringWithFormat:@"user/notifications/%@", userInfo.userId];
    [self GET:path parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary *data = (NSDictionary *)responseObject;
        if ([data[@"res"] boolValue]) {
            NSDictionary *body = data[@"body"];
            UserNotifications *userNotifications = [UserNotifications getInstance];
            [userNotifications updateBuzz:[body[@"groupBuzz"] boolValue] andInbox: [body[@"inbox"] boolValue]];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {

    }];
}

-(void)clearUserGroupBuzzNotifications
{
    NSDictionary *data = [[NSMutableDictionary alloc] init];
    NSDictionary *msg = @{@"msg" : data};
    NSString *path = [NSString stringWithFormat:@"user/notifications/clearbuzz/%@", userInfo.userId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary *data = (NSDictionary *)responseObject;
        if ([data[@"res"] boolValue]) {
            NSDictionary *body = data[@"body"];
            UserNotifications *userNotifications = [UserNotifications getInstance];
            [userNotifications updateBuzz:[body[@"groupBuzz"] boolValue] andInbox: [body[@"inbox"] boolValue]];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

-(void)clearUserGroupInviteNotifications
{
    NSDictionary *data = [[NSMutableDictionary alloc] init];
    NSDictionary *msg = @{@"msg" : data};
    NSString *path = [NSString stringWithFormat:@"user/notifications/clearinvites/%@", userInfo.userId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary *data = (NSDictionary *)responseObject;
        if ([data[@"res"] boolValue]) {
            NSDictionary *body = data[@"body"];
            UserNotifications *userNotifications = [UserNotifications getInstance];
            [userNotifications updateBuzz:[body[@"groupBuzz"] boolValue] andInbox: [body[@"inbox"] boolValue]];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];
}

-(void)requestVerificationCode: (NSString *)number
{
    if (userInfo.userId.integerValue < 0) {
        DDLogError(@"User ID is invalid");
        return;
    }
    NSDictionary *parameters = @{@"number" : number};
    
    NSString *path = [NSString stringWithFormat:@"sendSmsVerification/%@", userInfo.userId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary *data = (NSDictionary *)responseObject;
        [self.delegate userHttpClient:self sentVerificationCode:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"Failed to get verification response");
    }];
}

-(void)verifyPhoneNumber: (NSString *)number withVerification: (NSString *)verification
{
    NSDictionary *data = @{@"number": number,
                           @"verificationCode": verification};
    NSDictionary *msg = @{@"msg" : data};
    NSString *path = [NSString stringWithFormat:@"verifyPhoneNumber/%@", userInfo.userId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate userHttpClient:self gotVerificationResposne:responseObject];
//        if ([data[@"res"] boolValue]) {
//            NSDictionary *body = data[@"body"];
//            [self.delegate userHttpClient:self gotVerificationResposne:body];
//        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"Failed to verify code");
    }];
}

-(void)uploadContacts: (NSArray *)contacts
{
    NSMutableArray *userContactData = [[NSMutableArray alloc] init];
    for (IgnightContact *contact in contacts) {
        NSMutableDictionary *contactData = [[NSMutableDictionary alloc] init];
        contactData[@"fname"] = contact.firstName == nil ? @"" : contact.firstName;
        contactData[@"lname"] = contact.lastName == nil ? @"" : contact.lastName;
        contactData[@"contacts"] = contact.contacts;
        [userContactData addObject:contactData];
    }
    NSDictionary *data = @{@"addressBook" : userContactData};
    NSDictionary *msg = @{@"msg" : data};
    NSString *path = [NSString stringWithFormat:@"uploadAddressBook/%@", userInfo.userId];
    [self POST:path parameters:msg success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate userHttpClient:self uploadedAddressbook:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"Failed to post addressbook");
    }];
}

-(void)cancelAllOperations
{
    [[self operationQueue] cancelAllOperations];
}

@end
