//
//  IgnightTopBar.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/8/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "IgnightTopBar.h"
#import "SWRevealViewController.h"
#import "VenueViewController.h"

#define barBackgroundColor UIColorFromRGB(0x002946)

@implementation IgnightTopBar
{
    id viewTarget;
//    UserNotifications *userNotifications;
}

- (id)initWithTitle: (NSString *)title withSubtitle: (NSString *)subtitle withSideBarButtonTarget:(id)target
{
    CGRect rect = CGRectMake(0, 0, 320, 58);
    self = [super initWithFrame:rect];
    if (self) {
        [self initTopBarWithFrame:rect];
        [self setTitle:title withSubTitle:subtitle];
        [self addSideBarButtonWithTarget:target];
    }
    return self;
}

- (id)initWithTitle: (NSString *)title withSideBarButtonTarget:(id)target
{
    CGRect rect = CGRectMake(0, 0, 320, 58);
    self = [super initWithFrame:rect];
    if (self) {
        [self initTopBarWithFrame:rect];
        [self setTitle:title];
        [self addSideBarButtonWithTarget:target];
    }
    return self;
}

-(id)initWithWebViewTarget: (id)target
{
    CGRect rect = CGRectMake(0, 0, 320, 58);
    self = [super initWithFrame:rect];
    if (self) {
        [self initTopBarWithFrame:rect];
        viewTarget = target;
    }
    return self;

}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGRect rect = CGRectMake(0, 0, 320, 58);
        [self initTopBarWithFrame:rect];
    }
    return self;
}

- (id)init
{
    CGRect rect = CGRectMake(0, 0, 320, 58);
    self = [super initWithFrame:rect];
    if (self) {
        [self initTopBarWithFrame:rect];
    }
    return self;
}

-(void)initTopBarWithFrame: (CGRect)frame
{
    CGImageRef ref = [_backgroundImage CGImage];
    ref = CGImageCreateWithImageInRect(ref, frame);
    UIImage *newImg = [UIImage imageWithCGImage:ref];
    CGImageRelease(ref);
    
    _imageView = [[UIImageView alloc] initWithFrame:frame];
    _imageView.backgroundColor = barBackgroundColor;
    _imageView.image = newImg;
    _imageView.userInteractionEnabled = YES;
    [self addSubview:_imageView];
    _leftSideButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _rightSideButton = nil;
    _mainTitle = nil;
    _subTitle = nil;
    
//    userNotifications = [UserNotifications getInstance];
//    userNotifications.delegate = self;
//    if (userNotifications.groupBuzz || userNotifications.inbox) {
//        _leftSideButton.backgroundColor = [UIColor yellowColor];
//    } else {
//        _leftSideButton.backgroundColor = [UIColor clearColor];
//    }
}
//
//-(void)userNotificationsWasUpdated:(UserNotifications *)userNotification
//{
//    if (userNotifications.groupBuzz || userNotifications.inbox) {
//        _leftSideButton.backgroundColor = [UIColor yellowColor];
//    } else {
//        _leftSideButton.backgroundColor = [UIColor clearColor];
//    }
//}

-(void)addSideBarButtonWithTarget: (id)target
{
    if (target == nil)
        return;
    
    CGRect frame = CGRectMake(0, 0, self.frame.size.height, 60);
    self.leftSideButton.frame = frame;
    [_leftSideButton setBackgroundColor:[UIColor clearColor]];
    
    UIImage *sideButtonImage = [UIImage imageNamed:@"side-menu-button"];
    [self setMenuButtonImage:sideButtonImage];
    [self.leftSideButton addTarget:target action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.imageView addSubview:self.leftSideButton];
}

-(void)setMenuButtonImage: (UIImage *)image
{
    [_leftBtnBackgroundImage removeFromSuperview];
    _leftBtnBackgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 26, 25, 20)];
    _leftBtnBackgroundImage.image = image;
    _leftBtnBackgroundImage.userInteractionEnabled = YES;
    [_imageView insertSubview:_leftBtnBackgroundImage belowSubview:_leftSideButton];
}

-(void)setLeftButtonBackgroundImage:(UIImage *)leftButtonBackgroundImage
{
    [_leftBtnBackgroundImage removeFromSuperview];
    _leftBtnBackgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 26, 13, 20)];
    _leftBtnBackgroundImage.image = leftButtonBackgroundImage;
    _leftBtnBackgroundImage.userInteractionEnabled = YES;
    [_imageView insertSubview:_leftBtnBackgroundImage belowSubview:_leftSideButton];
}

-(void)addRightContextMenuButton
{
    CGRect frame = CGRectMake(288, 26, 20, 20);
    
    UIImageView *backGroundView = [[UIImageView alloc] initWithFrame:frame];
    backGroundView.tag = 500;
    backGroundView.image = [UIImage imageNamed:@"info_off"];
    
    frame = CGRectMake(self.frame.size.width - 50, 0, 50, 80);
    UIButton *icmMenuButton = [[UIButton alloc] initWithFrame:frame];
    [icmMenuButton addTarget:self action:@selector(rightSideButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    icmMenuButton.backgroundColor = clearC;
    self.rightSideButton.alpha = 1;
    [self.rightSideButton setUserInteractionEnabled:YES];
    self.rightSideButton = icmMenuButton;
    
    [self.imageView addSubview:backGroundView];
    [self.imageView addSubview:self.rightSideButton];
}

-(void)addAddGroupButton
{
    CGRect frame = CGRectMake(288, 26, 20, 20);
    
    UIImageView *backGroundView = [[UIImageView alloc] initWithFrame:frame];
    backGroundView.tag = 500;
    backGroundView.image = [UIImage imageNamed:@"add_group"];
    
    frame = CGRectMake(self.frame.size.width - 50, 0, 50, 80);
    UIButton *icmMenuButton = [[UIButton alloc] initWithFrame:frame];
    [icmMenuButton addTarget:self action:@selector(rightSideButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    icmMenuButton.backgroundColor = clearC;
    self.rightSideButton.alpha = 1;
    [self.rightSideButton setUserInteractionEnabled:YES];
    self.rightSideButton = icmMenuButton;
    
    [self.imageView addSubview:backGroundView];
    [self.imageView addSubview:self.rightSideButton];
}

-(void)setupWebView
{
    [_leftSideButton removeFromSuperview];
    [_rightSideButton removeFromSuperview];
    [_leftBtnBackgroundImage removeFromSuperview];
    [_mainTitle removeFromSuperview];
    [_subTitle removeFromSuperview];
    
    
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *forward = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *reload = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *exit = [UIButton buttonWithType:UIButtonTypeCustom];
    
    CGRect frame = CGRectMake(0, 14, 40, 44);
    back.frame = frame;
    
    frame = CGRectMake(frame.origin.x + frame.size.width, 14, 40, 44);
    forward.frame = frame;
    
    frame = CGRectMake(frame.origin.x + frame.size.width, 14, 40, 44);
    reload.frame = frame;
    
    frame = CGRectMake(320 - 100, 14, 100, 44);
    exit.frame = frame;
    [exit setBackgroundColor:[UIColor whiteColor]];
    [exit setTitle:@"Done" forState:UIControlStateNormal];
    [exit setTitleColor:grayHeaderFooterText forState:UIControlStateNormal];
    [exit addTarget:viewTarget action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [_imageView addSubview:exit];
}

-(void)hideRightContextMenuButton
{
    self.rightSideButton.alpha = 0;
    [self.rightSideButton setUserInteractionEnabled:NO];
    
    UIImageView *imgView = (UIImageView *)[self.imageView viewWithTag:500];
    imgView.alpha = 0;
}

-(IBAction)rightSideButtonClicked:(id)sender
{
    [self.delegate ignightTopBar:self rightSideButtonClicked:sender];
}

#define textColor UIColorFromRGB(0xffffff)

-(void)setTitleText: (NSString* )text
{
    [_mainTitle setText:text];
}

-(void)setTitle: (NSString *)title withFrame: (CGRect)frame
{
    [self setTitle:title];
    _mainTitle.frame = frame;
}

- (void) setTitle: (NSString *)title
{
    CGRect frame = CGRectMake(50, 26, 220, 20);
    self.mainTitle = [[UILabel alloc] initWithFrame:frame];
    UILabel *label = self.mainTitle;
    [label setText:title];
    [label setFont:[UIFont boldSystemFontOfSize:18.0]];
    [label setTextColor:textColor];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setShadowColor:[UIColor blackColor]];
    [label setShadowOffset:CGSizeMake(0, -1)];
    [label setTextAlignment:NSTextAlignmentCenter];

    [[self imageView] addSubview:label];
}

- (void) setTitle: (NSString *)title withSubTitle: (NSString *)subTitle
{
    CGRect frame = CGRectMake(50, 19, 220, 20);
    self.mainTitle = [[UILabel alloc] initWithFrame:frame];
    UILabel *mainLabel = self.mainTitle;
    [mainLabel setText:title];
    [mainLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [mainLabel setTextColor:textColor];
    [mainLabel setBackgroundColor:[UIColor clearColor]];
    [mainLabel setShadowColor:[UIColor blackColor]];
    [mainLabel setShadowOffset:CGSizeMake(0, -1)];
    [mainLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.imageView addSubview:mainLabel];
    
    frame = CGRectMake(50, 35, 220, 20);
    
    self.subTitle = [[UILabel alloc] initWithFrame:frame];
    UILabel *subLabel = self.subTitle;
    [subLabel setText:subTitle];
    [subLabel setFont:[UIFont systemFontOfSize:12.0]];
    [subLabel setTextColor:textColor];
    [subLabel setBackgroundColor:[UIColor clearColor]];
    [subLabel setShadowColor:[UIColor blackColor]];
    [subLabel setShadowOffset:CGSizeMake(0, -1)];
    [subLabel setTextAlignment:NSTextAlignmentCenter];
    [subLabel setAlpha:0.75];
    
    [self.imageView addSubview:subLabel];
}

@end
