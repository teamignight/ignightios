//
//  IgnightUtil.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 1/6/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Groups.h"
#import "Inbox.h"
#import "StoredUserData.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface IgnightUtil : NSObject

+(UIColor*)ignightBlue;
+(UIColor *)ignightGray;

+(NSString *)printUIEdgeInsets: (UIEdgeInsets)inset;
+(NSString *)printCGRect: (CGRect)rect;
+(NSString *)printCGPoint: (CGPoint)point;
+(NSString *)printCGSize: (CGSize)size;
+(NSString *)getBaseUrl;
+(void)setBaseUrl: (NSString *)url;
+(NSString *)getUrlForServlet: (NSString *)servlet;
+(NSString *)dynamicFormatPhoneNumber: (NSString *)number;
+(NSString *)formatPhoneNumber: (NSString *)number;
+(UIColor *)backgroundBlue;
+(UIColor *)backgroundGray;

+(UIImage *)getSquareImageFromImage:(UIImage *)image withSize: (CGSize )newSize;
+(UIImage*)getSquareImageFromImage: (UIImage *)image;
+(UIImage *)getBuzzUserPic: (UIImage *)image;
+(UIImage *)getProfilePic: (UIImage *)image;

+(UIImage *)scaleForUpload: (UIImage *)image;
+(UIImage *)scaleUserProfileImageForUpload: (UIImage *)image;
+(CGSize)getSizeForImageOnScreen: (UIImage *)image;
+(UIImage *)scaleToFitScreen: (UIImage *)image;

+(StoredUserData *)getUserData;
+(BOOL)isIosTokenSet;
+(void)setIsIOSTokenSet: (BOOL)set;
+(void)saveUserData;
+(void)clearUserCredentials;

+(void)getUserNotificationValues;

+(NSInteger)updateUserInfoOnLogin: (NSDictionary *)response;

+(BOOL) validEmail:(NSString*) emailString;
@end
