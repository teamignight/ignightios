//
//  Registration.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/27/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "Registration.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface Registration ()

@end

@implementation Registration
{
    UserHttpClient *userClient;
    UserInfo *userInfo;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    userClient = [UserHttpClient getInstance];
    userInfo = [UserInfo getInstance];
    
    _emailTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    _usernameTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    _passwordTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [gestureRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(respondToTapGesture:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
    
    [[_inputView layer] setCornerRadius:3];
    [[_inputView layer] setMasksToBounds:YES];
    
    [[_createAccountBtn layer] setCornerRadius:3];
    [[_createAccountBtn layer] setMasksToBounds:YES];
    [_createAccountBtn setBackgroundColor:blueBtnBackground];
    [_createAccountBtn setTitleColor:whiteBtnText forState:UIControlStateNormal];
    
    _sepOne.alpha = 0.7;
    _sepOne.backgroundColor = tabBorder;
    
    _sepTwo.alpha = 0.7;
    _sepTwo.backgroundColor = tabBorder;
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:
                                      @"By signing up for IgNight, you acknowledge having read and agree to our Terms of Use and Privacy Policy."];
    
    NSRange termsRange = [[str string] rangeOfString:@"Terms of Use"];
    [str addAttribute:NSLinkAttributeName value:@"http://www.ignight.com/terms" range:termsRange];
    
    NSRange privacyRange = [[str string] rangeOfString:@"Privacy Policy"];
    [str addAttribute:NSLinkAttributeName value:@"http://www.ignight.com/privacy" range:privacyRange];
    
    _smallPrint.attributedText = str;
    [_smallPrint setTextAlignment:NSTextAlignmentCenter];
    [_smallPrint setFont:[UIFont systemFontOfSize:11]];
    [_smallPrint setTextColor:[UIColor lightGrayColor]];
    [_smallPrint setUserInteractionEnabled:YES];
    [_smallPrint setEditable:NO];
    [_smallPrint setScrollEnabled:NO];
    [_smallPrint setSelectable:YES];
    [_smallPrint setDataDetectorTypes:UIDataDetectorTypeLink];
    [_smallPrint setDelegate:self];
    
    [_emailTxt setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_usernameTxt setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_passwordTxt setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self respondToTapGesture:nil];
}

-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    IgnightWebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"web"];
    DDLogDebug(@"Going to %@", [URL absoluteString]);
    webView.website = [URL absoluteString];
    webView.venueName = @"IgNight";
    [self.navigationController pushViewController:webView animated:YES];
    return NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _emailTxt.delegate = self;
    _usernameTxt.delegate = self;
    _passwordTxt.delegate = self;
    userClient.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardNotification:) name:UIKeyboardWillHideNotification object:nil];

    [[self navigationController] setNavigationBarHidden:NO];
    self.navigationController.navigationBar.tintColor = [IgnightUtil backgroundGray];
    self.navigationController.navigationBar.alpha = 0.9;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)handleSwipe: (UISwipeGestureRecognizer *)recognizer
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    self.navigationController.navigationBarHidden = YES;
    [self respondToTapGesture:nil];
}

- (void)keyboardNotification:(NSNotification *)notification {
    NSTimeInterval animationDuration;
    NSDictionary *uInfo = [notification userInfo];
    
    [[uInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    CGPoint kbPointBeg = [[uInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].origin;
    CGPoint kbPointEnd = [[uInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    CGFloat adjustment = kbPointEnd.y - kbPointBeg.y;
    CGRect frame = self.view.frame;
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = CGRectMake(frame.origin.x, frame.origin.y + adjustment, frame.size.width, frame.size.height);
    }];
}

-(IBAction)respondToTapGesture:(id)sender
{
    [_emailTxt resignFirstResponder];
    [_usernameTxt resignFirstResponder];
    [_passwordTxt resignFirstResponder];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTxt) {
        [_usernameTxt becomeFirstResponder];
    } else if (textField == _usernameTxt) {
        [_passwordTxt becomeFirstResponder];
    } else if (textField == _passwordTxt) {
        [self register:nil];
    }
    return YES;
}

- (BOOL)validateRegistrationInformation
{
    if ([_emailTxt.text isEqualToString:@""] ||
        [_usernameTxt.text isEqualToString:@""] ||
        [_passwordTxt.text isEqualToString:@""]) {
        IgAlert(nil, @"Please complete all 3 fields.", nil);
        return NO;
    } else if (![IgnightUtil validEmail:_emailTxt.text]) {
        IgAlert(nil, @"Please enter a valid email address.", nil);
        return NO;
    } else if (_passwordTxt.text.length < 6) {
        IgAlert(nil, @"Password must be at least 6 characters.", nil);
        return NO;
    }
    return YES;
}

#pragma mark - UserHttpClientDelegate

-(void)userHttpClient:(UserHttpClient *)client createdUserWithResponse:(NSDictionary *)response
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSDictionary *body = [response objectForKey:@"body"];
        NSNumber *userId = [body objectForKey:@"id"];
        userInfo.userId = userId;
        userInfo.userName = _usernameTxt.text;
        userInfo.email = _emailTxt.text;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self performSegueWithIdentifier:@"register" sender:self];
    } else {
        [_createAccountBtn setUserInteractionEnabled:YES];
        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [_createAccountBtn setUserInteractionEnabled:YES];
//    IgAlert(nil, @"Failed to reach server", nil);
}

#pragma mark - AlertView

- (IBAction)register:(id)sender {
    [self respondToTapGesture:nil];
    if ([self validateRegistrationInformation]) {
        [_createAccountBtn setUserInteractionEnabled:NO];
        DDLogDebug(@"Calling Create User");
        [userClient createUserWithEmail:_emailTxt.text withUserName:_usernameTxt.text withPassword:_passwordTxt.text];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

@end
