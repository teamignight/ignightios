//
//  TransitionToRegistration.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "TransitionToRegistration.h"
#import "LoginViewController.h"
#import "RegistrationViewController.h"
#import "RESTHandler.h"

@implementation TransitionToRegistration

-(void)perform
{
    LoginViewController *loginViewController = self.sourceViewController;
    RegistrationViewController *registrationViewController = self.destinationViewController;
    
    [UIView animateWithDuration:0.1 animations:^{
        loginViewController.view.alpha = 0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.25 animations:^{
            registrationViewController.view.alpha = 1;
        }];
    }];
    
    [loginViewController presentViewController:registrationViewController animated:YES completion:nil];
}

@end
