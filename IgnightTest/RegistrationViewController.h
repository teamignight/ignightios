//
//  RegistrationViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationViewController : UIViewController <UITextFieldDelegate>


// Register View

@property (weak, nonatomic) IBOutlet UIView *registerView;
@property (weak, nonatomic) IBOutlet UITextField *txtRegisterUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtRegisterPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtRegisterEmail;
@property (strong, nonatomic) IBOutlet UIButton *registrationBtn;
@property (strong, nonatomic) IBOutlet UIButton *returnToLoginBtn;

- (IBAction)btnRegisterAccount:(id)sender;

@end
