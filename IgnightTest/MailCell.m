//
//  MailCell.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/20/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "MailCell.h"

static NSInteger rowHeight = 60;

@implementation MailCell
{
    UIImageView *inviteeImage;
    UILabel *groupName;
    UILabel *requestMessage;
    UILabel *timestamp;
    
    UIButton *acceptBtn;
    UIImageView *acceptImage;
    
    UIButton *rejectBtn;
    UIImageView *rejectImage;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        CGRect frame = CGRectMake(6, 5, 50, 50);
        inviteeImage = [[UIImageView alloc] initWithFrame:frame];
        inviteeImage.image = [UIImage imageNamed:@"group_member_default"];
        [[inviteeImage layer] setCornerRadius:frame.size.width/2];
        [[inviteeImage layer] setMasksToBounds:YES];
        [self.contentView addSubview:inviteeImage];
        
        frame = CGRectMake(64, 7, 160, 15);
        groupName = [[UILabel alloc] initWithFrame:frame];
        [groupName setFont:[UIFont systemFontOfSize:13]];
        [groupName setTextColor:[UIColor blackColor]];
        [self.contentView addSubview:groupName];
        
        frame = CGRectMake(64, 22, 155, 15);
        requestMessage = [[UILabel alloc] initWithFrame:frame];
        [requestMessage setFont:[UIFont systemFontOfSize:12]];
        [requestMessage setTextColor:[UIColor darkGrayColor]];
        [self.contentView addSubview:requestMessage];
        
        frame = CGRectMake(64, 38, 155, 15);
        timestamp = [[UILabel alloc] initWithFrame:frame];
        [timestamp setFont:[UIFont systemFontOfSize:11]];
        [timestamp setTextColor:[UIColor lightGrayColor]];
        [self.contentView addSubview:timestamp];
        
        frame = CGRectMake(221, 15, 35, 30);
        acceptImage = [[UIImageView alloc] initWithFrame:frame];
        acceptImage.image = [UIImage imageNamed:@"icon_ok"];
        [self.contentView addSubview:acceptImage];
        
        frame = CGRectMake(205, 0, 55, 60);
        acceptBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        acceptBtn.frame = frame;
        [acceptBtn addTarget:self action:@selector(acceptInvite:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:acceptBtn];
        
        frame = CGRectMake(275, 15, 30, 30);
        rejectImage = [[UIImageView alloc] initWithFrame:frame];
        rejectImage.image = [UIImage imageNamed:@"icon_no"];
        [self.contentView addSubview:rejectImage];
        
        frame = CGRectMake(260, 0, 60, 60);
        rejectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        rejectBtn.frame = frame;
        [rejectBtn addTarget:self action:@selector(rejectInvite:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:rejectBtn];
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return self;
}

-(void)acceptInvite: (id)sender
{
    UIButton *button = (UIButton *)sender;
    [button setUserInteractionEnabled:NO];
    [self.delegate acceptInviteForGroup:_groupId];
}

-(void)rejectInvite: (id)sender
{
    UIButton *button = (UIButton *)sender;
    [button setUserInteractionEnabled:NO];
    [self.delegate rejectInviteForGroup:_groupId];
}

-(void)reset
{
    inviteeImage.image = [UIImage imageNamed:@"group_member_default"];
    groupName.text = @"";
    requestMessage.text = @"";
    timestamp.text = @"";
    acceptBtn.userInteractionEnabled = YES;
    rejectBtn.userInteractionEnabled = YES;
}

-(void)setInviteeImage: (UIImage *)image
{
    inviteeImage.image = image;
}

-(void)setGroupName: (NSString *)name
{
    groupName.text = name;
}

-(void)setRequestFromUser: (NSString *)username
{
    NSString *text = [NSString stringWithFormat:@"Request from %@", username];
    requestMessage.text = text;
}

-(void)setTimestamp: (NSNumber *)ts
{
    NSTimeInterval interval = [ts floatValue];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:interval / 1000];
    timestamp.text = [date timeAgo];
}

+(NSInteger)getCellHeight
{
    return rowHeight;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
