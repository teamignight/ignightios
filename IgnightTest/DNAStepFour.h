//
//  DNAStepFour.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/2/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightColors.h"
#import "TutorialViewController.h"

@interface DNAStepFour : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UserHttpClientDelegate>

@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIButton *continueBtn;
- (IBAction)registerUser:(id)sender;

- (IBAction)goBack:(id)sender;



@end
