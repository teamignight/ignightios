//
//  TrendingMapView.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/8/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "TrendingMapView.h"
#import "TrendingDataCache.h"
#import "TrendingCell.h"
#import "VenueAnnotation.h"
#import "VenueViewController.h"

@interface TrendingMapView ()

@end

@implementation TrendingMapView
{
    UserInfo *userInfo;
    TrendingDataCache *trendingDataCache;

    //CoreLocation
    CLLocationManager *locationManager;
    
    BOOL noRefresh;
    BOOL cityView;
    BOOL nearMe;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    userInfo = [UserInfo getInstance];
    trendingDataCache = [TrendingDataCache getInstance];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            [locationManager requestWhenInUseAuthorization];
        }
    }
    
    [self setRegionForMap];
}

- (MKCoordinateRegion)setRegionForMap
{
    CLLocationCoordinate2D cityCoordinate = CLLocationCoordinate2DMake(41.8820, -87.6278);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(cityCoordinate, 40 * METERS_PER_MILE, 40 * METERS_PER_MILE);
    [self.mapView setRegion:region animated:YES];
    return region;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_mainTrending resetUserClientDelegate];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    _mapView.delegate = self;
    noRefresh = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [locationManager stopUpdatingLocation];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - Core Location

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    if (userInfo.userLocation == nil) {
        userInfo.userLocation = newLocation;
        return;
    }
    
    CLLocationDistance meters = [newLocation distanceFromLocation:oldLocation];
    if (meters > 0.1) {
        userInfo.userLocation = newLocation;
    }
}

#pragma mark - MKMapViewDelegate
//
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    if (!self.groupMode) {
        if (noRefresh) {
            [_mainTrending setRefreshToDefault];
        }
    }
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    DDLogDebug(@"regionDidChangeAnimated");
    if (_groupMode)
        return;
    
    if (cityView || nearMe) {
        DDLogDebug(@"City Or Near Me");
        cityView = NO;
        nearMe = NO;
        [_mainTrending setRefreshToDefault];
        [self refreshMap];
    } else {
        if (noRefresh) {
            DDLogDebug(@"no Refresh Turned ON");
            noRefresh = NO;
            return;
        }
        [_mainTrending setRefreshActive];
    }
}

-(CLLocationDistance)getRadius
{
    CLLocationCoordinate2D centerCoor = [self.mapView centerCoordinate];
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
    CLLocationCoordinate2D topCenterCoor = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width / 2.0f, 0) toCoordinateFromView:self.mapView];
    CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
    CLLocationDistance radius = [centerLocation distanceFromLocation:topCenterLocation];
    return radius;
}

-(void)mapViewWillStartLoadingMap:(MKMapView *)mapView
{
    [self reAnnotateMap];
}

-(CLLocationCoordinate2D)findCenter
{
    float numVenues = _groupMode ? [trendingDataCache countForGroup] : [trendingDataCache count];
    
    //find for city trending
    float x = 0.0;
    float y = 0.0;
    float z = 0.0;
    
    for (int i = 0; i < numVenues; i++) {
        NSDictionary *data = _groupMode ? [trendingDataCache getGroupTrendingDataForVenueAtIndex:i] :
                                                    [trendingDataCache getTrendingDataForVenueAtIndex:i];
        float LAT = [[data objectForKey:@"latitude"] floatValue];
        float LON = [[data objectForKey:@"longitude"] floatValue];
        
        float lat = LAT * M_PI / 180;
        float lon = LON * M_PI / 180;
        
        float a = cosf(lat) * cosf(lon);
        float b = cosf(lat) * sinf(lon);
        float c = sinf(lat);
        
        x += a;
        y += b;
        z += c;
    }
    
    x /= numVenues;
    y /= numVenues;
    z /= numVenues;
    
    float lon = atan2f(y, x);
    float hyp = sqrtf(x * x + y * y);
    float lat = atan2f(z, hyp);
    
    return CLLocationCoordinate2DMake(lat * 180 / M_PI, lon * 180/M_PI);
}

-(CLLocationDistance)findRadiusForCenter: (CLLocationCoordinate2D)center
{
    float radius = 0;
    CLLocation *centerLoc = [[CLLocation alloc] initWithLatitude:center.latitude longitude:center.longitude];
    float numVenues = _groupMode ? [trendingDataCache countForGroup] : [trendingDataCache count];
    for (int i = 0; i < numVenues; i++) {
        NSDictionary *data = _groupMode ? [trendingDataCache getGroupTrendingDataForVenueAtIndex:i] :
        [trendingDataCache getTrendingDataForVenueAtIndex:i];
        float LAT = [[data objectForKey:@"latitude"] floatValue];
        float LON = [[data objectForKey:@"longitude"] floatValue];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:LAT longitude:LON];
        float dist = [loc distanceFromLocation:centerLoc];
        radius = MAX(dist, radius);
    }
    return radius;
}

-(void)reAnnotateMapWithCoordinate: (CLLocationCoordinate2D)coordinate withRadius: (CLLocationDistance)radius
{
    [_mapView setRegion:MKCoordinateRegionMakeWithDistance(coordinate, radius, radius) animated:YES];
    [self reAnnotateMap];
}

-(void)reAnnotateMap
{
    NSInteger trendingVenues = (self.groupMode) ? [trendingDataCache countForGroup] : [trendingDataCache count];
    
    for (VenueAnnotation *annotation in _mapView.annotations) {
        [_mapView removeAnnotation:annotation];
    }

    for (int i = 0; i < trendingVenues; i++) {
        NSDictionary *venueData = (self.groupMode) ? [trendingDataCache getGroupTrendingDataForVenueAtIndex:i] :
                                                    [trendingDataCache getTrendingDataForVenueAtIndex:i];
        
        NSNumber *venueId = [venueData objectForKey:@"venueId"];
        
        NSNumber *lat = [venueData objectForKey:@"latitude"];
        NSNumber *lon = [venueData objectForKey:@"longitude"];
        NSString *venueName = [venueData objectForKey:@"venueName"];
        NSNumber *userBarColor = venueData[@"userBarColor"];
        if (userBarColor == nil) {
            userBarColor = venueData[@"userVenueValue"];
        }
        DDLogDebug(@"User Bar Color: %@", userBarColor);
        
        MKCoordinateRegion venAnn = { {0.0, 0.0}, {0.0, 0.0} };
        venAnn.center.latitude = [lat doubleValue];
        venAnn.center.longitude = [lon doubleValue];
        VenueAnnotation *ann = [[VenueAnnotation alloc] init];
        ann.title = venueName;
        ann.coordinate = venAnn.center;
        ann.venueId = venueId;
        ann.userVenueValue = userBarColor;
        [self.mapView addAnnotation:ann];
    }
}

-(void)zoomToCurrentLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userInfo.userLocation.coordinate, 1.5 * METERS_PER_MILE, 1.5 * METERS_PER_MILE);
    _mainTrending.center = userInfo.userLocation.coordinate;
    _mainTrending.radius = METERS_PER_MILE;
    nearMe = YES;
    [self.mapView setRegion:region animated:YES];
}

-(void)zoomToCityView
{
    cityView = YES;
    [self setRegionForMap];
}

-(void)refreshMap
{
    if (!self.groupMode) {
        CLLocationDistance radius = _mainTrending.radius  = [self getRadius];
        CLLocationCoordinate2D centerCoor = _mainTrending.center = [self.mapView centerCoordinate];
        [_mainTrending trendingVenuesFromCoordinate:centerCoor withRadius:radius];
        [_mainTrending setRefreshToDefault];
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[VenueAnnotation class]]) {
        VenueAnnotation *vAnnotation = annotation;

        MKAnnotationView *annView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"venuePin"];
        if (annView == nil) {
            annView = [[MKPinAnnotationView alloc] initWithAnnotation:vAnnotation reuseIdentifier:@"venuePins"];
            annView.enabled = YES;
            [annView setCanShowCallout:YES];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            button.tag = [vAnnotation.venueId integerValue];
            button.tintColor = [IgnightUtil backgroundBlue];
            [button addTarget:self action:@selector(goToVenueFromMap:) forControlEvents:UIControlEventTouchUpInside];
            annView.rightCalloutAccessoryView = button;
            
            [[annView.rightCalloutAccessoryView superview] setBackgroundColor:[UIColor blackColor]];
        }

        NSInteger val = [vAnnotation.userVenueValue integerValue];
        if (val > 4) {
            val = 4;
        }

        //Set the image
        if (_groupMode) {
            NSInteger count = vAnnotation.userVenueValue.integerValue;
            UILabel *label = [[UILabel alloc] init];
            label.font = [UIFont boldSystemFontOfSize:10];
            label.backgroundColor = [UIColor clearColor];
            if (count > 0) {
                val = 0;
                label.textColor = UIColorFromRGB(0x62C012);
                label.text = [NSString stringWithFormat:@"+%ld", count];
            } else if (count < 0) {
                val = 3;
                label.textColor = UIColorFromRGB(0xE00708);
                label.text = [NSString stringWithFormat:@"%ld", count];
            } else {
                val = 4;
                label.textColor = [UIColor blackColor];
                label.text = [NSString stringWithFormat:@"%ld", count];
            }
            NSInteger xPos = 0;
            if (label.text.length == 1) {
                xPos = 12;
            } else if (label.text.length == 2) {
                xPos = 10;
            } else if (label.text.length == 3) {
                xPos = 7;
            } else if (label.text.length == 4) {
                xPos = 3;
            }
            label.frame = CGRectMake(xPos, -12, 32, 51);
            [annView addSubview:label];
        }
        
        val = (_groupMode) ? 4 : val;
        annView.image = [UIImage imageNamed:[NSString stringWithFormat:@"map_pin_%ld.png", (NSInteger)(4 - val)]];

        return annView;
    } else {
        DDLogDebug(@"GOT A DIFFERENT VIEW");
    }
    return nil;
}

-(IBAction)goToVenueFromMap:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSNumber* venueId = [NSNumber numberWithInteger:button.tag];
    VenueViewController *venueInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"VenueInfo"];
    venueInfo.goBack = YES;
    venueInfo.venueId = venueId;
    [self.navigationController pushViewController:venueInfo animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    DDLogDebug(@"Memmory Warning In TrendingMapView");
}

@end
