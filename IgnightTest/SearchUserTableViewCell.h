//
//  SearchUserTableViewCell.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 11/5/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchedUser.h"

@protocol SearchUserCellDelegate <NSObject>
@required
-(void)inviteUser:(SearchedUser *)user;
@end

@interface SearchUserTableViewCell : UITableViewCell

@property (strong, nonatomic) id <SearchUserCellDelegate> delegate;

+(NSInteger)getCellHeight;
-(void)setSearchedUser: (SearchedUser *)user;
-(void)setUserImage: (UIImage *)image;
-(BOOL)isUserImageSet;

@end
