//
//  Registration.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/27/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightWebViewController.h"

@interface Registration : UIViewController <UITextFieldDelegate,
UITextViewDelegate,
UserHttpClientDelegate>



@property (strong, nonatomic) IBOutlet UIView *inputView;
@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (strong, nonatomic) IBOutlet UITextField *usernameTxt;
@property (strong, nonatomic) IBOutlet UITextField *passwordTxt;

@property (strong, nonatomic) IBOutlet UIButton *createAccountBtn;

@property (strong, nonatomic) IBOutlet UIView *sepOne;
@property (strong, nonatomic) IBOutlet UIView *sepTwo;

@property (strong, nonatomic) IBOutlet UITextView *smallPrint;


- (IBAction)register:(id)sender;

@end
