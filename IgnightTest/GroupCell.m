//
//  GroupCell.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "GroupCell.h"
#import "NSDate+TimeAgo.h"

static NSString *publicGroup = @"venue_public_group_icon";
static NSString *privateGroup = @"venue_private_group_icon";

static NSString *messageOn = @"venue_buzz_on";
static NSString *messageOff = @"venue_buzz";

@implementation GroupCell
{
    UILabel *groupName;
    UILabel *memberCount;
    UIImageView *groupTypeImage;
    UIImageView *groupBuzzImage;
    UIButton *groupBuzzBtn;
    UILabel *lastBuzzTs;
    UIButton *_report;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        groupTypeImage = [[UIImageView alloc] init];
        
        groupName = [[UILabel alloc] init];
        CGRect frame = CGRectMake(55, 15, 220, 20);
        groupName.frame = frame;
        groupName.font = [UIFont systemFontOfSize:18];
        groupName.textColor = venueNameText;
        groupName.backgroundColor = clearC;
        
        memberCount = [[UILabel alloc] init];
        frame = CGRectMake(55, 35, 220, 15);
        memberCount.frame = frame;
        memberCount.font = [UIFont systemFontOfSize:12];
        memberCount.textColor = venueDistanceText;
        memberCount.backgroundColor = clearC;
        
        groupBuzzImage = [[UIImageView alloc] init];
        frame = CGRectMake(280, 17.5, 25, 25);
        groupBuzzImage.frame = frame;
        groupBuzzImage.alpha = 0;

       frame = CGRectMake(270, 0, 50, 60);
        groupBuzzBtn = [[UIButton alloc] initWithFrame:frame];
        [groupBuzzBtn addTarget:self action:@selector(goToBuzz) forControlEvents:UIControlEventTouchUpInside];
        
        frame = CGRectMake(200, 40, 105, 20);
        lastBuzzTs = [[UILabel alloc] initWithFrame:frame];
        lastBuzzTs.textColor = venueDistanceText;
        lastBuzzTs.font = [UIFont systemFontOfSize:10];
        lastBuzzTs.textAlignment = NSTextAlignmentRight;
        
        frame = CGRectMake(screenWidth - 50, 40, 50, 20);
        _report = [[UIButton alloc] initWithFrame:frame];
        [_report setTitleColor:venueDistanceText forState:UIControlStateNormal];
        [_report setTitleColor:venueDistanceText forState:UIControlStateHighlighted];
        [_report setTitleColor:venueDistanceText forState:UIControlStateSelected];
        [_report setTitle:@"Report" forState:UIControlStateNormal];
        [_report setTitle:@"Report" forState:UIControlStateSelected];
        [_report setTitle:@"Report" forState:UIControlStateHighlighted];
        [_report addTarget:self action:@selector(reportGroup:) forControlEvents:UIControlEventTouchUpInside];
        [_report setFont:[UIFont systemFontOfSize:10]];
        
        [self.contentView addSubview:groupTypeImage];
        [self.contentView addSubview:groupName];
        [self.contentView addSubview:memberCount];
        [self.contentView addSubview:groupBuzzImage];
        [self.contentView addSubview:groupBuzzBtn];
        [self.contentView addSubview:lastBuzzTs];
        [self.contentView addSubview:_report];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return self;
}

-(IBAction)reportGroup:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"IgNight" message:@"Are you sure you want to report this group as inappropriate?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [_delegate reportGroup:_groupId];
    }
}

-(void)goToBuzz
{
    DDLogDebug(@"GROUP BUZZZZZ");
    [self.delegate goToBuzz:_groupId];
}

+(NSInteger)getCellHeight
{
    return rowHeight;
}

-(void)setGroupName: (NSString *)name withCount: (NSInteger)count
{
    groupName.text = name;
    if (count > 1) {
        memberCount.text = [NSString stringWithFormat:@"%ld Members", count];
    } else {
        memberCount.text = [NSString stringWithFormat:@"%ld Member", count];
    }
}

-(void)setGroupType: (NSString *)type
{
    if ([[type lowercaseString] isEqualToString:@"public"]) {
        groupTypeImage.image = [UIImage imageNamed:publicGroup];
        CGRect frame = CGRectMake(10, 17.5, 30, 25);
        groupTypeImage.frame = frame;
    } else if ([[type lowercaseString] isEqualToString:@"private"]) {
        groupTypeImage.image = [UIImage imageNamed:privateGroup];
        CGRect frame = CGRectMake(12.5, 17.5, 25, 25);
        groupTypeImage.frame = frame;
    }
}

-(void)turnBuzzOn: (BOOL)buzzAvailable
{
    groupBuzzImage.alpha = 1;
    groupBuzzBtn.userInteractionEnabled = YES;
    if (!buzzAvailable) {
        groupBuzzImage.image = [UIImage imageNamed:messageOff];
    } else {
        groupBuzzImage.image = [UIImage imageNamed:messageOn];
    }
}

-(void)setLastBuzzTs: (NSNumber *)lastBuzzTsInMillis
{
    _report.alpha = 0;
    lastBuzzTs.alpha = 1;
    if ([lastBuzzTsInMillis integerValue] < 0) {
        lastBuzzTs.alpha = 0;
        return;
    }
    NSTimeInterval interval = [lastBuzzTsInMillis doubleValue];
    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:interval / 1000];
    lastBuzzTs.text = [date timeAgo];
}

-(void)showReportButton
{
    lastBuzzTs.alpha = 0;
    _report.alpha = 1;
}

-(void)hideBuzzIndicator
{
    groupBuzzImage.alpha = 0;
    groupBuzzBtn.userInteractionEnabled = NO;
}

-(void)hideLastBuzzTs
{
    lastBuzzTs.alpha = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
