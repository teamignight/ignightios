//
//  UserContactsTableViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 3/15/15.
//  Copyright (c) 2015 Abhinav Chordia. All rights reserved.
//

#import "UserContactsTableViewController.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "APAddressBook.h"
#import "APContact.h"
#import "APPhoneWithLabel.h"
#import "IgnightContact.h"
#import <MessageUI/MessageUI.h>

@interface UserContactsTableViewController ()

@end

@implementation UserContactsTableViewController
{
    NSArray *_userContacts;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!_goBack) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    }
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    _userContacts = [[NSArray alloc] init];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    APAddressBook *addressBook = [[APAddressBook alloc] init];
    addressBook.fieldsMask = APContactFieldFirstName | APContactFieldLastName | APContactFieldPhonesWithLabels;
    [addressBook loadContacts:^(NSArray *contacts, NSError *error) {
        NSMutableArray *userContacts = [[NSMutableArray alloc] init];
        for (int i =0 ; i < [contacts count]; i++) {
            IgnightContact *ignightContact = [[IgnightContact alloc] init];
            APContact *cont = (APContact*)[contacts objectAtIndex:i];
            
            if ((cont.firstName == nil || [cont.firstName isEqualToString: @""]) && cont.phones.count == 0) {
                continue;
            }
            
            ignightContact.firstName = cont.firstName;
            ignightContact.lastName = cont.lastName;
            DDLogDebug(@"FirstName : %@, LastName: %@", cont.firstName, cont.lastName);
            for (APPhoneWithLabel *number  in cont.phonesWithLabels) {
                NSString *phone = [number.phone stringByReplacingOccurrencesOfString:@" " withString:@""];
                phone = [phone stringByReplacingOccurrencesOfString:@"+" withString:@""];
                phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
                phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
                phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
                [ignightContact.contacts addObject:phone];
                DDLogDebug(@"Label: %@, Number: %@", number.label, number.phone);
            }
            [userContacts addObject:ignightContact];
            DDLogDebug(@"Emails: %@", cont.emails);
        }
        _userContacts = [userContacts sortedArrayUsingComparator:^NSComparisonResult(IgnightContact *obj1, IgnightContact *obj2) {
            return [obj1.firstName compare:obj2.firstName];
        }];
        [self.tableView reloadData];
    }];
}

- (void)revealToggle:(id)sender
{
    [self.revealViewController revealToggleAnimated:YES];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"Invite Your Friends";
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_userContacts count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"user"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"user"];
    }
    
    IgnightContact *contact = [_userContacts objectAtIndex:indexPath.row];
    NSString *name = contact.firstName;
    if (contact.lastName != nil) {
        name = [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];
    }
    cell.textLabel.text = name;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IgnightContact *contact = [_userContacts objectAtIndex:indexPath.row];
    
    if (![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSArray *recipents = @[[contact.contacts objectAtIndex:0]];
    NSString *message = [NSString stringWithFormat:@"I want to invite you to try IgNight! It's a great app to discover like minded people going out! Download it at https://itunes.apple.com/us/app/ignight/id914707174"];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
