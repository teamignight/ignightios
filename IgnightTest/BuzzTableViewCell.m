//
//  BuzzTableViewCell.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/27/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "BuzzTableViewCell.h"

static UIFont* font = nil;
static UIImage* lefthandImage = nil;
static UIImage* righthandImage = nil;

@implementation BuzzTableViewCell {
    UIImageView *userImage;
    UIImageView *userDnaImage;
    UILabel *timestamp;
    UIView *bubbleView;
    
    UILabel *separator;
    UILabel *userName;
}

+ (void)initialize
{
	if (self == [BuzzTableViewCell class])
	{
		font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
        
		lefthandImage = [[UIImage imageNamed:@"BubbleLefthand"]
                         stretchableImageWithLeftCapWidth:20 topCapHeight:19];
        
		righthandImage = [[UIImage imageNamed:@"BubbleRighthand"]
                          stretchableImageWithLeftCapWidth:20 topCapHeight:19];
	}
}

-(void)layoutSubviews
{
    [super layoutSubviews];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        userImage = [[UIImageView alloc] initWithFrame:CGRectZero];
        [userImage setImage:[UIImage imageNamed:@"profile-default-user"]];
        [self.contentView addSubview:userImage];
        
        userDnaImage = [[UIImageView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:userDnaImage];
        
        timestamp = [[UILabel alloc] initWithFrame:CGRectZero];
        timestamp.font =  [UIFont systemFontOfSize:13];
        [self.contentView addSubview:timestamp];
        
        bubbleView = [[UIView alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:bubbleView];
    }
    return self;
}

-(void)setMessage:(BuzzMessage *)message
{
    if (message.myMessage) {
        userImage.frame = CGRectZero;
        userDnaImage.frame = CGRectZero;
        [self setupMyMessaegBubble:message];
    } else {
        
    }
}

-(void)setupMyMessaegBubble: (BuzzMessage *)message
{
    CGRect rect;
    if (message.message != nil) {
        CGSize messageSize = [SpeechBubbleView sizeForMessage:message];
        rect.origin.y = 0;
        rect.origin.x = 0;
        rect.size = messageSize;
        
        UIGraphicsBeginImageContext(rect.size);
        [righthandImage drawInRect:rect];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        NSData *imageData = UIImagePNGRepresentation(newImage);
        newImage = [UIImage imageWithData:imageData];
        
        UIImageView *bubble = (UIImageView *)[bubbleView viewWithTag:101];
        if (bubble == nil) {
            bubble = [[UIImageView alloc] initWithFrame:rect];
            bubble.tag = 101;
            [bubbleView addSubview:bubble];
        }
        bubble.image = newImage;
        
        CGRect bFrame;
        bFrame.origin.y = 0;
        bFrame.origin.x = self.bounds.size.width - messageSize.width - 10;
        bFrame.size = messageSize;
        [bubbleView setFrame:bFrame];
        [bubbleView setBackgroundColor:[UIColor clearColor]];
        
        CGRect lRect;
        lRect.origin.y = 4;
        lRect.origin.x = 4;
        lRect.size.width = bFrame.size.width - 8;
        lRect.size.height = bFrame.size.height - 8;
        UILabel *label = (UILabel *)[bubbleView viewWithTag:102];
        if (label == nil) {
            label = [[UILabel alloc] initWithFrame:lRect];
            [label sizeToFit];
            label.textColor = [UIColor whiteColor];
            label.tag = 102;
            label.text = message.message;
            [bubbleView addSubview:label];
        }
        
        label.text = message.message;
        
        DDLogDebug(@"SUBVIEWS: %@", [bubbleView subviews]);
    } else {
        
    }
}












@end
