//
//  MusicFilter.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/14/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "MusicFilter.h"
#import "MusicTypes.h"

@interface MusicFilter ()

@end

@implementation MusicFilter
{
    UserInfo *userInfo;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    userInfo = [UserInfo getInstance];
    self.collectionView.backgroundColor = trendingToggleUnselectedColor;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [self setImageForCell:cell forRow:indexPath.row];
    [self.delegate musicFilter:self didSelectMusicType:indexPath.row];
    [self.collectionView reloadData];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [self removeImageForCell:cell forRow:indexPath.row];
}

-(void)setImageForCell: (UICollectionViewCell *)cell forRow: (NSInteger)row
{
    UIImage *rankImage = [UIImage imageNamed:@"dna_selected_lighter"];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:102];
    imageView.image = rankImage;
//    imageView = (UIImageView *)[cell viewWithTag:101];
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
    cover.alpha = 0.3;
}

-(void)removeImageForCell: (UICollectionViewCell*)cell forRow: (NSInteger)row
{
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:102];
    imageView.image = nil;
//    imageView = (UIImageView *)[cell viewWithTag:101];
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
    cover.alpha = 0;
}

#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return MUSIC_COUNT;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"musicOption" forIndexPath:indexPath];
    
    UILabel *name = (UILabel*)[cell viewWithTag:100];
    name.text = [MusicTypes getMusicNameFromId:(Music)indexPath.row];
    name.textAlignment = NSTextAlignmentCenter;
    name.adjustsFontSizeToFitWidth = YES;
    name.textColor = venueListText;
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:101];
    NSString *musicImage = [[[MusicTypes getMusicNameFromId:(Music)indexPath.row] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    imageView.image = [UIImage imageNamed:musicImage];
    [[imageView layer] setCornerRadius:imageView.frame.size.width/2];
    [[imageView layer] setMasksToBounds:YES];
    
    UIImageView *cover = (UIImageView *)[cell viewWithTag:103];
    [[cover layer] setCornerRadius:cover.frame.size.width/2];
    [[cover layer] setMasksToBounds:YES];
    
    if ([userInfo.musicFilterId integerValue] == indexPath.row) {
        [self setImageForCell:cell forRow:indexPath.row];
    } else {
        [self removeImageForCell:cell forRow:indexPath.row];
    }

    return cell;
}

@end
