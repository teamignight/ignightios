//
//  EditVenueViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/30/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightTopBar.h"
#import "SWRevealViewController.h"
#import "IgnightColors.h"
#import <QuartzCore/QuartzCore.h>
#import "Trending.h"
#import "GroupViewController.h"

@interface EditVenueViewController : UIViewController<SWRevealViewControllerDelegate,
                                                      UITextFieldDelegate,
                                                      UITextViewDelegate,
                                                      VenueHttpClientDelegate>

@property (strong, nonatomic) NSNumber *venueId;
@property (strong, nonatomic) NSString *venueName;
@property (strong, nonatomic) NSString *venueAddress;
@property (strong, nonatomic) NSString *venueWebsite;
@property (strong, nonatomic) NSString *venuePhone;

@property (strong, nonatomic) IBOutlet UIImageView *titleBar;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UILabel *header;

@property (strong, nonatomic) IBOutlet UIView *venueTitle;
@property (strong, nonatomic) IBOutlet UITextField *venueTitleTxt;

@property (strong, nonatomic) IBOutlet UIView *venueInfo;
@property (strong, nonatomic) IBOutlet UIView *address;
@property (strong, nonatomic) IBOutlet UITextField *addressTxt;

@property (strong, nonatomic) IBOutlet UIView *phone;
@property (strong, nonatomic) IBOutlet UITextField *phoneTxt;

@property (strong, nonatomic) IBOutlet UIView *website;
@property (strong, nonatomic) IBOutlet UITextField *websiteTxt;

@property (strong, nonatomic) IBOutlet UIView *comments;
@property (strong, nonatomic) IBOutlet UITextView *commentsTxtView;
@property (strong, nonatomic) IBOutlet UILabel *commentsPlaceholder;



@property (strong, nonatomic) IBOutlet UILabel *separator;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;

- (void)revealToggle:(id)sender;

- (IBAction)submitData:(id)sender;
- (IBAction)cancel:(id)sender;



@end
