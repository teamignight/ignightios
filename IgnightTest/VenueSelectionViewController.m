//
//  VenueSelectionViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 5/30/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "VenueSelectionViewController.h"
#import "RESTHandler.h"
#import "IgnightTestAppDelegate.h"
#import "VenueInfo.h"
#import "VenuesForCity.h"
#import "VenueDataCache.h"
#import "VenueData.h"
#import "VenueSelectionPosition.h"

@interface VenueSelectionViewController ()

@end

@implementation VenueSelectionViewController
{
    UITapGestureRecognizer *singleTap;
    RESTHandler *restHandler;

    NSMutableDictionary *venueDataDictionary;
    NSMutableArray *venArray;
    NSMutableArray *searchVenues;
//    NSMutableDictionary *nameToVenue;
    
    NSMutableArray *selections;
    NSMutableArray *selectionContainers;
    NSMutableArray *selectionLabels;
    
    NSMutableData *serverData;
    
    UserInfo *userInfo;
    VenueDataCache *venueDataCache;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    venArray = [[NSMutableArray alloc] init];
//    venues = [[NSMutableDictionary alloc] init];
//    nameToVenue = [[NSMutableDictionary alloc] init];

    searchVenues = [[NSMutableArray alloc] init];
    
    //Setup The SingleTap Gesture Recognizer
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    selections = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:-1],
                                                          [NSNumber numberWithInt:-1],
                                                          [NSNumber numberWithInt:-1],nil];
    
    selectionLabels = [[NSMutableArray alloc] initWithObjects:self.selectionOneLabel,
                                                              self.selectionTwoLabel,
                                                              self.selectionThreeLabel, nil];
    
    selectionContainers = [[NSMutableArray alloc] initWithObjects: self.selectionOneContainer,
                                                                    self.selectionTwoContainer,
                                                                    self.selectionThreeContainer, nil];
        
	// Do any additional setup after loading the view.
    restHandler = [RESTHandler getInstance];
    
    userInfo = [UserInfo getInstance];
    venueDataCache = [VenueDataCache getInstance];
}

-(void)viewDidAppear:(BOOL)animated
{
    //Get The Venue List
    if ([venueDataCache isDataSet]) {
        TFLog(@"Venues Already Available For City");
        [self getVenueList];
    } else {
        TFLog(@"Venues Not Available For City");
        if ([self checkIfVenuesAvailableForCity]) {
            [self getVenuesFromDB];
        } else
            [self performSelectorInBackground:@selector(waitForDataToAppear) withObject:nil];
    }
}

-(void)getVenuesFromDB
{
    NSError *error;
    IgnightTestAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    //Delete all previous content
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"VenueInfo" inManagedObjectContext:context]];
    [request setSortDescriptors:[[NSArray alloc] initWithObjects:[[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES] , nil]];
    [request setIncludesPropertyValues:YES];
    NSArray *venues = [context executeFetchRequest:request error:&error];
    [self saveVenueDataToCacheFromDB:venues];
}

-(void)saveVenueDataToCacheFromDB: (NSArray *)venueArray
{
    [venueDataCache saveVenueDataToCacheFromDB:venueArray];
    TFLog(@"Stored %d Venue Data into Cache", [venueArray count]);
}

-(void)waitForDataToAppear
{
    TFLog(@"waitForDataToAppear");
    
    self.view.alpha = 0.1;
    self.view.userInteractionEnabled = NO;
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.center = self.view.center;
    [spinner startAnimating];
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    
    while (![venueDataCache isDataSet]) {
        [NSThread sleepForTimeInterval:0.25f];
    }
    
    self.view.alpha = 1;
    self.view.userInteractionEnabled = YES;
    [spinner removeFromSuperview];
    [self getVenueList];
}


- (BOOL)checkIfVenuesAvailableForCity
{
    NSError *error;
    IgnightTestAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];

    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"VenuesForCity" inManagedObjectContext:context]];
    [request setIncludesPropertyValues:YES];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityId == %@", [userInfo cityId]];
    [request setPredicate:predicate];
    
    NSArray *venuesAvailableForCity = [context executeFetchRequest:request error:&error];

    if ([venuesAvailableForCity count] <= 0)
        return NO;
    
    VenuesForCity *venuesForCity = [venuesAvailableForCity objectAtIndex:0];
    return [[venuesForCity stored] boolValue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Get Venue Information


- (void)connection: (NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    serverData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [serverData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:serverData options:kNilOptions error:nil];
    NSNumber *res = [json objectForKey:@"res"];
    
    if (res) {
        [userInfo SetIsUserInfoSetTo:YES];
        [self performSegueWithIdentifier:@"trendingScreen" sender:self];
    }
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    TFLog(@"Connection Failed! Error - %@ %@", [error localizedDescription], [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)getVenueList
{
    TFLog(@"getVenueList: %d", [[venueDataCache getVenueDictionary] count]);
    
    venueDataDictionary = [[NSMutableDictionary alloc] initWithCapacity:[[venueDataCache getVenueDictionary] count]];
    
    for (VenueData *venData in [[venueDataCache getVenueDictionary] allValues]) {
        VenueSelectionPosition *venAtPos = [[VenueSelectionPosition alloc] init];
        venAtPos.selected = NO;
        venAtPos.selectionPosition = [NSNumber numberWithInt:-1];
        venAtPos.data = venData;
        [venArray addObject:venAtPos];
        [venueDataDictionary setObject:venAtPos forKey:[venData.venueId stringValue]];
    }

    [self.venueTableView reloadData];
}

# pragma mar - DismissKeyboard

- (void)dismissKeyboard
{
    [self.searchBar resignFirstResponder];
}

# pragma mark - Search Bar Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [UIView animateWithDuration:0.35 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - 156,
                                     self.view.frame.size.width, self.view.frame.size.height);
    }];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [UIView animateWithDuration:0.35 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 156,
                                          self.view.frame.size.width, self.view.frame.size.height);
    }];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.venueTableView reloadData];
}


# pragma mark - Sarch Bar Display Controller Delegate Protocols

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [searchVenues removeAllObjects];
    
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF.data.venueName contains[cd] %@",
                                    searchString];
    
    searchVenues = [[venArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    
    return TRUE;
}

#pragma mark - TableView DataSource Delegate Protocol

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    VenueSelectionPosition *venAtPos = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        venAtPos = [searchVenues objectAtIndex:indexPath.row];
        cell.textLabel.text = [[venAtPos data] venueName];
    } else {
        NSNumber *venId = [NSNumber numberWithInt: indexPath.row];
        venAtPos = [venueDataDictionary objectForKey:[venId stringValue]];
        cell.textLabel.text = [[venAtPos data] venueName];
    }
    
    if (venAtPos.selected) {
        [cell setAccessoryView: [[UIImageView alloc] initWithImage: [UIImage imageNamed:[NSString stringWithFormat:@"venue-selection-accessory-%d",
                                                                                            ([venAtPos.selectionPosition intValue] + 1)]]]];
        CGRect frame =  cell.accessoryView.frame;
        frame.size = CGSizeMake(44, 44);
        cell.accessoryView.frame = frame;
    } else {
        [cell setAccessoryView:nil];
    }
    
    return  cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchVenues count];
    } else {
        TFLog(@"THere are %d Venues Available", [venArray count]);
        return [venArray count];
    }
}

#pragma mark - TableView Delegate Protocol

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor colorWithRed:252.0/255.0 green:248.0/255.0 blue:236.0/255.0 alpha:1];
    cell.textLabel.textColor = [UIColor colorWithRed:132.0/255.0 green:121.0/255.0 blue:95.0/255.0 alpha:1];
    [cell.textLabel setFont: [UIFont fontWithName:@"Helvetica Neue" size:18.0]];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell-selection"]];
    if ([indexPath row] % 2 == 0) {
        cell.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:252.0/255.0 blue:248.0/255.0 alpha:1];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:249.0/255.0 blue:238.0/255.0 alpha:1];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {    
    //Get the Venue Selected
    VenueSelectionPosition *venAtPosition;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        venAtPosition = [searchVenues objectAtIndex:indexPath.row];
    } else {
        NSNumber *venId = [NSNumber numberWithInt:indexPath.row];
        venAtPosition = [venueDataDictionary objectForKey:[venId stringValue]];
    }
    
    //Dont do anything if the Venue is Already Selected
    if (venAtPosition.selected) {
        [self unSetVenueAtPosition:[venAtPosition selectionPosition] fromTableView:tableView atRow: [NSNumber numberWithInt: indexPath.row]];
        return;
    }
    
    //Get the selection Position
    VenueSelectionPosition *sel = nil;
    NSInteger selection = 0;
    for (int x = 0; x < 3; x++) {
        NSNumber *pos = [selections objectAtIndex:x];
        if ([pos intValue] > -1) {
            continue;
        }
        else {
            sel = venAtPosition;
            selection = x;
            break;
        }
    }
    
    if (sel == nil) {
        return;
    }
    
    [self setVenue:venAtPosition atPosition:[NSNumber numberWithInt:selection] forTableView:tableView forIndexPath:indexPath];
}

#pragma - Animations

- (void)setVenue: (VenueSelectionPosition*)venAtPosition atPosition: (NSNumber *)pos forTableView: (UITableView *)tableView forIndexPath: (NSIndexPath *)indexPath
{
    NSNumber *venId = [selections objectAtIndex: [pos intValue]];
    if ([venId intValue] > -1)
        return;
    
    //Get the cell that holds Venue
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryView:nil];
    
    [selections setObject:[[venAtPosition data] venueId] atIndexedSubscript: [pos intValue]];
    venAtPosition.selected = YES;
    venAtPosition.selectionPosition = pos;
    
    UILabel *lab = [selectionLabels objectAtIndex:[[venAtPosition selectionPosition] intValue]];
    lab.text = [[venAtPosition data] venueName];
    [lab setHidden:NO];
    
    UIView *container = [selectionContainers objectAtIndex:[pos intValue]];
    CGRect containerFrame = [container frame];
    CGRect labelFrame = [lab frame];
    
    containerFrame.origin.y = containerFrame.origin.y - 7;
    labelFrame.origin.y = labelFrame.origin.y + 11;
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         container.frame = containerFrame;
                         lab.frame = labelFrame;
                         lab.alpha = 1;
                     }
                     completion:nil];
    
    [UIView commitAnimations];
    
    
    [cell setAccessoryView: [[UIImageView alloc] initWithImage: [UIImage imageNamed:[NSString stringWithFormat:@"venue-selection-accessory-%d", [pos intValue]  + 1]]]];
    CGRect frame =  cell.accessoryView.frame;
    frame.size = CGSizeMake(44, 44);
    cell.accessoryView.frame = frame;
    
    
    BOOL allSelectionsMade = YES;
    for (int i = 0; i < 3; i++) {
        NSNumber *venId = [selections objectAtIndex:i];
        if ([venId intValue] == -1) {
            allSelectionsMade = NO;
            break;
        }
    }
    
    if (allSelectionsMade) {
        [self displayRegisterButton];
    }
}

- (void)displayRegisterButton
{
    UIImage *bg = [UIImage imageNamed:@"btn-register"];
    [self.continueButton setBackgroundImage:bg forState:UIControlStateNormal];
    bg = [UIImage imageNamed:@"btn-register-selected"];
    [self.continueButton setBackgroundImage:bg forState:UIControlStateHighlighted];
    [self.continueButton setTitle:@"IgNight your Night!" forState:UIControlStateNormal];
    [self.continueButton setTitleShadowColor:[UIColor colorWithRed:190.0/255.0 green:113.0/255.0 blue:10.0/255.0 alpha:1] forState:UIControlStateNormal];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.venueTableView.alpha = 0.35;
        self.continueButton.alpha = 1;
    }];
    
}

- (void)unDisplayRegisterButton
{
    [UIView animateWithDuration:0.25 animations:^{
        self.venueTableView.alpha = 1;
        self.continueButton.alpha = 0;
    }];
}

- (void)unSetVenueAtPosition: (NSNumber *)pos fromTableView: (UITableView *)tableView atRow:(NSNumber *) row
{    
    NSNumber *venId = [selections objectAtIndex: [pos intValue]];
    if ([venId intValue] == -1)
        return;
    
    [self unDisplayRegisterButton];
    
    if (row == nil)
        row = venId;
    
    //Get the cell that holds Venue
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[row intValue] inSection:0]];
    [cell setAccessoryView:nil];
    
    VenueSelectionPosition *venAtPosition = [venueDataDictionary objectForKey:[venId stringValue]];
    [selections setObject:[NSNumber numberWithInt:-1] atIndexedSubscript:[[venAtPosition selectionPosition] intValue]];
    
    venAtPosition.selected = NO;
    venAtPosition.selectionPosition = [NSNumber numberWithInt:-1];
    
    UILabel *lab = [selectionLabels objectAtIndex: [pos intValue]];
    lab.text = @"";
    [lab setHidden:YES];
    
    UIView *container = [selectionContainers objectAtIndex:[pos intValue]];
    CGRect containerFrame = [container frame];
    CGRect labelFrame = [lab frame];
    
    containerFrame.origin.y = containerFrame.origin.y + 7;
    labelFrame.origin.y = labelFrame.origin.y - 11;

    [UIView animateWithDuration:0.25
                     animations:^{
                         container.frame = containerFrame;
                         lab.frame = labelFrame;
                         lab.alpha = 1;
                     }
                     completion:nil];
    
    [UIView commitAnimations];
}

#pragma - Actions

- (IBAction)selectionButton:(id)sender {
    if (sender == [self selectionOneButton])
        [self unSetVenueAtPosition:[NSNumber numberWithInt:0] fromTableView:self.venueTableView atRow:nil];
    else if (sender == [self selectionTwoButton])
        [self unSetVenueAtPosition:[NSNumber numberWithInt:1] fromTableView:self.venueTableView atRow:nil];
    else if (sender == [self selectionThreeButton])
        [self unSetVenueAtPosition:[NSNumber numberWithInt:2] fromTableView:self.venueTableView atRow:nil];
}

- (IBAction)continueButton:(id)sender {
    NSDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:@"setCompleteUserInfo" forKey:@"type"];
    
    TFLog(@"userInfo %@", userInfo.firstName);
    
    [data setValue:userInfo.userId forKey:@"userId"];
    [data setValue:userInfo.firstName forKey:@"firstName"];
    [data setValue:userInfo.lastName forKey:@"lastName"];
    [data setValue:(userInfo.pictureString) ? userInfo.pictureString : @"" forKey:@"picture"];
    [data setValue:userInfo.cityId forKey:@"cityId"];
    [data setValue:userInfo.genderId forKey:@"genderId"];
    [data setValue:userInfo.ageBucketId forKey:@"age"];
    [data setValue:userInfo.music forKey:@"music"];
    [data setValue:userInfo.atmospheres forKey:@"atmosphere"];
    [data setValue:userInfo.spendingLimit forKey:@"spendingLimit"];
    
    NSDictionary *msg = [[NSMutableDictionary alloc] initWithObjectsAndKeys:data, @"msg", nil];
    [restHandler postToServlet:@"user" withBody:msg withDelegate:self];
}

@end
