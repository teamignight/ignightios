//
//  VenueInfoViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/22/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MusicTypes.h"
#import "AtmosphereTypes.h"
#import "SpendingTypes.h"
#import "AgeBuckets.h"
#import "TrendingCell.h"
#import "Cities.h"
#import "IgnightColors.h"
#import "EditVenueViewController.h"
#import "VenueViewController.h"
#import "IgnightWebViewController.h"
#import "CityTrendingTableViewCell.h"
#import "VenueMetaDataTableViewCell.h"

@interface VenueInfoViewController : UIViewController<
CityTrendingTableViewCellDelegate,
MKMapViewDelegate,
UITableViewDataSource,
UITableViewDelegate,
TrendingCellDelegate,
VenueMetaDataTableViewCellDelegate,
VenueHttpClientDelegate>
//
//@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
//@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) VenueData *venueData;

@property (strong, nonatomic) VenueViewController *venueController;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSNumber *venueId;
@property (strong, nonatomic) NSString *venueName;
@property (strong, nonatomic) NSString *venueAddres;
@property (strong, nonatomic) NSNumber *userBarColor;
@property (nonatomic) CLLocationCoordinate2D venueCoordinate;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *website;

@property (nonatomic) NSInteger musicTrendingId;
@property (nonatomic) NSInteger atmosphereTrenidngId;
@property (nonatomic) NSInteger ageBucketId;
@property (nonatomic) NSInteger spendingId;
@property (nonatomic, strong) NSArray *trendingGroups;

-(void)setupInitialView;
-(void)setVenueEventInfo:(NSString *)venueEventInfo;

@end
