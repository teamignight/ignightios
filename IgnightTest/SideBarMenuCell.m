//
//  SideBarMenuCell.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/23/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "SideBarMenuCell.h"
#import <QuartzCore/QuartzCore.h>
#import "IgnightColors.h"

#define CELL_HEIGHT 54

@implementation SideBarMenuCell
{
    UIColor *sbMainBg;
    UIColor *sbMainSeperatorBottom;
    
}

-(void)initColors
{
    sbMainBg = sideBarBackground;
    sbMainSeperatorBottom = sideBarListSeparator;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initColors];
        
        CGRect frame = CGRectMake(68, 0, self.contentView.frame.size.width - 104, 52);
        _label = [[UILabel alloc] initWithFrame:frame];
        _label.font = [UIFont systemFontOfSize:18.0];
        _label.textColor = sideBarText;
        _label.backgroundColor = clearC;
        _label.text = @"";
        
        frame = CGRectMake(10, 0, 54, CELL_HEIGHT);
        _icon = [[UIImageView alloc] initWithFrame:frame];
        
        _activityIndicatorBackgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width - 110, 12, 30, 30)];
        _activityIndicatorBackgroundImage.backgroundColor = trendingMusicToggleActiveColor;
        [[_activityIndicatorBackgroundImage layer] setCornerRadius:15];
        [[_activityIndicatorBackgroundImage layer] setMasksToBounds:YES];
        
        frame = CGRectMake(self.contentView.frame.size.width - 110, 12, 30, 30);
        _activityIndicator = [[UILabel alloc] initWithFrame:frame];
        _activityIndicator.layer.cornerRadius = 3.0;
        _activityIndicator.layer.masksToBounds = YES;
        _activityIndicator.font = [UIFont boldSystemFontOfSize:14.0];
        [_activityIndicator setTextColor:sideBarText];
        _activityIndicator.textAlignment = NSTextAlignmentCenter;
        
        _activityIndicator.alpha = 0;
        
//        // build seperator top
//        frame = CGRectMake(0, CELL_HEIGHT - 2, self.contentView.frame.size.width, 1);
//        _separatorTop = [[UIView alloc] initWithFrame:frame];
        
        // build seperator bottom
        frame = CGRectMake(0, CELL_HEIGHT - 1, self.contentView.frame.size.width, 1);
        _separatorBottom = [[UIView alloc] initWithFrame:frame];
        _separatorBottom.backgroundColor = sbMainSeperatorBottom;
        
        [self.contentView addSubview:_label];
        [self.contentView addSubview:_icon];
        [self.contentView addSubview:_activityIndicatorBackgroundImage];
        [self.contentView addSubview:_activityIndicator];
//        [self.contentView addSubview:_separatorTop];
        [self.contentView addSubview:_separatorBottom];
        
        self.backgroundColor = sbMainBg;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setTitle: (NSString *)title
{
    self.label.text = title;
}

-(void)setImageForIcon: (UIImage *)image
{
    _icon.backgroundColor = [UIColor clearColor];
    if ([_label.text isEqualToString:@"Groups"] || [_label.text isEqualToString:@"Tutorial"]) {
        CGRect frame = CGRectMake(CELL_HEIGHT / 4 + 5, CELL_HEIGHT / 4, 27, CELL_HEIGHT/2);
        self.icon.frame = frame;
    } else {
        CGRect frame = CGRectMake(5, 0, 54, CELL_HEIGHT);
        self.icon.frame = frame;
    }
    self.icon.image = image;
}

-(void)adjustUserImage
{
    CGRect frame = CGRectMake(10, 5, CELL_HEIGHT - 10, CELL_HEIGHT - 10);
    self.icon.frame = frame;
    [self.icon.layer setCornerRadius:(CELL_HEIGHT - 10) / 2];
    [self.icon.layer setMasksToBounds:YES];
    self.label.font = [UIFont boldSystemFontOfSize:20];
    _icon.backgroundColor = buzzUserProfileBackground;
}

-(void)hideSeparatorBottom
{
    _separatorBottom.alpha = 0;
}

-(void)showSeparatorBottom
{
    _separatorBottom.alpha = 1;
}

-(void)makeSeparatorRegular
{
    CGRect frame = CGRectMake(0, CELL_HEIGHT - 1, self.contentView.frame.size.width, 1);
    [_separatorBottom setFrame:frame];
    _separatorBottom.backgroundColor = sbMainSeperatorBottom;
}

-(void)makeSeparatorLightUp
{
    CGRect frame = CGRectMake(0, CELL_HEIGHT - 1, self.contentView.frame.size.width, 1);
    [_separatorBottom setFrame:frame];
    _separatorBottom.backgroundColor = UIColorFromRGB(0x62C012);
}

-(void)makeSeparatorThicker
{
    CGRect frame = CGRectMake(0, CELL_HEIGHT - 1, self.contentView.frame.size.width, 3);
    [_separatorBottom setFrame:frame];
    _separatorBottom.backgroundColor = sbMainSeperatorBottom;
}

-(void)makeActivityIndicatorVisibleWithText: (NSString *)text
{
    if (text) {
        _activityIndicatorBackgroundImage.alpha = 1;
        _activityIndicator.alpha = 1;
        _activityIndicator.text = text;
    } else {
        _activityIndicatorBackgroundImage.alpha = 0;
        _activityIndicator.alpha = 0;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
