//
//  UserNotifications.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 11/22/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "UserNotifications.h"

@implementation UserNotifications

+ (UserNotifications *)getInstance
{
    static UserNotifications* instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

-(void)updateGroupBuzz: (BOOL)buzz
{
    _groupBuzz = buzz;
    [_delegate userNotificationsWasUpdated:self];
}

-(void)updateInbox: (BOOL) inbox
{
    _inbox = inbox;
    [_delegate userNotificationsWasUpdated:self];
}

-(void)updateBuzz: (BOOL)buzz andInbox: (BOOL)inbox
{
    _groupBuzz = buzz;
    _inbox = inbox;
    [_delegate userNotificationsWasUpdated:self];
}

@end
