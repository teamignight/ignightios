//
//  Inbox.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/16/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "Inbox.h"
#import "Invitation.h"

@implementation Inbox
{
    NSMutableArray *invites;
    NSMutableDictionary *groupInvites;
    NSInteger activityCount;
}

+ (Inbox*)getInstance;
{
    static Inbox *instance = nil;
    @synchronized(self) {
        if (!instance) {
            instance = [[Inbox alloc] init];
        }
    }
    return instance;
}

-(void)clear
{
    [groupInvites removeAllObjects];
    [invites removeAllObjects];
}

-(NSInteger)count
{
    return [groupInvites count];
}

-(NSMutableArray *)getInvitesForGroups
{
    return invites;
}

-(void)gotNewGroupRequests: (NSArray *)requests
{
    groupInvites = [[NSMutableDictionary alloc] init];
    invites = [[NSMutableArray alloc] init];
    for (NSDictionary *invite in requests) {
        Invitation *invitation = [[Invitation alloc] initWithData:invite];
        [invites addObject:invitation];
        [groupInvites setObject:invitation forKey:invitation.groupId];
    }
}

-(void)deleteInviteFromGroup: (NSNumber *)groupId
{
    NSDictionary *invite = [groupInvites objectForKey:groupId];
    [groupInvites removeObjectForKey:groupId];
    
    if (invite != nil) {
        [invites removeObject:invite];
    }
}

@end