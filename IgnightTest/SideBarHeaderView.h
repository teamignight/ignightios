//
//  SideBarHeaderView.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/24/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightColors.h"

@interface SideBarHeaderView : UIView

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIButton *edit;
@property (nonatomic, strong) UIView *seperatorBottom;

-(id)initWithTitle: (NSString *)title forTarget: (id)target withTag: (NSInteger)tag;

@end
