//
//  SpendingViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/21/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "SpendingViewController.h"
#import "UserInfo.h"
#import "RESTHandler.h"
#import "SpendingTypes.h"

#define MAX_RANK 1
#define TAG_ADJ 100

@interface SpendingViewController ()

@end

@implementation SpendingViewController
{
    NSMutableData *serverData;
    
    UserInfo *userInfo;
    RESTHandler *restHandler;
    int optionsInRow;
    
    NSMutableDictionary *selectionToRank;
    NSMutableDictionary *rankToSelection;
    int selectionCounter;
    int maxSelectedRank;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    optionsInRow = 2;
    
    IgnightPreferences *prefs = [[IgnightPreferences alloc] initWithPreferenceSet:[SpendingTypes getArrayOfSpendingNames] container:self.scrollView optionsInRow:optionsInRow forClass:self width:320];
    [prefs populateScrollView];
    
    userInfo = [UserInfo getInstance];
    restHandler = [RESTHandler getInstance];
    
    selectionToRank = [[NSMutableDictionary alloc] init];
    rankToSelection = [[NSMutableDictionary alloc] init];
    selectionCounter = 0;
    maxSelectedRank = 0;
    
    
    TFLog(@"SpendingViewController: Loaded UserInfo with: %@", [SpendingTypes getSpendingNameFromId:[[userInfo spendingLimit] intValue]]);
    
    [self resetSelectiions];
    self.navigationController.navigationBarHidden = YES;
}

- (void)resetSelectiions
{
    NSInteger spendingId = [userInfo.spendingLimit intValue];
    if (spendingId < 0)
        return;
    
    NSString *name = [SpendingTypes getSpendingNameFromId:spendingId];
    [self setSelection:name asRank:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)selectionOptionPressed:(UIButton *)sender {
    UIButton *button = (UIButton *)sender;
    
    NSString *buttonTitle = [[button titleLabel] text];
    
    if (button.selected == NO) {
        NSInteger nextRank = [self findNextAvailableRank];
        
        if (nextRank == -1)
            return;
        
        [self setSelection:buttonTitle asRank:nextRank];
    } else {
        NSInteger rank = [[selectionToRank objectForKey:buttonTitle] integerValue];
        [self removeSelection:buttonTitle asRank:rank];
    }
}

-(NSInteger)findNextAvailableRank {
    for (int i = 0; i < MAX_RANK; i++) {
        if (![[rankToSelection allKeys] containsObject:[NSString stringWithFormat:@"%d", i]])
            return i;
    }
    return -1;
}

-(void)setSelection: (NSString *)name asRank: (NSInteger)rank
{
    TFLog(@"Setting Selection %@ as rank: %d", name, rank);
    
    [selectionToRank setObject:[NSString stringWithFormat:@"%d", rank] forKey:name];
    [rankToSelection setObject:name forKey:[NSString stringWithFormat:@"%d", rank]];
    
    NSString *imagePath = [NSString stringWithFormat:@"selection-option-button-selected-%i", rank + 1];
    UIImage *newImage = [UIImage imageNamed:imagePath];
    
    NSInteger tag = [SpendingTypes getSpendingFromName:name];
    UIButton *button = (UIButton*)[self.scrollView viewWithTag:(tag + TAG_ADJ)];
    
    [button setBackgroundImage:newImage forState:UIControlStateSelected];
    [button  setSelected:YES];
    selectionCounter++;
    
    [self checkIfReadyToContinue];
}

-(void)removeSelection: (NSString *)name asRank: (NSInteger)rank
{
    [selectionToRank removeObjectForKey:name];
    [rankToSelection removeObjectForKey:[NSString stringWithFormat:@"%d", rank]];
    
    NSInteger tag = [SpendingTypes getSpendingFromName:name];
    UIButton *button = (UIButton*)[self.scrollView viewWithTag:(tag + TAG_ADJ)];
    
    [button setBackgroundImage:nil forState:UIControlStateSelected];
    [button setSelected:NO];
    selectionCounter--;
    
    [self checkIfReadyToContinue];
}

-(void)checkIfReadyToContinue
{
    TFLog(@"selectionCounter : %d", selectionCounter);
    self.continueButton.alpha = selectionCounter;
    if (selectionCounter == MAX_RANK) {
        self.scrollView.alpha = 0.35;
    } else
        self.scrollView.alpha = 1;
}

-(void)updateUserInfo
{
    userInfo.spendingLimit = [NSNumber numberWithInt:-1];
    for (int i = 0; i < [rankToSelection count]; i++) {
        NSString *name = [rankToSelection objectForKey:[NSString stringWithFormat:@"%d", i]];
        NSNumber *dnaValue = [NSNumber numberWithInt:[SpendingTypes getSpendingFromName:name]];
        [userInfo setSpendingLimit:dnaValue];
    }
    TFLog(@"SpendingLimit Set To: %@", userInfo.spendingLimit);
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    [self updateUserInfo];
    return YES;
}

- (IBAction)continueButtonPressed:(id)sender {
    [self updateUserInfo];
    
    NSDictionary *data = [[NSMutableDictionary alloc] init];
    [data setValue:@"setCompleteUserInfo" forKey:@"type"];
    
    TFLog(@"userInfo %@", userInfo.firstName);
    
    [data setValue:userInfo.userId forKey:@"userId"];
    [data setValue:(userInfo.pictureString) ? userInfo.pictureString : @"" forKey:@"picture"];
    [data setValue:userInfo.cityId forKey:@"cityId"];
    [data setValue:userInfo.genderId forKey:@"genderId"];
    [data setValue:userInfo.ageBucketId forKey:@"age"];
    [data setValue:[self convertToValidArray:userInfo.music] forKey:@"music"];
    [data setValue:[self convertToValidArray:userInfo.atmospheres] forKey:@"atmosphere"];
    [data setValue:userInfo.spendingLimit forKey:@"spendingLimit"];
    
    NSDictionary *msg = [[NSMutableDictionary alloc] initWithObjectsAndKeys:data, @"msg", nil];
    [restHandler postToServlet:@"user" withBody:msg withDelegate:self];
}

-(NSArray *)convertToValidArray: (NSArray *)array
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (NSObject *val in array) {
        if ([val isKindOfClass:[NSNumber class]])
            [result addObject:val];
    }
    return result;
}

- (void)connection: (NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    serverData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [serverData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:serverData options:kNilOptions error:nil];
    NSNumber *res = [json objectForKey:@"res"];
    
    if (res) {
        [userInfo SetIsUserInfoSetTo:YES];
        [self performSegueWithIdentifier:@"trending" sender:self];
    }
}

@end
