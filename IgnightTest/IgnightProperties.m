//
//  IgnightProperties.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 11/19/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "IgnightProperties.h"

@implementation IgnightProperties

+(NSString *)getHeaderToken
{
    return @"E5DD71E0-710F-498D-A101-1E16B0617527";
}

+(float)getImageScalingValue
{
    return 1.0;
}


@end
