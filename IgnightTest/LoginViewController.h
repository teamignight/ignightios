//
//  LoginViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/16/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Groups.h"
#import "Inbox.h"
#import "UserHttpClient.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate,
                                                   UserHttpClientDelegate,
                                                   UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImgView;


@property (strong, nonatomic) IBOutlet UIView *loginInputView;
@property (strong, nonatomic) IBOutlet UITextField *usernameTxt;
@property (strong, nonatomic) IBOutlet UITextField *passwordTxt;

@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIButton *signUpBtn;
@property (strong, nonatomic) IBOutlet UIView *separator;

- (IBAction)signIn:(id)sender;

@end
