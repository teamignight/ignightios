//
//  SearchUserTableViewCell.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 11/5/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "SearchUserTableViewCell.h"

#define CELL_HEIGHT 50
static NSString *AVAILABLE_USER_INVITE = @"list_add_active_green";
static NSString *USER_INVITED = @"list_add";
static NSString *DEFAULT_USER_IMAGE = @"group_member_default";


@implementation SearchUserTableViewCell
{
    SearchedUser *user;
    
    UIImageView *userImage;
    UILabel *userName;
    UIButton *invite;
    UIImageView *inviteImage;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect userImageFrame = CGRectMake(5, 5, CELL_HEIGHT - 10, CELL_HEIGHT - 10);
        userImage = [[UIImageView alloc] initWithFrame:userImageFrame];
        userImage.contentMode = UIViewContentModeScaleAspectFill;
        userImage.backgroundColor = buzzUserProfileBackground;
        [[userImage layer] setCornerRadius:(CELL_HEIGHT - 10)/ 2];
        [[userImage layer] setMasksToBounds:YES];
        [self.contentView addSubview:userImage];
        
        CGRect userNameFrame = CGRectMake(55, 5, 205, 40);
        userName = [[UILabel alloc] initWithFrame:userNameFrame];
        [self.contentView addSubview:userName];
        
        invite = [UIButton buttonWithType:UIButtonTypeCustom];
        invite.frame = CGRectMake(320 - CELL_HEIGHT, 0, CELL_HEIGHT, CELL_HEIGHT);
        [invite addTarget:self action:@selector(inviteClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:invite];
        
        inviteImage = [[UIImageView alloc] initWithFrame:CGRectMake(320 - CELL_HEIGHT, 10, 30, 30)];
        inviteImage.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:inviteImage];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+(NSInteger)getCellHeight
{
    return CELL_HEIGHT;
}

-(IBAction)inviteClicked:(id)sender
{
    if (user.groupStatus == NON_MEMBER) {
        inviteImage.image = [UIImage imageNamed:USER_INVITED];
        [invite setUserInteractionEnabled:NO];
        user.groupStatus = PENDING_MEMBER;
        [self.delegate inviteUser:user];
    }
}

-(void)setUserImage: (UIImage *)image
{
    userImage.image = image;
}

-(BOOL)isUserImageSet
{
    return userImage.image != nil;
}

-(void)setSearchedUser: (SearchedUser *)searchUser;
{
    self->user = searchUser;
    userImage.image = [UIImage imageNamed:DEFAULT_USER_IMAGE];
    userName.text = user.userName;
    if (user.groupStatus == NON_MEMBER) {
        inviteImage.image = [UIImage imageNamed:AVAILABLE_USER_INVITE];
        [invite setUserInteractionEnabled:YES];
    } else if (user.groupStatus == PENDING_MEMBER) {
        inviteImage.image = [UIImage imageNamed:USER_INVITED];
        [invite setUserInteractionEnabled:NO];
    } else {
        inviteImage.image = nil;
        [invite setUserInteractionEnabled:NO];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
