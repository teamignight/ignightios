//
//  BuzzViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/9/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuzzInputTextView.h"
#import "BuzzMessage.h"
#import "SpeechBubbleView.h"
#import "MessageTableViewCell.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "Groups.h"
#import "Group.h"
#import "BuzzTableViewCell.h"
#import "VenueHttpClient.h"
#import "GroupHttpClient.h"
#import "IgnightTestAppDelegate.h"
#import "ImageSelectionController.h"

@protocol BuzzViewControllerDelegate;

@interface BuzzViewController : UIViewController<UITableViewDelegate,
                                                 UITableViewDataSource,
                                                 BuzzInputTextViewDelegate,
                                                 BuzzHttpClientDelegate,
                                                 ImageSelectionControllerDelegate,
                                                 UINavigationControllerDelegate,
                                                 UIImagePickerControllerDelegate,
                                                 MessageTableViewCellDelegate,
                                                 UIActionSheetDelegate>

@property (nonatomic, assign) id<BuzzViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *buzzTypeId;
@property BOOL isGroup;

-(void)hideKeyboard;
-(void)getBuzzData;

@end

@protocol BuzzViewControllerDelegate <NSObject>

-(void)clickedImageAtIndex: (NSInteger)row;
-(void)clickedImage: (UIImage *)image;

@end
