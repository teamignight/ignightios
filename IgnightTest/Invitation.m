//
//  Invitation.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/28/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "Invitation.h"

@implementation Invitation

-(Invitation *)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

-(Invitation *)initWithData: (NSDictionary *)data
{
    self = [super init];
    if (self) {
        _groupId = data[@"groupId"];
        _groupName = data[@"groupName"];
        _timestamp = data[@"timestamp"];
        _userName = data[@"userName"];
        _userPicture = data[@"userPicture"];
    }
    return self;
}

@end
