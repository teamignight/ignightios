//
//  VenueHttpClient.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/13/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

@protocol VenueHttpClientDelegate;

@interface VenueHttpClient : AFHTTPSessionManager

@property (nonatomic, weak) id<VenueHttpClientDelegate> delegate;

+(VenueHttpClient *)getInstance;
-(instancetype)initWithBaseURL:(NSURL *)url;
-(void)cancelAllOperations;

-(void)getVenueImages: (NSNumber *)venueId;

-(void)sendTextBuzz: (NSString *)buzz forVenue: (NSNumber *)venueId;
-(void)sendImageBuzz: (NSData *)buzz forVenue: (NSNumber *)venueId;
-(void)getLatestBuzz: (NSNumber *)lastBuzzId forVenue:(NSNumber *)venueId;

-(void)reportVenueDetailsError: (NSDictionary *)venueData forVenue: (NSNumber*)venueId;
-(void)reportVenueAddition: (NSDictionary *)venueData;
-(void)userActivityChangedTo: (NSInteger)userActivity forVenueId: (NSNumber *)venueId;

-(void)getVenueInfo: (NSNumber *)venueId;

-(void)sendVenue:(NSNumber *)venueId dailyEventInfo: (NSString *)eventInfo;

@end



@protocol VenueHttpClientDelegate <NSObject>

@optional

-(void)venueHttpClient: (VenueHttpClient *)client gotVenueImages: (NSDictionary *)response;

-(void)venueHttpClient: (VenueHttpClient *)client postedTextBuzz: (NSDictionary *)response;
-(void)venueHttpClient: (VenueHttpClient *)client postedImageBuzz: (NSDictionary *)response;
-(void)venueHttpClient: (VenueHttpClient *)client gotLatestBuzz: (NSDictionary *)response;
-(void)venueHttpClientPostedVenueErrors: (VenueHttpClient *)client;
-(void)venueHttpClientPostedVenueAdditions: (VenueHttpClient *)client;
-(void)venueHttpClient: (VenueHttpClient *)client userActivityChangedTo: (NSInteger)activity forVenueId: (NSNumber *)venueId withResponse: (NSDictionary *)response;
-(void)venueHttpClient: (VenueHttpClient *)client gotVenueInfo: (NSDictionary *)response;
-(void)venueHttpClientSubmittedVenueEventInfo: (VenueHttpClient *)client;
-(void)venueHttpClient: (VenueHttpClient *)client didFailWithError: (NSError *)error;

@end