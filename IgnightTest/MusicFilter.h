//
//  MusicFilter.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/14/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightColors.h"

@protocol MusicFilterDelegate;

@interface MusicFilter : UICollectionViewController

@property (nonatomic, assign) id<MusicFilterDelegate> delegate;

@end

@protocol MusicFilterDelegate <NSObject>

-(void)musicFilter: (MusicFilter *)musicFilter didSelectMusicType: (NSInteger)musicId;

@end