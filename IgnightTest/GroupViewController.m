//
//  GroupViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "GroupViewController.h"
#import "Invitation.h"

@interface GroupViewController ()

@end

#define GROUPS 0
#define EXPLORE 1
#define INBOX 2

static NSString *noSearchListText = @"You aren't in a group with that name. Check out the Explore tab to find one like it, or click the + on the top right to create it yourself!";
static NSString *noGroupListText = @"You aren't in any groups. Check out the Explore tab to find some!";
static NSString *noExploreGroupListText = @"No groups by that name exist. Click the + on the top right to create it yourself!";

static NSString *groupsActive = @"groups_list_active";
static NSString *groupsInactive = @"groups_list";
static NSString *exploreActive = @"explore_active";
static NSString *exploreInactive = @"explore";
static NSString *mailboxActive = @"group_inbox_active";
static NSString *mailboxInactive = @"group_inbox";

#define LIMIT 20

@implementation GroupViewController
{
    NSMutableArray *currentData;
    
    Groups *groups;
    NSMutableArray *exploreGroups;
    Inbox *inbox;
    
    NSInteger currentTab;
    
    UserHttpClient *userClient;
    GroupHttpClient *groupClient;
    
    IgnightTopBar *topBar;
    
    UIRefreshControl *refreshMe;
    
    /* Background View */
    UIView *bgView;
    UILabel *groupsLabel;
    
    UIImageView *inboxImg;
    UILabel *inboxLabel;
    
    BOOL searchMode;
    NSString *searchString;
    
    BOOL loadedGroups;
    BOOL loadedPopularGroups;
    BOOL loadedMail;
    
    NSInteger offset;
    
    UIActivityIndicatorView *spinner;
    UITextField *searchBarTxtField;
    UIView *searchBarLeftView;
    
    UserNotifications *userNotifications;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    DDLogDebug(@"GroupViewController: viewDidLoad");
    currentData = [[NSMutableArray alloc] init];
    exploreGroups = [[NSMutableArray alloc] init];
    offset = 0;

    loadedGroups = NO;
    loadedPopularGroups = NO;
    loadedMail = NO;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _searchBar.delegate = self;
    
    currentTab = -1;
    
    groupClient = [GroupHttpClient getInstance];
    userClient = [UserHttpClient getInstance];
    groups = [Groups getInstance];
    inbox = [Inbox getInstance];
    
    /*
     *Setup Title Bar
     */
    topBar = [[IgnightTopBar alloc] initWithTitle:@"My Groups" withSideBarButtonTarget:self.revealViewController];
    topBar.delegate = self;
    [_titleBar setUserInteractionEnabled:YES];
    [_titleBar addSubview:topBar];
    
    /*
     * Setup Group View
     */
    _groupMenu.backgroundColor = tabBackground;
    _groupsView.backgroundColor = tabBackground;
    _exploreView.backgroundColor = tabBackground;
    _inboxView.backgroundColor = tabBackground;
    
    _groupBtn.backgroundColor = clearC;
    _exploreBtn.backgroundColor = clearC;
    _inboxBtn.backgroundColor = clearC;
    
    CGColorRef borderColor = tabBorder.CGColor;
    CGFloat borderWidth = 0.3;
    [[_groupBtn layer] setBorderColor:borderColor];
    [[_groupBtn layer] setBorderWidth:borderWidth];
    [[_exploreBtn layer] setBorderColor:borderColor];
    [[_exploreBtn layer] setBorderWidth:borderWidth];
    [[_inboxBtn layer] setBorderColor:borderColor];
    [[_inboxBtn layer] setBorderWidth:borderWidth];
    
    [_groupBtnImg setImage:[UIImage imageNamed:groupsActive]];
    [_exploreBtnImg setImage:[UIImage imageNamed:exploreInactive]];
    [_inboxBtnImg setImage:[UIImage imageNamed:mailboxInactive]];
    /*
     * Setup tableview
     */
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [_tableView setBackgroundColor:[IgnightUtil backgroundGray]];
    _tableView.separatorInset = UIEdgeInsetsZero;
    refreshMe = [[UIRefreshControl alloc] init];
    [refreshMe setTintColor:[IgnightUtil backgroundBlue]];
    [refreshMe addTarget:self action:@selector(refreshGroups) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:refreshMe];
    
    /*
     * Setup search bar
     */
    _searchBar.searchBarStyle = UISearchBarStyleProminent;
    _searchBar.delegate = self;
    _searchBar.tintColor = [UIColor darkGrayColor];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    for (UIView *subview in [[_searchBar.subviews objectAtIndex:0] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            searchBarTxtField = (UITextField *)subview;
            searchBarLeftView = searchBarTxtField.leftView;
        }
    }
    
    /*
     * Setup background view
     */
    bgView = [[UIView alloc] initWithFrame:_tableView.backgroundView.frame];
    
    groupsLabel = [[UILabel alloc] initWithFrame:CGRectMake(
                       (_tableView.frame.size.width - 230) / 2,
                        (_tableView.frame.size.height - 250) / 2, 230, 200)];
    groupsLabel.text = noGroupListText;
    groupsLabel.textColor = [UIColor darkGrayColor];
    groupsLabel.numberOfLines = 8;
    groupsLabel.textAlignment = NSTextAlignmentCenter;
    
    inboxImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-mail"]];
    inboxImg.frame = CGRectMake((_tableView.frame.size.width - 200) / 2,
                                (_tableView.frame.size.height - 250) / 2, 200, 200);
    inboxLabel = [[UILabel alloc] initWithFrame:CGRectMake(
                       (_tableView.frame.size.width - 230) / 2,
                        (inboxImg.frame.origin.y + inboxImg.frame.size.height) - 60,
                                                           230, 100)];
    inboxLabel.text = @"You have no new group invites right now.";
    inboxLabel.textColor = [UIColor darkGrayColor];
    inboxLabel.numberOfLines = 5;
    inboxLabel.textAlignment = NSTextAlignmentCenter;
    
    userNotifications = [UserNotifications getInstance];
    userNotifications.delegate = self;
}

-(void)userNotificationsWasUpdated:(UserNotifications *)userNotification
{
    [self updateNotificationViews];
}

-(void)updateNotificationViews
{
    if (userNotifications.inbox) {
        [_inboxNotificationLine setBackgroundColor:UIColorFromRGB(0x62C012)];
    } else {
        [_inboxNotificationLine setBackgroundColor:[UIColor clearColor]];
    }
    
    if (userNotifications.groupBuzz) {
        [_groupsNotificationLine setBackgroundColor:UIColorFromRGB(0x62C012)];
    } else {
        [_groupsNotificationLine setBackgroundColor:[UIColor clearColor]];
    }
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft) {    //Hide
        userClient.delegate = self;
        if ([_searchBar.text isEqualToString:@""]) {
            _searchBar.showsCancelButton = NO;
        } else {
            [_searchBar becomeFirstResponder];
        }
    } else if (position == 4) { //Bring to front
        [_searchBar resignFirstResponder];
    }
}

- (void)revealController:(SWRevealViewController *)revealController animateToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeft) {
        userClient.delegate = self;
    }
}

-(void)refreshGroups
{
    searchMode = NO;
    [_searchBar resignFirstResponder];
    [_searchBar setText:@""];
    offset = 0;
    if (currentTab == GROUPS) {
        if (!loadedGroups) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            loadedGroups = YES;
        }
        [userClient getUserGroups];
    } else if (currentTab == EXPLORE) {
        if (!loadedPopularGroups) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            loadedPopularGroups = YES;
        }
        [groupClient getPopularGroupsWithOffset:offset WithLimit:LIMIT];
    } else if (currentTab == INBOX) {
        if (!loadedMail) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            loadedMail = YES;
        }
        [groupClient getIncomingRequestsForUser];
    } else {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    [refreshMe endRefreshing];
}

-(void)loadMore
{
    if (currentTab == EXPLORE) {
        if (searchString == nil || [searchString isEqualToString:@""]) {
            [groupClient getPopularGroupsWithOffset:offset WithLimit:LIMIT];
        } else {
            searchBarTxtField.leftView = spinner;
            [spinner startAnimating];
            [groupClient searchForGroups:searchString withOffset:offset WithLimit:LIMIT];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupPushNotificationObserver];
    
    DDLogDebug(@"GroupViewController: viewWillAppear");
    userClient.delegate = self;
    groupClient.delegate = self;
    
    self.revealViewController.delegate = self;
    if (self.revealViewController != nil) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    } else {
        DDLogError(@"Trending: RevealViewController is null");
    }

    [self updateNotificationViews];
    [self goToTabIndex];
    
    //Because of invite contacts nav bar
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void)goToTabIndex
{
    if (_tabIndex <= GROUPS) {
        [self goToGroups:nil];
    } else if (_tabIndex == EXPLORE) {
        [self goToExplore:nil];
    } else if (_tabIndex == INBOX) {
        [self goToInbox:nil];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGRect frame = CGRectMake(0, 0, _titleBar.frame.size.width, _titleBar.frame.size.height);
    _titleBar.frame = frame;
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removePushNotificationObserver];
}

- (IBAction)goToGroups:(id)sender {
    [_tableView reloadData];
    if (currentTab == GROUPS) {
        return;
    }
    [topBar setTitleText:@"My Groups"];
    if (currentTab == INBOX) {
        _searchBar.alpha = 1;
        CGRect sF = _searchBar.frame;
        _tableViewTopConstraint.constant = _tableViewTopConstraint.constant + sF.size.height;
        [_tableView needsUpdateConstraints];
    }
    currentTab = _tabIndex = GROUPS;
    [_groupBtnImg setImage:[UIImage imageNamed:groupsActive]];
    [_exploreBtnImg setImage: [UIImage imageNamed:exploreInactive]];
    [_inboxBtnImg setImage:[UIImage imageNamed:mailboxInactive]];
    _searchBar.placeholder = @"Search Your Groups";
    [_searchBar resignFirstResponder];
    _searchBar.text = @"";
    _searchBar.showsCancelButton = NO;
    searchString = nil;
    currentData = [[groups getAllGroups] copy];
    [self refreshGroups];
    [_tableView reloadData];
    [_groupBtn setUserInteractionEnabled:NO];
    [_exploreBtn setUserInteractionEnabled:YES];
    [_inboxBtn setUserInteractionEnabled:YES];
    [userNotifications updateGroupBuzz:NO];
}

- (IBAction)goToExplore:(id)sender {
    if (currentTab == EXPLORE) {
        return;
    }
    [topBar setTitleText:@"Explore Groups"];
    if (currentTab == INBOX) {
        _searchBar.alpha = 1;
        CGRect sF = _searchBar.frame;
        _tableViewTopConstraint.constant = _tableViewTopConstraint.constant + sF.size.height;
        [_tableView needsUpdateConstraints];
    }
    currentTab = _tabIndex = EXPLORE;
    [_groupBtnImg setImage:[UIImage imageNamed:groupsInactive]];
    [_exploreBtnImg setImage:[UIImage imageNamed:exploreActive]];
    [_inboxBtnImg setImage:[UIImage imageNamed:mailboxInactive]];
    _searchBar.placeholder = @"Search for Groups";
    [_searchBar resignFirstResponder];
    _searchBar.text = @"";
    _searchBar.showsCancelButton = NO;
    searchString = nil;
    currentData = [exploreGroups copy];
    [self refreshGroups];
    [_tableView reloadData];
    [_groupBtn setUserInteractionEnabled:YES];
    [_exploreBtn setUserInteractionEnabled:NO];
    [_inboxBtn setUserInteractionEnabled:YES];
}

- (IBAction)goToInbox:(id)sender {
    if (currentTab == INBOX) {
        return;
    }
    [topBar setTitleText:@"Inbox"];
    _searchBar.alpha = 0;
    CGRect sF = _searchBar.frame;
    searchString = nil;
    _tableViewTopConstraint.constant = _tableViewTopConstraint.constant - sF.size.height;
    [_tableView needsUpdateConstraints];
    
    currentTab = _tabIndex = INBOX;
    [_groupBtnImg setImage:[UIImage imageNamed:groupsInactive]];
    [_exploreBtnImg setImage:[UIImage imageNamed:exploreInactive]];
    [_inboxBtnImg setImage:[UIImage imageNamed:mailboxActive]];
    _searchBar.placeholder = @"Search Your Inbox";
    [_searchBar resignFirstResponder];
    _searchBar.text = @"";
    _searchBar.showsCancelButton = NO;
    currentData = [[inbox getInvitesForGroups] copy];
    [self refreshGroups];
    [_tableView reloadData];
    [_groupBtn setUserInteractionEnabled:YES];
    [_exploreBtn setUserInteractionEnabled:YES];
    [_inboxBtn setUserInteractionEnabled:NO];
    [userNotifications updateInbox:NO];
}

#pragma mark - IgnightTopBarDelegate

/*
 Create a group
 */
-(IBAction)createGroup:(id)sender
{
    CreateAGroupView *createGroupView = [self.storyboard instantiateViewControllerWithIdentifier:@"createGroup"];
    [self.navigationController pushViewController:createGroupView animated:YES];
}

#pragma mark - UserHttpClient

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    searchBarTxtField.leftView = searchBarLeftView;
    currentData = nil;
}

-(void)userHttpClient:(UserHttpClient *)client gotUserGroups:(NSDictionary *)response
{
    @synchronized (currentData) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        BOOL res = [response[@"res"] boolValue];
        if (currentTab != GROUPS) {
            return;
        }
        
        if (res) {
            currentData = nil;
            NSArray *grps = response[@"body"];
            [groups clearAllGroups];
            for (NSDictionary *g in grps) {
                Group *group = [[Group alloc] init];
                group.name = [g objectForKey:@"name"];
                group.groupId = [[g objectForKey:@"groupId"] integerValue];
                group.groupDescription = [g objectForKey:@"description"];
                group.publicGroup = ![[NSNumber numberWithInt:[[g objectForKey:@"private"] intValue]] boolValue];
                group.activity = [[NSNumber numberWithInt:[[g objectForKey:@"newGroupBuzzAvailable"] intValue]] boolValue];
                group.lastBuzzTs = [g objectForKey:@"lastBuzzTs"];
                if (!group.publicGroup) {
                    group.adminId = [g objectForKey:@"adminId"];
                }
                [groups addGroup:group];
                group.members = [[g objectForKey:@"members"] integerValue];
            }
            
            currentData = [[groups getAllGroups] copy];
            [_tableView reloadData];
        }
    }
}

#pragma mark - GroupHttpClientDelegat

-(void)groupHttpClient:(GroupHttpClient *)client didFailWithError:(NSError *)error
{
    [spinner stopAnimating];
    searchBarTxtField.leftView = searchBarLeftView;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    currentData = nil;
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)groupHttpClient:(GroupHttpClient *)client gotIncomingGroupRequests:(NSDictionary *)response
{
    @synchronized (currentData) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (currentTab != INBOX) {
            return;
        }
        
        BOOL res = [response[@"res"] boolValue];
        if (res) {
            currentData = nil;
            DDLogDebug(@"Got Response: %@", response);
            NSArray *body = [response objectForKey:@"body"];
            [inbox gotNewGroupRequests:body];
            currentData = [inbox getInvitesForGroups];
        } else {

        }
        [_tableView reloadData];
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client gotGroupSearchResults:(NSDictionary *)response withOffset:(NSInteger)dataOffset
{
    [spinner stopAnimating];
    searchBarTxtField.leftView = searchBarLeftView;
    @synchronized (currentData) {
        if (currentTab != EXPLORE) {
            return;
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        BOOL res = [response[@"res"] boolValue];
        if (res) {
            NSArray *body = [response objectForKey:@"body"];
            NSMutableArray *popularGroups = [[NSMutableArray alloc] initWithCapacity:body.count];
            for (NSDictionary *data in body) {
                PopularGroups *pg = [[PopularGroups alloc] initWithSearchDictionary:data];
                [popularGroups addObject:pg];
            }
            if (dataOffset == 0) {
                currentData = popularGroups;
            } else {
                [currentData addObjectsFromArray:popularGroups];
            }
        }
        [_tableView reloadData];
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client gotPopularGroups:(NSDictionary *)response withOffset:(NSInteger)dataOffset
{
    @synchronized (currentData) {
        if (currentTab != EXPLORE) {
            return;
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        BOOL res = [response[@"res"] boolValue];
        if (res) {
            NSArray *body = [response objectForKey:@"body"];
            NSMutableArray *popularGroups = [[NSMutableArray alloc] initWithCapacity:body.count];
            for (NSDictionary *data in body) {
                PopularGroups *pg = [[PopularGroups alloc] initWithDictionary:data];
                [popularGroups addObject:pg];
            }
            if (dataOffset == 0) {
                exploreGroups = popularGroups;
                currentData = [exploreGroups copy];
            } else {
                currentData = [currentData mutableCopy];
                [currentData addObjectsFromArray:[popularGroups copy]];
            }
        } else {
    //        IgAlert(nil, response[@"reason"], nil);
        }
        [_tableView reloadData];
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client groupInviteConfirmedForGroup:(NSNumber *)groupId withResponse:(NSDictionary *)response
{
    @synchronized (currentData) {
        BOOL res = [response[@"res"] boolValue];
        if (res) {
            NSDictionary *msg = response[@"body"];
            Group *acceptedGroup = [[Group alloc] init];
            acceptedGroup.name = [msg objectForKey:@"name"];
            acceptedGroup.publicGroup = ![[msg objectForKey:@"private"] boolValue];
            acceptedGroup.groupId = groupId.integerValue;
            [groups addGroup:acceptedGroup];
            [inbox deleteInviteFromGroup:[msg objectForKey:@"groupId"]];
            
            if (currentTab == INBOX) {
                currentData = [[inbox getInvitesForGroups] copy];
                [_tableView reloadData];
            }
        }
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client groupRejectedConfirmedForGroup:(NSNumber *)groupId withResponse:(NSDictionary *)response
{
    @synchronized (currentData) {
        BOOL res = [response[@"res"] boolValue];
        if (res) {
            NSDictionary *msg = response[@"body"];
            [inbox deleteInviteFromGroup:[msg objectForKey:@"groupId"]];
            if (currentTab == INBOX) {
                currentData = [[inbox getInvitesForGroups] copy];
                [_tableView reloadData];
            }
        }
    }
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (currentTab == GROUPS || currentTab == EXPLORE) {
        if (indexPath.section == 0) {
            return 30;
        }
    }
    if (currentTab == GROUPS) {
        return [GroupCell getCellHeight];
    }
    if (currentTab == EXPLORE) {
        return [GroupCell getCellHeight];
    }
    if (currentTab == INBOX) {
        
    }
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (currentTab == GROUPS) {
        Group *group = [currentData objectAtIndex:indexPath.row];
        Trending *trendingView = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
        trendingView.groupId = [NSNumber numberWithInteger:group.groupId];
        trendingView.groupMode = YES;
        trendingView.tabIndex = 0;
        trendingView.goBack = YES;
        trendingView.groupName = group.name;
        [self.navigationController pushViewController:trendingView animated:YES];
    }
    if (currentTab == EXPLORE) {
        PopularGroups *group = [currentData objectAtIndex:indexPath.row];
        if ([groups isUserMemberOfGroup:group.groupId]) {
            Trending *trendingView = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
            trendingView.groupId = group.groupId;
            trendingView.groupMode = YES;
            trendingView.tabIndex = 0;
            trendingView.goBack = YES;
            trendingView.groupName = group.name;
            [self.navigationController pushViewController:trendingView animated:YES];
        } else {
            GroupDetailsViewController *groupDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"groupDetails"];
            groupDetails.groupId = group.groupId;
            groupDetails.allowToJoin = YES;
            groupDetails.dontGoBack = NO;
            groupDetails.acceptInvite = NO;
            [self.navigationController pushViewController:groupDetails animated:YES];
        }
    }
    if (currentTab == INBOX) {
        Invitation *invite = [currentData objectAtIndex:indexPath.row];
        GroupDetailsViewController *groupDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"groupDetails"];
        groupDetails.groupId = invite.groupId;
        groupDetails.allowToJoin = YES;
        groupDetails.dontGoBack = NO;
        groupDetails.acceptInvite = YES;
        [self.navigationController pushViewController:groupDetails animated:YES];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (currentTab == GROUPS || currentTab == EXPLORE) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (currentTab == GROUPS || currentTab == EXPLORE) {
        if (section == 0) {
            return 1;
        }
    }
    NSInteger count = [currentData count];
    if (count == 0) {
        [self showEmptyView];
        _tableView.backgroundView = bgView;
    } else {
        _tableView.backgroundView = nil;
    }
    return count;
}

-(void)showEmptyView
{
    [groupsLabel removeFromSuperview];
    [inboxImg removeFromSuperview];
    [inboxLabel removeFromSuperview];
    if (currentTab == GROUPS) {
        [bgView addSubview:groupsLabel];
        if ([_searchBar.text isEqualToString:@""]) {
            groupsLabel.text = noGroupListText;
        } else {
            groupsLabel.text = noSearchListText;
        }
    }
    if (currentTab == EXPLORE) {
        if (![_searchBar.text isEqualToString:@""]) {
            [bgView addSubview:groupsLabel];
            groupsLabel.text = noExploreGroupListText;
        }
    }
    if (currentTab == INBOX) {
        [bgView addSubview:inboxImg];
        [bgView addSubview:inboxLabel];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (currentTab == GROUPS || currentTab == EXPLORE) {
        if (indexPath.section == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreateGroup"];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CreateGroup"];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                UIButton *createGroup = [[UIButton alloc] initWithFrame:cell.contentView.frame];
                [createGroup setTitle:@"Create Group" forState:UIControlStateNormal];
                [createGroup setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [createGroup setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
                [createGroup setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
                [createGroup setBackgroundColor:grayHeaderFooterText];
                [cell.contentView addSubview:createGroup];
                [createGroup addTarget:self action:@selector(createGroup:) forControlEvents:UIControlEventTouchUpInside];
            }
            return cell;
        }
    }
    
    UITableViewCell *cell;
    NSInteger currentDataSize = [currentData count];
    NSInteger currentRow = indexPath.row;
    if ((currentTab == EXPLORE) &&      //Only Do this for explore tab
        (offset < currentDataSize) &&   //offset cant be greater then data size
        (currentRow > (currentDataSize - 5)) && //Only do it just (~5) before you reach end
        (currentDataSize >= LIMIT) &&
        (currentRow <= currentDataSize))
    {
        offset = currentDataSize;
        [self loadMore];
    }
    
    if (currentTab == GROUPS) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell"];
        if (cell == nil) {
            cell = [[GroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"groupCell"];
        }
        GroupCell *groupCell = (GroupCell *)cell;
        groupCell.delegate = self;
        Group *group = [currentData objectAtIndex:indexPath.row];
        [groupCell setGroupId: [NSNumber numberWithInteger:group.groupId]];
        [groupCell setGroupName:group.name withCount:group.members];
        [groupCell setGroupType:group.publicGroup ? @"public" : @"private"];
        [groupCell turnBuzzOn:group.activity];
        [groupCell setLastBuzzTs:group.lastBuzzTs];
    } else if (currentTab == EXPLORE) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell"];
        if (cell == nil) {
            cell = [[GroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"groupCell"];
        }
        GroupCell *groupCell = (GroupCell *)cell;
        PopularGroups *group = [currentData objectAtIndex:indexPath.row];
        [groupCell setGroupName:group.name withCount:group.memberCount.integerValue];
        [groupCell setGroupType:@"public"];
        [groupCell hideBuzzIndicator];
        [groupCell showReportButton];
    } else if (currentTab == INBOX) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"mail"];
        if (cell == nil) {
            cell = [[MailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"mail"];
            ((MailCell *)cell).delegate = self;
        }
        Invitation *invite = [currentData objectAtIndex:indexPath.row];
        MailCell *mailCell = (MailCell *)cell;
        [mailCell reset];
        [mailCell setGroupId:invite.groupId];
        [mailCell setGroupName:invite.groupName];
        [mailCell setRequestFromUser:invite.userName];
        [mailCell setTimestamp:invite.timestamp];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:invite.userPicture]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFImageResponseSerializer serializer];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [mailCell setInviteeImage:responseObject];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DDLogDebug(@"Failed To Retrieve Image From : %@ with error: %@", invite.userPicture, error);
        }];
        [operation start];
    }
    return cell;
}

-(void)goToBuzz:(NSNumber *)groupId
{
    Group *group = [groups getGroupForId:groupId];
    group.activity = NO;
    Trending *trendingView = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
    trendingView.groupId = [NSNumber numberWithInteger:group.groupId];
    trendingView.groupMode = YES;
    trendingView.tabIndex = 1;
    trendingView.goBack = YES;
    trendingView.groupName = group.name;
    [self.navigationController pushViewController:trendingView animated:YES];
    [_tableView reloadData];
}

-(void)reportGroup:(NSNumber *)groupId
{
    [groupClient flagGroup:groupId];
}

//Removes the empty cells
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] init];
}

#pragma mark - MailCellDelegate

-(void)acceptInviteForGroup:(NSNumber *)groupId
{
    DDLogDebug(@"Accepted Invite For Group: %@", groupId);
    [groupClient respondToGroup:groupId withResponse:YES];
}

-(void)rejectInviteForGroup:(NSNumber *)groupId
{
    DDLogDebug(@"Rejected Invite For Group: %@", groupId);
    [groupClient respondToGroup:groupId withResponse:NO];
}

#pragma mark - UISearchBarDelegate


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar   // return NO to not becom first responder
{
    _searchBar.showsCancelButton = YES;
    return YES;
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0) // called before text changes
{
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar   // called when keyboard search button pressed
{
    NSString *searchText = searchBar.text;
    if ([searchText isEqualToString:@""]) {
        return;
    }
    
    [self filterCurrentDataWithString:searchText];
    [_tableView reloadData];
    
    [_searchBar resignFirstResponder];
}

-(void)filterCurrentDataWithString: (NSString *)search
{
    offset = 0;
    searchString = [search lowercaseString];
    NSMutableArray *data = [[NSMutableArray alloc] init];
    if (currentTab == GROUPS) {
        for (Group *g in [groups getAllGroups]) {
            NSString *name = [g.name lowercaseString];
            if ([name rangeOfString:searchString].location != NSNotFound) {
                [data addObject:g];
            }
        }
    } else if (currentTab == EXPLORE) {
        searchBarTxtField.leftView = spinner;
        [spinner startAnimating];
        [groupClient searchForGroups:searchString withOffset:offset WithLimit:LIMIT];
    } else if (currentTab == INBOX) {
        
    }
    currentData = data;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar   // called when cancel button pressed
{
    searchString = nil;
    searchMode = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    _searchBar.showsCancelButton = NO;
    if (currentTab == GROUPS) {
        currentData = [[groups getAllGroups] copy];
    } else if (currentTab == EXPLORE) {
        currentData = exploreGroups;
    }
    [_tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    DDLogError(@"GroupViewController : Received memory warning!");
}

#pragma mark - Remote Notifications

-(void)setupPushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewMailPush:) name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewGroupBuzz:) name:@"NewGroupBuzzNotification" object:nil];
}

-(void)removePushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewGroupBuzzNotification" object:nil];
}

-(void)handleNewMailPush: (NSNotification *)notification
{
    [self goToInbox:nil];
    [self refreshGroups];
}

-(void)handleNewGroupBuzz: (NSNotification *)notification
{
    NSDictionary *data = notification.userInfo;
    Trending *trending = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
    trending.goBack = YES;
    trending.groupMode = YES;
    trending.groupName = data[@"groupName"];
    trending.groupId = data[@"groupId"];
    trending.tabIndex = 1;
    [self.navigationController pushViewController:trending animated:YES];
}


@end
