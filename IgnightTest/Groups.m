//
//  Groups.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/1/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "Groups.h"
#import "Group.h"

@implementation Groups
{
    NSMutableArray *groups;
    NSMutableDictionary *allGroups;
}

+ (Groups *)getInstance
{
    static Groups *instance = nil;
    @synchronized(self) {
        if (!instance) {
            instance = [[Groups alloc] init];
        }
    }
    return instance;
}

- (Groups *)init
{
    self = [super init];
    if (self) {
        groups = [[NSMutableArray alloc] init];
        allGroups = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(NSInteger)count
{
    return [allGroups count];
}


-(void)addGroup: (Group *)group
{
    if ([allGroups objectForKey:[NSString stringWithFormat:@"%ld", group.groupId]] == nil) {
        [groups addObject:group];
        [allGroups setObject:group forKey:[NSString stringWithFormat:@"%ld",group.groupId]];
    }
}

-(void)clearAllGroups
{
    [groups removeAllObjects];
    [allGroups removeAllObjects];
}

-(void)removeGroup: (Group *)group
{
    Group *removeGroup;
    for (Group *g in groups) {
        if (g.groupId == group.groupId) {
            removeGroup = g;
        }
    }
    [groups removeObject:removeGroup];
    [allGroups removeObjectForKey:[NSString stringWithFormat:@"%ld", group.groupId]];
}

-(BOOL)isUserMemberOfGroup:(NSNumber *)groupId
{
    return [allGroups objectForKey:[groupId stringValue]] != nil;
}

-(NSMutableArray *)getAllGroups
{
    return [groups mutableCopy];
}

-(NSMutableArray *)getAllGroupsAlphabatezied
{
    groups = [[groups sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Group *first = (Group*)obj1;
        Group *second = (Group*)obj2;
        return [first compare:second];
    }] mutableCopy];
    
    return groups;
}

- (Group*)getGroupForId: (NSNumber *)groupId {
    return [allGroups objectForKey:[groupId stringValue]];
}

@end
