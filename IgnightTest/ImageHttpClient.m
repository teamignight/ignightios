//
//  ImageHttpClient.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 6/6/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "ImageHttpClient.h"

@implementation ImageHttpClient
{
    UserInfo *userInfo;
}

+(ImageHttpClient *)getInstance
{
    static ImageHttpClient *_sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *url = server_url;
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:url]];
        [_sharedInstance.requestSerializer setValue:[IgnightProperties getHeaderToken] forHTTPHeaderField:@"X-IGNIGHT-TOKEN"];
        [_sharedInstance.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"Platform"];
        UserInfo* userInfo = [UserInfo getInstance];
        if (userInfo.userId != nil) {
            [_sharedInstance.requestSerializer setValue:userInfo.userId.stringValue forHTTPHeaderField:@"USER_ID"];
        }
        if (TARGET_IPHONE_SIMULATOR) {
        } else {
            _sharedInstance.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
            _sharedInstance.securityPolicy.allowInvalidCertificates = YES;
        }
    });
    return _sharedInstance;
}

-(instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        userInfo = [UserInfo getInstance];
    }
    
    return self;
}

-(void)getVenueImages: (NSNumber *)venueId
{
    NSDictionary *parameters = @{@"buzzImageStartId" : [NSNumber numberWithInt:-1],
                                 @"noOfBuzzImages" : [NSNumber numberWithInt:51]};
    
    NSString *path = [NSString stringWithFormat:@"images/venue/%@", venueId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate imageHttpClient:self gotGroupImages:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate imageHttpClient:self didFailWithError:error];
    }];
}

-(void)getGroupImages: (NSNumber *)groupId
{
    NSDictionary *parameters = @{@"buzzImageStartId" : [NSNumber numberWithInt:-1],
                                 @"noOfBuzzImages" : [NSNumber numberWithInt:51]};
    
    NSString *path = [NSString stringWithFormat:@"images/group/%@", groupId];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate imageHttpClient:self gotGroupImages:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate imageHttpClient:self didFailWithError:error];
    }];
}



@end
