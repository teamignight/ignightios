//
//  GroupDetailsViewController.m
//  IgnightTest
//
//  Created by Rob Chipman on 9/7/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "GroupDetailsViewController.h"
#import "IgnightTopBar.h"
#import "SWRevealViewController.h"
#import "Group.h"
#import "Groups.h"
#import "SideBarViewController.h"
#import "MusicTypes.h"
#import "AtmosphereTypes.h"
#import "SpendingTypes.h"
#import "AgeBuckets.h"
#import "SBGroupInfo.h"

@interface GroupDetailsViewController ()

@end

@implementation GroupDetailsViewController
{
    IgnightTopBar *titleBar;
    
    UserInfo *userInfo;
    GroupHttpClient *groupClient;
    Group *group;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    userInfo = [UserInfo getInstance];
    groupClient = [GroupHttpClient getInstance];
    
    [self.backgroundView setBackgroundColor:[IgnightUtil backgroundGray]];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [_btnJoinLeave setBackgroundColor:reportSubmitButtonBackground];
    [_btnJoinLeave setTitleColor:reportSubmitButtonText forState:UIControlStateNormal];
    [_btnJoinLeave setAlpha:0];
    DDLogDebug(@"Group Details View Controller: Allow To Join: %@", _allowToJoin ? @"YES" : @"NO");
    
    _backgroundView.backgroundColor = UIColorFromRGB(0xf2f2f2);
    _subscriptionLbl.textColor = venueListText;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    groupClient.delegate = self;
    [self getGroupInfo];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

#pragma mark - IgnightTopBarDelegate

- (void)revealToggle:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)joinGroup:(id) sender {
    if (group.isMember) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"IgNight" message:@"Are you sure you want to leave this group?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
        [alertView show];
    } else {
        if (_acceptInvite) {
            [groupClient respondToGroup:_groupId withResponse:YES];
        } else {
            [groupClient joinGroup:_groupId];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [groupClient leaveGroup:_groupId];
    }
}

- (IBAction)toggleSubscription:(id)sender {
    UISwitch *pushSwitch = (UISwitch *)sender;
    DDLogDebug(@"SWITCH: %@", pushSwitch.on ? @"YES" : @"NO");
    if (pushSwitch.on) {
        [groupClient registerForGroupPush:_groupId];
    } else {
        [groupClient unregisterForGroupPush:_groupId];
    }
}

- (void)getGroupInfo
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [groupClient getGroupInfo:_groupId];
}

#pragma mark - GroupHttpClientDelegate

-(void)groupHttpClient:(GroupHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)groupHttpClient:(GroupHttpClient *)client registeredForPushWithResponse:(NSDictionary *)response
{
    BOOL res = [response[@"res"] boolValue];
    if (!res) {
        _subscribtionSwitch.on = NO;
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client unregisteredForPushWithResponse:(NSDictionary *)response
{
    BOOL res = [response[@"res"] boolValue];
    if (!res) {
        _subscribtionSwitch.on = YES;
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client gotGroupInfo:(NSDictionary *)response
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSDictionary *info = response[@"body"];
        if (info != nil) {
            NSNumber *grpId = info[@"id"];
            if ([_groupId isEqualToNumber:grpId]) {
                group = [[Group alloc] init];
                group.groupId = _groupId.integerValue;
                group.name = info[@"name"];
                group.publicGroup = [info[@"public"] boolValue];
                group.groupDescription = info[@"description"];
                group.members = [info[@"membersCount"] integerValue];
                group.isMember = [info[@"isMember"] boolValue];
                
                DDLogDebug(@"Group Details View Controller: Public: %@", group.publicGroup ? @"YES" : @"NO");
                if (group.isMember) {
                    if (!group.publicGroup) {
                        _subscribtionSwitch.alpha = 1;
                        _subscriptionLbl.alpha = 1;
                    }
                    [_btnJoinLeave setTitle:@"Leave Group" forState:UIControlStateNormal];
                    [_btnJoinLeave setAlpha:1];
                } else {
                    _subscribtionSwitch.alpha = 0;
                    _subscriptionLbl.alpha = 0;
                    if (group.publicGroup || _allowToJoin) {
                        DDLogDebug(@"Chnaging Alpha");
                        [_btnJoinLeave setTitle:@"Join Group" forState:UIControlStateNormal];
                        [_btnJoinLeave setAlpha:1];
                    }
                }
                
                DDLogDebug(@"GOT DATA: %@", info);
                
                titleBar = [[IgnightTopBar alloc] initWithTitle:group.name withSideBarButtonTarget:_dontGoBack ? self.revealViewController : self];
                if (!_dontGoBack) {
                    [titleBar setLeftButtonBackgroundImage:[UIImage imageNamed:@"back_gray"]];
                }
                [self.topBar addSubview:titleBar];
                [self.topBar setUserInteractionEnabled:YES];
                
                NSInteger mId = [info[@"musicTypeMode"] integerValue];
                NSString *mName = [MusicTypes getMusicNameFromId:(Music)mId];
                _musicLbl.text = mName;
                mName = [self replaceCharacters:mName];
                _musicImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"detail_music_%@", mName]];
                
                NSInteger aId = [info[@"atmosphereTypeMode"] integerValue];
                NSString *aName = [AtmosphereTypes getAtmosphereNameFromId:(Atmosphere)aId];
                _atmosphereLbl.text = aName;
                aName = [self replaceCharacters:aName];
                _atmosphereImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"detail_atmosphere_%@", aName]];
                
                NSInteger sId = [info[@"spendingLimitMode"] integerValue];
                NSString *sName = [SpendingTypes getSpendingNameFromId:(Spending)sId];
                _spendingLbl.text = sName;
                _spendingImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"detail_spending_%ld", sId + 1]];
                
                NSInteger ageId = [info[@"ageBucketMode"] integerValue];
                _ageLbl.text = [AgeBuckets getAgeBucketNameFromId:(AgeBucket)ageId];
                _ageImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"detail_age_%ld", ageId + 1]];
                
                _subscribtionSwitch.on = [info[@"subscribed"] boolValue];
                
                [_lblGroupType setText:group.publicGroup ? @"Public Group" : @"Private Group"];
                [_tvGroupDescription setText:group.groupDescription];
                [_lblGroupMemberCount setText:[NSString stringWithFormat:@"%ld Member%@", (long)group.members, (group.members > 1) ? @"s" : @""]];
            }
        } else {
//            IgAlert(nil, response[@"reason"], nil);
        }
    } else {
//        IgAlert(nil, @"Failed To Get Group Information", nil);
    }
}

-(NSString *)replaceCharacters: (NSString *)name
{
    name = [name lowercaseString];
    name = [name stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    name = [name stringByReplacingOccurrencesOfString:@"&" withString:@"n"];
    return name;
}

-(void)groupHttpClient:(GroupHttpClient *)client joinedGroupWithResponse:(NSDictionary *)response
{
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        SBGroupInfo *info = [[SBGroupInfo alloc] init];
        info.groupId = _groupId;
        info.groupName = group.name;
        info.publicGroup = group.publicGroup;
        [[Groups getInstance] addGroup:group];
        [self.revealViewController.rearViewController performSegueWithIdentifier:@"groups" sender:info];
    } else {
//        IgAlert(nil, @"Failed To Join Group", nil);
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client groupInviteConfirmedForGroup:(NSNumber *)groupId withResponse:(NSDictionary *)response
{
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        SBGroupInfo *info = [[SBGroupInfo alloc] init];
        info.groupId = _groupId;
        info.groupName = group.name;
        info.publicGroup = group.publicGroup;
        [[Groups getInstance] addGroup:group];
        [self.revealViewController.rearViewController performSegueWithIdentifier:@"groups" sender:info];
    } else {
//        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client leftGroup:(NSNumber *)groupId withResponse:(NSDictionary *)response
{
    DDLogDebug(@"Gor Response: %@", response);
    
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSDictionary *msg = response[@"body"];
        [[Inbox getInstance] deleteInviteFromGroup:[msg objectForKey:@"groupId"]];
        [self.revealViewController.rearViewController performSegueWithIdentifier:@"groupView" sender:nil];
    } else {
//        IgAlert(nil, response[@"reason"], nil);
    }
}

#pragma - GroupHttpClientDelegate

@end
