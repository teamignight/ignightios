//
//  IgnightProperties.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 11/19/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IgnightProperties : NSObject

+(NSString *)getHeaderToken;
+(float)getImageScalingValue;

@end
