//
//  InviteViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/15/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "InviteViewController.h"

#define CELL_HEIGHT 50

@interface InviteViewController ()

@end

@implementation InviteViewController
{
    UserHttpClient *userClient;
    GroupHttpClient *groupClient;
    UserInfo *userInfo;
    
    NSTimer *searchTimer;
    
    NSMutableArray *users;
    NSMutableArray *invitedUsers;
    NSMutableDictionary *idToImage;
    
    UISearchBar *searchBar;
    UISearchDisplayController *searchDisplayController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initVariables];
    [self setupSearchBar];
    [self setupSearchDisplayController];
    
    invitedUsers = [[NSMutableArray alloc] init];
    idToImage = [[NSMutableDictionary alloc] init];
}

- (void)initVariables
{
    userClient = [UserHttpClient getInstance];
    [userClient setDelegate:self];
    groupClient = [GroupHttpClient getInstance];
    [groupClient setDelegate:self];
    userInfo = [UserInfo getInstance];
}

-(void)setupSearchBar
{
    CGRect frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 10);
    searchBar = [[UISearchBar alloc] initWithFrame:frame];
    searchBar.delegate = self;
    [self.view addSubview:searchBar];
}

-(void)setupSearchDisplayController
{
    searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    [searchDisplayController setValue:[NSNumber numberWithInt:UITableViewStylePlain]
                             forKey:@"_searchResultsTableViewStyle"];
    searchDisplayController.delegate = self;
    searchDisplayController.searchResultsDataSource = self;
    searchDisplayController.searchResultsDelegate = self;
    searchDisplayController.searchResultsTableView.delegate = self;
    searchDisplayController.searchResultsTableView.separatorInset = UIEdgeInsetsZero;
    [searchDisplayController.searchResultsTableView setAlwaysBounceHorizontal:NO];
    [searchDisplayController.searchResultsTableView flashScrollIndicators];
}

-(void)userHttpClient:(UserHttpClient *)client returnedUsersForSearch:(NSDictionary *)response
{
    BOOL res = [[response objectForKey:@"res"] boolValue];
    if (res) {
        NSArray *body = [response objectForKey:@"body"];
        NSLog(@"GOT BODY: %@", body);
        users = [[NSMutableArray alloc] init];
        int i = 0;
        for (NSDictionary *user in body) {
            if (![[user objectForKey:@"userId"] isEqualToNumber:userInfo.userId]) {
                SearchedUser *searchedUser = [[SearchedUser alloc] initWithParams:user];
                [users addObject:searchedUser];
                
                if (![[idToImage allKeys] containsObject:searchedUser.userId]) {
                    [idToImage setObject:[UIImage imageNamed:@"profile-default-user.png"] forKey:searchedUser.userId];
                    
                    NSURL *url = [NSURL URLWithString:searchedUser.profilePictureURL];
                    NSURLRequest *request = [NSURLRequest requestWithURL:url];
                    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                    operation.responseSerializer = [AFImageResponseSerializer serializer];
                    
                    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSLog(@"Got Image For User: %@", searchedUser.userName);
                        [idToImage setObject:responseObject forKey:searchedUser.userId];
                        [searchDisplayController.searchResultsTableView reloadData];
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        NSLog(@"Failed To Donwload Image From: %@", searchedUser.profilePictureURL);
                    }];
                    [operation start];
                }
            }
            i++;
        }
        [searchDisplayController.searchResultsTableView reloadData];
    } else {
        NSLog(@"Failed To Return Any Users");
    }
}

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cant Connect To Server" message:[error description] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - SearchDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [searchTimer invalidate];
    searchTimer = nil;
    searchTimer = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(doDelayedSearch:) userInfo:searchText repeats:NO];
}

- (void)doDelayedSearch: (NSTimer *)t
{
    assert(t == searchTimer);
//    [userClient searchForUsers:t.userInfo];
}

#pragma mark UISearchDisplayDelegate

// called when table is shown/hidden
- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didHideSearchResultsTableView:(UITableView *)tableView
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void) keyboardWillHide {
    
    UITableView *tableView = [searchDisplayController searchResultsTableView];
    
    [tableView setContentInset:UIEdgeInsetsZero];
    
    [tableView setScrollIndicatorInsets:UIEdgeInsetsZero];
    
}

#pragma mark SearchDelegate

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    SearchedUser *user = [users objectAtIndex:index];
    return [self tableView:tableView createUserCellWithName:user.userName withUserId:user.userId];
}

-(UITableViewCell *)tableView:(UITableView *)tableView createUserCellWithName: (NSString *)name withUserId: (NSNumber *)userId
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"users"];
    if (cell == nil) {
        cell  = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"users"];
        CGRect userImageFrame = CGRectMake(5, 5, CELL_HEIGHT - 10, CELL_HEIGHT - 10);
        UIImageView *userImage = [[UIImageView alloc] initWithFrame:userImageFrame];
        [[userImage layer] setCornerRadius:(CELL_HEIGHT - 10)/ 2];
        [[userImage layer] setMasksToBounds:YES];
        userImage.tag = 200;
        [cell.contentView addSubview:userImage];
        
        CGRect userNameFrame = CGRectMake(55, 5, 205, 40);
        UILabel *userName = [[UILabel alloc] initWithFrame:userNameFrame];
        userName.tag = 201;
        [cell.contentView addSubview:userName];
        
        UIButton *invite = [UIButton buttonWithType:UIButtonTypeCustom];
        invite.frame = CGRectMake(320 - CELL_HEIGHT, 0, CELL_HEIGHT, CELL_HEIGHT);
        invite.tag = 202;
        [invite addTarget:self action:@selector(inviteUser:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:invite];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    UIImageView *userImage = (UIImageView *)[cell viewWithTag:200];
    [userImage setImage:[idToImage objectForKey:userId]];
    
    UILabel *userName = (UILabel *)[cell viewWithTag:201];
    userName.text = name;
    
    UIButton *invite = (UIButton *)[cell viewWithTag:202];
    [invite setUserInteractionEnabled:YES];
    
    UIImage *image = [UIImage imageNamed:@"invite_plus_black"];
    [invite setImage:image forState:UIControlStateNormal];
    [invite setImage:image forState:UIControlStateSelected];
    [invite setImage:image forState:UIControlStateHighlighted];
    
    return cell;
}

-(IBAction)inviteUser:(id)sender
{
    @synchronized(invitedUsers) {
        if ([invitedUsers count] > 0) {
            return;
        }
    }
    
    UIButton *inviteButton = (UIButton *)sender;
    NSIndexPath *indexPath = [searchDisplayController.searchResultsTableView indexPathForCell:(UITableViewCell *)[[[inviteButton superview] superview] superview]];
    
    SearchedUser *user = [users objectAtIndex:indexPath.row];
    NSNumber *targetUserId = user.userId;
    
    [invitedUsers addObject:[NSNumber numberWithInt:(int)indexPath.row]];
    [groupClient inviteUser:targetUserId toGroup:self.groupId];
    
    [inviteButton setUserInteractionEnabled:NO];
    [inviteButton setImage:[UIImage imageNamed:@"invite_plus_gray"] forState:UIControlStateNormal];
    [inviteButton setImage:[UIImage imageNamed:@"invite_plus_gray"] forState:UIControlStateHighlighted];
    [inviteButton setImage:[UIImage imageNamed:@"invite_plus_gray"] forState:UIControlStateSelected];
}

-(void)groupHttpClient:(GroupHttpClient *)client successfullyInvitedUser:(NSDictionary *)response
{
    @synchronized(invitedUsers) {
        if ([invitedUsers count] > 0) {
            NSInteger index = [[invitedUsers objectAtIndex:0] intValue];
            
            [users removeObjectAtIndex:index];
            [invitedUsers removeObjectAtIndex:0];
            
            [searchDisplayController.searchResultsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

-(void)groupHttpClient:(GroupHttpClient *)client gotLatestBuzz:(NSDictionary *)response
{
    NSLog(@"InviteView Controller: GOT LATEST BUZZ");
}

-(void)groupHttpClient:(GroupHttpClient *)client didFailWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cant Connect To Server" message:[error description] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

#pragma mark TableViewDelegates -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
