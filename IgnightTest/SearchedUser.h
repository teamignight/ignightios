//
//  InvitedUser.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/13/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchedUser : NSObject

typedef enum {
    NON_MEMBER,
    PENDING_MEMBER,
    MEMBER
} GroupStatus;

@property (nonatomic, copy) NSNumber *userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *profilePictureURL;
@property (nonatomic, copy) NSString *chatPictureURL;
@property (nonatomic) GroupStatus groupStatus;

-(id)initWithParams:(NSDictionary *)params;

@end
