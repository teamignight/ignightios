//
//  Inbox.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/16/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Inbox : NSObject

+ (Inbox*)getInstance;

-(NSMutableArray *)getInvitesForGroups;

-(void)gotNewGroupRequests: (NSArray *)requests;

-(void)deleteInviteFromGroup: (NSNumber *)groupId;

-(void)clear;

-(NSInteger)count;

@end
