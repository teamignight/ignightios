//
//  DNAStepOne.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/29/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Cities.h"
#import "AgeBuckets.h"
#import "Gender.h"

@interface DNAStepOne : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *continueBtn;
@property (strong, nonatomic) IBOutlet UILabel *headerLabel;


- (IBAction)goBack:(id)sender;

@end
