//
//  MailCell.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/20/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSDate+TimeAgo.h"

@protocol MailCellDelegate <NSObject>

@required

-(void)acceptInviteForGroup: (NSNumber *)groupId;
-(void)rejectInviteForGroup: (NSNumber *)groupId;

@end

@interface MailCell : UITableViewCell

@property (strong, nonatomic) id <MailCellDelegate> delegate;
@property (nonatomic, weak) NSNumber *groupId;

+(NSInteger)getCellHeight;
-(void)reset;
-(void)setInviteeImage: (UIImage *)image;
-(void)setGroupName: (NSString *)name;
-(void)setRequestFromUser: (NSString *)username;
-(void)setTimestamp: (NSNumber *)ts;

@end
