//
//  DNAStepTwo.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/30/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IgnightColors.h"

@interface DNAStepTwo : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIButton *continueBtn;

- (IBAction)goBack:(id)sender;


@end
