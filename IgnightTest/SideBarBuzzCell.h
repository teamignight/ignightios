//
//  SideBarBuzzCell.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/24/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SideBarBuzzCellDelegate;

@interface SideBarBuzzCell : UITableViewCell

@property (strong, nonatomic) id<SideBarBuzzCellDelegate> delegate;

@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIButton *activityIndicatorButton;
@property (nonatomic, strong) UIView *separatorTop;
@property (nonatomic, strong) UIView *separatorBottom;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, assign) BOOL group;
@property (nonatomic, strong) NSNumber* buzzId;

-(void)setTitle: (NSString *)title;
-(void)setImageForIcon: (UIImage *)image;
-(void)setImageForActivityIndicatorButton: (UIImage *)image;
-(void)showSeparatorBottom;
-(void)hideSeparatorBottom;

@end


@protocol SideBarBuzzCellDelegate <NSObject>

-(void)goToVenueBuzz: (NSNumber *)venueId;
-(void)goToGroupBuzz: (NSNumber *)groupId;
-(void)deleteBuzzCell: (SideBarBuzzCell *)sideBarCell;


@end
