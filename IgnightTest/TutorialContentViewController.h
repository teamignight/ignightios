//
//  TutorialContentViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 8/12/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface TutorialContentViewController : UIViewController<SWRevealViewControllerDelegate>

@property NSString *imageFile;
@property NSInteger index;

@end
