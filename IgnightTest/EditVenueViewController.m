//
//  EditVenueViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/30/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "EditVenueViewController.h"
#import "IgnightTestAppDelegate.h"

static NSString *COMMENTS_PLACEHOLDER = @"Other Comments";

@interface EditVenueViewController ()
{
    IgnightTopBar *topBar;
    
    VenueHttpClient *client;
    
    BOOL inTextView;
    BOOL keyboardUp;
    CGFloat scrollViewHeight;
}

@end

@implementation EditVenueViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupTitleBar];
    [self setupView];
    [self setupTapGestureRecognizer];
    
    client = [VenueHttpClient getInstance];
    
    COMMENTS_PLACEHOLDER = _commentsTxtView.text;
    [[_venueTitle layer] setCornerRadius:3];
    [[_venueTitle layer] setMasksToBounds:YES];
    [[_venueInfo layer] setCornerRadius:3];
    [[_venueInfo layer] setMasksToBounds:YES];
    [[_cancelBtn layer] setCornerRadius:3];
    [[_cancelBtn layer] setMasksToBounds:YES];
    [[_submitBtn layer] setCornerRadius:3];
    [[_submitBtn layer] setMasksToBounds:YES];
    
    scrollViewHeight = _scrollView.frame.size.height;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupPushNotificationObserver];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillHideNotification object:nil];
    
    client.delegate = self;
    _commentsTxtView.text = COMMENTS_PLACEHOLDER;
}

-(void)viewWillDisappear:(BOOL)animated
{
    DDLogDebug(@"View Will Dissapear");
    [super viewWillDisappear:animated];
    [self removePushNotificationObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    client.delegate = nil;
    
    [_commentsTxtView resignFirstResponder];
    [_venueTitleTxt resignFirstResponder];
    [_addressTxt resignFirstResponder];
    [_phoneTxt resignFirstResponder];
    [_websiteTxt resignFirstResponder];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSTimeInterval animationDuration;
    NSDictionary *uInfo = [notification userInfo];
    
    [[uInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    CGPoint kbPointBeg = [[uInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].origin;
    CGPoint kbPointEnd = [[uInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    CGFloat adjustment = kbPointEnd.y - kbPointBeg.y;
    
    CGRect f = _scrollView.frame;
    if ([[notification name] isEqualToString:UIKeyboardWillShowNotification]) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:animationDuration];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(stoppedKeyboard)];
        _scrollView.frame = CGRectMake(f.origin.x, f.origin.y, f.size.width, f.size.height + adjustment);
        keyboardUp = YES;
        [UIView commitAnimations];
    } else if ([[notification name] isEqualToString:UIKeyboardWillHideNotification]) {
        [UIView animateWithDuration:animationDuration animations:^{
            _scrollView.contentOffset = CGPointMake(0, 0);
            _scrollView.frame = CGRectMake(f.origin.x, f.origin.y, f.size.width, f.size.height + adjustment);
        }];
        keyboardUp = NO;
    }
}

-(void)stoppedKeyboard
{
    [UIView animateWithDuration:0 animations:^{
        if (inTextView) {
            _scrollView.contentOffset = CGPointMake(0, _venueTitle.frame.origin.y + 180);
        }
    }];
}

-(void)setupTitleBar
{
    if (_venueName == nil) {
        topBar = [[IgnightTopBar alloc] initWithTitle:@"Add a Venue" withSideBarButtonTarget:self];
    } else {
        topBar = [[IgnightTopBar alloc] initWithTitle:_venueName withSubtitle:@"Report an Error" withSideBarButtonTarget:self];
    }
    [topBar setLeftButtonBackgroundImage:[UIImage imageNamed:@"back_gray"]];
    [_titleBar addSubview:topBar];
    [_titleBar setUserInteractionEnabled:YES];
    self.revealViewController.delegate = self;
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    [_commentsTxtView resignFirstResponder];
    [_venueTitleTxt resignFirstResponder];
    [_addressTxt resignFirstResponder];
    [_phoneTxt resignFirstResponder];
    [_websiteTxt resignFirstResponder];
}

#pragma mark - IgnightTopBarDelegate

- (void)revealToggle:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

static CGFloat separatorWidth = 0.25;
static UIColor *textColor;

-(void)setupView
{
    textColor = reportHeaderText;
    
    [_scrollView setBackgroundColor:reportMainBackground];
    
    _header.textColor = reportHeaderText;
    
    _venueTitleTxt.text = _venueName;
    _venueTitleTxt.placeholder = @"Venue Name";
    [_venueTitleTxt setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    _venueTitleTxt.textColor = reportHeaderText;
    _venueTitleTxt.delegate = self;
    [[_venueTitle layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_venueTitle layer] setBorderWidth:separatorWidth];
    
    [[_venueInfo layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_venueInfo layer] setBorderWidth:separatorWidth];
    
    _addressTxt.text = _venueAddress;
    _addressTxt.placeholder = @"Address";
    [_addressTxt setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    _addressTxt.textColor = textColor;
    _addressTxt.delegate = self;
    [[_address layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_address layer] setBorderWidth:separatorWidth];
    
    _phoneTxt.text = [IgnightUtil formatPhoneNumber:_venuePhone];
    _phoneTxt.placeholder = @"Phone Number";
    [_phoneTxt setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    _phoneTxt.textColor = textColor;
    _phoneTxt.delegate = self;
    [[_phone layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_phone layer] setBorderWidth:separatorWidth];
    
    _websiteTxt.text = _venueWebsite;
    _websiteTxt.placeholder = @"Website";
    [_websiteTxt setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    _websiteTxt.textColor = textColor;
    _websiteTxt.delegate = self;
    [[_website layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_website layer] setBorderWidth:separatorWidth];
    
    [[_comments layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_comments layer] setBorderWidth:separatorWidth];
    _commentsTxtView.textColor = textColor;
    _commentsTxtView.delegate = self;
    
    [_submitBtn setBackgroundColor:reportSubmitButtonBackground];
    [_submitBtn setTitleColor:reportSubmitButtonText forState:UIControlStateNormal];

    [[_submitBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_submitBtn layer] setBorderWidth:separatorWidth];
    
    [_cancelBtn setBackgroundColor:reportCancelButtonBackground];
    [_cancelBtn setTitleColor:reportSubmitButtonText forState:UIControlStateNormal];
    
    [[_cancelBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [[_cancelBtn layer] setBorderWidth:separatorWidth];
    
    _commentsPlaceholder.textColor = [UIColor lightGrayColor];
    
    if (_venueName == nil) {
        _header.text = @"";
    }
}

-(void)setupTapGestureRecognizer
{
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(respondToTapGesture:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
}

-(IBAction)respondToTapGesture:(id)sender
{
    [_venueTitleTxt resignFirstResponder];
    [_addressTxt resignFirstResponder];
    [_phoneTxt resignFirstResponder];
    [_websiteTxt resignFirstResponder];
    [_commentsTxtView resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_phoneTxt == textField) {
        _phoneTxt.text = [IgnightUtil formatPhoneNumber:_phoneTxt.text];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (![string isEqualToString:@""]) {
        _phoneTxt.text = [IgnightUtil dynamicFormatPhoneNumber:_phoneTxt.text];
    }
    _phoneTxt.text = [IgnightUtil formatPhoneNumber:_phoneTxt.text];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField              // called when 'return' key pressed. return NO to ignore.
{
    if (textField == _venueTitleTxt) {
        [_addressTxt becomeFirstResponder];
    } else if (textField == _addressTxt) {
        [_phoneTxt becomeFirstResponder];
    } else if (textField == _phoneTxt) {
        [_websiteTxt becomeFirstResponder];
    } else if (textField == _websiteTxt) {
        [_commentsTxtView becomeFirstResponder];
    }
    return YES;
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView;
{
    if ([textView.text isEqualToString:@""]) {
        _commentsPlaceholder.alpha = 0;
    }
    inTextView = YES;
    [UIView animateWithDuration:0 animations:^{
        if (keyboardUp) {
            _scrollView.contentOffset = CGPointMake(0, _venueTitle.frame.origin.y + 180);
        }
    }];
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    inTextView = NO;
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if ([[textView.text stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""]) {
        _commentsPlaceholder.alpha = 1;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitData:(id)sender {
    NSDictionary *data = @{@"venueName" : _venueTitleTxt.text,
                           @"venueAddress" : _addressTxt.text,
                           @"venuePhone" : _phoneTxt.text,
                           @"venueUrl": _websiteTxt.text,
                           @"venueComments" : _commentsTxtView.text};
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (_venueId == nil) {
        [client reportVenueAddition:data];
    } else {
        [client reportVenueDetailsError:data forVenue:_venueId];
    }
}

- (IBAction)cancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - VenueHttpClientDelegate

-(void)venueHttpClientPostedVenueErrors:(VenueHttpClient *)client
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)venueHttpClientPostedVenueAdditions:(VenueHttpClient *)client
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)venueHttpClient:(VenueHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    IgAlert(nil, [error description], nil);
}

#pragma mark - Remote Notifications

-(void)setupPushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewMailPush:) name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNewGroupBuzz:) name:@"NewGroupBuzzNotification" object:nil];
}

-(void)removePushNotificationObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewMessageInInboxNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewGroupBuzzNotification" object:nil];
}

-(void)handleNewMailPush: (NSNotification *)notification
{
    GroupViewController *groupView = [self.storyboard instantiateViewControllerWithIdentifier:@"allGroupView"];
    groupView.tabIndex = 2;
    [self.navigationController pushViewController:groupView animated:YES];
}


-(void)handleNewGroupBuzz: (NSNotification *)notification
{
    NSDictionary *data = notification.userInfo;
    Trending *trending = [self.storyboard instantiateViewControllerWithIdentifier:@"trendingView"];
    trending.goBack = NO;
    trending.groupMode = YES;
    trending.groupName = data[@"groupName"];
    trending.groupId = data[@"groupId"];
    trending.tabIndex = 1;
    [self.navigationController pushViewController:trending animated:YES];
}

@end
