//
//  SideBarMenuCell.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/23/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideBarMenuCell : UITableViewCell

@property (nonatomic, strong) UIImageView *icon;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIImageView *activityIndicatorBackgroundImage;
@property (nonatomic, strong) UILabel *activityIndicator;
@property (nonatomic, strong) UIView *separatorTop;
@property (nonatomic, strong) UIView *separatorBottom;

-(void)setTitle: (NSString *)title;
-(void)setImageForIcon: (UIImage *)image;
-(void)adjustUserImage;
-(void)makeActivityIndicatorVisibleWithText: (NSString *)text;
-(void)hideSeparatorBottom;
-(void)showSeparatorBottom;

-(void)makeSeparatorRegular;
-(void)makeSeparatorLightUp;
-(void)makeSeparatorThicker;

@end
