//
//  SelectionViewController.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/20/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MusicViewController : UIViewController <UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIButton *continueButton;

- (IBAction)continueButtonPressed:(id)sender;

@end
