//
//  Trending.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/3/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "SWRevealViewController.h"
#import "MusicFilter.h"
#import "IgnightTopBar.h"
#import "GroupMembersViewController.h"
#import "InviteViewController.h"
#import "GroupTabBarController.h"
#import <QuartzCore/QuartzCore.h>
#import "IgnightColors.h"
#import "ImageViewViewController.h"
#import "BuzzViewController.h"
#import "FSImageViewerViewController.h"
#import "FSBasicImageSource.h"
#import "FSBasicImage.h"
#import "BuzzImageData.h"
#import "GroupViewController.h"

#define maxTrending 15

@interface Trending : UIViewController< UserHttpClientDelegate,
                                        GroupHttpClientDelegate,
                                        CLLocationManagerDelegate,
                                        SWRevealViewControllerDelegate,
                                        IgnightTopBarDelegate,
                                        MusicFilterDelegate,
                                        UITextViewDelegate,
                                        FSImageViewerViewControllerDelegate,
                                        ImageViewControllerDelegate,
                                        BuzzViewControllerDelegate>

//<UICollectionViewDataSource, UICollectionViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *groupInfoVIew;
@property (strong, nonatomic) IBOutlet UIView *groupView;
@property (strong, nonatomic) IBOutlet UITextView *groupInfoTxtView;

@property (strong, nonatomic) IBOutlet UIView *cityFilters;
@property (strong, nonatomic) IBOutlet UIView *groupMenu;

@property (strong, nonatomic) IBOutlet UIImageView *grpTrendingImg;
@property (strong, nonatomic) IBOutlet UIImageView *grpBuzzImg;
@property (strong, nonatomic) IBOutlet UIImageView *groupMembersImg;
@property (strong, nonatomic) IBOutlet UIImageView *grpInviteImg;

@property (strong, nonatomic) IBOutlet UIButton *groupTrendingBtn;
@property (strong, nonatomic) IBOutlet UIButton *groupBuzzBtn;
@property (strong, nonatomic) IBOutlet UIButton *groupMembersBtn;
@property (strong, nonatomic) IBOutlet UIButton *groupInviteBtn;


@property (strong, nonatomic) IBOutlet UIView *mainTrendingView;
@property (strong, nonatomic) IBOutlet UIView *listMapView;

@property (weak,nonatomic) UIViewController *destinationViewController;
@property (strong, nonatomic) NSString *destinationIdentifier;
@property (strong, nonatomic) UIViewController *oldViewController;


@property (strong, nonatomic) IBOutlet UIView *musicFilterView;

@property (strong, nonatomic) IBOutlet UIImageView *titleBarImgView;

@property (strong, nonatomic) IBOutlet UIButton *musicFilterBtn;
@property (strong, nonatomic) IBOutlet UIImageView *musicFilterImg;
@property (strong, nonatomic) IBOutlet UILabel *musicFilterLbl;

@property (strong, nonatomic) IBOutlet UIButton *upvoteFilterBtn;
@property (strong, nonatomic) IBOutlet UIImageView *upvoteFilterImg;
@property (strong, nonatomic) IBOutlet UILabel *upvoteFilterLbl;

@property (strong, nonatomic) IBOutlet UIButton *clearMusicFilterBtn;

@property (strong, nonatomic) IBOutlet UIView *mapFilterView;

@property (strong, nonatomic) IBOutlet UIButton *mapNearMeBtn;
@property (strong, nonatomic) IBOutlet UILabel *nearMeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *nearMeImageView;


@property (strong, nonatomic) IBOutlet UIButton *refreshBtn;
@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;
@property (strong, nonatomic) IBOutlet UIImageView *refreshImageView;

@property (strong, nonatomic) IBOutlet UIButton *cityViewBtn;
@property (strong, nonatomic) IBOutlet UILabel *cityViewLabel;

@property (strong, nonatomic) IBOutlet UIButton *listMapToggle;

@property (nonatomic) BOOL goBack;
@property (nonatomic) BOOL groupMode;
@property (strong, nonatomic) NSString *groupName;
@property (strong, nonatomic) NSNumber *groupId;
@property (nonatomic) NSInteger tabIndex;

@property (nonatomic) CLLocationCoordinate2D center;
@property (nonatomic) CLLocationDistance radius;


-(void)searchVenuesWithString: (NSString *)searchString;
-(void)trendingVenuesFromCoordinate: (CLLocationCoordinate2D)coordinate withRadius: (CLLocationDistance)radius;

-(BOOL)inSearchMode;
-(NSInteger)maxS;
-(NSInteger)maxT;
-(void)enteredSearchMode;
-(void)cancelButtonPressed;
-(void)askForTrendingData;
-(void)loadMore;
-(void)addVenue;
-(void)exitSearchModeAndAskForTrending: (BOOL)ask;
-(void)setRefreshActive;
-(void)setRefreshToDefault;

-(void)resetUserClientDelegate;

#pragma ACTIONS

- (IBAction)switchTrendingView:(id)sender;
- (IBAction)selectMusicFilter:(id)sender;
- (IBAction)selectUpvoteFilter:(id)sender;
- (IBAction)clearMusicFilter:(id)sender;
- (IBAction)nearMe:(id)sender;
- (IBAction)cityView:(id)sender;
- (IBAction)refreshMap:(id)sender;

- (IBAction)cityViewSelected:(id)sender;
- (IBAction)cityViewUnselected:(id)sender;

- (IBAction)refreshSelected:(id)sender;
- (IBAction)refreshUnselected:(id)sender;

- (IBAction)nearMeSelected:(id)sender;
- (IBAction)nearMeUnselected:(id)sender;

- (IBAction)goToGroupMembers:(id)sender;
- (IBAction)goToGroupBuzz:(id)sender;
- (IBAction)goToGroupTrending:(id)sender;
- (IBAction)goToGroupInvites:(id)sender;



@end
