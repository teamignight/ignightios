//
//  MessageTableViewCell.h
//  PushChatStarter
//
//  Created by Kauserali on 28/03/13.
//  Copyright (c) 2013 Ray Wenderlich. All rights reserved.
//

#import "IgnightColors.h"
#import "NSDate+TimeAgo.h"

#define BuzzCellRow 100

@protocol MessageTableViewCellDelegate <NSObject>

-(void)imageSelectedWithBuzzId: (NSNumber *)buzzId withImageIndex: (NSNumber *)imageIndex;
-(void)imageSelected: (UIImage *)image;
-(void)reportBuzz: (NSNumber *)buzzId;

@end


@class BuzzMessage;

// Table view cell that displays a Message. The message text appears in a
// speech bubble; the sender name and date are shown in a UILabel below that.
@interface MessageTableViewCell : UITableViewCell

@property (strong, nonatomic) id <MessageTableViewCellDelegate> delegate;

- (void)setMessage:(BuzzMessage*)message;

@end
