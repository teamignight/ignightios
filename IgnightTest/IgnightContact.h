//
//  IgnightContact.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 3/15/15.
//  Copyright (c) 2015 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IgnightContact : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSMutableArray *contacts;

@end
