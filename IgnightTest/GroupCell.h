//
//  GroupCell.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/15/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSInteger rowHeight = 60;

@protocol GroupCellDelegate <NSObject>
@required
- (void)goToBuzz:(NSNumber *)groupId;
- (void)reportGroup: (NSNumber *)groupId;
@end

@interface GroupCell : UITableViewCell

@property (strong, nonatomic) id <GroupCellDelegate> delegate;
@property (strong, nonatomic) NSNumber *groupId;

+(NSInteger)getCellHeight;
-(void)setGroupName: (NSString *)name withCount: (NSInteger)count;
-(void)setGroupType: (NSString *)type;
-(void)turnBuzzOn: (BOOL)buzzAvailable;
-(void)hideBuzzIndicator;
-(void)hideLastBuzzTs;
-(void)setLastBuzzTs: (NSNumber *)lastBuzzTs;
-(void)showReportButton;

@end
