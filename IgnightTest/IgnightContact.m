//
//  IgnightContact.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 3/15/15.
//  Copyright (c) 2015 Abhinav Chordia. All rights reserved.
//

#import "IgnightContact.h"

@implementation IgnightContact

-(id)init
{
    self = [super init];
    if (self) {
        _contacts = [[NSMutableArray alloc] init];
    }
    return self;
}

@end
