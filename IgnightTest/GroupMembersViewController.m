//
//  GroupMembersViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 2/3/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "GroupMembersViewController.h"
#import "SearchedUser.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "APAddressBook.h"
#import "APContact.h"
#import "APPhoneWithLabel.h"
#import "ConfirmUserPhoneNumberViewController.h"
#import "UserContactsTableViewController.h"

#define CELL_HEIGHT 50
#define LIMIT 20

@interface GroupMembersViewController ()
{
    GroupHttpClient *groupClient;
    UserHttpClient *userClient;
    Groups *groups;
    Group *currentGroup;
    UserInfo *userInfo;
    
    NSMutableArray *groupMembers;
    NSMutableDictionary *images;
    
    NSMutableArray *users;
    NSMutableDictionary *idToImage;
    
    NSNumber *adminId;
    
    UISearchBar *userSearchBar;
    BOOL searchMode;
    NSInteger offset;
    
    UIActivityIndicatorView *spinner;
    UITextField *searchBarTxtField;
    UIView *searchBarLeftView;
}

@end

@implementation GroupMembersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    offset = 0;
    
    groupClient = [GroupHttpClient getInstance];
    userClient = [UserHttpClient getInstance];
    groupMembers = [[NSMutableArray alloc] init];
    images = [[NSMutableDictionary alloc] init];
    groups = [Groups getInstance];
    userInfo = [UserInfo getInstance];
    users = [[NSMutableArray alloc] init];
    
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    
    userSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    userSearchBar.searchBarStyle = UISearchBarStyleProminent;
    userSearchBar.placeholder = @"Search for Users";
    userSearchBar.tintColor = [UIColor darkGrayColor];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    for (UIView *subview in [[userSearchBar.subviews objectAtIndex:0] subviews]) {
        if ([subview isKindOfClass:[UITextField class]]) {
            searchBarTxtField = (UITextField *)subview;
            searchBarLeftView = searchBarTxtField.leftView;
        }
    }
    
    idToImage = [[NSMutableDictionary alloc] init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    groupClient.delegate = self;
    userClient.delegate = self;
    userSearchBar.delegate = self;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [userSearchBar resignFirstResponder];
}

-(void)loadData: (NSNumber *)gId
{
    currentGroup = [groups getGroupForId:gId];
    
    if (currentGroup.publicGroup) {
        [self.tableView setTableHeaderView:userSearchBar];
    } else if ([currentGroup.adminId isEqualToNumber:userInfo.userId]) {
        [self.tableView setTableHeaderView:userSearchBar];
    }
    
    if (groupMembers.count <= 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    [groupClient getMembersForGroup:gId];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - SWRevealViewControllerDelegate

-(void)sideBarWillMoveToPosition: (FrontViewPosition )position
{
    if (position == FrontViewPositionLeft) {
        userClient.delegate = self;
    }
}

#pragma mark - GroupHttpClientDelegate

-(void)groupHttpClient:(GroupHttpClient *)client didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)groupHttpClient:(GroupHttpClient *)client successfullyInvitedUser:(NSDictionary *)response
{
    
}

-(void)groupHttpClient:(GroupHttpClient *)client gotGroupMembers:(NSDictionary *)response
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL res = [response[@"res"] boolValue];
    if (res) {
        NSDictionary *body = response[@"body"];
        NSArray *members = body[@"groupMembers"];
        adminId = body[@"adminId"];
        
        DDLogDebug(@"Got Group Members: %@", body);
        if (!currentGroup.publicGroup && userInfo.userId.integerValue == adminId.integerValue) {
            [self.tableView setTableHeaderView:userSearchBar];
        }
        
        groupMembers = [NSMutableArray arrayWithArray:members];
        [self.tableView reloadData];
    }
}

#pragma mark - UserHttpClientDelegate

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    [spinner stopAnimating];
    searchBarTxtField.leftView = searchBarLeftView;
//    IgAlert(nil, @"Failed to reach server", nil);
}

-(void)userHttpClient:(UserHttpClient *)client returnedUsersForSearch:(NSDictionary *)response withOffset:(NSInteger)offset
{
    [spinner stopAnimating];
    searchBarTxtField.leftView = searchBarLeftView;
    BOOL res = [[response objectForKey:@"res"] boolValue];
    if (res) {
        NSArray *body = [response objectForKey:@"body"];
        if (offset == 0) {
            users = [[NSMutableArray alloc] init];
        }
        DDLogDebug(@"Got Users: %@", body);
        for (NSDictionary *user in body) {
//            DDLogDebug(@"Adding User: %@", user);
            SearchedUser *searchedUser = [[SearchedUser alloc] initWithParams:user];
            [users addObject:searchedUser];
        }
        [self.tableView reloadData];
    } else {
        DDLogDebug(@"Failed To Return Any Users");
    }
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar                      // return NO to not become first responder
{
    searchBar.showsCancelButton = YES;
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    searchMode = YES;
    offset = 0;
    searchBarTxtField.leftView = spinner;
    [spinner startAnimating];
    [userClient searchForUsers:searchBar.text inGroup:_groupId withOffset:offset withLimit:LIMIT];
    [searchBar resignFirstResponder];
}

-(void)loadMoreSearchResults
{
    [userClient searchForUsers:userSearchBar.text inGroup:_groupId withOffset:offset withLimit:LIMIT];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    searchMode = NO;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [SearchUserTableViewCell getCellHeight];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    if (searchMode) {
        return [users count];
    }
    return [groupMembers count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
        if (status == kABAuthorizationStatusNotDetermined) {
            self.navigationController.title = @"";
            ConfirmUserPhoneNumberViewController *phoneViewController = [[ConfirmUserPhoneNumberViewController alloc] init];
            [self.navigationController pushViewController:phoneViewController animated:YES];
        } else if (status == kABAuthorizationStatusAuthorized) {
            self.navigationController.title = @"";
            UserContactsTableViewController *userContactsView = [[UserContactsTableViewController alloc] init];
            userContactsView.goBack = YES;
            [self.navigationController pushViewController:userContactsView animated:YES];
        } else if (status == kABAuthorizationStatusDenied || status == kABAuthorizationStatusRestricted) {
            [[[UIAlertView alloc] initWithTitle:@"Contacts Settings" message:@"If you would like to use your contacts, please go to your settings and give IgNight access to your contacts." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        if (addressBookRef != nil) {
            CFRelease(addressBookRef);
        }
    }
}

-(void)addContactsToAddressBook
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"AskForContacts"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AskForContacts"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UILabel *name = [[UILabel alloc] initWithFrame:cell.contentView.frame];
            name.text = @"Invite Contacts";
            name.backgroundColor = grayHeaderFooterText;
            name.textColor = [UIColor whiteColor];
            name.textAlignment = NSTextAlignmentCenter;
            name.tag = 1000;
            [cell.contentView addSubview:name];
        }
        return cell;
    }
    
    if (searchMode) {
        NSInteger index = indexPath.row;
        NSInteger currentRow = indexPath.row;
        NSInteger currentDataSize = [users count];
        if (offset < currentDataSize && currentRow > (currentDataSize - 5) && currentRow <= (currentDataSize)) {
            offset = currentDataSize;
            [self loadMoreSearchResults];
        }
        SearchedUser *user = [users objectAtIndex:index];
        SearchUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"users"];
        if (cell == nil) {
            cell  = [[SearchUserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"users"];
        }
        cell.delegate = self;
        [cell setSearchedUser:user];
        UIImage *imageFromServer = [idToImage objectForKey:user.userId];
        if (imageFromServer != nil) {
            [cell setUserImage:imageFromServer];
        } else {
            if (![[idToImage allKeys] containsObject:user.userId]) {
                NSURL *url = [NSURL URLWithString:user.chatPictureURL];
                if (url != nil) {
                    NSURLRequest *request = [NSURLRequest requestWithURL:url];
                    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                    operation.responseSerializer = [AFImageResponseSerializer serializer];
                    
                    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                        [idToImage setObject:responseObject forKey:user.userId];
                        [self.tableView reloadData];
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        DDLogDebug(@"Failed To Donwload Image From: %@", user.chatPictureURL);
                    }];
                    [operation start];
                }
            }
        }
        return cell;
    } else {
        static NSString *CellIdentifier = @"groupMembers";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        UIImageView *userImage = (UIImageView *)[cell viewWithTag:100];
        userImage.contentMode = UIViewContentModeScaleAspectFill;
        userImage.backgroundColor = buzzUserProfileBackground;
        [[userImage layer] setCornerRadius:(CELL_HEIGHT - 10)/ 2];
        [[userImage layer] setMasksToBounds:YES];
        
        UILabel *userName = (UILabel *)[cell viewWithTag:101];
        UILabel *adminLabel = (UILabel *)[cell viewWithTag:102];
        
        NSDictionary *memberName = [groupMembers objectAtIndex:indexPath.row];
        userName.text = [memberName objectForKey:@"userName"];
        
        NSNumber *memberId = [memberName objectForKey:@"userId"];
        
        UIImage *image = [images objectForKey:memberId];
        if (image != nil) {
            userImage.image = image;
            userImage.contentMode = UIViewContentModeScaleAspectFill;
            [userImage.layer setCornerRadius:20];
            [userImage.layer setMasksToBounds:YES];
        } else {
            NSDictionary *member = memberName;
            NSString *imageUrl = [member objectForKey:@"chatPictureUrl"];
            NSURL *url = [NSURL URLWithString:imageUrl];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            operation.responseSerializer = [AFImageResponseSerializer serializer];
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                [images setObject:responseObject forKey:member[@"userId"]];
                [self.tableView reloadData];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                DDLogDebug(@"Failed To Donwload Image From: %@", imageUrl);
            }];
            [operation start];
            
            userImage.image = [UIImage imageNamed:@"group_member_default"];
        }
        
        if (!currentGroup.publicGroup && memberId.integerValue == adminId.integerValue) {
            adminLabel.text = @"Admin";
        } else {
            adminLabel.text = @"";
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
}

-(void)inviteUser:(SearchedUser *)user
{
    NSLog(@"Inviting User: %@ (%@)", user.userName, user.userId);
    [groupClient inviteUser:user.userId toGroup:_groupId];
}

@end
