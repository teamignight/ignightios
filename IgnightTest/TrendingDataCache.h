//
//  TrendingDataCache.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/17/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrendingDataCache : NSObject

+ (TrendingDataCache *)getInstance;

-(void)setTrendingData: (NSArray *)data forGroup:(BOOL)groupMode;
-(NSInteger)getUserInputForVenue: (NSNumber *)venueId;
-(NSInteger)setNewUserInput:(NSInteger)input ForVenue: (NSNumber *)venueId;

-(NSInteger)count;
-(NSInteger)countForGroup;

-(void)setShouldRefetchTo:(BOOL)fetch;
-(BOOL)shouldRefetch;

-(NSDictionary *)getVenueTrendingDataForVenue: (NSNumber *)venueId;
-(NSDictionary *)getGroupTrendingDataForVenueAtIndex:(NSInteger)index;
-(NSDictionary *)getTrendingDataForVenueAtIndex:(NSInteger)index;
-(void)clear;

@end
