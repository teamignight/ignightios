//
//  PopularGroups.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/16/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PopularGroups : NSObject

@property (strong, nonatomic) NSNumber *groupId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSNumber *memberCount;

-(id)initWithDictionary: (NSDictionary *)data;
-(id)initWithSearchDictionary: (NSDictionary *)data;

@end
