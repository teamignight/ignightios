//
//  UserNotifications.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 11/22/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UserNotificationsDelegate;

@interface UserNotifications : NSObject

@property (nonatomic, weak) id<UserNotificationsDelegate> delegate;
@property (nonatomic) BOOL groupBuzz;
@property (nonatomic) BOOL inbox;

+ (UserNotifications *)getInstance;

-(void)updateGroupBuzz: (BOOL)buzz;
-(void)updateInbox: (BOOL) inbox;
-(void)updateBuzz: (BOOL)buzz andInbox: (BOOL)inbox;

@end


@protocol UserNotificationsDelegate <NSObject>

@optional

-(void)userNotificationsWasUpdated:(UserNotifications *) userNotification;

@end