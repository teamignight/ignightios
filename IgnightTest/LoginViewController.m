//
//  LoginViewController.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 4/16/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import "LoginViewController.h"
#import "IgnightTestAppDelegate.h"
#import "MBProgressHUD.h"

@interface LoginViewController ()
{
    UserInfo *userInfo;
    Groups *groups;
    Inbox *inbox;
    
    UserHttpClient *client;
    MBProgressHUD *hud;
}

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    DDLogDebug(@"LoginViewController : viewDidLoad");
    userInfo = [UserInfo getInstance];
    groups = [Groups getInstance];
    inbox = [Inbox getInstance];
    client = [UserHttpClient getInstance];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(respondToTapGesture:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [[_loginInputView layer] setCornerRadius:3];
    [[_loginInputView layer] setMasksToBounds:YES];
    
    [[_loginBtn layer] setCornerRadius:3];
    [[_loginBtn layer] setMasksToBounds:YES];
    [_loginBtn setBackgroundColor:blueBtnBackground];
    [_loginBtn setTitleColor:whiteBtnText forState:UIControlStateNormal];
    
    [[_signUpBtn layer] setCornerRadius:3];
    [[_signUpBtn layer] setMasksToBounds:YES];
    [_signUpBtn setBackgroundColor:blueBtnBackground];
    [_signUpBtn setTitleColor:whiteBtnText forState:UIControlStateNormal];
    
    [_separator setBackgroundColor:tabBorder];
    [_separator setAlpha:0.7];
    
    [_usernameTxt setClearButtonMode:UITextFieldViewModeWhileEditing];
    [_passwordTxt setClearButtonMode:UITextFieldViewModeWhileEditing];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self respondToTapGesture:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    DDLogDebug(@"LoginViewController : viewWillAppear");
    
    [[self navigationController] setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.navigationController.navigationBar.topItem.title = @"";
    
    [client setDelegate:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [_usernameTxt resignFirstResponder];
    [_passwordTxt resignFirstResponder];
    [client setDelegate:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    NSTimeInterval animationDuration;
    NSDictionary *uInfo = [notification userInfo];
    
    CGPoint kbPoint = [[uInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    [[uInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    CGRect frame = self.view.frame;
    
    [UIView animateWithDuration:animationDuration animations:^{
        self.view.frame = CGRectMake(frame.origin.x, kbPoint.y - frame.size.height, frame.size.width, frame.size.height);
    }];
}

#pragma mark UITapGestureRecognizer

-(IBAction)respondToTapGesture:(id)sender
{
    [_usernameTxt resignFirstResponder];
    [_passwordTxt resignFirstResponder];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField              // called when 'return' key pressed. return NO to ignore.
{
    if (textField == self.usernameTxt) {
        [self.passwordTxt becomeFirstResponder];
    } else if (textField == self.passwordTxt) {
        [self.view setUserInteractionEnabled:NO];
        if ([self validateInfoSet]) {
            [self signIn];
        } else {
            [self.view setUserInteractionEnabled:YES];
            return NO;
        }
    }
    return YES;
}

-(BOOL)validateInfoSet
{
    return ([self.usernameTxt.text length] > 0) && ([self.passwordTxt.text length] > 0);
}

-(void)signIn
{
    [userInfo setUserName:self.usernameTxt.text];
    [userInfo setPassword:self.passwordTxt.text];
    [client login];
    [self.usernameTxt resignFirstResponder];
    [self.passwordTxt resignFirstResponder];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

#pragma mark UserHttpClientDelegate

-(void)userHttpClient:(UserHttpClient *)client didLoginWithResponse:(NSDictionary *)response
{
    [hud hide:YES];
    BOOL result = [[response objectForKey:@"res"] boolValue];
    if (result) {
        [self setupUserAfterLoggingIn:response];
    } else {
        [self.view setUserInteractionEnabled:YES];
        IgAlert(nil, response[@"reason"], nil);
    }
}

-(void)userHttpClient:(UserHttpClient *)client didFailWithError:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Could Not Connect To The Server" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [hud hide:YES];
    [self.view setUserInteractionEnabled:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //DONT DO ANYTHING FOR NOW
}

-(void)setupUserAfterLoggingIn: (NSDictionary*)response
{
    [self.view setUserInteractionEnabled:YES];
    NSInteger result = [IgnightUtil updateUserInfoOnLogin:response];
    
    if (result > 0) {
        [IgnightUtil saveUserData];
        [client getUserNotifications];
        [self performSegueWithIdentifier:@"login" sender:self];
    } else {
        [self performSegueWithIdentifier:@"setupProfile" sender:self];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signIn:(id)sender {
    if ([_usernameTxt.text isEqualToString:@""] || [_passwordTxt.text isEqualToString:@""])
        return;
    [self signIn];
}
@end
