//
//  DNAStepThree.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/2/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "IgnightColors.h"

@interface DNAStepThree : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIButton *continueBtn;
@property (strong, nonatomic) IBOutlet UILabel *headerLabel;

- (IBAction)goBack:(id)sender;

@end
