//
//  Invitation.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/28/14.
//  Copyright (c) 2014 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Invitation : NSObject

-(Invitation *)initWithData: (NSDictionary *)data;

@property (strong, nonatomic) NSNumber* groupId;
@property (strong, nonatomic) NSString* groupName;
@property (strong, nonatomic) NSNumber* timestamp;
@property (strong, nonatomic) NSString* userName;
@property (strong, nonatomic) NSString* userPicture;

@end
