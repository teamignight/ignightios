//
//  SideBarBuzzCell.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/24/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "SideBarBuzzCell.h"
#import <QuartzCore/QuartzCore.h>

#define CELL_HEIGHT 40

@implementation SideBarBuzzCell
{
    UIImageView *activityImage;
    
    UILabel *activitySeparator;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        CGRect frame = CGRectMake(45, 0, self.contentView.frame.size.width - 170, CELL_HEIGHT);
        _label = [[UILabel alloc] initWithFrame:frame];
        _label.font = [UIFont italicSystemFontOfSize:14.0];
        _label.textColor = sideBarText;
        _label.backgroundColor = clearC;
        _label.text = @"";
        
        frame = CGRectMake(15, 12.5, 15, 15);
        _icon = [[UIImageView alloc] initWithFrame:frame];
        
        frame = CGRectMake(self.contentView.frame.size.width - 120, 5, 0.4, CELL_HEIGHT - 10);
        activitySeparator = [[UILabel alloc] initWithFrame:frame];
        activitySeparator.backgroundColor = [UIColor lightGrayColor];
        
        frame = CGRectMake(self.contentView.frame.size.width - 95, (CELL_HEIGHT - 15)/2, 15, 15);
        activityImage = [[UIImageView alloc] initWithFrame:frame];
        
        frame = CGRectMake(self.contentView.frame.size.width - 120, 0, 150, CELL_HEIGHT);
        _activityIndicatorButton = [[UIButton alloc] initWithFrame:frame];
        [_activityIndicatorButton addTarget:self action:@selector(buzzButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        // build seperator top
//        frame = CGRectMake(0, CELL_HEIGHT - 2, self.contentView.frame.size.width, 1);
//        _separatorTop = [[UIView alloc] initWithFrame:frame];
//        _separatorTop.backgroundColor = sideBarListSeparator;
        
        // build seperator bottom
        frame = CGRectMake(0, CELL_HEIGHT - 1, self.contentView.frame.size.width, 1);
        _separatorBottom = [[UIView alloc] initWithFrame:frame];
        _separatorBottom.backgroundColor = sideBarListSeparator;
        
        [self.contentView addSubview:_label];
        [self.contentView addSubview:_icon];
        [self.contentView addSubview:_activityIndicatorButton];
        [self.contentView addSubview:activityImage];
        [self.contentView addSubview:activitySeparator];
//        [self.contentView addSubview:_separatorTop];
        [self.contentView addSubview:_separatorBottom];
        
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = sideBarListBackground;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.group = YES;
        self.buzzId = nil;
    }
    return self;
}

-(IBAction)buzzButtonPressed:(id)sender
{
    if (self.buzzId != nil) {
        if (self.group) {
            [self.delegate goToGroupBuzz:self.buzzId];
        } else {
            [self.delegate goToVenueBuzz:self.buzzId];
        }
    }
}

-(void)setTitle: (NSString *)title
{
    self.label.text = title;
}

-(void)setImageForIcon: (UIImage *)image
{
    self.icon.image = image;
}

-(void)setImageForActivityIndicatorButton: (UIImage *)image
{
    activityImage.image = image;
//    [self.activityIndicatorButton setImage:image forState:UIControlStateNormal];
//    [self.activityIndicatorButton setImage:image forState:UIControlStateHighlighted];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)showSeparatorBottom
{
    _separatorBottom.alpha = 1;
}
-(void)hideSeparatorBottom
{
    _separatorBottom.alpha = 0;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    UIButton *delete = (UIButton *)[self.contentView viewWithTag:100];
    if (!self.editing) {
        delete.alpha = 0;
        return;
    }
    
    delete.alpha = 1;
    
    if (delete == nil) {
        delete = [UIButton buttonWithType:UIButtonTypeCustom];
        delete.frame = CGRectMake(222, 0, 84, CELL_HEIGHT);
        delete.backgroundColor = [UIColor redColor];
        delete.tag = 100;
        [delete setTitle:@"Delete" forState:UIControlStateNormal];
        [delete titleLabel].textAlignment = NSTextAlignmentCenter;
        [delete addTarget:self action:@selector(deleteMe:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:delete];
    }
}

-(IBAction)deleteMe:(id)sender
{
    [self.delegate deleteBuzzCell:self];
}

@end
