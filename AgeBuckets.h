//
//  AgeBuckets.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/7/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AgeBuckets : NSObject

typedef enum {
    EIGHTEEN_TO_20,
    TWENTYONE_TO_25,
    TWENTYSIX_TO_30,
    THIRTYONE_TO_35,
    THIRTYSIX_TO_40,
    FOURTYONEPLUS,
    
    AGE_BUCKETS_COUNT
} AgeBucket;

+(NSString *)getAgeBucketNameFromId: (AgeBucket)ageBucket;
+(AgeBucket)getAgeBucketFromName: (NSString *)ageBucket;
+(NSArray *)getListOfValues;

@end