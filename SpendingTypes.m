//
//  SpendingTypes.m
//  IgnightTest
//
//  Created by Abhinav Chordia on 7/21/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import "SpendingTypes.h"

@implementation SpendingTypes

+(NSString *)getSpendingNameFromId: (Spending )spendingId
{
    switch (spendingId) {
        case BROKE:
            return @"Broke";
        case GETTING_BY:
            return @"Getting By";
        case DOING_WELL:
            return @"Doing Well";
        case LIVING_THE_DREAM:
            return @"Living The Dream";
        default:
            return @"N/A";
    }
}

+(NSString *)getSpendingSymbolFromId: (Spending )spendingId
{
    switch (spendingId) {
        case BROKE:
            return @"$";
        case GETTING_BY:
            return @"$$";
        case DOING_WELL:
            return @"$$$";
        case LIVING_THE_DREAM:
            return @"$$$$";
        default:
            return @"N/A";
    }
}

+(Spending)getSpendingFromName: (NSString* )spendingName
{
    spendingName = [spendingName lowercaseString];
    
    if ([spendingName isEqualToString:@"broke"])
        return BROKE;
    if ([spendingName isEqualToString:@"getting by"])
        return GETTING_BY;
    if ([spendingName isEqualToString:@"doing well"])
        return DOING_WELL;
    if ([spendingName isEqualToString:@"living the dream"])
        return LIVING_THE_DREAM;
    
    return SPENDING_COUNT;
}

+(NSArray *)getArrayOfSpendingNames
{
    return @[@"Broke", @"Getting By", @"Doing Well", @"Living The Dream"];
}

@end
