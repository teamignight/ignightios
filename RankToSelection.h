//
//  RankToSelection.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 10/1/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RankToSelection : NSObject

-(id)initWithMaxSelections: (NSInteger)max minSelections: (NSInteger)min;
-(NSInteger)addToSelection:(id)selection;
-(NSInteger)removeFromSelection: (id)selection;
-(NSInteger)count;
-(NSNumber *)selectionAtRank: (NSInteger)rank;
-(NSNumber *)rankForSelection: (id)selection;
-(NSArray *)selections;

-(BOOL)isRankAvailable;
-(BOOL)selectedMinSelections;
@end
