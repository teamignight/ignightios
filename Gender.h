//
//  Gender.h
//  IgnightTest
//
//  Created by Abhinav Chordia on 9/30/13.
//  Copyright (c) 2013 Abhinav Chordia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Gender : NSObject

typedef enum {
    MALE,
    FEMALE,
    
    GENDER_COUNT
} Genders;

+(NSString *)getNameFromId: (Genders)genderId;
+(Genders)getGenderFromName: (NSString *)name;
+(NSArray *)getListOfValues;

@end
